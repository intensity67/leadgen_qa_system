-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 03, 2019 at 12:34 PM
-- Server version: 10.1.6-MariaDB-log
-- PHP Version: 5.5.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `asterisk`
--

-- --------------------------------------------------------

--
-- Table structure for table `vicidial_lists`
--

CREATE TABLE IF NOT EXISTS `vicidial_lists` (
  `list_id` bigint(14) unsigned NOT NULL,
  `list_name` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `campaign_id` varchar(8) COLLATE utf8_unicode_ci DEFAULT NULL,
  `active` enum('Y','N') COLLATE utf8_unicode_ci DEFAULT NULL,
  `list_description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `list_changedate` datetime DEFAULT NULL,
  `list_lastcalldate` datetime DEFAULT NULL,
  `reset_time` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `agent_script_override` varchar(20) COLLATE utf8_unicode_ci DEFAULT '',
  `campaign_cid_override` varchar(20) COLLATE utf8_unicode_ci DEFAULT '',
  `am_message_exten_override` varchar(100) COLLATE utf8_unicode_ci DEFAULT '',
  `drop_inbound_group_override` varchar(20) COLLATE utf8_unicode_ci DEFAULT '',
  `xferconf_a_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `xferconf_b_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `xferconf_c_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `xferconf_d_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `xferconf_e_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT '',
  `web_form_address` text COLLATE utf8_unicode_ci,
  `web_form_address_two` text COLLATE utf8_unicode_ci,
  `time_zone_setting` enum('COUNTRY_AND_AREA_CODE','POSTAL_CODE','NANPA_PREFIX','OWNER_TIME_ZONE_CODE') COLLATE utf8_unicode_ci DEFAULT 'COUNTRY_AND_AREA_CODE',
  `inventory_report` enum('Y','N') COLLATE utf8_unicode_ci DEFAULT 'Y',
  `expiration_date` date DEFAULT '2099-12-31',
  `na_call_url` text COLLATE utf8_unicode_ci,
  `local_call_time` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'campaign',
  `web_form_address_three` text COLLATE utf8_unicode_ci,
  `status_group_id` varchar(20) COLLATE utf8_unicode_ci DEFAULT '',
  `user_new_lead_limit` smallint(5) DEFAULT '-1',
  `inbound_list_script_override` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `default_xfer_group` varchar(20) COLLATE utf8_unicode_ci DEFAULT '---NONE---',
  `daily_reset_limit` smallint(5) DEFAULT '-1',
  `resets_today` smallint(5) unsigned DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `vicidial_lists`
--

INSERT INTO `vicidial_lists` (`list_id`, `list_name`, `campaign_id`, `active`, `list_description`, `list_changedate`, `list_lastcalldate`, `reset_time`, `agent_script_override`, `campaign_cid_override`, `am_message_exten_override`, `drop_inbound_group_override`, `xferconf_a_number`, `xferconf_b_number`, `xferconf_c_number`, `xferconf_d_number`, `xferconf_e_number`, `web_form_address`, `web_form_address_two`, `time_zone_setting`, `inventory_report`, `expiration_date`, `na_call_url`, `local_call_time`, `web_form_address_three`, `status_group_id`, `user_new_lead_limit`, `inbound_list_script_override`, `default_xfer_group`, `daily_reset_limit`, `resets_today`) VALUES
(999, 'Default inbound list', 'TESTCAMP', 'N', NULL, NULL, NULL, '', '', '', '', '', '', '', '', '', '', NULL, NULL, 'COUNTRY_AND_AREA_CODE', 'Y', '2099-12-31', NULL, 'campaign', NULL, '', -1, NULL, '---NONE---', -1, 0),
(998, 'Default Manual list', 'TESTCAMP', 'N', NULL, NULL, '2018-12-11 18:26:15', '', '', '', '', '', '', '', '', '', '', NULL, NULL, 'COUNTRY_AND_AREA_CODE', 'Y', '2099-12-31', NULL, 'campaign', NULL, '', -1, NULL, '---NONE---', -1, 0),
(3001, 'QF Leads', '3000', 'Y', 'QF Leads', '2018-12-09 04:51:07', '2019-01-03 12:34:16', '', '', '', '', '', '', '', '', '', '', '', '', 'COUNTRY_AND_AREA_CODE', 'Y', '2099-12-31', '', 'campaign', '', '', -1, '', '---NONE---', -1, 0),
(3002, 'PreManual', '3000', 'N', 'Pre Manual Dialed List', '2018-12-03 11:31:14', '2018-12-17 11:03:52', '', '', '', '', '', '', '', '', '', '', NULL, NULL, 'COUNTRY_AND_AREA_CODE', 'Y', '2099-12-31', NULL, 'campaign', NULL, '', -1, NULL, '---NONE---', -1, 0),
(3003, 'Dialable', '6000', 'Y', 'Post Manual Dial Leads', '2018-12-17 09:32:24', '2018-12-19 11:09:02', '', '', '', '', '', '', '', '', '', '', '', '', 'COUNTRY_AND_AREA_CODE', 'Y', '2099-12-31', '', 'campaign', '', '', -1, '', '---NONE---', -1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `vicidial_lists`
--
ALTER TABLE `vicidial_lists`
  ADD PRIMARY KEY (`list_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
