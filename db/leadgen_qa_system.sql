-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 07, 2019 at 06:40 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `leadgen_qa_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `disposition`
--

CREATE TABLE `disposition` (
  `disposition_id` int(11) NOT NULL,
  `disposition` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disposition`
--

INSERT INTO `disposition` (`disposition_id`, `disposition`) VALUES
(1, 'Answering Machine'),
(2, 'Busy'),
(3, 'Call Back'),
(4, 'Disconnected Number'),
(5, 'Do Not Call'),
(6, 'No Answer'),
(7, 'Not Interested'),
(8, 'Dead Air'),
(9, 'Dial Time Out'),
(10, 'Dump Leads'),
(11, 'Hung Up'),
(12, 'Insurance Hung Up'),
(13, 'Language Barrier'),
(14, 'Not Qualified'),
(15, 'Not right now'),
(16, 'Prank Call'),
(17, 'Redial'),
(18, 'RING'),
(19, 'Robo Call'),
(20, 'TrFail '),
(21, 'TrHup'),
(22, 'TrSuc '),
(23, 'Voicemail'),
(24, 'Wrong Number');

-- --------------------------------------------------------

--
-- Table structure for table `flagged_calls`
--

CREATE TABLE `flagged_calls` (
  `flagged_calls_id` int(11) NOT NULL,
  `auditor_id` int(11) NOT NULL,
  `script_used_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `agent_name` varchar(255) NOT NULL,
  `client_feedback_id` int(11) NOT NULL,
  `phone_number` int(11) NOT NULL,
  `call_id` int(11) NOT NULL,
  `customer` varchar(255) NOT NULL,
  `recording_link` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `vici_disposition` varchar(255) NOT NULL,
  `webform_disposition` varchar(255) NOT NULL,
  `client_feedback_validation` enum('valid','invalid') NOT NULL,
  `quotefire_comment` int(11) NOT NULL,
  `quotefire_priority` int(11) NOT NULL,
  `quotefire_notes_finding` int(11) NOT NULL,
  `SME` int(11) NOT NULL,
  `TL` int(11) NOT NULL,
  `site` enum('OITC','NY','Dumaguete','Etelecare') NOT NULL,
  `lol_ztp` int(11) NOT NULL,
  `qa_comment` varchar(255) NOT NULL,
  `record_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `incident_report`
--

CREATE TABLE `incident_report` (
  `incident_report_id` int(11) NOT NULL,
  `vici_dispo` varchar(255) NOT NULL,
  `btn` varchar(255) NOT NULL,
  `duration` varchar(255) NOT NULL,
  `evaluator` varchar(255) NOT NULL,
  `offnse` varchar(255) NOT NULL,
  `call_synopsis` varchar(255) NOT NULL,
  `sanction` varchar(255) NOT NULL,
  `findings_or_sanctions` varchar(255) NOT NULL,
  `sanction_level` int(11) NOT NULL,
  `file_status` varchar(255) NOT NULL,
  `email_status` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `long_call_audits`
--

CREATE TABLE `long_call_audits` (
  `audit_id` int(11) NOT NULL,
  `audit_timestart` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `audit_timeend` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `week_beginning_date` date NOT NULL,
  `call_id` int(11) NOT NULL,
  `phone_number` int(11) NOT NULL,
  `customer` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  `agent_name` varchar(255) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `recording` int(11) NOT NULL,
  `agent_disposition_id` int(11) NOT NULL,
  `correct_disposition_id` int(11) NOT NULL,
  `agent_issue` enum('Agent','System','Others','N/A') NOT NULL,
  `agent_issue_comment` varchar(255) NOT NULL,
  `ztp_mark` enum('ZTP','LOL') NOT NULL,
  `ztp_mark_comment` varchar(255) NOT NULL,
  `general_observation` text NOT NULL,
  `qa_remarks` text NOT NULL,
  `audit_date` date NOT NULL,
  `recording_datetime` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `auditor_id` int(11) NOT NULL,
  `soundboard_voice_id` int(11) NOT NULL,
  `team_lead_id` int(11) NOT NULL,
  `floor_support_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roosters`
--

CREATE TABLE `roosters` (
  `rooster_login_id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  `team_lead_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `users_id` int(11) NOT NULL,
  `user_type` enum('auditor','agent','team_lead') NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_password` varchar(255) NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `disposition`
--
ALTER TABLE `disposition`
  ADD PRIMARY KEY (`disposition_id`);

--
-- Indexes for table `flagged_calls`
--
ALTER TABLE `flagged_calls`
  ADD PRIMARY KEY (`flagged_calls_id`);

--
-- Indexes for table `incident_report`
--
ALTER TABLE `incident_report`
  ADD PRIMARY KEY (`incident_report_id`);

--
-- Indexes for table `long_call_audits`
--
ALTER TABLE `long_call_audits`
  ADD PRIMARY KEY (`audit_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`users_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `disposition`
--
ALTER TABLE `disposition`
  MODIFY `disposition_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `flagged_calls`
--
ALTER TABLE `flagged_calls`
  MODIFY `flagged_calls_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `incident_report`
--
ALTER TABLE `incident_report`
  MODIFY `incident_report_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `long_call_audits`
--
ALTER TABLE `long_call_audits`
  MODIFY `audit_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `users_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
