$("document").ready(function(){
		
		var audit_list 				= $("#audit_list").DataTable({"pageLength": 15,

																"order": [ 0, "desc" ],


																});
 
		var issue_comment_report	= $("#issue_comment_report").DataTable({

																"paging": false,
																"searching" : false,
																"order": [ 1, "desc" ],
															  	"lengthChange": false
																});
		$("#hide_audit_table_btn").click(function(){

			$("#audit_table_container").toggle();

		});

});

