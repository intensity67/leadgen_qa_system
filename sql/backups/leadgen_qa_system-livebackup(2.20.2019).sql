-- phpMyAdmin SQL Dump
-- version 4.0.10deb1ubuntu0.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 19, 2019 at 03:59 PM
-- Server version: 5.5.62-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `leadgen_qa_system`
--

-- --------------------------------------------------------

--
-- Table structure for table `agent_logins`
--

CREATE TABLE IF NOT EXISTS `agent_logins` (
  `employee_id` int(11) NOT NULL,
  `user_account_id` int(11) NOT NULL,
  `vicidial_agent_id_1` int(11) NOT NULL,
  `vicidial_agent_id_2` int(11) NOT NULL,
  `call_login_1` int(11) NOT NULL,
  `call_login_2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `audits`
--

CREATE TABLE IF NOT EXISTS `audits` (
  `audit_id` int(11) NOT NULL AUTO_INCREMENT,
  `recording_cluster_id` int(11) NOT NULL,
  `audit_timestart` timestamp NULL DEFAULT NULL,
  `audit_timeend` timestamp NULL DEFAULT NULL,
  `work_week` date NOT NULL,
  `recording_id` int(11) NOT NULL,
  `prospect_tone` enum('1','2','3') NOT NULL,
  `lead_id` int(11) NOT NULL,
  `phone_number` int(11) NOT NULL,
  `customer` varchar(255) DEFAULT NULL,
  `agent_login_id` int(11) NOT NULL,
  `agent_name` varchar(255) NOT NULL,
  `call_timestamp` timestamp NULL DEFAULT NULL,
  `cluster_no` enum('1','2','3','4') NOT NULL,
  `recording_location` varchar(255) NOT NULL,
  `agent_disposition_id` int(11) DEFAULT NULL,
  `agent_disposition` varchar(255) DEFAULT NULL,
  `correct_disposition_id` int(11) NOT NULL,
  `agent_issue` enum('Agent','System','Others','N/A') NOT NULL,
  `issue_comment_id` int(11) NOT NULL,
  `mark_status` enum('ZTP','LOL') DEFAULT NULL,
  `incorrect_response` enum('Yes','No') NOT NULL,
  `ztp_mark_comment` varchar(255) NOT NULL,
  `general_observation_id` int(11) NOT NULL,
  `qa_remarks` text NOT NULL,
  `audit_timestamp` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `auditor_id` int(11) NOT NULL,
  `auditor` varchar(255) NOT NULL,
  `soundboard_voice_id` int(11) NOT NULL,
  `tl` varchar(255) DEFAULT NULL,
  `fs` varchar(255) DEFAULT NULL,
  `team_lead_user_id` int(11) DEFAULT NULL,
  `floor_support_user_id` int(11) DEFAULT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `audit_type` enum('short_call','long_call') NOT NULL,
  `incident_report_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`audit_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `audits`
--

INSERT INTO `audits` (`audit_id`, `recording_cluster_id`, `audit_timestart`, `audit_timeend`, `work_week`, `recording_id`, `prospect_tone`, `lead_id`, `phone_number`, `customer`, `agent_login_id`, `agent_name`, `call_timestamp`, `cluster_no`, `recording_location`, `agent_disposition_id`, `agent_disposition`, `correct_disposition_id`, `agent_issue`, `issue_comment_id`, `mark_status`, `incorrect_response`, `ztp_mark_comment`, `general_observation_id`, `qa_remarks`, `audit_timestamp`, `auditor_id`, `auditor`, `soundboard_voice_id`, `tl`, `fs`, `team_lead_user_id`, `floor_support_user_id`, `last_updated`, `audit_type`, `incident_report_id`) VALUES
(1, 30, '2019-02-19 21:55:34', '2019-02-19 21:58:53', '2019-02-18', 15530848, '2', 476803014, 2147483647, NULL, 3229, 'Villarin, Platon Jr.', '2019-02-19 08:17:57', '4', 'http://38.107.176.254/RECORDINGS/20190219-131800_3479382662-all.wav', NULL, 'NI', 0, 'N/A', 0, '', '', '', 5, '', '2019-02-19 21:58:53', 1, '', 3, 'Miao', 'Platon', NULL, NULL, '2019-02-19 21:58:53', 'long_call', NULL),
(2, 12, '2019-02-19 22:00:07', '2019-02-19 22:02:37', '2019-02-18', 15531271, '2', 476842525, 2147483647, NULL, 3351, 'Nacario, Philip', '2019-02-19 08:19:41', '4', 'http://38.107.176.252/RECORDINGS/20190219-131944_7025568287-all.wav', NULL, 'NI', 0, 'N/A', 0, '', '', '', 5, '', '2019-02-19 22:02:37', 1, '', 10, 'Miao', 'Platon', NULL, NULL, '2019-02-19 22:02:37', 'long_call', NULL),
(3, 25, '2019-02-19 22:02:58', '2019-02-19 22:05:16', '2019-02-18', 15530985, '2', 476811950, 2147483647, NULL, 3662, '', '2019-02-19 08:18:28', '4', 'http://38.107.174.247/RECORDINGS/20190219-131832_4436249382-all.wav', NULL, 'NI', 0, 'N/A', 0, '', '', '', 5, '', '2019-02-19 22:05:16', 1, '', 3, '', '', NULL, NULL, '2019-02-19 22:05:16', 'long_call', NULL),
(4, 24, '2019-02-19 22:05:23', '2019-02-19 22:07:23', '2019-02-18', 15531006, '2', 476839869, 2147483647, NULL, 3659, '', '2019-02-19 08:18:33', '4', 'http://38.102.225.171/RECORDINGS/20190219-131836_3157505478-all.wav', NULL, 'NI', 0, 'N/A', 0, '', '', '', 1, 'failed to throw 2nd rebuttals', '2019-02-19 22:07:23', 1, '', 3, '', '', NULL, NULL, '2019-02-19 22:07:23', 'long_call', NULL),
(5, 92, '2019-02-19 22:24:56', '2019-02-19 22:29:16', '2019-02-18', 1390436, '2', 19628867, 2147483647, NULL, 3679, '', '2019-02-19 09:16:29', '4', 'http://103.44.234.206/RECORDINGS/MP3/20190220-061631_5035680663-all.mp3', NULL, 'NI', 0, 'Agent', 0, '', '', '', 18, '', '2019-02-19 22:29:16', 1, '', 3, '', '', NULL, NULL, '2019-02-19 22:29:16', 'long_call', NULL),
(6, 112, '2019-02-19 22:29:25', '2019-02-19 22:31:09', '2019-02-18', 1387915, '2', 19606577, 2147483647, NULL, 3426, 'Contado, Nino', '2019-02-19 08:54:02', '4', 'http://103.44.234.203/RECORDINGS/20190220-055405_8048943664-all.wav', NULL, 'NI', 0, 'Agent', 0, '', '', '', 1, 'interruption failed to used paused script.', '2019-02-19 22:31:09', 1, '', 3, 'Steve', 'Vivian Omega', NULL, NULL, '2019-02-19 22:31:09', 'long_call', NULL),
(7, 102, '2019-02-19 22:31:35', '2019-02-19 22:33:33', '2019-02-18', 1389319, '2', 19583503, 2147483647, NULL, 3032, 'Pacana, Pete', '2019-02-19 09:06:43', '4', 'http://103.44.234.206/RECORDINGS/MP3/20190220-060646_3187621500-all.mp3', NULL, 'NI', 0, 'Agent', 0, '', '', '', 1, 'wrong dispo should tagged as NQ.', '2019-02-19 22:33:33', 1, '', 3, 'Anthony', 'Edbonson & Norwin', NULL, NULL, '2019-02-19 22:33:33', 'long_call', NULL),
(8, 106, '2019-02-19 22:33:58', '2019-02-19 22:36:36', '2019-02-18', 1389004, '2', 19530500, 2147483647, NULL, 3022, 'Del Castillo, Regine', '2019-02-19 09:03:54', '4', 'http://103.44.234.206/RECORDINGS/MP3/20190220-060356_3303148857-all.mp3', NULL, 'NI', 0, 'N/A', 0, '', '', '', 5, '', '2019-02-19 22:36:36', 1, '', 10, 'Steve', 'Merben', NULL, NULL, '2019-02-19 22:36:36', 'long_call', NULL),
(9, 118, '2019-02-19 22:37:10', '2019-02-19 22:39:54', '2019-02-18', 1387474, '2', 19515287, 2147483647, NULL, 3279, 'Fernandez, Steven', '2019-02-19 08:50:04', '4', 'http://103.44.234.203/RECORDINGS/20190220-055007_5133483872-all.wav', NULL, 'NI', 0, 'N/A', 0, '', '', '', 5, 'prospect was really not interested', '2019-02-19 22:39:54', 1, '', 3, 'Anthony', 'Edbonson & Norwin', NULL, NULL, '2019-02-19 22:39:54', 'long_call', NULL),
(10, 95, '2019-02-19 22:40:08', '2019-02-19 22:46:08', '2019-02-18', 1389967, '2', 19614442, 2147483647, NULL, 3686, '', '2019-02-19 09:12:27', '4', 'http://103.44.234.205/RECORDINGS/20190220-061229_4353199157-all.wav', NULL, 'NI', 0, 'N/A', 0, '', '', '', 5, '', '2019-02-19 22:46:08', 1, '', 3, '', '', NULL, NULL, '2019-02-19 22:46:08', 'long_call', NULL),
(11, 111, '2019-02-19 22:46:20', '2019-02-19 22:48:41', '2019-02-18', 1388138, '2', 19565575, 2147483647, NULL, 3032, 'Pacana, Pete', '2019-02-19 08:55:58', '4', 'http://103.44.234.206/RECORDINGS/MP3/20190220-055601_4436227669-all.mp3', NULL, 'NI', 0, 'N/A', 0, '', '', '', 1, 'failed to acknowlegde prospect question', '2019-02-19 22:48:41', 1, '', 3, 'Anthony', 'Edbonson & Norwin', NULL, NULL, '2019-02-19 22:48:41', 'long_call', NULL),
(12, 115, '2019-02-19 22:48:50', '2019-02-19 22:50:33', '2019-02-18', 1387842, '2', 19624273, 2147483647, NULL, 3094, 'Cruz, Analito', '2019-02-19 08:53:23', '4', 'http://103.44.234.204/RECORDINGS/20190220-055326_2544621844-all.wav', NULL, 'NI', 0, 'Agent', 0, '', '', '', 1, 'failed to throw 2nd rebuttals', '2019-02-19 22:50:33', 1, '', 1, 'Anthony', 'Edbonson & Norwin', NULL, NULL, '2019-02-19 22:50:33', 'long_call', NULL),
(13, 116, '2019-02-19 22:51:02', '2019-02-19 22:54:49', '2019-02-18', 1387727, '2', 19595130, 2147483647, NULL, 3388, 'Abrea, Neil Joseph', '2019-02-19 08:52:22', '4', 'http://103.44.234.206/RECORDINGS/MP3/20190220-055225_7573925371-all.mp3', NULL, 'NI', 0, 'Agent', 0, '', '', '', 1, 'failed to throw 2nd rebuttals ', '2019-02-19 22:54:49', 1, '', 3, 'Steve', 'Merben', NULL, NULL, '2019-02-19 22:54:49', 'long_call', NULL),
(14, 120, '2019-02-19 22:56:55', '2019-02-19 22:58:35', '2019-02-18', 1387435, '2', 19566058, 2147483647, NULL, 3160, 'Largo, Arnel', '2019-02-19 08:49:45', '4', 'http://103.44.234.206/RECORDINGS/MP3/20190219-134947_4406454863-all.mp3', NULL, 'NI', 0, 'N/A', 0, '', '', '', 5, 'prospect disconnect the call', '2019-02-19 22:58:35', 1, '', 1, 'Anthony', 'Ezron & Jacky', NULL, NULL, '2019-02-19 22:58:35', 'long_call', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `client_feedback_list`
--

CREATE TABLE IF NOT EXISTS `client_feedback_list` (
  `client_feedback_list_id` int(11) NOT NULL AUTO_INCREMENT,
  `client_feedback_desc` varchar(255) NOT NULL,
  PRIMARY KEY (`client_feedback_list_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `client_feedback_list`
--

INSERT INTO `client_feedback_list` (`client_feedback_list_id`, `client_feedback_desc`) VALUES
(1, 'Stated they weren''t interested'),
(2, 'Too much background noise'),
(3, 'Intro not smooth'),
(4, 'Didn''t want to be transferred'),
(5, 'Caller confused'),
(6, 'Caller angry'),
(7, 'Agent rushed'),
(8, 'Agent accent too thick'),
(9, 'Language barrier'),
(10, 'Call prospect back'),
(11, 'Already with my carrier'),
(12, 'DNC'),
(13, 'Prank');

-- --------------------------------------------------------

--
-- Table structure for table `cluster_recordings`
--

CREATE TABLE IF NOT EXISTS `cluster_recordings` (
  `recording_cluster_id` int(11) NOT NULL AUTO_INCREMENT,
  `recording_id` int(11) NOT NULL,
  `channel` varchar(255) NOT NULL,
  `server_ip` int(11) NOT NULL,
  `extension` int(11) NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `start_epoch` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_epoch` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `length_in_sec` int(11) NOT NULL,
  `length_in_min` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `user_group` varchar(255) NOT NULL,
  `vicidial_id` varchar(255) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `list_id` int(11) NOT NULL,
  `cluster_no` int(11) NOT NULL,
  `call_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` varchar(255) NOT NULL,
  `phone_number` int(11) NOT NULL,
  `called_count` int(11) NOT NULL,
  `alt_dial` varchar(255) NOT NULL,
  `recording_log_status` enum('new','flagged','audited','archived') NOT NULL DEFAULT 'new',
  `auditor_id` int(11) NOT NULL,
  PRIMARY KEY (`recording_cluster_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=121 ;

--
-- Dumping data for table `cluster_recordings`
--

INSERT INTO `cluster_recordings` (`recording_cluster_id`, `recording_id`, `channel`, `server_ip`, `extension`, `start_time`, `start_epoch`, `end_time`, `end_epoch`, `length_in_sec`, `length_in_min`, `filename`, `location`, `lead_id`, `user`, `user_group`, `vicidial_id`, `unique_id`, `list_id`, `cluster_no`, `call_date`, `status`, `phone_number`, `called_count`, `alt_dial`, `recording_log_status`, `auditor_id`) VALUES
(12, 15531271, 'Local/58600052@default', 38107, 8309, '2019-02-19 22:02:37', '0000-00-00 00:00:00', '2019-02-19 08:21:23', '0000-00-00 00:00:00', 98, 2, '20190219-131944_7025568287', 'http://38.107.176.252/RECORDINGS/20190219-131944_7025568287-all.wav', 476842525, 3351, 'TEAMFERNANDO', '1550611169.141964', '', 0, 4, '2019-02-19 08:19:41', 'NI', 2147483647, 1, 'MAIN', 'audited', 0),
(24, 15531006, 'Local/58600060@default', 38102, 8309, '2019-02-19 22:07:23', '0000-00-00 00:00:00', '2019-02-19 08:19:51', '0000-00-00 00:00:00', 74, 1, '20190219-131836_3157505478', 'http://38.102.225.171/RECORDINGS/20190219-131836_3157505478-all.wav', 476839869, 3659, 'QF', '1550611097.528993', '', 0, 4, '2019-02-19 08:18:33', 'NI', 2147483647, 1, 'MAIN', 'audited', 0),
(25, 15530985, 'Local/58600074@default', 38107, 8309, '2019-02-19 22:05:16', '0000-00-00 00:00:00', '2019-02-19 08:19:46', '0000-00-00 00:00:00', 73, 1, '20190219-131832_4436249382', 'http://38.107.174.247/RECORDINGS/20190219-131832_4436249382-all.wav', 476811950, 3662, 'QF', '1550611087.22065159', '', 0, 4, '2019-02-19 08:18:28', 'NI', 2147483647, 1, 'MAIN', 'audited', 0),
(30, 15530848, 'Local/58600067@default', 38107, 8309, '2019-02-19 21:58:53', '0000-00-00 00:00:00', '2019-02-19 08:19:13', '0000-00-00 00:00:00', 72, 1, '20190219-131800_3479382662', 'http://38.107.176.254/RECORDINGS/20190219-131800_3479382662-all.wav', 476803014, 3229, 'TEAMFERNANDO', '1550611046.14788651', '', 0, 4, '2019-02-19 08:17:57', 'NI', 2147483647, 1, 'MAIN', 'audited', 0),
(91, 1390479, 'Local/58600052@default', 10344, 8309, '2019-02-19 09:16:54', '0000-00-00 00:00:00', '2019-02-19 09:18:16', '0000-00-00 00:00:00', 82, 1, '20190220-061653_4693058576', 'http://103.44.234.204/RECORDINGS/20190220-061653_4693058576-all.wav', 19649665, 3082, 'OKCL4', '1550614590.680065', '', 3001, 4, '2019-02-19 09:16:51', 'NI', 2147483647, 1, 'MAIN', 'new', 0),
(92, 1390436, 'Local/58600058@default', 10344, 8309, '2019-02-19 22:29:16', '0000-00-00 00:00:00', '2019-02-19 09:17:34', '0000-00-00 00:00:00', 62, 1, '20190220-061631_5035680663', 'http://103.44.234.206/RECORDINGS/MP3/20190220-061631_5035680663-all.mp3', 19628867, 3679, 'QF', '1550614570.1130064', '', 3001, 4, '2019-02-19 09:16:29', 'NI', 2147483647, 1, 'MAIN', 'audited', 0),
(93, 1390343, 'Local/58600062@default', 10344, 8309, '2019-02-19 09:15:40', '0000-00-00 00:00:00', '2019-02-19 09:17:28', '0000-00-00 00:00:00', 108, 2, '20190220-061539_3035873463', 'http://103.44.234.204/RECORDINGS/20190220-061539_3035873463-all.wav', 19569202, 3518, 'OKCL4', '1550614524.1170905', '', 3001, 4, '2019-02-19 09:15:36', 'NI', 2147483647, 1, 'MAIN', 'new', 0),
(94, 1390118, 'Local/58600053@default', 10344, 8309, '2019-02-19 09:13:44', '0000-00-00 00:00:00', '2019-02-19 09:14:53', '0000-00-00 00:00:00', 69, 1, '20190219-141343_6083541873', 'http://103.44.234.204/RECORDINGS/20190219-141343_6083541873-all.wav', 19636290, 3062, 'OKCL4', '1550614402.1168569', '', 3001, 4, '2019-02-19 09:13:41', 'NI', 2147483647, 1, 'MAIN', 'new', 0),
(95, 1389967, 'Local/58600067@default', 10344, 8309, '2019-02-19 22:46:08', '0000-00-00 00:00:00', '2019-02-19 09:14:20', '0000-00-00 00:00:00', 110, 2, '20190220-061229_4353199157', 'http://103.44.234.205/RECORDINGS/20190220-061229_4353199157-all.wav', 19614442, 3686, 'QF', '1550614325.4194291', '', 3001, 4, '2019-02-19 09:12:27', 'NI', 2147483647, 1, 'MAIN', 'audited', 0),
(96, 1389935, 'Local/58600052@default', 10344, 8309, '2019-02-19 09:12:17', '0000-00-00 00:00:00', '2019-02-19 09:13:36', '0000-00-00 00:00:00', 79, 1, '20190220-061216_3854998026', 'http://103.44.234.204/RECORDINGS/20190220-061216_3854998026-all.wav', 19623931, 3082, 'OKCL4', '1550614321.1125509', '', 3001, 4, '2019-02-19 09:12:10', 'NI', 2147483647, 1, 'MAIN', 'new', 0),
(97, 1389720, 'Local/58600059@default', 10344, 8309, '2019-02-19 09:10:20', '0000-00-00 00:00:00', '2019-02-19 09:12:00', '0000-00-00 00:00:00', 100, 2, '20190219-141019_5712854200', 'http://103.44.234.204/RECORDINGS/20190219-141019_5712854200-all.wav', 19586353, 3283, 'OKCL4', '1550614192.1164874', '', 3001, 4, '2019-02-19 09:10:17', 'NI', 2147483647, 1, 'MAIN', 'new', 0),
(98, 1389644, 'Local/58600056@default', 10344, 8309, '2019-02-19 09:09:43', '0000-00-00 00:00:00', '2019-02-19 09:11:44', '0000-00-00 00:00:00', 121, 2, '20190219-140942_7402945831', 'http://103.44.234.204/RECORDINGS/20190219-140942_7402945831-all.wav', 19500409, 3428, 'OKCL4', '1550614163.4191659', '', 3001, 4, '2019-02-19 09:09:39', 'NI', 2147483647, 1, 'MAIN', 'new', 0),
(99, 1389511, 'Local/58600054@default', 10344, 8309, '2019-02-19 09:08:30', '0000-00-00 00:00:00', '2019-02-19 09:10:03', '0000-00-00 00:00:00', 93, 2, '20190220-060829_4407149738', 'http://103.44.234.206/RECORDINGS/MP3/20190220-060829_4407149738-all.mp3', 19612894, 3038, 'OKCL4', '1550614088.1163151', '', 0, 4, '2019-02-19 09:08:27', 'NI', 2147483647, 1, 'MAIN', 'new', 0),
(100, 1389507, 'Local/58600052@default', 10344, 8309, '2019-02-19 09:08:29', '0000-00-00 00:00:00', '2019-02-19 09:09:36', '0000-00-00 00:00:00', 67, 1, '20190220-060828_7175426591', 'http://103.44.234.204/RECORDINGS/20190220-060828_7175426591-all.wav', 19587481, 3082, 'OKCL4', '1550614089.671193', '', 0, 4, '2019-02-19 09:08:25', 'NI', 2147483647, 1, 'MAIN', 'new', 0),
(101, 1389395, 'Local/58600067@default', 10344, 8309, '2019-02-19 09:07:30', '0000-00-00 00:00:00', '2019-02-19 09:09:28', '0000-00-00 00:00:00', 118, 2, '20190219-140729_4235981015', 'http://103.44.234.204/RECORDINGS/20190219-140729_4235981015-all.wav', 19584233, 3213, 'OKCL4', '1550614021.1120782', '', 0, 4, '2019-02-19 09:07:26', 'NI', 2147483647, 1, 'MAIN', 'new', 0),
(102, 1389319, 'Local/58600060@default', 10344, 8309, '2019-02-19 22:33:33', '0000-00-00 00:00:00', '2019-02-19 09:07:48', '0000-00-00 00:00:00', 61, 1, '20190220-060646_3187621500', 'http://103.44.234.206/RECORDINGS/MP3/20190220-060646_3187621500-all.mp3', 19583503, 3032, 'OKCL4', '1550613972.4188523', '', 0, 4, '2019-02-19 09:06:43', 'NI', 2147483647, 1, 'MAIN', 'audited', 0),
(103, 1389235, 'Local/58600056@default', 10344, 8309, '2019-02-19 09:06:05', '0000-00-00 00:00:00', '2019-02-19 09:07:31', '0000-00-00 00:00:00', 86, 1, '20190220-060604_7577611513', 'http://103.44.234.205/RECORDINGS/20190220-060604_7577611513-all.wav', 19605273, 3605, 'WAVE19', '1550613944.668800', '', 0, 4, '2019-02-19 09:06:01', 'NI', 2147483647, 1, 'MAIN', 'new', 0),
(104, 1389195, 'Local/58600051@default', 10344, 8309, '2019-02-19 09:05:40', '0000-00-00 00:00:00', '2019-02-19 09:06:42', '0000-00-00 00:00:00', 62, 1, '20190220-060539_2515543546', 'http://103.44.234.206/RECORDINGS/MP3/20190220-060539_2515543546-all.mp3', 19566263, 3514, 'OKCL4', '1550613912.1160149', '', 0, 4, '2019-02-19 09:05:36', 'NI', 2147483647, 1, 'MAIN', 'new', 0),
(105, 1389121, 'Local/58600051@default', 10344, 8309, '2019-02-19 09:04:55', '0000-00-00 00:00:00', '2019-02-19 09:06:05', '0000-00-00 00:00:00', 70, 1, '20190220-060454_3023821346', 'http://103.44.234.205/RECORDINGS/20190220-060454_3023821346-all.wav', 19466390, 3060, 'QF', '1550613861.4186686', '', 0, 4, '2019-02-19 09:04:51', 'NI', 2147483647, 1, 'MAIN', 'new', 0),
(106, 1389004, 'Local/58600056@default', 10344, 8309, '2019-02-19 22:36:37', '0000-00-00 00:00:00', '2019-02-19 09:04:57', '0000-00-00 00:00:00', 60, 1, '20190220-060356_3303148857', 'http://103.44.234.206/RECORDINGS/MP3/20190220-060356_3303148857-all.mp3', 19530500, 3022, 'OKCL4', '1550613812.4185920', '', 0, 4, '2019-02-19 09:03:54', 'NI', 2147483647, 1, 'MAIN', 'audited', 0),
(107, 1388918, 'Local/58600061@default', 10344, 8309, '2019-02-19 09:03:09', '0000-00-00 00:00:00', '2019-02-19 09:05:20', '0000-00-00 00:00:00', 131, 2, '20190220-060308_5092373694', 'http://103.44.234.206/RECORDINGS/MP3/20190220-060308_5092373694-all.mp3', 19608170, 3186, 'QF', '1550613773.666022', '', 0, 4, '2019-02-19 09:03:06', 'NI', 2147483647, 1, 'MAIN', 'new', 0),
(108, 1388831, 'Local/58600051@default', 10344, 8309, '2019-02-19 09:02:19', '0000-00-00 00:00:00', '2019-02-19 09:04:48', '0000-00-00 00:00:00', 149, 2, '20190220-060218_7752991082', 'http://103.44.234.205/RECORDINGS/20190220-060218_7752991082-all.wav', 19521061, 3060, 'QF', '1550613722.1156972', '', 0, 4, '2019-02-19 09:02:14', 'NI', 2147483647, 1, 'MAIN', 'new', 0),
(109, 1388759, 'Local/58600056@default', 10344, 8309, '2019-02-19 09:01:48', '0000-00-00 00:00:00', '2019-02-19 09:03:04', '0000-00-00 00:00:00', 76, 1, '20190220-060147_7579668767', 'http://103.44.234.205/RECORDINGS/20190220-060147_7579668767-all.wav', 19629985, 3605, 'WAVE19', '1550613679.1156259', '', 0, 4, '2019-02-19 09:01:44', 'NI', 2147483647, 1, 'MAIN', 'new', 0),
(110, 1388758, 'Local/58600067@default', 10344, 8309, '2019-02-19 09:01:48', '0000-00-00 00:00:00', '2019-02-19 09:02:56', '0000-00-00 00:00:00', 68, 1, '20190220-060147_7244073429', 'http://103.44.234.205/RECORDINGS/20190220-060147_7244073429-all.wav', 19595448, 3686, 'QF', '1550613691.1156478', '', 0, 4, '2019-02-19 09:01:45', 'NI', 2147483647, 1, 'MAIN', 'new', 0),
(111, 1388138, 'Local/58600060@default', 10344, 8309, '2019-02-19 22:48:41', '0000-00-00 00:00:00', '2019-02-19 08:57:10', '0000-00-00 00:00:00', 68, 1, '20190220-055601_4436227669', 'http://103.44.234.206/RECORDINGS/MP3/20190220-055601_4436227669-all.mp3', 19565575, 3032, 'OKCL4', '1550613329.1150273', '', 0, 4, '2019-02-19 08:55:58', 'NI', 2147483647, 1, 'MAIN', 'audited', 0),
(112, 1387915, 'Local/58600056@default', 10344, 8309, '2019-02-19 22:31:09', '0000-00-00 00:00:00', '2019-02-19 08:55:07', '0000-00-00 00:00:00', 61, 1, '20190220-055405_8048943664', 'http://103.44.234.203/RECORDINGS/20190220-055405_8048943664-all.wav', 19606577, 3426, 'OKCL4', '1550613230.4176086', '', 0, 4, '2019-02-19 08:54:02', 'NI', 2147483647, 1, 'MAIN', 'audited', 0),
(113, 1387913, 'Local/58600067@default', 10344, 8309, '2019-02-19 08:54:04', '0000-00-00 00:00:00', '2019-02-19 08:55:06', '0000-00-00 00:00:00', 62, 1, '20190220-055403_7574690570', 'http://103.44.234.206/RECORDINGS/MP3/20190220-055403_7574690570-all.mp3', 19608238, 3388, 'OKCL4', '1550613227.1108050', '', 0, 4, '2019-02-19 08:54:01', 'NI', 2147483647, 1, 'MAIN', 'new', 0),
(114, 1387856, 'Local/58600061@default', 10344, 8309, '2019-02-19 08:53:35', '0000-00-00 00:00:00', '2019-02-19 08:54:57', '0000-00-00 00:00:00', 82, 1, '20190220-055334_5712797811', 'http://103.44.234.206/RECORDINGS/MP3/20190220-055334_5712797811-all.mp3', 19605869, 3186, 'QF', '1550613194.1147912', '', 0, 4, '2019-02-19 08:53:32', 'NI', 2147483647, 1, 'MAIN', 'new', 0),
(115, 1387842, 'Local/58600057@default', 10344, 8309, '2019-02-19 22:50:33', '0000-00-00 00:00:00', '2019-02-19 08:54:38', '0000-00-00 00:00:00', 71, 1, '20190220-055326_2544621844', 'http://103.44.234.204/RECORDINGS/20190220-055326_2544621844-all.wav', 19624273, 3094, 'OKCL4', '1550613194.1107531', '', 0, 4, '2019-02-19 08:53:23', 'NI', 2147483647, 1, 'MAIN', 'audited', 0),
(116, 1387727, 'Local/58600067@default', 10344, 8309, '2019-02-19 22:54:49', '0000-00-00 00:00:00', '2019-02-19 08:53:33', '0000-00-00 00:00:00', 67, 1, '20190220-055225_7573925371', 'http://103.44.234.206/RECORDINGS/MP3/20190220-055225_7573925371-all.mp3', 19595130, 3388, 'OKCL4', '1550613121.4174222', '', 0, 4, '2019-02-19 08:52:22', 'NI', 2147483647, 1, 'MAIN', 'audited', 0),
(117, 1387500, 'Local/58600066@default', 10344, 8309, '2019-02-19 08:50:23', '0000-00-00 00:00:00', '2019-02-19 08:51:59', '0000-00-00 00:00:00', 96, 2, '20190220-055022_2562140891', 'http://103.44.234.206/RECORDINGS/MP3/20190220-055022_2562140891-all.mp3', 19610502, 3530, 'QF', '1550612993.1144396', '', 0, 4, '2019-02-19 08:50:20', 'NI', 2147483647, 1, 'MAIN', 'new', 0),
(118, 1387474, 'Local/58600060@default', 10344, 8309, '2019-02-19 22:39:54', '0000-00-00 00:00:00', '2019-02-19 08:51:55', '0000-00-00 00:00:00', 107, 2, '20190220-055007_5133483872', 'http://103.44.234.203/RECORDINGS/20190220-055007_5133483872-all.wav', 19515287, 3279, 'OKCL4', '1550612989.4171896', '', 0, 4, '2019-02-19 08:50:04', 'NI', 2147483647, 1, 'MAIN', 'audited', 0),
(119, 1387462, 'Local/58600058@default', 10344, 8309, '2019-02-19 08:50:04', '0000-00-00 00:00:00', '2019-02-19 08:51:23', '0000-00-00 00:00:00', 79, 1, '20190220-055003_7247105458', 'http://103.44.234.205/RECORDINGS/20190220-055003_7247105458-all.wav', 19579915, 3100, 'OKCL4', '1550612976.4171695', '', 0, 4, '2019-02-19 08:50:01', 'NI', 2147483647, 1, 'MAIN', 'new', 0),
(120, 1387435, 'Local/58600065@default', 10344, 8309, '2019-02-19 22:58:35', '0000-00-00 00:00:00', '2019-02-19 08:50:50', '0000-00-00 00:00:00', 62, 1, '20190219-134947_4406454863', 'http://103.44.234.206/RECORDINGS/MP3/20190219-134947_4406454863-all.mp3', 19566058, 3160, 'OKCL4', '1550612963.1143856', '', 0, 4, '2019-02-19 08:49:45', 'NI', 2147483647, 1, 'MAIN', 'audited', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cluster_recordings_archive`
--

CREATE TABLE IF NOT EXISTS `cluster_recordings_archive` (
  `recording_cluster_id` int(11) NOT NULL AUTO_INCREMENT,
  `recording_id` int(11) NOT NULL,
  `channel` varchar(255) NOT NULL,
  `server_ip` int(11) NOT NULL,
  `extension` int(11) NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `start_epoch` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_epoch` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `length_in_sec` int(11) NOT NULL,
  `length_in_min` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `user_group` varchar(255) NOT NULL,
  `vicidial_id` varchar(255) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `list_id` int(11) NOT NULL,
  `cluster_no` int(11) NOT NULL,
  `call_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` varchar(255) NOT NULL,
  `phone_number` int(11) NOT NULL,
  `called_count` int(11) NOT NULL,
  `alt_dial` varchar(255) NOT NULL,
  `recording_log_status` enum('new','flagged','audited','archived') NOT NULL DEFAULT 'new',
  `auditor_id` int(11) NOT NULL,
  `reason_archive` varchar(255) NOT NULL,
  PRIMARY KEY (`recording_cluster_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=385 ;

--
-- Dumping data for table `cluster_recordings_archive`
--

INSERT INTO `cluster_recordings_archive` (`recording_cluster_id`, `recording_id`, `channel`, `server_ip`, `extension`, `start_time`, `start_epoch`, `end_time`, `end_epoch`, `length_in_sec`, `length_in_min`, `filename`, `location`, `lead_id`, `user`, `user_group`, `vicidial_id`, `unique_id`, `list_id`, `cluster_no`, `call_date`, `status`, `phone_number`, `called_count`, `alt_dial`, `recording_log_status`, `auditor_id`, `reason_archive`) VALUES
(1, 15531916, 'IAX2/ASTloop-3316', 38102, 2147483647, '2019-02-19 08:22:23', '0000-00-00 00:00:00', '2019-02-19 21:26:52', '0000-00-00 00:00:00', 90, 2, '20190219162223_3202_44355517865773660', 'http://38.102.225.171/RECORDINGS/20190219162223_3202_44355517865773660-all.wav', 476860650, 3202, 'TEAMGRANVILLE', '1550611342.711777', '', 3001, 2, '2019-02-19 08:18:57', 'TrSuc', 2147483647, 1, 'MAIN', 'new', 0, 'Empty Recording'),
(3, 15531762, 'Local/58600073@default', 38102, 8309, '2019-02-19 08:21:44', '0000-00-00 00:00:00', '2019-02-19 08:23:02', '0000-00-00 00:00:00', 78, 1, '20190220-052143_4134173387', 'http://38.102.225.171/RECORDINGS/20190220-052143_4134173387-all.wav', 476827681, 400106, 'Negros', '1550611271.358404', '', 3001, 2, '2019-02-19 08:21:39', 'NQ', 2147483647, 1, 'MAIN', 'new', 0, 'Empty Recording'),
(5, 15531715, 'IAX2/ASTloop-8414', 38102, 2147483647, '2019-02-19 08:21:33', '0000-00-00 00:00:00', '2019-02-19 21:26:52', '0000-00-00 00:00:00', 83, 1, '20190219162133_400114_44355518325489284', 'http://38.102.225.171/RECORDINGS/20190219162133_400114_44355518325489284-all.wav', 476865537, 400114, 'Negros', '1550611293.710760', '', 3001, 2, '2019-02-19 08:15:59', 'TrSuc', 2107898845, 1, 'MAIN', 'new', 0, 'Empty Recording'),
(7, 15531577, 'Local/58600052@default', 38107, 8309, '2019-02-19 08:21:02', '0000-00-00 00:00:00', '2019-02-19 08:22:02', '0000-00-00 00:00:00', 60, 1, '20190220-052101_8042444316', 'http://38.107.174.247/RECORDINGS/20190220-052101_8042444316-all.wav', 476845909, 400115, 'Negros', '1550611236.143252', '', 0, 2, '2019-02-19 08:20:58', 'NQ', 2147483647, 1, 'MAIN', 'new', 0, 'Empty Recording'),
(8, 15531525, 'Local/58600072@default', 38102, 8309, '2019-02-19 08:20:50', '0000-00-00 00:00:00', '2019-02-19 08:22:38', '0000-00-00 00:00:00', 108, 2, '20190219-132049_6175999727', 'http://38.102.225.171/RECORDINGS/20190219-132049_6175999727-all.wav', 476836980, 3557, 'TEAMELNA', '1550611231.709535', '', 0, 2, '2019-02-19 08:20:47', 'HUP', 2147483647, 1, 'NONE', 'new', 0, 'Empty Recording'),
(11, 15531364, 'Local/58600064@default', 38102, 8309, '2019-02-19 08:20:08', '0000-00-00 00:00:00', '2019-02-19 08:23:30', '0000-00-00 00:00:00', 202, 3, '20190219-132007_2566685254', 'http://38.102.225.170/RECORDINGS/20190219-132007_2566685254-all.wav', 476745372, 3665, 'QF', '1550611179.14791044', '', 3001, 2, '2019-02-19 08:20:05', 'TrSuc', 2147483647, 1, 'MAIN', 'new', 0, 'Empty Recording'),
(16, 15531220, 'Local/58600052@default', 38107, 8309, '2019-02-19 08:19:32', '0000-00-00 00:00:00', '2019-02-19 08:20:43', '0000-00-00 00:00:00', 71, 1, '20190220-051931_4135077868', 'http://38.107.174.247/RECORDINGS/20190220-051931_4135077868-all.wav', 476812453, 400115, 'Negros', '1550611152.707942', '', 0, 2, '2019-02-19 08:19:26', 'NQ', 2147483647, 1, 'MAIN', 'new', 0, 'Empty Recording'),
(17, 15531216, 'Local/58600060@default', 38107, 8309, '2019-02-19 08:19:31', '0000-00-00 00:00:00', '2019-02-19 08:21:23', '0000-00-00 00:00:00', 112, 2, '20190219-131930_5033607617', 'http://38.107.174.248/RECORDINGS/20190219-131930_5033607617-all.wav', 476822268, 3536, 'TEAMFERNANDO', '1550611139.14790288', '', 0, 2, '2019-02-19 08:19:26', 'DNC', 2147483647, 1, 'MAIN', 'new', 0, 'Empty Recording'),
(18, 15531204, 'Local/58600067@default', 38107, 8309, '2019-02-19 08:19:28', '0000-00-00 00:00:00', '2019-02-19 08:23:08', '0000-00-00 00:00:00', 220, 4, '20190220-051927_3172380054', 'http://38.107.174.247/RECORDINGS/20190220-051927_3172380054-all.wav', 476819861, 400123, 'Negros', '1550611140.529821', '', 3001, 2, '2019-02-19 08:19:22', 'RD', 2147483647, 1, 'MAIN', 'new', 0, 'Empty Recording'),
(19, 15531160, 'Local/58600054@default', 38102, 8309, '2019-02-19 08:19:14', '0000-00-00 00:00:00', '2019-02-19 08:20:58', '0000-00-00 00:00:00', 104, 2, '20190219-131913_7206015634', 'http://38.102.225.170/RECORDINGS/20190219-131913_7206015634-all.wav', 476857646, 3657, 'QF', '1550611135.169191', '', 0, 2, '2019-02-19 08:19:10', 'HUP', 2147483647, 1, 'MAIN', 'new', 0, 'Empty Recording'),
(20, 15531138, 'Local/58600068@default', 38102, 8309, '2019-02-19 08:19:10', '0000-00-00 00:00:00', '2019-02-19 08:22:10', '0000-00-00 00:00:00', 180, 3, '20190219-131909_7209825005', 'http://38.102.225.171/RECORDINGS/20190219-131909_7209825005-all.wav', 476861087, 3330, 'TEAMFERNANDO', '1550611122.168897', '', 0, 2, '2019-02-19 08:19:06', 'DNC', 2147483647, 1, 'MAIN', 'new', 0, 'Empty Recording'),
(21, 15531109, 'Local/58600064@default', 38102, 8309, '2019-02-19 08:19:01', '0000-00-00 00:00:00', '2019-02-19 08:23:07', '0000-00-00 00:00:00', 246, 4, '20190219-131900_9543910574', 'http://38.102.225.171/RECORDINGS/20190219-131900_9543910574-all.wav', 476860650, 3202, 'TEAMGRANVILLE', '1550611119.22065774', '', 3001, 2, '2019-02-19 08:18:57', 'TrSuc', 2147483647, 1, 'MAIN', 'new', 0, 'Empty Recording'),
(23, 15531040, 'Local/58600071@default', 38102, 8309, '2019-02-19 08:18:45', '0000-00-00 00:00:00', '2019-02-19 08:21:31', '0000-00-00 00:00:00', 166, 3, '20190219-131844_5718889917', 'http://38.102.225.171/RECORDINGS/20190219-131844_5718889917-all.wav', 476759685, 3051, 'TEAMFERNANDO', '1550611111.529162', '', 0, 2, '2019-02-19 08:18:41', 'DUMP', 2147483647, 1, 'MAIN', 'new', 0, 'Empty Recording'),
(26, 15530949, 'Local/58600062@default', 38107, 8309, '2019-02-19 08:18:24', '0000-00-00 00:00:00', '2019-02-19 08:19:42', '0000-00-00 00:00:00', 78, 1, '20190220-051823_3376933862', 'http://38.107.174.247/RECORDINGS/20190220-051823_3376933862-all.wav', 476840543, 3337, 'Duma', '1550611071.140231', '', 0, 2, '2019-02-19 08:18:20', 'DUMP', 2147483647, 1, 'MAIN', 'new', 0, 'Empty Recording'),
(27, 15530865, 'Local/58600055@default', 38107, 8309, '2019-02-19 08:18:05', '0000-00-00 00:00:00', '2019-02-19 08:19:24', '0000-00-00 00:00:00', 79, 1, '20190219-131804_7577598382', 'http://38.107.174.247/RECORDINGS/20190219-131804_7577598382-all.wav', 476865759, 3670, 'QF', '1550611070.167909', '', 0, 2, '2019-02-19 08:18:02', 'DUMP', 2147483647, 1, 'MAIN', 'new', 0, 'Empty Recording'),
(28, 15530863, 'Local/58600070@default', 38107, 8309, '2019-02-19 08:18:04', '0000-00-00 00:00:00', '2019-02-19 08:20:54', '0000-00-00 00:00:00', 170, 3, '20190219-131803_3023447127', 'http://38.107.174.247/RECORDINGS/20190219-131803_3023447127-all.wav', 476839057, 3131, 'TEAMFERNANDO', '1550611063.528161', '', 0, 2, '2019-02-19 08:18:01', 'TrSuc', 2147483647, 1, 'MAIN', 'new', 0, 'Empty Recording'),
(381, 34734278, 'IAX2/ASTloop-10263', 38102, 2147483647, '2019-02-15 07:59:21', '0000-00-00 00:00:00', '2019-02-16 01:28:22', '0000-00-00 00:00:00', 67, 1, '20190215155921_3610_44355512818363195', 'http://38.102.225.165/RECORDINGS/20190215155921_3610_44355512818363195-all.wav', 608705731, 3610, 'TEAMMERBEN', '1550264361.504192', '', 3001, 1, '2019-02-15 07:57:10', 'RING', 2147483647, 1, 'MAIN', 'new', 0, 'Empty Recording'),
(382, 0, '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, '', '', 0, 0, '', '', '', 0, 0, '0000-00-00 00:00:00', '', 0, 0, '', 'new', 0, 'Empty Recording'),
(383, 0, '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, '', '', 0, 0, '', '', '', 0, 0, '0000-00-00 00:00:00', '', 0, 0, '', 'new', 0, 'Empty Recording'),
(384, 0, '', 0, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, '', '', 0, 0, '', '', '', 0, 0, '0000-00-00 00:00:00', '', 0, 0, '', 'new', 0, 'Empty Recording');

-- --------------------------------------------------------

--
-- Table structure for table `cluster_recordings_meta`
--

CREATE TABLE IF NOT EXISTS `cluster_recordings_meta` (
  `cluster_recordings_meta_id` int(11) NOT NULL AUTO_INCREMENT,
  `cluster_no` int(11) NOT NULL,
  `last_recording_id` int(11) NOT NULL,
  `last_access_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`cluster_recordings_meta_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `cluster_recordings_meta`
--

INSERT INTO `cluster_recordings_meta` (`cluster_recordings_meta_id`, `cluster_no`, `last_recording_id`, `last_access_date`) VALUES
(1, 1, 2, '2019-02-13 00:40:11'),
(2, 2, 2, '2019-02-13 00:40:11'),
(3, 3, 3, '2019-02-13 00:40:11'),
(4, 4, 1120989, '2019-02-13 00:40:11');

-- --------------------------------------------------------

--
-- Table structure for table `config`
--

CREATE TABLE IF NOT EXISTS `config` (
  `config_id` int(11) NOT NULL AUTO_INCREMENT,
  `config_desc` varchar(255) NOT NULL,
  `config_value` varchar(255) NOT NULL,
  PRIMARY KEY (`config_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `config`
--

INSERT INTO `config` (`config_id`, `config_desc`, `config_value`) VALUES
(1, 'cluster1_last_recording_id', ''),
(2, 'cluster2_last_recording_id', ''),
(3, 'cluster3_last_recording_id', ''),
(4, 'cluster4_last_long_recordingid', '1120989'),
(5, 'shortcalls_next_cluster_no', '2'),
(6, 'longcalls_next_cluster_no', '1');

-- --------------------------------------------------------

--
-- Table structure for table `filters_and_data_lists`
--

CREATE TABLE IF NOT EXISTS `filters_and_data_lists` (
  `filters_and_data_lists_id` int(11) NOT NULL AUTO_INCREMENT,
  `filters_and_data_desc` varchar(255) NOT NULL,
  `table_name` varchar(255) NOT NULL,
  PRIMARY KEY (`filters_and_data_lists_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `flagged_calls`
--

CREATE TABLE IF NOT EXISTS `flagged_calls` (
  `flagged_calls_id` int(11) NOT NULL AUTO_INCREMENT,
  `auditor_id` int(11) NOT NULL,
  `script_used_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `agent_name` varchar(255) NOT NULL,
  `client_feedback_id` int(11) NOT NULL,
  `phone_number` int(11) NOT NULL,
  `recording_id` int(11) NOT NULL,
  `customer` varchar(255) NOT NULL,
  `recording_link` varchar(255) NOT NULL,
  `vici_disposition_id` int(11) NOT NULL,
  `webform_disposition_id` int(11) NOT NULL,
  `client_feedback_validation` enum('valid','invalid') NOT NULL,
  `quotefire_comment` varchar(255) NOT NULL,
  `quotefire_priority` int(11) NOT NULL,
  `quotefire_notes_finding` int(11) NOT NULL,
  `sme_id` int(11) NOT NULL,
  `team_lead_id` int(11) NOT NULL,
  `site` enum('OITC','NY','Dumaguete','Etelecare') NOT NULL,
  `lol_ztp` int(11) NOT NULL,
  `qa_comment` varchar(255) NOT NULL,
  `record_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_updated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`flagged_calls_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `flagged_calls`
--

INSERT INTO `flagged_calls` (`flagged_calls_id`, `auditor_id`, `script_used_id`, `user_id`, `agent_name`, `client_feedback_id`, `phone_number`, `recording_id`, `customer`, `recording_link`, `vici_disposition_id`, `webform_disposition_id`, `client_feedback_validation`, `quotefire_comment`, `quotefire_priority`, `quotefire_notes_finding`, `sme_id`, `team_lead_id`, `site`, `lol_ztp`, `qa_comment`, `record_date`, `last_updated`) VALUES
(1, 14, 1, 13, 'sadas', 1, 9123123, 1, 'sadasd', 'sadsa', 1, 1, 'valid', '1', 1, 1, 1, 1, 'OITC', 1, 'dasdsada', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `general_observation_list`
--

CREATE TABLE IF NOT EXISTS `general_observation_list` (
  `general_observation_list_id` int(11) NOT NULL AUTO_INCREMENT,
  `general_observation` varchar(255) NOT NULL,
  PRIMARY KEY (`general_observation_list_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Dumping data for table `general_observation_list`
--

INSERT INTO `general_observation_list` (`general_observation_list_id`, `general_observation`) VALUES
(1, 'N/A'),
(5, 'Call OK'),
(6, 'Agent did not clearly obtain the insurance provider'),
(7, 'Did not obtain proper information'),
(8, 'Did not obtain proper information- Agent did not clearly obtain name and proceeded to the next question'),
(9, 'Did not obtain proper information- Agent did not clearly obtain the insurance provider and proceeded to the next question'),
(10, 'Did not obtain proper information- Agent did not clearly obtain zipcode and proceeded to the next question'),
(11, 'DNC request not honored'),
(12, 'Failed Forced Transfer- Prospect busy/driving however agent pushed through with transfer'),
(13, 'Forced Transfer- Prospect asked to be called back however agent pushed through with transfer'),
(14, 'Forced Transfer- Prospect was not interested and agent pushed through with transfer'),
(15, 'Forced Transfer- Prospect clearly stated he was not interested and was transferred'),
(16, 'Inappropriate Response- Misleading information'),
(17, 'Inappropriate Response- Rebuttals'),
(18, 'Inappropriate Response- Responses to prospect questions'),
(19, 'Incorrect Tagging'),
(20, 'Language Barrier- prospect can answer but can barely be understood and can barely speak english'),
(21, 'No positive response- Agent did not obtain a positive response before the transfer question'),
(22, 'Not Qualified- Prospect was not qualified yet was still transferred'),
(23, 'Prank- Prospect was not serious/prank yet was still transferred'),
(24, 'Prospect Angry- Prospect was angry/yelling yet was still transferred'),
(25, 'Prospect Confused- clearly confused and seems to have no idea what''s going on'),
(26, 'Prospect Confused- Elderly prospect that doesn''t understand the call''s purpose and just answering questions'),
(27, 'Prospect Confused- incoherent/not answering clearly; doesn''t seem to have an idea of the call''s purpose'),
(28, 'Prospect Sarcastic- prospect was sarcastic; agent did not understand the sarcasm and transferred'),
(29, 'Prospect Sarcastic- prospect was sarcastic; agent understood or seemed to understand the sarcasm and transferred'),
(30, 'Script Adherence- Agent deviated from from the script; one or more vital questions'),
(31, 'Script Adherence- Agent deviated from the intro and purpose script'),
(32, 'Script Adherence- Agent deviated from the purpose script'),
(33, 'Script Adherence- Agent skipped questions');

-- --------------------------------------------------------

--
-- Table structure for table `incident_reports`
--

CREATE TABLE IF NOT EXISTS `incident_reports` (
  `incident_reports_id` int(11) NOT NULL AUTO_INCREMENT,
  `source` enum('Flagged Calls','Internal Audit') NOT NULL,
  `audit_id` int(11) NOT NULL,
  `agent_name` varchar(255) NOT NULL,
  `agent_login_id` int(11) NOT NULL,
  `recording_id` int(11) NOT NULL,
  `call_timestamp` timestamp NULL DEFAULT NULL,
  `team_lead_id` int(11) DEFAULT NULL,
  `team_lead` varchar(255) NOT NULL,
  `program_id` int(11) NOT NULL,
  `call_date` date NOT NULL,
  `auditor_id` int(11) NOT NULL,
  `evaluation_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `disposition_id` varchar(11) NOT NULL,
  `phone_number` varchar(255) NOT NULL,
  `offense_type` enum('ZTP','LOL') NOT NULL,
  `offense_id` int(11) NOT NULL,
  `sanction_id` int(11) NOT NULL,
  `call_synopsis` varchar(255) NOT NULL,
  `file_status` enum('pending','approved','pending_disputed','disputed_approved') NOT NULL,
  `last_updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`incident_reports_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `incident_reports`
--

INSERT INTO `incident_reports` (`incident_reports_id`, `source`, `audit_id`, `agent_name`, `agent_login_id`, `recording_id`, `call_timestamp`, `team_lead_id`, `team_lead`, `program_id`, `call_date`, `auditor_id`, `evaluation_date`, `disposition_id`, `phone_number`, `offense_type`, `offense_id`, `sanction_id`, `call_synopsis`, `file_status`, `last_updated`) VALUES
(1, 'Flagged Calls', 0, 'sada', 3002, 1, NULL, 1, '', 1, '2019-01-31', 4, '2019-02-13 16:00:00', '1', '9123132', 'ZTP', 5, 1, 'asdasd', 'pending', '2019-02-14 01:52:35'),
(2, 'Internal Audit', 0, '', 3020, 0, NULL, NULL, '', 0, '0000-00-00', 6, '2019-02-14 01:51:47', '2', '2147483647', 'ZTP', 3, 1, 'asdasdasd', 'pending', '2019-02-14 01:52:36'),
(3, 'Internal Audit', 0, '', 3061, 0, NULL, NULL, '', 0, '0000-00-00', 6, '2019-02-14 01:52:55', '4', '2147483647', '', 11, 9, 'asdasds', 'pending', '2019-02-14 01:52:55'),
(4, 'Internal Audit', 0, '', 3020, 0, NULL, NULL, '', 0, '0000-00-00', 6, '2019-02-14 01:53:29', '12', '2147483647', 'ZTP', 3, 10, 'sadsadsadsa', 'pending', '2019-02-14 01:53:29'),
(5, 'Internal Audit', 0, '', 3020, 0, NULL, NULL, '', 0, '0000-00-00', 6, '2019-02-14 16:47:33', '4', '2147483647', '', 0, 2, 'sdasdsada', 'pending', '2019-02-14 16:47:33'),
(6, 'Internal Audit', 0, '', 3061, 0, NULL, NULL, '', 0, '0000-00-00', 6, '2019-02-14 16:48:41', '', '2147483647', '', 0, 9, 'sadsadsa', 'pending', '2019-02-14 16:48:41'),
(7, 'Internal Audit', 47, '', 3020, 0, NULL, NULL, '', 0, '0000-00-00', 6, '2019-02-15 19:10:09', '', '2147483647', '', 7, 9, 'asdsad', 'pending', '2019-02-15 19:10:09'),
(8, 'Internal Audit', 46, '', 0, 0, NULL, NULL, '', 0, '0000-00-00', 6, '2019-02-15 19:12:48', '', '', '', 2, 9, 'sadsadsada', 'pending', '2019-02-15 19:12:48');

-- --------------------------------------------------------

--
-- Table structure for table `issue_comments`
--

CREATE TABLE IF NOT EXISTS `issue_comments` (
  `issue_comment_id` int(11) NOT NULL AUTO_INCREMENT,
  `issue_comment` varchar(255) NOT NULL,
  PRIMARY KEY (`issue_comment_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `issue_comments`
--

INSERT INTO `issue_comments` (`issue_comment_id`, `issue_comment`) VALUES
(1, 'N/A'),
(2, 'Active Listening'),
(3, 'Active Listening Issue- agent keeps having prospect repeat'),
(4, 'Timing Issue / Delayed Responses'),
(5, 'Delayed Intro'),
(6, 'Inappropriate Response- Comprehension Issue'),
(7, 'Inappropriate Response'),
(8, 'Interruption Issue'),
(9, 'No Rebuttals used'),
(10, 'Call Handling'),
(11, 'Script Mastery'),
(12, 'DNC request not honore'),
(13, 'Call avoidance'),
(14, 'Failed to play Z02'),
(15, 'No proper closing'),
(16, 'Echo between Insurance Agent and Prospect'),
(17, 'Echo between Insurance Agent and Prospect and Agent'),
(18, 'Echo between Agent and Prospect'),
(19, 'Static between Insurance Agent and Prospect'),
(20, 'Static between Insurance Agent and Prospect and Agent'),
(21, 'Static between Agent and Prospect'),
(22, 'Background noise');

-- --------------------------------------------------------

--
-- Table structure for table `lol_mark_list`
--

CREATE TABLE IF NOT EXISTS `lol_mark_list` (
  `lol_mark_list_id` int(11) NOT NULL AUTO_INCREMENT,
  `lol_mark` varchar(255) NOT NULL,
  PRIMARY KEY (`lol_mark_list_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `lol_mark_list`
--

INSERT INTO `lol_mark_list` (`lol_mark_list_id`, `lol_mark`) VALUES
(1, 'Call avoidance'),
(2, 'Escalated the call to the SUP without customer request'),
(3, 'Transferred the call inappropriately'),
(4, 'Did not brand the call'),
(5, 'Inappropriate response'),
(6, 'Unprofessional'),
(7, 'Making disparaging remarks'),
(8, 'Speaking in Vernacular'),
(9, 'Tagging the call incorrectly on the webform'),
(10, 'Tagging the call incorrectly on vicidial'),
(11, 'Submitting webform more than once'),
(12, 'Aggressive Selling'),
(13, 'Script'),
(14, 'Webform Information'),
(15, 'Language Barrier/Confused');

-- --------------------------------------------------------

--
-- Table structure for table `long_call_audit_scripts`
--

CREATE TABLE IF NOT EXISTS `long_call_audit_scripts` (
  `long_call_audit_scripts_id` int(11) NOT NULL AUTO_INCREMENT,
  `audit_id` int(11) NOT NULL,
  `script_lists_id` int(11) NOT NULL,
  `customer_statement` varchar(255) DEFAULT NULL,
  `agent_acknowledgement` enum('Yes','No') DEFAULT NULL,
  `agent_accuracy_response` varchar(255) DEFAULT NULL,
  `agent_accuracy_speed` varchar(255) DEFAULT NULL,
  `agent_accuracy_cor_response` varchar(255) DEFAULT NULL,
  `info_cust_details` varchar(255) DEFAULT NULL,
  `info_agent_input` varchar(255) DEFAULT NULL,
  `info_call_ends` varchar(255) DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`long_call_audit_scripts_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=601 ;

--
-- Dumping data for table `long_call_audit_scripts`
--

INSERT INTO `long_call_audit_scripts` (`long_call_audit_scripts_id`, `audit_id`, `script_lists_id`, `customer_statement`, `agent_acknowledgement`, `agent_accuracy_response`, `agent_accuracy_speed`, `agent_accuracy_cor_response`, `info_cust_details`, `info_agent_input`, `info_call_ends`, `comment`) VALUES
(1, 11, 1, NULL, 'Yes', '-412', NULL, NULL, '', '', '', ''),
(2, 11, 2, NULL, '', NULL, NULL, NULL, '', '', '', ''),
(3, 11, 3, NULL, '', NULL, NULL, NULL, '', '', '', ''),
(4, 11, 4, NULL, '', NULL, NULL, NULL, '', '', '', ''),
(5, 11, 5, NULL, '', NULL, NULL, NULL, '', '', '', ''),
(6, 11, 6, NULL, '', NULL, NULL, NULL, '', '', '', ''),
(7, 11, 7, NULL, '', NULL, NULL, NULL, '', '', '', ''),
(8, 11, 8, NULL, '', NULL, NULL, NULL, '', '', '', ''),
(9, 11, 9, NULL, '', NULL, NULL, NULL, '', '', '', ''),
(10, 11, 10, NULL, '', NULL, NULL, NULL, '', '', '', ''),
(11, 11, 11, NULL, '', NULL, NULL, NULL, '', '', '', ''),
(12, 11, 12, NULL, '', NULL, NULL, NULL, '', '', '', ''),
(13, 11, 13, NULL, '', NULL, NULL, NULL, '', '', '', ''),
(14, 12, 1, '3', 'Yes', '1', NULL, NULL, '', '', '', ''),
(15, 12, 2, '', '', NULL, NULL, NULL, '', '', '', ''),
(16, 12, 3, '', '', NULL, NULL, NULL, '', '', '', ''),
(17, 12, 4, '', '', NULL, NULL, NULL, '', '', '', ''),
(18, 12, 5, '', '', NULL, NULL, NULL, '', '', '', ''),
(19, 12, 6, '', '', NULL, NULL, NULL, '', '', '', ''),
(20, 12, 7, '', '', NULL, NULL, NULL, '', '', '', ''),
(21, 12, 8, '', '', NULL, NULL, NULL, '', '', '', ''),
(22, 12, 9, '', '', NULL, NULL, NULL, '', '', '', ''),
(23, 12, 10, '', '', NULL, NULL, NULL, '', '', '', ''),
(24, 12, 11, '', '', NULL, NULL, NULL, '', '', '', ''),
(25, 12, 12, '', '', NULL, NULL, NULL, '', '', '', ''),
(26, 12, 13, '', '', NULL, NULL, NULL, '', '', '', ''),
(27, 13, 1, '', '', NULL, NULL, NULL, '', '', '', ''),
(28, 13, 2, '', '', NULL, NULL, NULL, '', '', '', ''),
(29, 13, 3, '', '', NULL, NULL, NULL, '', '', '', ''),
(30, 13, 4, '', '', NULL, NULL, NULL, '', '', '', ''),
(31, 13, 5, '', '', NULL, NULL, NULL, '', '', '', ''),
(32, 13, 6, '', '', NULL, NULL, NULL, '', '', '', ''),
(33, 13, 7, '', '', NULL, NULL, NULL, '', '', '', ''),
(34, 13, 8, '', '', NULL, NULL, NULL, '', '', '', ''),
(35, 13, 9, '', '', NULL, NULL, NULL, '', '', '', ''),
(36, 13, 10, '', '', NULL, NULL, NULL, '', '', '', ''),
(37, 13, 11, '', '', NULL, NULL, NULL, '', '', '', ''),
(38, 13, 12, '', '', NULL, NULL, NULL, '', '', '', ''),
(39, 13, 13, '', '', NULL, NULL, NULL, '', '', '', ''),
(40, 13, 14, '', '', NULL, NULL, NULL, '', '', '', ''),
(41, 14, 1, '', '', NULL, NULL, NULL, '', '', '', ''),
(42, 14, 2, '', '', NULL, NULL, NULL, '', '', '', ''),
(43, 14, 3, '', '', NULL, NULL, NULL, '', '', '', ''),
(44, 14, 4, '', '', NULL, NULL, NULL, '', '', '', ''),
(45, 14, 5, '', '', NULL, NULL, NULL, '', '', '', ''),
(46, 14, 6, '', '', NULL, NULL, NULL, '', '', '', ''),
(47, 14, 7, '', '', NULL, NULL, NULL, '', '', '', ''),
(48, 14, 8, '', '', NULL, NULL, NULL, '', '', '', ''),
(49, 14, 9, '', '', NULL, NULL, NULL, '', '', '', ''),
(50, 14, 10, '', '', NULL, NULL, NULL, '', '', '', ''),
(51, 14, 11, '', '', NULL, NULL, NULL, '', '', '', ''),
(52, 14, 12, '', '', NULL, NULL, NULL, '', '', '', ''),
(53, 14, 13, '', '', NULL, NULL, NULL, '', '', '', ''),
(54, 14, 14, '', '', NULL, NULL, NULL, '', '', '', ''),
(55, 15, 1, '', '', NULL, NULL, NULL, '', '', '', ''),
(56, 15, 2, '', '', NULL, NULL, NULL, '', '', '', ''),
(57, 15, 3, '', '', NULL, NULL, NULL, '', '', '', ''),
(58, 15, 4, '', '', NULL, NULL, NULL, '', '', '', ''),
(59, 15, 5, '', '', NULL, NULL, NULL, '', '', '', ''),
(60, 15, 6, '', '', NULL, NULL, NULL, '', '', '', ''),
(61, 15, 7, '', '', NULL, NULL, NULL, '', '', '', ''),
(62, 15, 8, '', '', NULL, NULL, NULL, '', '', '', ''),
(63, 15, 9, '', '', NULL, NULL, NULL, '', '', '', ''),
(64, 15, 10, '', '', NULL, NULL, NULL, '', '', '', ''),
(65, 15, 11, '', '', NULL, NULL, NULL, '', '', '', ''),
(66, 15, 12, '', '', NULL, NULL, NULL, '', '', '', ''),
(67, 15, 13, '', '', NULL, NULL, NULL, '', '', '', ''),
(68, 15, 14, '', '', NULL, NULL, NULL, '', '', '', ''),
(69, 16, 1, '', '', NULL, NULL, NULL, '', '', '', ''),
(70, 16, 2, '', '', NULL, NULL, NULL, '', '', '', ''),
(71, 16, 3, '', '', NULL, NULL, NULL, '', '', '', ''),
(72, 16, 4, '', '', NULL, NULL, NULL, '', '', '', ''),
(73, 16, 5, '', '', NULL, NULL, NULL, '', '', '', ''),
(74, 16, 6, '', '', NULL, NULL, NULL, '', '', '', ''),
(75, 16, 7, '', '', NULL, NULL, NULL, '', '', '', ''),
(76, 16, 8, '', '', NULL, NULL, NULL, '', '', '', ''),
(77, 16, 9, '', '', NULL, NULL, NULL, '', '', '', ''),
(78, 16, 10, '', '', NULL, NULL, NULL, '', '', '', ''),
(79, 16, 11, '', '', NULL, NULL, NULL, '', '', '', ''),
(80, 16, 12, '', '', NULL, NULL, NULL, '', '', '', ''),
(81, 16, 13, '', '', NULL, NULL, NULL, '', '', '', ''),
(82, 16, 14, '', '', NULL, NULL, NULL, '', '', '', ''),
(83, 17, 1, 'asdsa', 'Yes', NULL, NULL, NULL, 'r2', 'r2', 'r2', 'r2'),
(84, 17, 2, '', '', NULL, NULL, NULL, '', '', '', ''),
(85, 17, 3, '', '', NULL, NULL, NULL, '', '', '', ''),
(86, 17, 4, '', '', NULL, NULL, NULL, '', '', '', ''),
(87, 17, 5, '', '', NULL, NULL, NULL, '', '', '', ''),
(88, 17, 6, '', '', NULL, NULL, NULL, '', '', '', ''),
(89, 17, 7, '', '', NULL, NULL, NULL, '', '', '', ''),
(90, 17, 8, '', '', NULL, NULL, NULL, '', '', '', ''),
(91, 17, 9, '', '', NULL, NULL, NULL, '', '', '', ''),
(92, 17, 10, '', '', NULL, NULL, NULL, '', '', '', ''),
(93, 17, 11, '', '', NULL, NULL, NULL, '', '', '', ''),
(94, 17, 12, '', '', NULL, NULL, NULL, '', '', '', ''),
(95, 17, 13, '', '', NULL, NULL, NULL, '', '', '', ''),
(96, 17, 14, '', '', NULL, NULL, NULL, '', '', '', ''),
(97, 18, 1, 'asdsa', 'Yes', NULL, NULL, NULL, 'r2', 'r2', 'r2', 'r2'),
(98, 18, 2, '', '', NULL, NULL, NULL, '', '', '', ''),
(99, 18, 3, '', '', NULL, NULL, NULL, '', '', '', ''),
(100, 18, 4, '', '', NULL, NULL, NULL, '', '', '', ''),
(101, 18, 5, '', '', NULL, NULL, NULL, '', '', '', ''),
(102, 18, 6, '', '', NULL, NULL, NULL, '', '', '', ''),
(103, 18, 7, '', '', NULL, NULL, NULL, '', '', '', ''),
(104, 18, 8, '', '', NULL, NULL, NULL, '', '', '', ''),
(105, 18, 9, '', '', NULL, NULL, NULL, '', '', '', ''),
(106, 18, 10, '', '', NULL, NULL, NULL, '', '', '', ''),
(107, 18, 11, '', '', NULL, NULL, NULL, '', '', '', ''),
(108, 18, 12, '', '', NULL, NULL, NULL, '', '', '', ''),
(109, 18, 13, '', '', NULL, NULL, NULL, '', '', '', ''),
(110, 18, 14, '', '', NULL, NULL, NULL, '', '', '', ''),
(111, 19, 1, '', '', NULL, NULL, NULL, '', '', '', ''),
(112, 19, 2, '', '', NULL, NULL, NULL, '', '', '', ''),
(113, 19, 3, '', '', NULL, NULL, NULL, '', '', '', ''),
(114, 19, 4, '', '', NULL, NULL, NULL, '', '', '', ''),
(115, 19, 5, '', '', NULL, NULL, NULL, '', '', '', ''),
(116, 19, 6, '', '', NULL, NULL, NULL, '', '', '', ''),
(117, 19, 7, '', '', NULL, NULL, NULL, '', '', '', ''),
(118, 19, 8, '', '', NULL, NULL, NULL, '', '', '', ''),
(119, 19, 9, '', '', NULL, NULL, NULL, '', '', '', ''),
(120, 19, 10, '', '', NULL, NULL, NULL, '', '', '', ''),
(121, 19, 11, '', '', NULL, NULL, NULL, '', '', '', ''),
(122, 19, 12, '', '', NULL, NULL, NULL, '', '', '', ''),
(123, 19, 13, '', '', NULL, NULL, NULL, '', '', '', ''),
(124, 19, 14, '', '', NULL, NULL, NULL, '', '', '', ''),
(125, 20, 1, '', '', NULL, NULL, NULL, '', '', '', ''),
(126, 20, 2, '', '', NULL, NULL, NULL, '', '', '', ''),
(127, 20, 3, '', '', NULL, NULL, NULL, '', '', '', ''),
(128, 20, 4, '', '', NULL, NULL, NULL, '', '', '', ''),
(129, 20, 5, '', '', NULL, NULL, NULL, '', '', '', ''),
(130, 20, 6, '', '', NULL, NULL, NULL, '', '', '', ''),
(131, 20, 7, '', '', NULL, NULL, NULL, '', '', '', ''),
(132, 20, 8, '', '', NULL, NULL, NULL, '', '', '', ''),
(133, 20, 9, '', '', NULL, NULL, NULL, '', '', '', ''),
(134, 20, 10, '', '', NULL, NULL, NULL, '', '', '', ''),
(135, 20, 11, '', '', NULL, NULL, NULL, '', '', '', ''),
(136, 20, 12, '', '', NULL, NULL, NULL, '', '', '', ''),
(137, 20, 13, '', '', NULL, NULL, NULL, '', '', '', ''),
(138, 20, 14, '', '', NULL, NULL, NULL, '', '', '', ''),
(139, 21, 1, '', '', NULL, NULL, NULL, '', '', '', ''),
(140, 21, 2, '', '', NULL, NULL, NULL, '', '', '', ''),
(141, 21, 3, '', '', NULL, NULL, NULL, '', '', '', ''),
(142, 21, 4, '', '', NULL, NULL, NULL, '', '', '', ''),
(143, 21, 5, '', '', NULL, NULL, NULL, '', '', '', ''),
(144, 21, 6, '', '', NULL, NULL, NULL, '', '', '', ''),
(145, 21, 7, '', '', NULL, NULL, NULL, '', '', '', ''),
(146, 21, 8, '', '', NULL, NULL, NULL, '', '', '', ''),
(147, 21, 9, '', '', NULL, NULL, NULL, '', '', '', ''),
(148, 21, 10, '', '', NULL, NULL, NULL, '', '', '', ''),
(149, 21, 11, '', '', NULL, NULL, NULL, '', '', '', ''),
(150, 21, 12, '', '', NULL, NULL, NULL, '', '', '', ''),
(151, 21, 13, '', '', NULL, NULL, NULL, '', '', '', ''),
(152, 21, 14, '', '', NULL, NULL, NULL, '', '', '', ''),
(153, 22, 1, '', '', NULL, NULL, NULL, '', '', '', ''),
(154, 22, 2, '', '', NULL, NULL, NULL, '', '', '', ''),
(155, 22, 3, '', '', NULL, NULL, NULL, '', '', '', ''),
(156, 22, 4, '', '', NULL, NULL, NULL, '', '', '', ''),
(157, 22, 5, '', '', NULL, NULL, NULL, '', '', '', ''),
(158, 22, 6, '', '', NULL, NULL, NULL, '', '', '', ''),
(159, 22, 7, '', '', NULL, NULL, NULL, '', '', '', ''),
(160, 22, 8, '', '', NULL, NULL, NULL, '', '', '', ''),
(161, 22, 9, '', '', NULL, NULL, NULL, '', '', '', ''),
(162, 22, 10, '', '', NULL, NULL, NULL, '', '', '', ''),
(163, 22, 11, '', '', NULL, NULL, NULL, '', '', '', ''),
(164, 22, 12, '', '', NULL, NULL, NULL, '', '', '', ''),
(165, 22, 13, '', '', NULL, NULL, NULL, '', '', '', ''),
(166, 22, 14, '', '', NULL, NULL, NULL, '', '', '', ''),
(167, 23, 1, '', '', NULL, NULL, NULL, '', '', '', ''),
(168, 23, 2, '', '', NULL, NULL, NULL, '', '', '', ''),
(169, 23, 3, '', '', NULL, NULL, NULL, '', '', '', ''),
(170, 23, 4, '', '', NULL, NULL, NULL, '', '', '', ''),
(171, 23, 5, '', '', NULL, NULL, NULL, '', '', '', ''),
(172, 23, 6, '', '', NULL, NULL, NULL, '', '', '', ''),
(173, 23, 7, '', '', NULL, NULL, NULL, '', '', '', ''),
(174, 23, 8, '', '', NULL, NULL, NULL, '', '', '', ''),
(175, 23, 9, '', '', NULL, NULL, NULL, '', '', '', ''),
(176, 23, 10, '', '', NULL, NULL, NULL, '', '', '', ''),
(177, 23, 11, '', '', NULL, NULL, NULL, '', '', '', ''),
(178, 23, 12, '', '', NULL, NULL, NULL, '', '', '', ''),
(179, 23, 13, '', '', NULL, NULL, NULL, '', '', '', ''),
(180, 23, 14, '', '', NULL, NULL, NULL, '', '', '', ''),
(181, 24, 1, '', '', NULL, NULL, NULL, '', '', '', ''),
(182, 24, 2, '', '', NULL, NULL, NULL, '', '', '', ''),
(183, 24, 3, '', '', NULL, NULL, NULL, '', '', '', ''),
(184, 24, 4, '', '', NULL, NULL, NULL, '', '', '', ''),
(185, 24, 5, '', '', NULL, NULL, NULL, '', '', '', ''),
(186, 24, 6, '', '', NULL, NULL, NULL, '', '', '', ''),
(187, 24, 7, '', '', NULL, NULL, NULL, '', '', '', ''),
(188, 24, 8, '', '', NULL, NULL, NULL, '', '', '', ''),
(189, 24, 9, '', '', NULL, NULL, NULL, '', '', '', ''),
(190, 24, 10, '', '', NULL, NULL, NULL, '', '', '', ''),
(191, 24, 11, '', '', NULL, NULL, NULL, '', '', '', ''),
(192, 24, 12, '', '', NULL, NULL, NULL, '', '', '', ''),
(193, 24, 13, '', '', NULL, NULL, NULL, '', '', '', ''),
(194, 24, 14, '', '', NULL, NULL, NULL, '', '', '', ''),
(195, 25, 1, 'asdsa', 'Yes', NULL, NULL, NULL, 'dsad', 'asda', 'asda', 'da'),
(196, 25, 2, '', '', NULL, NULL, NULL, '', '', '', ''),
(197, 25, 3, '', '', NULL, NULL, NULL, '', '', '', ''),
(198, 25, 4, '', '', NULL, NULL, NULL, '', '', '', ''),
(199, 25, 5, '', '', NULL, NULL, NULL, '', '', '', ''),
(200, 25, 6, '', '', NULL, NULL, NULL, '', '', '', ''),
(201, 25, 7, '', '', NULL, NULL, NULL, '', '', '', ''),
(202, 25, 8, '', '', NULL, NULL, NULL, '', '', '', ''),
(203, 25, 9, '', '', NULL, NULL, NULL, '', '', '', ''),
(204, 25, 10, '', '', NULL, NULL, NULL, '', '', '', ''),
(205, 25, 11, '', '', NULL, NULL, NULL, '', '', '', ''),
(206, 25, 12, '', '', NULL, NULL, NULL, '', '', '', ''),
(207, 25, 13, '', '', NULL, NULL, NULL, '', '', '', ''),
(208, 25, 14, '', '', NULL, NULL, NULL, '', '', '', ''),
(209, 26, 1, 'sadsad', 'Yes', NULL, NULL, NULL, 'sadsadsad', 'sadsa', 'sadsa', 'da'),
(210, 26, 2, '', '', NULL, NULL, NULL, '', '', '', ''),
(211, 26, 3, '', '', NULL, NULL, NULL, '', '', '', ''),
(212, 26, 4, '', '', NULL, NULL, NULL, '', '', '', ''),
(213, 26, 5, '', '', NULL, NULL, NULL, '', '', '', ''),
(214, 26, 6, '', '', NULL, NULL, NULL, '', '', '', ''),
(215, 26, 7, '', '', NULL, NULL, NULL, '', '', '', ''),
(216, 26, 8, '', '', NULL, NULL, NULL, '', '', '', ''),
(217, 26, 9, '', '', NULL, NULL, NULL, '', '', '', ''),
(218, 26, 10, '', '', NULL, NULL, NULL, '', '', '', ''),
(219, 26, 11, '', '', NULL, NULL, NULL, '', '', '', ''),
(220, 26, 12, '', '', NULL, NULL, NULL, '', '', '', ''),
(221, 26, 13, '', '', NULL, NULL, NULL, '', '', '', ''),
(222, 26, 14, '', '', NULL, NULL, NULL, '', '', '', ''),
(223, 27, 1, '', '', '', NULL, '', '', '', '', ''),
(224, 27, 2, '', '', '', NULL, '', '', '', '', ''),
(225, 27, 3, '', '', '', NULL, '', '', '', '', ''),
(226, 27, 4, '', '', '', NULL, '', '', '', '', ''),
(227, 27, 5, '', '', '', NULL, '', '', '', '', ''),
(228, 27, 6, '', '', '', NULL, '', '', '', '', ''),
(229, 27, 7, '', '', '', NULL, '', '', '', '', ''),
(230, 27, 8, '', '', '', NULL, '', '', '', '', ''),
(231, 27, 9, '', '', '', NULL, '', '', '', '', ''),
(232, 27, 10, '', '', '', NULL, '', '', '', '', ''),
(233, 27, 11, '', '', '', NULL, '', '', '', '', ''),
(234, 27, 12, '', '', '', NULL, '', '', '', '', ''),
(235, 27, 13, '', '', '', NULL, '', '', '', '', ''),
(236, 27, 14, '', '', '', NULL, '', '', '', '', ''),
(237, 28, 1, 'sdasdas', 'Yes', 'r2 ', NULL, '4', 'sa', 'asdas', 'asdas', 'sadsad'),
(238, 28, 2, '', '', '', NULL, '', '', '', '', ''),
(239, 28, 3, '', '', '', NULL, '', '', '', '', ''),
(240, 28, 4, '', '', '', NULL, '', '', '', '', ''),
(241, 28, 5, '', '', '', NULL, '', '', '', '', ''),
(242, 28, 6, '', '', '', NULL, '', '', '', '', ''),
(243, 28, 7, '', '', '', NULL, '', '', '', '', ''),
(244, 28, 8, '', '', '', NULL, '', '', '', '', ''),
(245, 28, 9, '', '', '', NULL, '', '', '', '', ''),
(246, 28, 10, '', '', '', NULL, '', '', '', '', ''),
(247, 28, 11, '', '', '', NULL, '', '', '', '', ''),
(248, 28, 12, '', '', '', NULL, '', '', '', '', ''),
(249, 28, 13, '', '', '', NULL, '', '', '', '', ''),
(250, 28, 14, '', '', '', NULL, '', '', '', '', ''),
(251, 29, 1, 'asd', 'Yes', 'sadsad', NULL, 'sadsa', 'dsadasd', 'sad', 'sad', 'da'),
(252, 29, 2, '', '', '', NULL, '', '', '', '', ''),
(253, 29, 3, '', '', '', NULL, '', '', '', '', ''),
(254, 29, 4, '', '', '', NULL, '', '', '', '', ''),
(255, 29, 5, '', '', '', NULL, '', '', '', '', ''),
(256, 29, 6, '', '', '', NULL, '', '', '', '', ''),
(257, 29, 7, '', '', '', NULL, '', '', '', '', ''),
(258, 29, 8, '', '', '', NULL, '', '', '', '', ''),
(259, 29, 9, '', '', '', NULL, '', '', '', '', ''),
(260, 29, 10, '', '', '', NULL, '', '', '', '', ''),
(261, 29, 11, '', '', '', NULL, '', '', '', '', ''),
(262, 29, 12, '', '', '', NULL, '', '', '', '', ''),
(263, 29, 13, '', '', '', NULL, '', '', '', '', ''),
(264, 29, 14, '', '', '', NULL, '', '', '', '', ''),
(265, 30, 1, 'asdsad', 'Yes', '121', 'asda', 'asd', 'sadsa', 'dsada', 'dsada', 'sadsa'),
(266, 30, 2, 'sasd', '', '12312', 'asda', '', '', 'dsa', 'dsa', 'dsad'),
(267, 30, 3, 'asda', 'Yes', '121', 'asdsa', 'das', 'dsa', 'sad', 'sad', ''),
(268, 30, 4, 'asdas', '', '41213', 'd', 'dsad', 'dsa', 'asdsa', 'asdsa', 'dsasa'),
(269, 30, 5, '', '', '', '', '', '', '', '', ''),
(270, 30, 6, '', '', '', '', '', '', '', '', ''),
(271, 30, 7, '', '', '', '', '', '', '', '', ''),
(272, 30, 8, '', '', '', '', '', '', '', '', ''),
(273, 30, 9, '', '', '', '', '', '', '', '', ''),
(274, 30, 10, '', '', '', '', '', '', '', '', ''),
(275, 30, 11, '', '', '', '', '', '', '', '', ''),
(276, 30, 12, '', '', '', '', '', '', '', '', ''),
(277, 30, 13, '', '', '', '', '', '', '', '', ''),
(278, 30, 14, '', '', '', '', '', '', '', '', ''),
(279, 31, 1, '', '', '', '', '', '', '', '', ''),
(280, 31, 2, '', '', '', '', '', '', '', '', ''),
(281, 31, 3, '', '', '', '', '', '', '', '', ''),
(282, 31, 4, '', '', '', '', '', '', '', '', ''),
(283, 31, 5, '', '', '', '', '', '', '', '', ''),
(284, 31, 6, '', '', '', '', '', '', '', '', ''),
(285, 31, 7, '', '', '', '', '', '', '', '', ''),
(286, 31, 8, '', '', '', '', '', '', '', '', ''),
(287, 31, 9, '', '', '', '', '', '', '', '', ''),
(288, 31, 10, '', '', '', '', '', '', '', '', ''),
(289, 31, 11, '', '', '', '', '', '', '', '', ''),
(290, 31, 12, '', '', '', '', '', '', '', '', ''),
(291, 31, 13, '', '', '', '', '', '', '', '', ''),
(292, 31, 14, '', '', '', '', '', '', '', '', ''),
(293, 41, 1, '', '', '', '', '', '', '', '', ''),
(294, 41, 2, '', '', '', '', '', '', '', '', ''),
(295, 41, 3, '', '', '', '', '', '', '', '', ''),
(296, 41, 4, '', '', '', '', '', '', '', '', ''),
(297, 41, 5, '', '', '', '', '', '', '', '', ''),
(298, 41, 6, '', '', '', '', '', '', '', '', ''),
(299, 41, 7, '', '', '', '', '', '', '', '', ''),
(300, 41, 8, '', '', '', '', '', '', '', '', ''),
(301, 41, 9, '', '', '', '', '', '', '', '', ''),
(302, 41, 10, '', '', '', '', '', '', '', '', ''),
(303, 41, 11, '', '', '', '', '', '', '', '', ''),
(304, 41, 12, '', '', '', '', '', '', '', '', ''),
(305, 41, 13, '', '', '', '', '', '', '', '', ''),
(306, 41, 14, '', '', '', '', '', '', '', '', ''),
(349, 45, 1, '', '', '', '', '', '', '', '', ''),
(350, 45, 2, '', '', '', '', '', '', '', '', ''),
(351, 45, 3, '', '', '', '', '', '', '', '', ''),
(352, 45, 4, '', '', '', '', '', '', '', '', ''),
(353, 45, 5, '', '', '', '', '', '', '', '', ''),
(354, 45, 6, '', '', '', '', '', '', '', '', ''),
(355, 45, 7, '', '', '', '', '', '', '', '', ''),
(356, 45, 8, '', '', '', '', '', '', '', '', ''),
(357, 45, 9, '', '', '', '', '', '', '', '', ''),
(358, 45, 10, '', '', '', '', '', '', '', '', ''),
(359, 45, 11, '', '', '', '', '', '', '', '', ''),
(360, 45, 12, '', '', '', '', '', '', '', '', ''),
(361, 45, 13, '', '', '', '', '', '', '', '', ''),
(362, 45, 14, '', '', '', '', '', '', '', '', ''),
(363, 46, 1, '', '', '', '', '', '', '', '', ''),
(364, 46, 2, '', '', '', '', '', '', '', '', ''),
(365, 46, 3, '', '', '', '', '', '', '', '', ''),
(366, 46, 4, '', '', '', '', '', '', '', '', ''),
(367, 46, 5, '', '', '', '', '', '', '', '', ''),
(368, 46, 6, '', '', '', '', '', '', '', '', ''),
(369, 46, 7, '', '', '', '', '', '', '', '', ''),
(370, 46, 8, '', '', '', '', '', '', '', '', ''),
(371, 46, 9, '', '', '', '', '', '', '', '', ''),
(372, 46, 10, '', '', '', '', '', '', '', '', ''),
(373, 46, 11, '', '', '', '', '', '', '', '', ''),
(374, 46, 12, '', '', '', '', '', '', '', '', ''),
(375, 46, 13, '', '', '', '', '', '', '', '', ''),
(376, 46, 14, '', '', '', '', '', '', '', '', ''),
(377, 47, 1, '', '', '', '', '', '', '', '', ''),
(378, 47, 2, '', '', '', '', '', '', '', '', ''),
(379, 47, 3, '', '', '', '', '', '', '', '', ''),
(380, 47, 4, '', '', '', '', '', '', '', '', ''),
(381, 47, 5, '', '', '', '', '', '', '', '', ''),
(382, 47, 6, '', '', '', '', '', '', '', '', ''),
(383, 47, 7, '', '', '', '', '', '', '', '', ''),
(384, 47, 8, '', '', '', '', '', '', '', '', ''),
(385, 47, 9, '', '', '', '', '', '', '', '', ''),
(386, 47, 10, '', '', '', '', '', '', '', '', ''),
(387, 47, 11, '', '', '', '', '', '', '', '', ''),
(388, 47, 12, '', '', '', '', '', '', '', '', ''),
(389, 47, 13, '', '', '', '', '', '', '', '', ''),
(390, 47, 14, '', '', '', '', '', '', '', '', ''),
(391, 48, 1, 'sdaa', 'Yes', 'r2 ', '3', 'asdsa', 'r2', 'r2', 'r2', ''),
(392, 48, 2, '', '', '', '', '', '', '', '', ''),
(393, 48, 3, '', '', '', '', '', '', '', '', ''),
(394, 48, 4, '', '', '', '', '', '', '', '', ''),
(395, 48, 5, '', '', '', '', '', '', '', '', ''),
(396, 48, 6, '', '', '', '', '', '', '', '', ''),
(397, 48, 7, '', '', '', '', '', '', '', '', ''),
(398, 48, 8, '', '', '', '', '', '', '', '', ''),
(399, 48, 9, '', '', '', '', '', '', '', '', ''),
(400, 48, 10, '', '', '', '', '', '', '', '', ''),
(401, 48, 11, '', '', '', '', '', '', '', '', ''),
(402, 48, 12, '', '', '', '', '', '', '', '', ''),
(403, 48, 13, '', '', '', '', '', '', '', '', ''),
(404, 48, 14, '', '', '', '', '', '', '', '', ''),
(405, 1, 1, 'excuse me!', '', '7', '', '', '', '', '', ''),
(406, 1, 2, 'i dont do it anyway - excuse me? - i dont do it thank you.', 'Yes', 'R7 - 3 - 3', '', '', '', '', '', ''),
(407, 1, 3, '', '', '', '', '', '', '', '', ''),
(408, 1, 4, '', '', '', '', '', '', '', '', ''),
(409, 1, 5, '', '', '', '', '', '', '', '', ''),
(410, 1, 6, '', '', '', '', '', '', '', '', ''),
(411, 1, 7, '', '', '', '', '', '', '', '', ''),
(412, 1, 8, '', '', '', '', '', '', '', '', ''),
(413, 1, 9, '', '', '', '', '', '', '', '', ''),
(414, 1, 10, '', '', '', '', '', '', '', '', ''),
(415, 1, 11, '', '', '', '', '', '', '', '', ''),
(416, 1, 12, '', '', '', '', '', '', '', '', ''),
(417, 1, 13, '', '', '', '', '', '', '', '', ''),
(418, 1, 14, '', '', '', '', '', '', '', '', ''),
(419, 2, 1, '', 'Yes', '1', '', '', '', '', '', ''),
(420, 2, 2, '', '', '', '', '', '89106', '', '', ''),
(421, 2, 3, '', '', '', '', '', 'yes', '', '', ''),
(422, 2, 4, '', '', '', '', '', 'farmers', '', '', ''),
(423, 2, 5, '', '', '', '', '', 'yes', '', '', ''),
(424, 2, 6, '', '', '', '', '', 'yes', '', '', ''),
(425, 2, 7, '', '', '', '', '', 'no dui', '', '', ''),
(426, 2, 8, '', '', '', '', '', 'own', '', '', ''),
(427, 2, 9, 'im pretty happy wtih farmers', 'Yes', 'r6 ', '', 'r6', 'no', '', '', ''),
(428, 2, 10, '', '', '', '', '', '', '', '', ''),
(429, 2, 11, '', '', '', '', '', '', '', '', ''),
(430, 2, 12, '', '', '', '', '', '', '', '', ''),
(431, 2, 13, '', '', '', '', '', '', '', '', ''),
(432, 2, 14, '', '', '', '', '', '', '', '', ''),
(433, 3, 1, 'what do you mean my car insurance payment?', 'Yes', '7', '', '', '', '', '', ''),
(434, 3, 2, 'i just got insurance through some body else.', 'Yes', 'r3', '', '', '80657', '', '', ''),
(435, 3, 3, '', '', '', '', '', '', '', '', ''),
(436, 3, 4, '', '', '', '', '', '', '', '', ''),
(437, 3, 5, '', '', '', '', '', '', '', '', ''),
(438, 3, 6, '', '', '', '', '', '', '', '', ''),
(439, 3, 7, '', '', '', '', '', '', '', '', ''),
(440, 3, 8, '', '', '', '', '', '', '', '', ''),
(441, 3, 9, '', '', '', '', '', '', '', '', ''),
(442, 3, 10, '', '', '', '', '', '', '', '', ''),
(443, 3, 11, '', '', '', '', '', '', '', '', ''),
(444, 3, 12, '', '', '', '', '', '', '', '', ''),
(445, 3, 13, '', '', '', '', '', '', '', '', ''),
(446, 3, 14, '', '', '', '', '', '', '', '', ''),
(447, 4, 1, '', 'Yes', '1', '', '', '', '', '', ''),
(448, 4, 2, 'i already have state farm - i already have stae farm', 'Yes', 'r3 - X', '', 'r3 - r7', '', '', '', ''),
(449, 4, 3, '', '', '', '', '', '', '', '', ''),
(450, 4, 4, '', '', '', '', '', '', '', '', ''),
(451, 4, 5, '', '', '', '', '', '', '', '', ''),
(452, 4, 6, '', '', '', '', '', '', '', '', ''),
(453, 4, 7, '', '', '', '', '', '', '', '', ''),
(454, 4, 8, '', '', '', '', '', '', '', '', ''),
(455, 4, 9, '', '', '', '', '', '', '', '', ''),
(456, 4, 10, '', '', '', '', '', '', '', '', ''),
(457, 4, 11, '', '', '', '', '', '', '', '', ''),
(458, 4, 12, '', '', '', '', '', '', '', '', ''),
(459, 4, 13, '', '', '', '', '', '', '', '', ''),
(460, 4, 14, '', '', '', '', '', '', '', '', ''),
(461, 5, 1, '', 'Yes', '7', '', '', '', '', '', ''),
(462, 5, 2, 'your trying to sell me insurance? - s what are you doing? - no', 'Yes', 'Q - 3', '', 'r1 - r7', '', '', '', ''),
(463, 5, 3, '', '', '', '', '', '', '', '', ''),
(464, 5, 4, '', '', '', '', '', '', '', '', ''),
(465, 5, 5, '', '', '', '', '', '', '', '', ''),
(466, 5, 6, '', '', '', '', '', '', '', '', ''),
(467, 5, 7, '', '', '', '', '', '', '', '', ''),
(468, 5, 8, '', '', '', '', '', '', '', '', ''),
(469, 5, 9, '', '', '', '', '', '', '', '', ''),
(470, 5, 10, '', '', '', '', '', '', '', '', ''),
(471, 5, 11, '', '', '', '', '', '', '', '', ''),
(472, 5, 12, '', '', '', '', '', '', '', '', ''),
(473, 5, 13, '', '', '', '', '', '', '', '', ''),
(474, 5, 14, '', '', '', '', '', '', '', '', ''),
(475, 6, 1, '', 'Yes', '1', '', '', '', '', '', ''),
(476, 6, 2, 'what insurance? what car?', 'Yes', '3', '', '', '', '', '', ''),
(477, 6, 3, '', '', '', '', '', '', '', '', ''),
(478, 6, 4, '', '', '', '', '', '', '', '', ''),
(479, 6, 5, '', '', '', '', '', '', '', '', ''),
(480, 6, 6, '', '', '', '', '', '', '', '', ''),
(481, 6, 7, '', '', '', '', '', '', '', '', ''),
(482, 6, 8, '', '', '', '', '', '', '', '', ''),
(483, 6, 9, '', '', '', '', '', '', '', '', ''),
(484, 6, 10, '', '', '', '', '', '', '', '', ''),
(485, 6, 11, '', '', '', '', '', '', '', '', ''),
(486, 6, 12, '', '', '', '', '', '', '', '', ''),
(487, 6, 13, '', '', '', '', '', '', '', '', ''),
(488, 6, 14, '', '', '', '', '', '', '', '', ''),
(489, 7, 1, '', 'Yes', '1', '', '', '', '', '', ''),
(490, 7, 2, '', '', '', '', '', '71108', '', '', ''),
(491, 7, 3, 'but im not driving', 'No', 'X', '', '', 'yes', '', '', ''),
(492, 7, 4, '', '', '', '', '', '', '', '', ''),
(493, 7, 5, '', '', '', '', '', '', '', '', ''),
(494, 7, 6, '', '', '', '', '', '', '', '', ''),
(495, 7, 7, '', '', '', '', '', '', '', '', ''),
(496, 7, 8, '', '', '', '', '', '', '', '', ''),
(497, 7, 9, '', '', '', '', '', '', '', '', ''),
(498, 7, 10, '', '', '', '', '', '', '', '', ''),
(499, 7, 11, '', '', '', '', '', '', '', '', ''),
(500, 7, 12, '', '', '', '', '', '', '', '', ''),
(501, 7, 13, '', '', '', '', '', '', '', '', ''),
(502, 7, 14, '', '', '', '', '', '', '', '', ''),
(503, 8, 1, 'what car?', 'Yes', '7', '', '', '', '', '', ''),
(504, 8, 2, 'i dont do that. my wife does - no - i have insurance ', 'Yes', '{ - r4 -r6', '', '', '', '', '', ''),
(505, 8, 3, '', '', '', '', '', '', '', '', ''),
(506, 8, 4, '', '', '', '', '', '', '', '', ''),
(507, 8, 5, '', '', '', '', '', '', '', '', ''),
(508, 8, 6, '', '', '', '', '', '', '', '', ''),
(509, 8, 7, '', '', '', '', '', '', '', '', ''),
(510, 8, 8, '', '', '', '', '', '', '', '', ''),
(511, 8, 9, '', '', '', '', '', '', '', '', ''),
(512, 8, 10, '', '', '', '', '', '', '', '', ''),
(513, 8, 11, '', '', '', '', '', '', '', '', ''),
(514, 8, 12, '', '', '', '', '', '', '', '', ''),
(515, 8, 13, '', '', '', '', '', '', '', '', ''),
(516, 8, 14, '', '', '', '', '', '', '', '', ''),
(517, 9, 1, '', 'Yes', '1', '', '', '', '', '', ''),
(518, 9, 2, '', '', '', '', '', '45140', '', '', ''),
(519, 9, 3, '', '', '', '', '', 'yes', '', '', ''),
(520, 9, 4, '', '', '', '', '', 'statefarm', '', '', ''),
(521, 9, 5, '', '', '', '', '', 'yes', '', '', ''),
(522, 9, 6, '', '', '', '', '', 'yes', '', '', ''),
(523, 9, 7, '', '', '', '', '', 'no dui', '', '', ''),
(524, 9, 8, '', '', '', '', '', 'rent', '', '', ''),
(525, 9, 9, '', '', '', '', '', '', '', '', ''),
(526, 9, 10, '', '', '', '', '', '1', '', '', ''),
(527, 9, 11, 'you dont have my  first name? - no thnaks im not interested', 'Yes', 'r2', '', '', '', '', '', ''),
(528, 9, 12, '', '', '', '', '', '', '', '', ''),
(529, 9, 13, '', '', '', '', '', '', '', '', ''),
(530, 9, 14, '', '', '', '', '', '', '', '', ''),
(531, 10, 1, '', 'Yes', '1', '', '', '', '', '', ''),
(532, 10, 2, 'i have farmers now - i said i have farmers - i sadi i have farmers now', 'Yes', 'r3 - r7 - X', '', '', '', '', '', ''),
(533, 10, 3, '', '', '', '', '', '', '', '', ''),
(534, 10, 4, '', '', '', '', '', '', '', '', ''),
(535, 10, 5, '', '', '', '', '', '', '', '', ''),
(536, 10, 6, '', '', '', '', '', '', '', '', ''),
(537, 10, 7, '', '', '', '', '', '', '', '', ''),
(538, 10, 8, '', '', '', '', '', '', '', '', ''),
(539, 10, 9, '', '', '', '', '', '', '', '', ''),
(540, 10, 10, '', '', '', '', '', '', '', '', ''),
(541, 10, 11, '', '', '', '', '', '', '', '', ''),
(542, 10, 12, '', '', '', '', '', '', '', '', ''),
(543, 10, 13, '', '', '', '', '', '', '', '', ''),
(544, 10, 14, '', '', '', '', '', '', '', '', ''),
(545, 11, 1, 'i dont have any car insurance', 'Yes', '7', '', '', '', '', '', ''),
(546, 11, 2, 'i work with statefarm - i going to go right now.', 'Yes', 'r3 - 2', '', '', '', '', '', ''),
(547, 11, 3, '', '', '', '', '', '', '', '', ''),
(548, 11, 4, '', '', '', '', '', '', '', '', ''),
(549, 11, 5, '', '', '', '', '', '', '', '', ''),
(550, 11, 6, '', '', '', '', '', '', '', '', ''),
(551, 11, 7, '', '', '', '', '', '', '', '', ''),
(552, 11, 8, '', '', '', '', '', '', '', '', ''),
(553, 11, 9, '', '', '', '', '', '', '', '', ''),
(554, 11, 10, '', '', '', '', '', '', '', '', ''),
(555, 11, 11, '', '', '', '', '', '', '', '', ''),
(556, 11, 12, '', '', '', '', '', '', '', '', ''),
(557, 11, 13, '', '', '', '', '', '', '', '', ''),
(558, 11, 14, '', '', '', '', '', '', '', '', ''),
(559, 12, 1, 'which insurance are you - ahm which insurance are you?', 'Yes', '3 - F ,7', '', '', '', '', '', ''),
(560, 12, 2, 'i dont have any insurance with you - im not interested in this call', 'Yes', 'r7 - x', '', '', '', '', '', ''),
(561, 12, 3, '', '', '', '', '', '', '', '', ''),
(562, 12, 4, '', '', '', '', '', '', '', '', ''),
(563, 12, 5, '', '', '', '', '', '', '', '', ''),
(564, 12, 6, '', '', '', '', '', '', '', '', ''),
(565, 12, 7, '', '', '', '', '', '', '', '', ''),
(566, 12, 8, '', '', '', '', '', '', '', '', ''),
(567, 12, 9, '', '', '', '', '', '', '', '', ''),
(568, 12, 10, '', '', '', '', '', '', '', '', ''),
(569, 12, 11, '', '', '', '', '', '', '', '', ''),
(570, 12, 12, '', '', '', '', '', '', '', '', ''),
(571, 12, 13, '', '', '', '', '', '', '', '', ''),
(572, 12, 14, '', '', '', '', '', '', '', '', ''),
(573, 13, 1, 'your calling about my what?', 'Yes', '7', '', '', '', '', '', ''),
(574, 13, 2, 'im happy with my insurance - no maam i dont htink right now.', 'Yes', 'r3 - x', '', '', '', '', '', ''),
(575, 13, 3, '', '', '', '', '', '', '', '', ''),
(576, 13, 4, '', '', '', '', '', '', '', '', ''),
(577, 13, 5, '', '', '', '', '', '', '', '', ''),
(578, 13, 6, '', '', '', '', '', '', '', '', ''),
(579, 13, 7, '', '', '', '', '', '', '', '', ''),
(580, 13, 8, '', '', '', '', '', '', '', '', ''),
(581, 13, 9, '', '', '', '', '', '', '', '', ''),
(582, 13, 10, '', '', '', '', '', '', '', '', ''),
(583, 13, 11, '', '', '', '', '', '', '', '', ''),
(584, 13, 12, '', '', '', '', '', '', '', '', ''),
(585, 13, 13, '', '', '', '', '', '', '', '', ''),
(586, 13, 14, '', '', '', '', '', '', '', '', ''),
(587, 14, 1, '', 'Yes', '1', '', '', '', '', '', ''),
(588, 14, 2, 'its not of your business', '', '', '', '', '', '', '', ''),
(589, 14, 3, '', '', '', '', '', '', '', '', ''),
(590, 14, 4, '', '', '', '', '', '', '', '', ''),
(591, 14, 5, '', '', '', '', '', '', '', '', ''),
(592, 14, 6, '', '', '', '', '', '', '', '', ''),
(593, 14, 7, '', '', '', '', '', '', '', '', ''),
(594, 14, 8, '', '', '', '', '', '', '', '', ''),
(595, 14, 9, '', '', '', '', '', '', '', '', ''),
(596, 14, 10, '', '', '', '', '', '', '', '', ''),
(597, 14, 11, '', '', '', '', '', '', '', '', ''),
(598, 14, 12, '', '', '', '', '', '', '', '', ''),
(599, 14, 13, '', '', '', '', '', '', '', '', ''),
(600, 14, 14, '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `nav_menus`
--

CREATE TABLE IF NOT EXISTS `nav_menus` (
  `nav_menus_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_parent` enum('True','False') DEFAULT NULL,
  `fa_icon` varchar(255) NOT NULL,
  `status` enum('Active','Inactive') NOT NULL,
  PRIMARY KEY (`nav_menus_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `nav_menus`
--

INSERT INTO `nav_menus` (`nav_menus_id`, `title`, `url`, `parent_id`, `is_parent`, `fa_icon`, `status`) VALUES
(1, 'Dashboard', 'Dashboard', NULL, 'True', 'dashboard', 'Active'),
(2, 'Users', 'Users/list_users', NULL, 'True', 'users', 'Active'),
(3, 'List Users', 'Users/list_users', 2, '', 'list', 'Active'),
(4, 'Add User', 'Users/add_user', 2, '', 'plus', 'Active'),
(5, 'Rosters', 'Rosters/list_rosters', NULL, 'True', 'headphones', 'Active'),
(6, 'Rosters', 'Rosters/list_rosters', 5, NULL, 'list', 'Active'),
(8, 'Team Lead List', 'Rosters/list_teamleads', 5, NULL, 'list', 'Active'),
(9, 'Floor Support List', 'Rosters/list_fs', 5, NULL, 'list', 'Active'),
(10, 'Agents List', 'Rosters/list_agents', 5, NULL, 'list', 'Active'),
(11, 'Recordings', '', NULL, 'True', 'file-audio-o', 'Active'),
(12, 'Long Call Form Recordings', 'Recording/longcall_recording_list', 11, 'False', 'file-audio-o', 'Active'),
(13, 'Audit Lists', 'Audit/audit_list', NULL, 'True', 'check-square-o', 'Active'),
(14, 'Incident Reports ', 'Incident_Report/incident_reports', NULL, 'True', 'bell', 'Active'),
(15, 'Flagged Calls', 'Flagged_Call/flagged_calls', NULL, 'True', 'flag', 'Active'),
(16, 'Reports', '', NULL, 'True', 'line-chart', 'Active'),
(17, 'Q.A. Results', 'Report/qa_results_summary', 16, 'True', 'bar-chart', 'Active'),
(18, 'Filters and Data', 'FilterData/list_filter_data_list', 0, 'True', 'gear', 'Active'),
(19, 'All Filter and Data', 'FilterData/list_filter_data_list', 17, 'False', 'list', 'Active'),
(20, 'Add Filter and Data ', 'FilterData/add_filter_data_list', 17, 'False', 'plist', 'Active'),
(22, 'Short Call Recordings', 'Recording/shortcall_recording_list', 11, 'False', 'file-audio-o', 'Active'),
(23, 'Quality Production Report', 'Report/quality_production_report', 16, 'False', 'area-chart', 'Active'),
(24, 'QA Team', 'Rosters/list_qa', 5, 'False', 'list', 'Active'),
(25, 'Wave List', 'Rosters/list_wave', 5, 'False', 'list', 'Active'),
(26, 'Search Recordings (Flagged Calls)', 'Flagged_Call/flagged_calls_search_form', 11, 'False', 'search', 'Active'),
(27, 'Search Recordings (Agents Specific )', 'Recording/agents_search', 11, 'False', 'users', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `nav_menus_privilege`
--

CREATE TABLE IF NOT EXISTS `nav_menus_privilege` (
  `nav_menu_privilege_id` int(11) NOT NULL AUTO_INCREMENT,
  `nav_menu_id` int(11) NOT NULL,
  `user_role_id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`nav_menu_privilege_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `nav_menus_privilege`
--

INSERT INTO `nav_menus_privilege` (`nav_menu_privilege_id`, `nav_menu_id`, `user_role_id`, `timestamp`) VALUES
(1, 1, 1, '2019-02-06 19:22:13'),
(2, 1, 2, '2019-02-06 19:22:13'),
(3, 1, 3, '2019-02-06 19:22:13'),
(4, 1, 4, '2019-02-06 19:22:13'),
(5, 1, 5, '2019-02-06 19:22:13'),
(6, 1, 6, '2019-02-06 19:22:13'),
(7, 1, 7, '2019-02-06 19:22:13'),
(8, 1, 8, '2019-02-06 19:22:13'),
(9, 1, 9, '2019-02-06 19:22:13'),
(10, 1, 10, '2019-02-06 19:22:13'),
(11, 1, 11, '2019-02-06 19:22:13'),
(12, 1, 12, '2019-02-06 19:22:13'),
(13, 1, 13, '2019-02-06 19:22:13'),
(14, 1, 14, '2019-02-06 19:22:13'),
(15, 1, 15, '2019-02-06 19:22:13'),
(16, 1, 16, '2019-02-06 19:22:13'),
(17, 1, 17, '2019-02-06 19:22:13'),
(18, 1, 18, '2019-02-06 19:22:13'),
(19, 1, 19, '2019-02-06 19:22:13'),
(20, 2, 1, '2019-02-06 19:23:57'),
(21, 2, 11, '2019-02-06 19:23:57'),
(22, 2, 12, '2019-02-06 19:23:57'),
(23, 2, 13, '2019-02-06 19:23:57');

-- --------------------------------------------------------

--
-- Table structure for table `position_list`
--

CREATE TABLE IF NOT EXISTS `position_list` (
  `position_list_id` int(11) NOT NULL AUTO_INCREMENT,
  `position_desc` varchar(255) NOT NULL,
  `position_dept_id` int(11) NOT NULL,
  PRIMARY KEY (`position_list_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `position_list`
--

INSERT INTO `position_list` (`position_list_id`, `position_desc`, `position_dept_id`) VALUES
(1, 'Managing Director', 0),
(2, 'CFO', 0),
(3, 'Team Leader', 0),
(4, 'Operations  Manager', 0),
(5, 'Report Specialist', 0),
(6, 'IT Specialist', 0),
(7, 'IT Manager', 0),
(8, 'IT Supervisor', 0),
(9, 'IT Programmer', 0),
(10, 'HR Manager', 0),
(11, 'Facilities and Network', 0),
(12, 'PR and Admin Officer', 0),
(13, 'Personal Assistant', 0),
(14, 'Recruitment Officer', 0);

-- --------------------------------------------------------

--
-- Table structure for table `program_list`
--

CREATE TABLE IF NOT EXISTS `program_list` (
  `program_list_id` int(11) NOT NULL AUTO_INCREMENT,
  `program_name` varchar(255) NOT NULL,
  PRIMARY KEY (`program_list_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `program_list`
--

INSERT INTO `program_list` (`program_list_id`, `program_name`) VALUES
(1, 'Insurance - NY'),
(2, 'Insurance - OITC'),
(3, 'Insurance - Duma'),
(4, 'Insurance - Etel'),
(5, 'EDU/Lyft');

-- --------------------------------------------------------

--
-- Table structure for table `qa_team_emails`
--

CREATE TABLE IF NOT EXISTS `qa_team_emails` (
  `qa_team_emails_id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY (`qa_team_emails_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `quotefire`
--

CREATE TABLE IF NOT EXISTS `quotefire` (
  `quotefire_id` int(11) NOT NULL AUTO_INCREMENT,
  `quotefire_desc` varchar(255) NOT NULL,
  `quotefire_type` enum('unsuccessful','not_interested','others') NOT NULL,
  PRIMARY KEY (`quotefire_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=24 ;

--
-- Dumping data for table `quotefire`
--

INSERT INTO `quotefire` (`quotefire_id`, `quotefire_desc`, `quotefire_type`) VALUES
(1, 'Does not want to reveal information', 'unsuccessful'),
(2, 'hung up during call back', 'unsuccessful'),
(3, 'no answer', 'unsuccessful'),
(4, 'voicemail reached', 'unsuccessful'),
(5, 'callback rescheduled', 'unsuccessful'),
(6, 'unspecific', 'unsuccessful'),
(7, 'schedule call back', 'unsuccessful'),
(9, 'Not Interested ', 'not_interested'),
(10, 'Loyal to Company', 'not_interested'),
(11, 'Unspecific', 'not_interested'),
(12, 'Price of Quote', 'not_interested'),
(13, 'Coverage of Policy', 'not_interested'),
(14, 'E- mail request for review', 'not_interested'),
(15, 'Change of Company unaccepted', 'not_interested'),
(16, 'unqualified', 'others'),
(17, 'busy', 'others'),
(18, 'confused', 'others'),
(19, 'lang barrier', 'others'),
(20, 'DNC', 'others'),
(21, 'prank', 'others'),
(22, 'arranged call back', 'others'),
(23, 'Didn''t want to be transfer', 'others');

-- --------------------------------------------------------

--
-- Table structure for table `recording_archive`
--

CREATE TABLE IF NOT EXISTS `recording_archive` (
  `recording_id` int(11) NOT NULL,
  `channel` varchar(255) NOT NULL,
  `server_ip` int(11) NOT NULL,
  `extension` int(11) NOT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `start_epoch` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_epoch` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `length_in_sec` int(11) NOT NULL,
  `length_in_min` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `lead_id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `user_group` varchar(255) NOT NULL,
  `vicidial_id` varchar(255) NOT NULL,
  `unique_id` varchar(255) NOT NULL,
  `list_id` int(11) NOT NULL,
  `call_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `status` varchar(255) NOT NULL,
  `phone_number` int(11) NOT NULL,
  `called_count` int(11) NOT NULL,
  `alt_dial` varchar(255) NOT NULL,
  `recording_log_status` enum('new','flagged','audited','archived') NOT NULL DEFAULT 'new',
  `reason_archive` varchar(255) NOT NULL,
  PRIMARY KEY (`recording_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rosters`
--

CREATE TABLE IF NOT EXISTS `rosters` (
  `roster_login_id` int(11) NOT NULL,
  `agent_id` int(11) NOT NULL,
  `team_lead_id` int(11) NOT NULL,
  `supervisor_id` int(11) NOT NULL,
  `wave_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `rosters_employee`
--

CREATE TABLE IF NOT EXISTS `rosters_employee` (
  `employee_id` int(11) NOT NULL AUTO_INCREMENT,
  `roster_login1_id` int(11) NOT NULL,
  `roster_login2_id` int(11) NOT NULL,
  PRIMARY KEY (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roster_raw`
--

CREATE TABLE IF NOT EXISTS `roster_raw` (
  `login` int(11) NOT NULL,
  `emp_id` varchar(255) NOT NULL,
  `agent` varchar(255) NOT NULL,
  `tl` varchar(255) NOT NULL,
  `fs` varchar(255) NOT NULL,
  `wave` varchar(255) NOT NULL,
  `qa` varchar(255) NOT NULL,
  `agent_cluster_no` int(11) NOT NULL,
  `site_id` int(11) NOT NULL,
  PRIMARY KEY (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roster_raw`
--

INSERT INTO `roster_raw` (`login`, `emp_id`, `agent`, `tl`, `fs`, `wave`, `qa`, `agent_cluster_no`, `site_id`) VALUES
(203, '0', 'Aves, John', 'QA', '', 'QA', '', 0, 0),
(3001, '0', 'Amodia, Rommel', 'Steve', 'Granville', 'SB Lead Gen', 'Francis', 0, 0),
(3002, '0', 'Amodia, Rommel', 'Steve', 'Granville', 'PRO', 'Francis', 0, 0),
(3010, '0', 'Galenzoga, Lester', 'QA', '', '', '', 0, 0),
(3011, '0', 'Baterna, Merben', 'Steve', 'Merben', 'Oakridge', 'Francis', 0, 0),
(3012, '0', 'Baterna, Merben', 'Steve', 'Merben', 'Oakridge', 'Francis', 0, 0),
(3013, '0', 'Labrado, Joey Boy', 'Anthony', 'Edbonson & Norwin', 'Oakridge', 'Lester', 0, 0),
(3014, '0', 'Labrado, Joey Boy', 'Anthony', 'Edbonson & Norwin', 'Oakridge', 'Lester', 0, 0),
(3015, '0', 'Sumaya, Ronald', 'Miao', 'Miao', 'SB Lead Gen', 'Ish', 0, 0),
(3016, '0', 'Sumaya, Ronald', 'Miao', 'Miao', 'PRO', 'Ish', 0, 0),
(3017, '0', 'Labitad, Francis', 'QA', '', 'QA', '', 0, 0),
(3019, '0', 'Achumbre, Ghian', 'Steve', 'Vivian Omega', 'Oakridge', 'Francis', 0, 0),
(3020, '0', 'Achumbre, Ghian', 'Steve', 'Vivian Omega', 'Oakridge', 'Francis', 0, 0),
(3021, '0', 'Del Castillo, Regine', 'Steve', 'Merben', 'SB Lead Gen', 'Francis', 0, 0),
(3022, '0', 'Del Castillo, Regine', 'Steve', 'Merben', 'PRO', 'Francis', 0, 0),
(3023, '0', 'Manlucob, Joshua', 'Steve', 'Granville', 'SB Lead Gen', 'Francis', 0, 0),
(3024, '0', 'Manlucob, Joshua', 'Steve', 'Granville', 'PRO', 'Francis', 0, 0),
(3027, '0', 'Najarro, Hansel Andrew', 'Anthony', 'Monette Cadungog', '', 'Lester', 0, 0),
(3028, '0', 'Najarro, Hansel Andrew', 'Anthony', 'Monette Cadungog', '', 'Lester', 0, 0),
(3031, '0', 'Pacana, Pete', 'Anthony', 'Edbonson & Norwin', 'SB Lead Gen', 'Lester', 0, 0),
(3032, '0', 'Pacana, Pete', 'Anthony', 'Edbonson & Norwin', 'PRO', 'Lester', 0, 0),
(3037, '0', 'Tonacao, Norwin', 'Anthony', 'Edbonson & Norwin', 'Oakridge', 'Lester', 0, 0),
(3038, '0', 'Tonacao, Norwin', 'Anthony', 'Edbonson & Norwin', 'Oakridge', 'Lester', 0, 0),
(3039, '0', 'Vargas, Isaiah', 'Miao', 'Miao', 'SB Lead Gen', 'Ish', 0, 0),
(3040, '0', 'Vargas, Isaiah', 'Miao', 'Miao', 'PRO', 'Ish', 0, 0),
(3041, '0', 'Boa, Belen', 'Miao', 'Platon', '', 'Ish', 0, 0),
(3042, '0', 'Boa, Belen', 'Miao', 'Platon', '', 'Ish', 0, 0),
(3045, '0', 'Jolbot, Mariel', 'Miao', 'Miao', 'SB Lead Gen', 'Ish', 0, 0),
(3046, '0', 'Jolbot, Mariel', 'Miao', 'Miao', 'PRO', 'Ish', 0, 0),
(3047, '0', 'Magtagad, Antonio', 'Steve', 'Granville', 'Oakridge', 'Francis', 0, 0),
(3048, '0', 'Magtagad, Antonio', 'Steve', 'Granville', 'Oakridge', 'Francis', 0, 0),
(3049, '0', 'Ogtip, Granville', 'Steve', 'Granville', 'SB Lead Gen', 'Francis', 0, 0),
(3050, '0', 'Ogtip, Granville', 'Steve', 'Granville', 'PRO', 'Francis', 0, 0),
(3051, '0', 'Palantang, Jason', 'Miao', 'Miao', 'SB Lead Gen', 'Ish', 0, 0),
(3052, '0', 'Palantang, Jason', 'Miao', 'Miao', 'PRO', 'Ish', 0, 0),
(3053, '0', 'Cui, John Ahlexis', 'Anthony', 'Ezron & Jacky', 'SB Lead Gen', 'Lester', 0, 0),
(3054, '0', 'Cui, John Ahlexis', 'Anthony', 'Ezron & Jacky', 'PRO', 'Lester', 0, 0),
(3055, '0', 'Cristoria, Christian Darius', 'Steve', 'Granville', 'Oakridge', 'Francis', 0, 0),
(3056, '0', 'Cristoria, Christian Darius', 'Steve', 'Granville', 'Oakridge', 'Francis', 0, 0),
(3057, '0', 'Odango, Bush Carl', 'Steve', 'Merben', 'SB Lead Gen', 'Francis', 0, 0),
(3058, '0', 'Odango, Bush Carl', 'Steve', 'Merben', 'PRO', 'Francis', 0, 0),
(3059, '0', 'Tariao, Roel Jr', 'Anthony', 'Edbonson & Norwin', 'Oakridge', 'Lester', 0, 0),
(3060, '0', 'Tariao, Roel Jr', 'Anthony', 'Edbonson & Norwin', 'Oakridge', 'Lester', 0, 0),
(3061, '0', 'Delantar, Nina Mae', 'Anthony', 'Monette Cadungog', 'SB Lead Gen', 'Lester', 0, 0),
(3062, '0', 'Delantar, Nina Mae', 'Anthony', 'Monette Cadungog', 'PRO', 'Lester', 0, 0),
(3079, '0', 'Sato, Jomar', 'Anthony', 'Monette Cadungog', 'SB Lead Gen', 'Lester', 0, 0),
(3080, '0', 'Sato, Jomar', 'Anthony', 'Monette Cadungog', 'PRO', 'Lester', 0, 0),
(3081, '0', 'Bungie, Manuel', 'Steve', 'Merben', 'Oakridge', 'Francis', 0, 0),
(3082, '0', 'Bungie, Manuel', 'Steve', 'Merben', 'Oakridge', 'Francis', 0, 0),
(3083, '0', 'Sulpico, Charisse', 'Miao', 'Miao', 'SB Lead Gen', 'Ish', 0, 0),
(3084, '0', 'Sulpico, Charisse', 'Miao', 'Miao', 'PRO', 'Ish', 0, 0),
(3085, '0', 'Macalua, Jenalyn', 'Miao', 'Miao', 'SB Lead Gen', 'Ish', 0, 0),
(3086, '0', 'Macalua, Jenalyn', 'Miao', 'Miao', 'PRO', 'Ish', 0, 0),
(3091, '0', 'Costanilla, Anna Angelica', 'Miao', 'Platon', '', 'Ish', 0, 0),
(3093, '0', 'Cruz, Analito', 'Anthony', 'Edbonson & Norwin', 'Oakridge', 'Lester', 0, 0),
(3094, '0', 'Cruz, Analito', 'Anthony', 'Edbonson & Norwin', 'Oakridge', 'Lester', 0, 0),
(3098, '0', 'Go, Cristine', 'QA', '', 'QA', '', 0, 0),
(3099, '0', 'Caong-ong, Janray', 'Anthony', 'Monette Cadungog', 'Oakridge', 'Lester', 0, 0),
(3100, '0', 'Caong-ong, Janray', 'Anthony', 'Monette Cadungog', 'Oakridge', 'Lester', 0, 0),
(3109, '0', 'Cabanero, Christer', 'Steve', 'Vivian Omega', 'WAVE 01', 'Francis', 0, 0),
(3110, '0', 'Cabanero, Christer', 'Steve', 'Vivian Omega', 'PRO', 'Francis', 0, 0),
(3111, '0', 'Costanilla, Anna Nica', 'QA', '', 'QA', '', 0, 0),
(3113, '0', 'Navaja, Rodel', 'Anthony', 'Monette Cadungog', 'Oakridge', 'Lester', 0, 0),
(3114, '0', 'Navaja, Rodel', 'Anthony', 'Monette Cadungog', 'Oakridge', 'Lester', 0, 0),
(3115, '0', 'Najarro, Fritz Mitchel', 'Anthony', 'Ezron & Jacky', 'WAVE 01', 'Lester', 0, 0),
(3116, '0', 'Najarro, Fritz Mitchel', 'Anthony', 'Ezron & Jacky', 'PRO', 'Lester', 0, 0),
(3121, '0', 'Dumagpi, Mary Windi', 'Miao', 'Miao', '', 'Ish', 0, 0),
(3122, '0', 'Dumagpi, Mary Windi', 'Miao', 'Miao', '', 'Ish', 0, 0),
(3125, '0', 'Costanilla, Anna Angelica', 'Miao', 'Platon', '', 'Ish', 0, 0),
(3127, '0', 'Caballero, Jacqueline', 'Anthony', 'Ezron & Jacky', 'Oakridge', 'Lester', 0, 0),
(3128, '0', 'Caballero, Jacqueline', 'Anthony', 'Ezron & Jacky', 'Oakridge', 'Lester', 0, 0),
(3131, '0', 'Denila, Fineflor', 'Miao', 'Miao', '', 'Ish', 0, 0),
(3132, '0', 'Denila, Fineflor', 'Miao', 'Miao', '', 'Ish', 0, 0),
(3133, '0', 'Dela Cerna, Arlene', 'Anthony', 'Ezron & Jacky', 'WAVE 02', 'Lester', 0, 0),
(3134, '0', 'Dela Cerna, Arlene', 'Anthony', 'Ezron & Jacky', 'PRO', 'Lester', 0, 0),
(3139, '0', 'Dinampo, Joshua Adrian', 'Steve', 'Vivian Omega', 'WAVE 02', 'Francis', 0, 0),
(3140, '0', 'Dinampo, Joshua Adrian', 'Steve', 'Vivian Omega', 'PRO', 'Francis', 0, 0),
(3145, '0', 'Miral, Raymundo', 'Miao', 'Miao', '', 'Ish', 0, 0),
(3146, '0', 'Miral, Raymundo', 'Miao', 'Miao', '', 'Ish', 0, 0),
(3147, '0', 'Ogatis, Yearee', 'Miao', 'Miao', 'WAVE 03', 'Ish', 0, 0),
(3148, '0', 'Ogatis, Yearee', 'Miao', 'Miao', 'PRO', 'Ish', 0, 0),
(3149, '0', 'Burdas, Mark Reland', 'Miao', 'Miao', 'WAVE 03', 'Ish', 0, 0),
(3150, '0', 'Burdas, Mark Reland', 'Miao', 'Miao', 'PRO', 'Ish', 0, 0),
(3153, '0', 'Calinao, Sharmaine Jane', 'Steve', 'Merben', 'SB Lead Gen', 'Francis', 0, 0),
(3154, '0', 'Calinao, Sharmaine Jane', 'Steve', 'Merben', 'PRO', 'Francis', 0, 0),
(3155, '0', 'Pasaporte, Daniel', 'Steve', 'Granville', 'WAVE 04', 'Francis', 0, 0),
(3156, '0', 'Pasaporte, Daniel', 'Steve', 'Granville', 'PRO', 'Francis', 0, 0),
(3159, '0', 'Largo, Arnel', 'Anthony', 'Ezron & Jacky', 'WAVE 04', 'Lester', 0, 0),
(3160, '0', 'Largo, Arnel', 'Anthony', 'Ezron & Jacky', 'PRO', 'Lester', 0, 0),
(3161, '0', 'Roma, Joe Marie', 'Miao', 'Miao', 'WAVE 04', 'Ish', 0, 0),
(3162, '0', 'Roma, Joe Marie', 'Miao', 'Miao', 'PRO', 'Ish', 0, 0),
(3163, '0', 'Navarro, Myko Julius', 'Steve', 'Granville', 'WAVE 04', 'Francis', 0, 0),
(3164, '0', 'Navarro, Myko Julius', 'Steve', 'Granville', 'PRO', 'Francis', 0, 0),
(3165, '0', 'Abapo, Rose Ann Jennen', 'Steve', 'Merben', '', 'Francis', 0, 0),
(3166, '0', 'Abapo, Rose Ann Jennen', 'Steve', 'Merben', '', 'Francis', 0, 0),
(3169, '0', 'Albino, Jenny', 'Miao', 'Platon', '', 'Ish', 0, 0),
(3170, '0', 'Albino, Jenny', 'Miao', 'Platon', '', 'Ish', 0, 0),
(3171, '0', 'Nierves, Jayson', 'Miao', 'Platon', '', 'Ish', 0, 0),
(3172, '0', 'Nierves, Jayson', 'Miao', 'Platon', '', 'Ish', 0, 0),
(3173, '0', 'Ignacio, Renalyn', 'Steve', 'Merben', '', 'Francis', 0, 0),
(3174, '0', 'Ignacio, Renalyn', 'Steve', 'Merben', '', 'Francis', 0, 0),
(3177, '0', 'Teofilo, Michael Duane', 'Steve', 'Vivian Omega', 'WAVE 05', 'Francis', 0, 0),
(3178, '0', 'Teofilo, Michael Duane', 'Steve', 'Vivian Omega', 'PRO', 'Francis', 0, 0),
(3185, '0', 'Fajardo, Mark Louie', 'Anthony', 'Edbonson & Norwin', 'WAVE 05', 'Lester', 0, 0),
(3186, '0', 'Fajardo, Mark Louie', 'Anthony', 'Edbonson & Norwin', 'PRO', 'Lester', 0, 0),
(3187, '0', 'Tabada, Christian Isaiah', 'Steve', 'Merben', 'WAVE 05', 'Francis', 0, 0),
(3188, '0', 'Tabada, Christian Isaiah', 'Steve', 'Merben', 'PRO', 'Francis', 0, 0),
(3189, '0', 'Arceno, Eden Rose', 'Miao', 'Platon', '', 'Ish', 0, 0),
(3190, '0', 'Arceno, Eden Rose', 'Miao', 'Platon', '', 'Ish', 0, 0),
(3191, '0', 'Montebon, Melvin', 'Steve', 'Granville', 'WAVE 05', 'Francis', 0, 0),
(3192, '0', 'Montebon, Melvin', 'Steve', 'Granville', 'PRO', 'Francis', 0, 0),
(3193, '0', 'Rodriguez, Ezron', 'Anthony', 'Ezron & Jacky', 'WAVE 05', 'Lester', 0, 0),
(3194, '0', 'Rodriguez, Ezron', 'Anthony', 'Ezron & Jacky', 'PRO', 'Lester', 0, 0),
(3195, '0', 'Rodriguez, Joshua', 'Steve', 'Vivian Omega', 'WAVE 06', 'Francis', 0, 0),
(3196, '0', 'Rodriguez, Joshua', 'Steve', 'Vivian Omega', 'PRO', 'Francis', 0, 0),
(3202, '0', 'Fajardo, John Paul', 'Steve', 'Granville', 'WAVE 06', 'Francis', 0, 0),
(3203, '0', 'Fajardo, John Paul', 'Steve', 'Granville', 'PRO', 'Francis', 0, 0),
(3212, '0', 'Manlucob, Merben', 'Anthony', 'Ezron & Jacky', 'WAVE 07', 'Lester', 0, 0),
(3213, '0', 'Manlucob, Merben', 'Anthony', 'Ezron & Jacky', 'NST', 'Lester', 0, 0),
(3216, '0', 'Anasco, Ryan', 'Anthony', 'Monette Cadungog', '', 'Lester', 0, 0),
(3217, '0', 'Anasco, Ryan', 'Anthony', 'Monette Cadungog', '', 'Lester', 0, 0),
(3220, '0', 'Fat, Kent Bryan', 'Miao', 'Miao', 'WAVE 07', 'Ish', 0, 0),
(3221, '0', 'Fat, Kent Bryan', 'Miao', 'Miao', 'PRT', 'Ish', 0, 0),
(3222, '0', 'Sabior, Jun Ryan', 'Miao', 'Elna', 'WAVE 07', 'Ish', 0, 0),
(3223, '0', 'Sabior, Jun Ryan', 'Miao', 'Elna', 'PRT', 'Ish', 0, 0),
(3226, '0', 'Mabunay, Abraham', 'Miao', 'Elna', 'WAVE 07', 'Ish', 0, 0),
(3227, '0', 'Mabunay, Abraham', 'Miao', 'Elna', 'PRT', 'Ish', 0, 0),
(3228, '0', 'Villarin, Platon Jr.', 'Miao', 'Platon', '', 'Ish', 0, 0),
(3229, '0', 'Villarin, Platon Jr.', 'Miao', 'Platon', '', 'Ish', 0, 0),
(3240, '0', 'Cabanero, Christopher Chlyde', 'Steve', 'Granville', 'wave 8', 'Francis', 0, 0),
(3241, '0', 'Cabanero, Christopher Chlyde', 'Steve', 'Granville', 'wave 8', 'Francis', 0, 0),
(3242, '0', 'Parmisana, Lyndon', 'Steve', 'Vivian Omega', 'wave 8', 'Francis', 0, 0),
(3243, '0', 'Parmisana, Lyndon', 'Steve', 'Vivian Omega', 'wave 8', 'Francis', 0, 0),
(3244, '0', 'Enrile, Onyx', 'Steve', 'Gran', 'wave 8', 'Francis', 0, 0),
(3245, '0', 'Enrile, Onyx', 'Steve', 'Gran', 'wave 8', 'Francis', 0, 0),
(3248, '0', 'Oltiano, Cassandra', 'Miao', 'Miao', 'wave 8', 'Ish', 0, 0),
(3249, '0', 'Oltiano, Cassandra', 'Miao', 'Miao', 'wave 8', 'Ish', 0, 0),
(3252, '0', 'Sanchez, Jerica', 'Anthony', 'Ezron & Jacky', 'wave 8', 'Lester', 0, 0),
(3253, '0', 'Sanchez, Jerica', 'Anthony', 'Ezron & Jacky', 'wave 8', 'Lester', 0, 0),
(3254, '0', 'Entera, William Jr.', 'Miao', 'Miao', 'wave 8', 'Ish', 0, 0),
(3255, '0', 'Entera, William Jr.', 'Miao', 'Miao', 'wave 8', 'Ish', 0, 0),
(3256, '0', 'Torejas, Wilson', 'Miao', 'Miao', 'wave 8', 'Ish', 0, 0),
(3257, '0', 'Torejas, Wilson', 'Miao', 'Miao', 'wave 8', 'Ish', 0, 0),
(3258, '0', 'Ugbinada, Edbonson', 'Anthony', 'Edbonson & Norwin', 'wave 8', 'Lester', 0, 0),
(3259, '0', 'Ugbinada, Edbonson', 'Anthony', 'Edbonson & Norwin', 'wave 8', 'Lester', 0, 0),
(3270, '0', 'Cose, Mary Joy', 'Steve', 'Merben', 'WAVE 09', 'Francis', 0, 0),
(3271, '0', 'Cose, Mary Joy', 'Steve', 'Merben', 'WAVE 09', 'Francis', 0, 0),
(3278, '0', 'Fernandez, Steven', 'Anthony', 'Edbonson & Norwin', 'WAVE 09', 'Lester', 0, 0),
(3279, '0', 'Fernandez, Steven', 'Anthony', 'Edbonson & Norwin', 'WAVE 09', 'Lester', 0, 0),
(3282, '0', 'Morales, Jin Irish', 'Steve', 'Granville', 'WAVE 09', 'Francis', 0, 0),
(3283, '0', 'Morales, Jin Irish', 'Steve', 'Granville', 'WAVE 09', 'Francis', 0, 0),
(3286, '0', 'Omega, Vivian', 'Steve', 'Vivian Omega', 'WAVE 09', 'Francis', 0, 0),
(3287, '0', 'Omega, Vivian', 'Steve', 'Vivian Omega', 'WAVE 09', 'Francis', 0, 0),
(3288, '0', 'Sardoncillo, Windel', 'Anthony', 'Ezron & Jacky', 'WAVE 09', 'Lester', 0, 0),
(3289, '0', 'Sardoncillo, Windel', 'Anthony', 'Ezron & Jacky', 'WAVE 09', 'Lester', 0, 0),
(3292, '0', 'Bayla, Krystal Jane', 'Miao', 'Elna', 'Wave 10', 'Ish', 0, 0),
(3293, '0', 'Bayla, Krystal Jane', 'Miao', 'Elna', 'Wave 10', 'Ish', 0, 0),
(3294, '0', 'Nardo, Lyndon Dave', 'Steve', 'Merben', 'Wave 10', 'Francis', 0, 0),
(3295, '0', 'Nardo, Lyndon Dave', 'Steve', 'Merben', 'Wave 10', 'Francis', 0, 0),
(3298, '0', 'Feniza, Christian', 'Miao', 'Elna', 'Wave 10', 'Ish', 0, 0),
(3299, '0', 'Feniza, Christian', 'Miao', 'Elna', 'Wave 10', 'Ish', 0, 0),
(3302, '0', 'Lastimado, Rojenne', 'Miao', 'Elna', 'Wave 10', 'Ish', 0, 0),
(3303, '0', 'Lastimado, Rojenne', 'Miao', 'Elna', 'Wave 10', 'Ish', 0, 0),
(3309, '0', 'Velez, Marck Donis', 'Miao', 'Elna', 'AAL', 'Ish', 0, 0),
(3323, '0', 'Nacario, Philip', 'Miao', 'Platon', 'AAL', 'Ish', 0, 0),
(3325, '0', 'Enolpe, Renan', 'Miao', 'Elna', 'Wave 11', 'Ish', 0, 0),
(3326, '0', 'Enolpe, Renan', 'Miao', 'Elna', 'Wave 11', 'Ish', 0, 0),
(3327, '0', 'Ludripas, Kimberly', 'Miao', 'Elna', 'Wave 11', 'Ish', 0, 0),
(3328, '0', 'Ludripas, Kimberly', 'Miao', 'Elna', 'Wave 11', 'Ish', 0, 0),
(3329, '0', 'Cabague, Chlyde', 'Miao', 'Elna', 'Wave 11', 'Ish', 0, 0),
(3330, '0', 'Cabague, Chlyde', 'Miao', 'Elna', 'Wave 11', 'Ish', 0, 0),
(3337, '0', 'Filosofo, Shiella Mae', 'Jelly', 'Jelly', 'Dumaguete', 'Francis', 0, 0),
(3338, '0', 'Filosofo, Shiella Mae', 'Jelly', 'Jelly', 'Dumaguete', 'Francis', 0, 0),
(3343, '0', 'Tajada, Marybeth', 'Jelly', 'Jelly', 'Dumaguete', 'Francis', 0, 0),
(3344, '0', 'Tajada, Marybeth', 'Jelly', 'Jelly', 'Dumaguete', 'Francis', 0, 0),
(3345, '0', 'Badoy, Mark Julius', 'Jelly', 'Jelly', 'Dumaguete', 'Francis', 0, 0),
(3346, '0', 'Badoy, Mark Julius', 'Jelly', 'Jelly', 'Dumaguete', 'Francis', 0, 0),
(3347, '0', 'Velez, Marck Donis', 'Miao', 'Elna', 'AAL', 'Ish', 0, 0),
(3351, '0', 'Nacario, Philip', 'Miao', 'Platon', 'AAL', 'Ish', 0, 0),
(3354, '0', 'Alforo, Brian', 'Steve', 'Granville', 'Oakridge', 'Francis', 0, 0),
(3355, '0', 'Alforo, Brian', 'Steve', 'Granville', 'Oakridge', 'Francis', 0, 0),
(3368, '0', 'Ajero, Ma.Erica', 'Anthony', 'Ezron & Jacky', 'Wave 13', 'Lester', 0, 0),
(3371, '0', 'Morley, Khimberly', 'Anthony', 'Monette Cadungog', 'Wave 13', 'Lester', 0, 0),
(3372, '0', 'Morley, Khimberly', 'Anthony', 'Monette Cadungog', 'Wave 13', 'Lester', 0, 0),
(3375, '0', 'Sardoncillo, Mariane', 'Steve', 'Granville', 'Wave 13', 'Francis', 0, 0),
(3376, '0', 'Sardoncillo, Mariane', 'Steve', 'Granville', 'Wave 13', 'Francis', 0, 0),
(3377, '0', 'Nu?ez, Janine', 'Steve', 'Granville', 'Wave 13', 'Francis', 0, 0),
(3378, '0', 'Nu?ez, Janine', 'Steve', 'Granville', 'Wave 13', 'Francis', 0, 0),
(3379, '0', 'Bontuyan, Princess Mae', 'Steve', 'Vivian Omega', 'Wave 13', 'Francis', 0, 0),
(3380, '0', 'Bontuyan, Princess Mae', 'Steve', 'Vivian Omega', 'Wave 13', 'Francis', 0, 0),
(3381, '0', 'Magsipoc, Carl Harris', 'Anthony', 'Ezron & Jacky', 'Wave 13', 'Lester', 0, 0),
(3382, '0', 'Magsipoc, Carl Harris', 'Anthony', 'Ezron & Jacky', 'Wave 13', 'Lester', 0, 0),
(3383, '0', 'Catapon, Miel Jason', 'Anthony', 'Edbonson & Norwin', 'Wave 13', 'Lester', 0, 0),
(3384, '0', 'Catapon, Miel Jason', 'Anthony', 'Edbonson & Norwin', 'Wave 13', 'Lester', 0, 0),
(3385, '0', 'Baldo, Carlito', 'Anthony', 'Monette Cadungog', 'Wave 13', 'Lester', 0, 0),
(3386, '0', 'Baldo, Carlito', 'Anthony', 'Monette Cadungog', 'Wave 13', 'Lester', 0, 0),
(3387, '0', 'Abrea, Neil Joseph', 'Steve', 'Merben', 'Wave 13', 'Francis', 0, 0),
(3388, '0', 'Abrea, Neil Joseph', 'Steve', 'Merben', 'Wave 13', 'Francis', 0, 0),
(3391, '0', 'Ambalate, Ryan', 'Jelly', 'Jelly', 'Dumaguete', 'Francis', 0, 0),
(3392, '0', 'Ambalate, Ryan', 'Jelly', 'Jelly', 'Dumaguete', 'Francis', 0, 0),
(3393, '0', 'Ajero, Ma.Erica', 'Anthony', 'Ezron & Jacky', 'Wave 13', 'Lester', 0, 0),
(3394, '0', 'Catacutan, Jasper', 'Jelly', 'Jelly', 'Dumaguete', 'Francis', 0, 0),
(3395, '0', 'Catacutan, Jasper', 'Jelly', 'Jelly', 'Dumaguete', 'Francis', 0, 0),
(3398, '0', 'Vailoces, Joy', 'Jelly', 'Jelly', 'Dumaguete', 'Francis', 0, 0),
(3399, '0', 'Vailoces, Joy', 'Jelly', 'Jelly', 'Dumaguete', 'Francis', 0, 0),
(3400, '0', 'Francisco, Reah Mae', 'Jelly', 'Jelly', 'Dumaguete', 'Francis', 0, 0),
(3401, '0', 'Francisco, Reah Mae', 'Jelly', 'Jelly', 'Dumaguete', 'Francis', 0, 0),
(3402, '0', 'Mangubat, Wally Dean', 'Anthony', 'Monette Cadungog', '', 'Lester', 0, 0),
(3403, '0', 'Mangubat, Wally Dean', 'Anthony', 'Monette Cadungog', '', 'Lester', 0, 0),
(3407, '0', 'Lopez, Bhen Michael', 'Steve', 'Vivian Omega', 'Wave 14', 'Francis', 0, 0),
(3408, '0', 'Lopez, Bhen Michael', 'Steve', 'Vivian Omega', 'Wave 14', 'Francis', 0, 0),
(3409, '0', 'Butalid, Fanny Mae', 'Anthony', 'Monette Cadungog', 'Wave 14', 'Lester', 0, 0),
(3410, '0', 'Butalid, Fanny Mae', 'Anthony', 'Monette Cadungog', 'Wave 14', 'Lester', 0, 0),
(3411, '0', 'Chicote, Glen', 'Steve', 'Merben', 'Wave 14', 'Francis', 0, 0),
(3412, '0', 'Chicote, Glen', 'Steve', 'Merben', 'Wave 14', 'Francis', 0, 0),
(3419, '0', 'Magarang, Leo James', 'Anthony', 'Monette Cadungog', 'Wave 14', 'Lester', 0, 0),
(3420, '0', 'Magarang, Leo James', 'Anthony', 'Monette Cadungog', 'Wave 14', 'Lester', 0, 0),
(3421, '0', 'Kalinga, Louie James', 'Steve', 'Vivian Omega', 'Wave 14', 'Francis', 0, 0),
(3422, '0', 'Kalinga, Louie James', 'Steve', 'Vivian Omega', 'Wave 14', 'Francis', 0, 0),
(3423, '0', 'Pardillo, Mark Davis', 'Anthony', 'Monette Cadungog', 'Wave 14', 'Lester', 0, 0),
(3424, '0', 'Pardillo, Mark Davis', 'Anthony', 'Monette Cadungog', 'Wave 14', 'Lester', 0, 0),
(3425, '0', 'Contado, Nino', 'Steve', 'Vivian Omega', 'Wave 14', 'Francis', 0, 0),
(3426, '0', 'Contado, Nino', 'Steve', 'Vivian Omega', 'Wave 14', 'Francis', 0, 0),
(3427, '0', 'Teofilo, Ray Michael', 'Anthony', 'Ezron & Jacky', 'Wave 14', 'Lester', 0, 0),
(3428, '0', 'Teofilo, Ray Michael', 'Anthony', 'Ezron & Jacky', 'Wave 14', 'Lester', 0, 0),
(3429, '0', 'Costanilla, Iris', 'QA', 'QA', '', '', 0, 0),
(3431, '0', 'Molina, Joyce Mae', 'Miao', 'Miao', '', 'Ish', 0, 0),
(3432, '0', 'Molina, Joyce Mae', 'Miao', 'Miao', '', 'Ish', 0, 0),
(3433, '0', 'Morata, Jenilyn', 'Anthony', 'Edbonson & Norwin', 'Wave 15', 'Lester', 0, 0),
(3434, '0', 'Morata, Jenilyn', 'Anthony', 'Edbonson & Norwin', 'Wave 15', 'Lester', 0, 0),
(3443, '0', 'Casquejo, Jade', 'Steve', 'Granville', 'Wave 15', 'Francis', 0, 0),
(3444, '0', 'Casquejo, Jade', 'Steve', 'Granville', 'Wave 15', 'Francis', 0, 0),
(3445, '0', 'Bollozos, Renato Jr.', 'Steve', 'Merben', 'Wave 15', 'Francis', 0, 0),
(3446, '0', 'Bollozos, Renato Jr.', 'Steve', 'Merben', 'Wave 15', 'Francis', 0, 0),
(3447, '0', 'Magsanay, Rodgen', 'Anthony', 'Monette Cadungog', 'Wave 15', 'Lester', 0, 0),
(3448, '0', 'Magsanay, Rodgen', 'Anthony', 'Monette Cadungog', 'Wave 15', 'Lester', 0, 0),
(3451, '0', 'Abadilla, James Ian', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3452, '0', 'Abadilla, James Ian', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3465, '0', 'Juarez, Mae Anne', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3466, '0', 'Juarez, Mae Anne', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3471, '0', 'Caruana, Toni Rose', 'Miao', 'elna', 'Wave 16', 'Ish', 0, 0),
(3472, '0', 'Caruana, Toni Rose', 'Miao', 'elna', 'Wave 16', 'Ish', 0, 0),
(3475, '0', 'Sialana, Ninfa', 'Miao', 'elna', 'Wave 16', 'Ish', 0, 0),
(3476, '0', 'Sialana, Ninfa', 'Miao', 'elna', 'Wave 16', 'Ish', 0, 0),
(3477, '0', 'Pielago, Kevin', 'Miao', 'elna', 'Wave 16', 'Ish', 0, 0),
(3478, '0', 'Pielago, Kevin', 'Miao', 'elna', 'Wave 16', 'Ish', 0, 0),
(3479, '0', 'Ganabis, Kelly John', 'Miao', 'elna', 'Wave 16', 'Ish', 0, 0),
(3480, '0', 'Ganabis, Kelly John', 'Miao', 'elna', 'Wave 16', 'Ish', 0, 0),
(3485, '0', 'Manayon, Lea Monique', 'Miao', 'elna', 'Wave 16', 'Ish', 0, 0),
(3486, '0', 'Manayon, Lea Monique', 'Miao', 'elna', 'Wave 16', 'Ish', 0, 0),
(3487, '0', 'Canton, Desiree', 'Miao', 'elna', 'Wave 16', 'Ish', 0, 0),
(3488, '0', 'Canton, Desiree', 'Miao', 'elna', 'Wave 16', 'Ish', 0, 0),
(3491, '0', 'Calago, Bryan', 'Miao', 'elna', 'Wave 16', 'Ish', 0, 0),
(3492, '0', 'Calago, Bryan', 'Miao', 'elna', 'Wave 16', 'Ish', 0, 0),
(3495, '0', 'Belonta, Jacob', 'Jelly', 'Jelly', 'Dumaguete', 'Francis', 0, 0),
(3496, '0', 'Belonta, Jacob', 'Jelly', 'Jelly', 'Dumaguete', 'Francis', 0, 0),
(3503, '0', 'Borja, Jonathan Adrian', 'Steve', 'Vivian Omega', 'Wave 17', 'Francis', 0, 0),
(3504, '0', 'Borja, Jonathan Adrian', 'Steve', 'Vivian Omega', 'Wave 17', 'Francis', 0, 0),
(3505, '0', 'Cadungog, Mary Monette', 'Anthony', 'Monette Cadungog', 'Wave 17', 'Lester', 0, 0),
(3506, '0', 'Cadungog, Mary Monette', 'Anthony', 'Monette Cadungog', 'Wave 17', 'Lester', 0, 0),
(3507, '0', 'Suelto, Stewart', 'Anthony', 'Ezron & Jacky', 'Wave 17', 'Lester', 0, 0),
(3508, '0', 'Suelto, Stewart', 'Anthony', 'Ezron & Jacky', 'Wave 17', 'Lester', 0, 0),
(3511, '0', 'Alvarez, Mark Ian', 'Anthony', 'Ezron & Jacky', 'Wave 17', 'Lester', 0, 0),
(3512, '0', 'Alvarez, Mark Ian', 'Anthony', 'Ezron & Jacky', 'Wave 17', 'Lester', 0, 0),
(3513, '0', 'Rameso, Christian', 'Anthony', 'Edbonson & Norwin', 'Wave 17', 'Lester', 0, 0),
(3514, '0', 'Rameso, Christian', 'Anthony', 'Edbonson & Norwin', 'Wave 17', 'Lester', 0, 0),
(3515, '0', 'Polestico, Maria Thanh', 'Steve', 'Vivian Omega', 'Wave 17', 'Francis', 0, 0),
(3516, '0', 'Polestico, Maria Thanh', 'Steve', 'Vivian Omega', 'Wave 17', 'Francis', 0, 0),
(3517, '0', 'Gocotano, Maria', 'Anthony', 'Edbonson & Norwin', 'Wave 17', 'Lester', 0, 0),
(3518, '0', 'Gocotano, Maria', 'Anthony', 'Edbonson & Norwin', 'Wave 17', 'Lester', 0, 0),
(3519, '0', 'Nilles, Glyndel', 'Steve', 'Merben', 'Wave 17', 'Francis', 0, 0),
(3520, '0', 'Nilles, Glyndel', 'Steve', 'Merben', 'Wave 17', 'Francis', 0, 0),
(3525, '0', 'Elle, Mariel', 'Steve', 'Vivian Omega', 'Wave 17', 'Francis', 0, 0),
(3526, '0', 'Elle, Mariel', 'Steve', 'Vivian Omega', 'Wave 17', 'Francis', 0, 0),
(3529, '0', 'Arnoza, John Michael', 'Steve', 'Merben', 'Wave 17', 'Francis', 0, 0),
(3530, '0', 'Arnoza, John Michael', 'Steve', 'Merben', 'Wave 17', 'Francis', 0, 0),
(3533, '0', 'Artajo, Benson', 'Miao', 'Miao', 'Wave 18', 'Ish', 0, 0),
(3534, '0', 'Artajo, Benson', 'Miao', 'Miao', 'Wave 18', 'Ish', 0, 0),
(3535, '0', 'Zozobrado, Vince Klyn', 'Miao', 'Miao', 'Wave 18', 'Ish', 0, 0),
(3536, '0', 'Zozobrado, Vince Klyn', 'Miao', 'Miao', 'Wave 18', 'Ish', 0, 0),
(3539, '0', 'Taraya, Khareen', 'Miao', 'Miao', 'Wave 18', 'Ish', 0, 0),
(3540, '0', 'Taraya, Khareen', 'Miao', 'Miao', 'Wave 18', 'Ish', 0, 0),
(3541, '0', 'Derehe, Caryl Vhee', 'Miao', 'Miao', 'Wave 18', 'Ish', 0, 0),
(3542, '0', 'Derehe, Caryl Vhee', 'Miao', 'Miao', 'Wave 18', 'Ish', 0, 0),
(3543, '0', 'Villamor, Mark Vincent', 'Miao', 'Miao', 'Wave 18', 'Ish', 0, 0),
(3544, '0', 'Villamor, Mark Vincent', 'Miao', 'Miao', 'Wave 18', 'Ish', 0, 0),
(3545, '0', 'Daclan, Kent', 'Miao', 'Miao', 'Wave 18', 'Ish', 0, 0),
(3546, '0', 'Daclan, Kent', 'Miao', 'Miao', 'Wave 18', 'Ish', 0, 0),
(3547, '0', 'Abellanosa, Wendy', 'Miao', 'Miao', 'Wave 18', 'Ish', 0, 0),
(3548, '0', 'Abellanosa, Wendy', 'Miao', 'Miao', 'Wave 18', 'Ish', 0, 0),
(3549, '0', 'Abejo, Ralph', 'Miao', 'elna', 'Wave 18', 'Ish', 0, 0),
(3550, '0', 'Abejo, Ralph', 'Miao', 'elna', 'Wave 18', 'Ish', 0, 0),
(3551, '0', 'Loremas, Errol Franz Jesus', 'Miao', 'elna', 'Wave 18', 'Ish', 0, 0),
(3552, '0', 'Loremas, Errol Franz Jesus', 'Miao', 'elna', 'Wave 18', 'Ish', 0, 0),
(3553, '0', 'De Los Santos, Francis Lyndon', 'Miao', 'elna', 'Wave 18', 'Ish', 0, 0),
(3554, '0', 'De Los Santos, Francis Lyndon', 'Miao', 'elna', 'Wave 18', 'Ish', 0, 0),
(3555, '0', 'Costanilla, Jason', 'Miao', 'elna', 'Wave 18', 'Ish', 0, 0),
(3556, '0', 'Costanilla, Jason', 'Miao', 'elna', 'Wave 18', 'Ish', 0, 0),
(3557, '0', 'Cabulao, Uniel', 'Miao', 'elna', 'Wave 18', 'Ish', 0, 0),
(3558, '0', 'Cabulao, Uniel', 'Miao', 'elna', 'Wave 18', 'Ish', 0, 0),
(3559, '0', 'Diaz, Brandon', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3560, '0', 'Diaz, Brandon', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3561, '0', 'Ca?edo, John Michael', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3562, '0', 'Ca?edo, John Michael', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3565, '0', 'Creus, Ria Jean', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3566, '0', 'Creus, Ria Jean', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3567, '0', 'Abellaneda, Ralph', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3568, '0', 'Abellaneda, Ralph', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3569, '0', 'Bentazal, Mike Frances', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3570, '0', 'Bentazal, Mike Frances', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3575, '0', 'Requilme, Jhon Rey', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3576, '0', 'Requilme, Jhon Rey', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3577, '0', 'Creus, Rose Jean', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3578, '0', 'Creus, Rose Jean', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3579, '0', 'Romeo, Roderick', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3580, '0', 'Romeo, Roderick', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3581, '0', 'Espinosa, Jay Carter', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3582, '0', 'Espinosa, Jay Carter', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3585, '0', 'Gonzales, Renil', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3586, '0', 'Gonzales, Renil', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3587, '0', 'Badayos, Hanna Jane', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3588, '0', 'Badayos, Hanna Jane', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3589, '0', 'Creus, Mieflor', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3590, '0', 'Creus, Mieflor', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3591, '0', 'Lopez, Murphy', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3592, '0', 'Lopez, Murphy', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3597, '0', 'Donaire, John Bryll', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3598, '0', 'Donaire, John Bryll', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3599, '0', 'Nadal, Mark Vannie', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3600, '0', 'Nadal, Mark Vannie', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3601, '0', 'Buquiron, Rafael', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3602, '0', 'Buquiron, Rafael', 'Juvelyn', 'Juvelyn', 'E-Tel', 'Lester', 0, 0),
(3603, '0', 'Ostia, Adonnis Cyriel Ardina ', 'Steve', 'Vivian Omega', 'Wave 19', 'Francis', 0, 0),
(3604, '0', 'Ostia, Adonnis Cyriel Ardina ', 'Steve', 'Vivian Omega', 'Wave 19', 'Francis', 0, 0),
(3605, '0', 'Sagarino, Betzaida Ortiz ', 'Anthony', 'Edbonson & Norwin', 'Wave 19', 'Lester', 0, 0),
(3606, '0', 'Sagarino, Betzaida Ortiz ', 'Anthony', 'Edbonson & Norwin', 'Wave 19', 'Lester', 0, 0),
(3607, '0', 'Del Valle Lines, Harlyn ', 'Anthony', 'Edbonson & Norwin', 'Wave 19', 'Lester', 0, 0),
(3608, '0', 'Del Valle Lines, Harlyn ', 'Anthony', 'Edbonson & Norwin', 'Wave 19', 'Lester', 0, 0),
(3609, '0', 'Villacarlos, Ronel Aloba ', 'Steve', 'Merben', 'Wave 19', 'Francis', 0, 0),
(3610, '0', 'Villacarlos, Ronel Aloba ', 'Steve', 'Merben', 'Wave 19', 'Francis', 0, 0),
(3611, '0', 'Pajuay, Mervin ', 'Anthony', 'Monette Cadungog', 'Wave 19', 'Lester', 0, 0),
(3612, '0', 'Pajuay, Mervin ', 'Anthony', 'Monette Cadungog', 'Wave 19', 'Lester', 0, 0),
(3613, '0', 'Tibay, Loe Angelo F. ', 'Anthony', 'Ezron & Jacky', 'Wave 19', 'Lester', 0, 0),
(3614, '0', 'Tibay, Loe Angelo F. ', 'Anthony', 'Ezron & Jacky', 'Wave 19', 'Lester', 0, 0),
(3621, '0', 'Nacar, Jade Martin', 'Miao', '', 'Wave 20', 'Ish', 0, 0),
(3622, '0', 'Nacar, Jade Martin', 'Miao', '', 'Wave 20', 'Ish', 0, 0),
(3623, '0', 'Villegas, Nathaniel', 'Miao', '', 'Wave 20', 'Ish', 0, 0),
(3624, '0', 'Villegas, Nathaniel', 'Miao', '', 'Wave 20', 'Ish', 0, 0),
(3625, '0', 'Pantuan, Jeanrose', 'Miao', '', 'Wave 20', 'Ish', 0, 0),
(3626, '0', 'Pantuan, Jeanrose', 'Miao', '', 'Wave 20', 'Ish', 0, 0),
(3627, '0', 'Bantay, Rachel Ann', 'Miao', '', 'Wave 20', 'Ish', 0, 0),
(3628, '0', 'Bantay, Rachel Ann', 'Miao', '', 'Wave 20', 'Ish', 0, 0),
(3629, '0', 'Bacalso, Kristian', 'Miao', '', 'Wave 20', 'Ish', 0, 0),
(3630, '0', 'Bacalso, Kristian', 'Miao', '', 'Wave 20', 'Ish', 0, 0),
(3631, '0', 'Catubigan, Rafael', 'Miao', '', 'Wave 20', 'Ish', 0, 0),
(3632, '0', 'Catubigan, Rafael', 'Miao', '', 'Wave 20', 'Ish', 0, 0),
(3633, '0', 'Radana, Angelica Mae', 'Miao', '', 'Wave 20', 'Ish', 0, 0),
(3634, '0', 'Radana, Angelica Mae', 'Miao', '', 'Wave 20', 'Ish', 0, 0),
(3635, '0', 'Chavez, Jean Esther', 'Ana', '', 'Wave 21', 'Francis', 0, 0),
(3636, '0', 'Sebial, Stephanie', 'Ana', '', 'Wave 21', 'Francis', 0, 0),
(3637, '0', 'Lunay, Jessyl', 'Ana', '', 'Wave 21', 'Francis', 0, 0),
(3638, '0', 'Eledia, Ellen Mae', 'Ana', '', 'Wave 21', 'Francis', 0, 0),
(3639, '0', 'Areola, Jamil', 'Ana', '', 'Wave 21', 'Francis', 0, 0),
(3640, '0', 'Villan, Jay-ar', 'Ana', '', 'Wave 21', 'Francis', 0, 0),
(3641, '0', 'Pahaganas, Rasty', 'Ana', '', 'Wave 21', 'Francis', 0, 0),
(3642, '0', 'Chavez, Jean Esther', 'Ana', '', 'Wave 21', 'Francis', 0, 0),
(3643, '0', 'Sebial, Stephanie', 'Ana', '', 'Wave 21', 'Francis', 0, 0),
(3644, '0', 'Lunay, Jessyl', 'Ana', '', 'Wave 21', 'Francis', 0, 0),
(3645, '0', 'Eledia, Ellen Mae', 'Ana', '', 'Wave 21', 'Francis', 0, 0),
(3646, '0', 'Areola, Jamil', 'Ana', '', 'Wave 21', 'Francis', 0, 0),
(3647, '0', 'Villan, Jay-ar', 'Ana', '', 'Wave 21', 'Francis', 0, 0),
(3648, '0', 'Pahaganas, Rasty', 'Ana', '', 'Wave 21', 'Francis', 0, 0),
(400105, '0', 'Acabal, Patrick Joseph?', 'Cathy', '', 'TOTI', '', 0, 0),
(400106, '0', 'Agullana, Serafin', 'Cathy', '', 'TOTI', '', 0, 0),
(400107, '0', 'Barot, Ryan Gil?', 'Cathy', '', 'TOTI', '', 0, 0),
(400108, '0', 'Batocael, Mary Knoll?', 'Cathy', '', 'TOTI', '', 0, 0),
(400109, '0', 'Bermjio, Michael?', 'Cathy', '', 'TOTI', '', 0, 0),
(400110, '0', 'Casil, Anna Marie?', 'Cathy', '', 'TOTI', '', 0, 0),
(400111, '0', 'Catan, Florence Angelica', 'Cathy', '', 'TOTI', '', 0, 0),
(400112, '0', 'Crismunch, Terjen Kim?', 'Cathy', '', 'TOTI', '', 0, 0),
(400113, '0', 'Dela Cerna, Claudine Mae?', 'Cathy', '', 'TOTI', '', 0, 0),
(400114, '0', 'Enojo, Armie?', 'Cathy', '', 'TOTI', '', 0, 0),
(400115, '0', 'Girasol, Paul Vincent?', 'Cathy', '', 'TOTI', '', 0, 0),
(400116, '0', 'Kadusale, Jennie Ann?', 'Cathy', '', 'TOTI', '', 0, 0),
(400117, '0', 'Lomeda, Lemuel?', 'Cathy', '', 'TOTI', '', 0, 0),
(400118, '0', 'Manangquil, Kimberly?', 'Cathy', '', 'TOTI', '', 0, 0),
(400119, '0', 'Navarro, Joanna Marie?', 'Cathy', '', 'TOTI', '', 0, 0),
(400120, '0', 'Octavio, Avid Zillah?', 'Cathy', '', 'TOTI', '', 0, 0),
(400121, '0', 'Repollo, Jekcson?', 'Cathy', '', 'TOTI', '', 0, 0),
(400122, '0', 'Vallega, Kreiza Mae', 'Cathy', '', 'TOTI', '', 0, 0),
(400123, '0', 'Vano, Cyril?', 'Cathy', '', 'TOTI', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sanction_list`
--

CREATE TABLE IF NOT EXISTS `sanction_list` (
  `sanction_list_id` int(11) NOT NULL AUTO_INCREMENT,
  `sanction_list_desc` varchar(255) NOT NULL,
  PRIMARY KEY (`sanction_list_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `sanction_list`
--

INSERT INTO `sanction_list` (`sanction_list_id`, `sanction_list_desc`) VALUES
(1, 'For Coaching'),
(2, 'Document Verbal Warning '),
(3, '1st Written Warning'),
(4, 'Final  Written Warning'),
(5, '1st to 3 days suspension'),
(6, '2nd to 3 days suspension'),
(7, '3 days suspension'),
(8, '1st to 5 days suspension'),
(9, '2nd to 5 days suspension'),
(10, '5 days suspension'),
(11, '1st to End of Contract '),
(12, '2nd to End of Contract'),
(13, 'End of Contract ');

-- --------------------------------------------------------

--
-- Table structure for table `script_lists`
--

CREATE TABLE IF NOT EXISTS `script_lists` (
  `script_lists_id` int(11) NOT NULL AUTO_INCREMENT,
  `script_list_code` varchar(255) NOT NULL,
  PRIMARY KEY (`script_lists_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `script_lists`
--

INSERT INTO `script_lists` (`script_lists_id`, `script_list_code`) VALUES
(1, 'z01'),
(2, 'z02'),
(3, 'z03'),
(4, 'z04'),
(5, 'z16'),
(6, 'z05'),
(7, 'z06'),
(8, 'z07'),
(9, 'z15'),
(10, 'z08'),
(11, 'z09'),
(12, 'z10'),
(13, 'z11'),
(14, 'z12');

-- --------------------------------------------------------

--
-- Table structure for table `script_source`
--

CREATE TABLE IF NOT EXISTS `script_source` (
  `script_source_id` int(11) NOT NULL AUTO_INCREMENT,
  `script_desc` varchar(255) NOT NULL,
  PRIMARY KEY (`script_source_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `script_source`
--

INSERT INTO `script_source` (`script_source_id`, `script_desc`) VALUES
(1, 'Laura'),
(2, 'Lisa'),
(3, 'Travis'),
(4, 'Rose'),
(5, 'Josh'),
(6, 'Abigail'),
(7, 'Miguel'),
(8, 'Emma'),
(9, 'Jason'),
(10, 'Andrea'),
(11, 'Voice Team');

-- --------------------------------------------------------

--
-- Table structure for table `sites`
--

CREATE TABLE IF NOT EXISTS `sites` (
  `site_id` int(11) NOT NULL AUTO_INCREMENT,
  `site_name` varchar(255) NOT NULL,
  `site_nickname` varchar(255) NOT NULL,
  PRIMARY KEY (`site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `sites`
--

INSERT INTO `sites` (`site_id`, `site_name`, `site_nickname`) VALUES
(1, 'OITC', 'OK'),
(2, 'Inayawan', 'NY'),
(3, 'Dumaguete', 'DU'),
(4, 'E-Telecare', 'ETEL'),
(5, 'TAN', 'TAN');

-- --------------------------------------------------------

--
-- Table structure for table `soundboard_voice`
--

CREATE TABLE IF NOT EXISTS `soundboard_voice` (
  `soundboard_voice_id` int(11) NOT NULL AUTO_INCREMENT,
  `soundboard_voice_name` varchar(255) NOT NULL,
  PRIMARY KEY (`soundboard_voice_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `soundboard_voice`
--

INSERT INTO `soundboard_voice` (`soundboard_voice_id`, `soundboard_voice_name`) VALUES
(1, 'Jason'),
(2, 'Sam'),
(3, 'Abigail'),
(4, 'Josh'),
(5, 'Laura'),
(6, 'Emma'),
(7, 'Miguel'),
(8, 'Andrea'),
(9, 'Travis'),
(10, 'Tony'),
(11, 'Lisa');

-- --------------------------------------------------------

--
-- Table structure for table `users_info`
--

CREATE TABLE IF NOT EXISTS `users_info` (
  `users_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_account_id` int(11) DEFAULT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  PRIMARY KEY (`users_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `users_info`
--

INSERT INTO `users_info` (`users_id`, `user_account_id`, `first_name`, `last_name`) VALUES
(1, 6, 'Administrator', 'Administrator'),
(2, 3, 'Lester ', 'Galenzoga'),
(3, 3, 'Francis ', 'Jomar '),
(4, 3, 'Mary Cristine ', 'Go'),
(5, 3, 'Iris ', 'Constanilla'),
(6, 3, 'Nica ', 'Constanilla'),
(7, 3, 'John ', 'Aves'),
(8, 4, 'Ana De ', 'Guzman'),
(9, NULL, 'Jelly', 'Yap'),
(10, NULL, 'Revie', ''),
(11, NULL, 'Juvelyn', ''),
(12, NULL, 'Anthony', ''),
(13, NULL, 'Steve', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_accounts`
--

CREATE TABLE IF NOT EXISTS `user_accounts` (
  `user_account_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_roles_id` int(11) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `acct_nick_name` varchar(255) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_account_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `user_accounts`
--

INSERT INTO `user_accounts` (`user_account_id`, `user_roles_id`, `employee_id`, `username`, `password`, `email`, `acct_nick_name`, `date_created`) VALUES
(1, 2, 201700169, 'ish', '288eda9777b3eb80bd0b1c5594c6e3b3', '', 'Ish', '2019-02-13 18:14:39'),
(2, 5, 201700113, 'TJ', '202cb962ac59075b964b07152d234b70', '', 'TJ', '2019-02-12 22:22:57'),
(3, 2, 201700013, 'cristine', '202cb962ac59075b964b07152d234b70', '', 'Cristine', '2019-02-12 22:22:36'),
(4, 2, 201700011, 'lester', '9387bff6722456057e6be54d789de17c', '', 'Lester', '2019-02-13 18:15:29'),
(5, 2, 201700017, 'francis', '202cb962ac59075b964b07152d234b70', '', 'Francis', '2019-02-12 22:22:11'),
(6, 1, 0, 'admin', '20f1016e09d458f0190d50c4b24be5fb', '', 'Administrator', '2019-02-13 18:27:11'),
(23, 2, 201700061, 'Nica', '202cb962ac59075b964b07152d234b70', '', 'Nica', '2019-02-12 22:21:50'),
(24, 2, 201700153, 'John', '202cb962ac59075b964b07152d234b70', '', 'John', '2019-02-12 22:22:00');

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `user_roles_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_roles_desc` varchar(255) NOT NULL,
  PRIMARY KEY (`user_roles_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`user_roles_id`, `user_roles_desc`) VALUES
(1, 'Admin'),
(2, 'Auditor- Q.A '),
(3, 'Agent'),
(4, 'Agent Team Lead'),
(5, 'Supervisor');

-- --------------------------------------------------------

--
-- Table structure for table `vici_disposition_list`
--

CREATE TABLE IF NOT EXISTS `vici_disposition_list` (
  `vici_dispo_list_id` int(11) NOT NULL AUTO_INCREMENT,
  `vici_dispo_desc` varchar(255) NOT NULL,
  `dispo_code` varchar(255) NOT NULL,
  PRIMARY KEY (`vici_dispo_list_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=37 ;

--
-- Dumping data for table `vici_disposition_list`
--

INSERT INTO `vici_disposition_list` (`vici_dispo_list_id`, `vici_dispo_desc`, `dispo_code`) VALUES
(1, 'Hang Up', 'HUP'),
(2, 'Language Barrier', 'Lang'),
(3, 'Not Qualified', 'NQ'),
(4, 'Not Right Now', 'NRN'),
(5, 'Redial', 'RD'),
(6, 'Transfer Failed', 'TrFail'),
(7, 'Transferred Successfully', 'TrSuc'),
(8, 'Wrong Number', 'WRNGN'),
(9, 'Dial Time Out', 'DTO'),
(10, 'Dead Air', 'DEAD'),
(11, 'Closer Rang Forever', 'RING'),
(12, 'Closer VoiceMail', 'VM'),
(13, 'Caller HUP on Transfer', 'TRHUP'),
(14, 'Dump Leads', 'DUMP'),
(15, 'Prank Call', 'Prank'),
(16, 'Insurance Hangup', 'InsHUP'),
(17, 'Suspect Robo Calling', 'ROBOT'),
(35, 'Answering Machine', 'A'),
(36, 'Not Interested', 'NI');

-- --------------------------------------------------------

--
-- Table structure for table `wave_list`
--

CREATE TABLE IF NOT EXISTS `wave_list` (
  `wave_id` int(11) NOT NULL AUTO_INCREMENT,
  `wave_desc` varchar(255) NOT NULL,
  `wave_short_desc` varchar(255) NOT NULL,
  PRIMARY KEY (`wave_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `webform_disposition_list`
--

CREATE TABLE IF NOT EXISTS `webform_disposition_list` (
  `webform_disp_list_id` int(11) NOT NULL AUTO_INCREMENT,
  `webform_dispo_desc` varchar(255) NOT NULL,
  PRIMARY KEY (`webform_disp_list_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `webform_disposition_list`
--

INSERT INTO `webform_disposition_list` (`webform_disp_list_id`, `webform_dispo_desc`) VALUES
(1, 'Closer - said "I''ll call you back"'),
(2, 'tsHU'),
(3, 'Closer - Voicemail or IVR'),
(4, 'Closer - Too long to answer'),
(5, 'Closer OR caller hangup BEFORE X seconds'),
(6, 'Closer OR caller still present AT X seconds'),
(7, 'Transfer OK'),
(8, 'N/A');

-- --------------------------------------------------------

--
-- Table structure for table `ztp_mark_list`
--

CREATE TABLE IF NOT EXISTS `ztp_mark_list` (
  `ztp_mark_list_id` int(11) NOT NULL AUTO_INCREMENT,
  `ztp_mark_desc` varchar(255) NOT NULL,
  PRIMARY KEY (`ztp_mark_list_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `ztp_mark_list`
--

INSERT INTO `ztp_mark_list` (`ztp_mark_list_id`, `ztp_mark_desc`) VALUES
(1, 'DNC'),
(2, 'ROBO call'),
(3, 'Call Riding'),
(4, 'Score Manipulation');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
