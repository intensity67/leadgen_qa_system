using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace Leadgen_qa_system
{
    #region Roster_raw
    public class Roster_raw
    {
        #region Member Variables
        protected int _login;
        protected string _emp_id;
        protected string _agent;
        protected string _tl;
        protected string _fs;
        protected string _wave;
        protected string _qa;
        protected int _agent_cluster_no;
        protected int _site_id;
        #endregion
        #region Constructors
        public Roster_raw() { }
        public Roster_raw(string emp_id, string agent, string tl, string fs, string wave, string qa, int agent_cluster_no, int site_id)
        {
            this._emp_id=emp_id;
            this._agent=agent;
            this._tl=tl;
            this._fs=fs;
            this._wave=wave;
            this._qa=qa;
            this._agent_cluster_no=agent_cluster_no;
            this._site_id=site_id;
        }
        #endregion
        #region Public Properties
        public virtual int Login
        {
            get {return _login;}
            set {_login=value;}
        }
        public virtual string Emp_id
        {
            get {return _emp_id;}
            set {_emp_id=value;}
        }
        public virtual string Agent
        {
            get {return _agent;}
            set {_agent=value;}
        }
        public virtual string Tl
        {
            get {return _tl;}
            set {_tl=value;}
        }
        public virtual string Fs
        {
            get {return _fs;}
            set {_fs=value;}
        }
        public virtual string Wave
        {
            get {return _wave;}
            set {_wave=value;}
        }
        public virtual string Qa
        {
            get {return _qa;}
            set {_qa=value;}
        }
        public virtual int Agent_cluster_no
        {
            get {return _agent_cluster_no;}
            set {_agent_cluster_no=value;}
        }
        public virtual int Site_id
        {
            get {return _site_id;}
            set {_site_id=value;}
        }
        #endregion
    }
    #endregion
}