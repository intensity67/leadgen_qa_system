<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('audio_directory'))
{
    function audio_directory($data) {

        // if ( $_SERVER['HTTP_HOST'] == 'localhost') 
        if (AUDIO_LOCATION_STATUS == 'REAL_TIME') 
        	
            $audio_directory = $data['location'];

        else 

            $audio_directory = base_url(AUDIO_DIRECTORY.$data['recording_id'].'.wav');

        return $audio_directory;
    }

}

?>
