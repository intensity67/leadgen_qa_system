<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('auth'))
{ 

	function auth($param) {
    
    if($param == 'user_id'){

      return $this->CI->session->userdata('login_info')['user_account_id'];

    }

    if($param == 'user_role'){
    
      return $this->CI->session->userdata('login_info')['user_roles_id'];

    }

  }
  
}

if ( ! function_exists('is_local'))
{

	function is_local() {

		$whitelist = array(
		    '127.0.0.1',
		    '::1'
		);

		if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
		
		    return false;
		
		}else {

			return true;

		}
	}

}

?>
