<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Recording extends MY_Controller  {

	public function __construct(){
 
			parent::__construct();

            // Your own constructor code
       

    }


  public function longcall_recording_list(){

        $data['page_title']         = "Long Call Recording Lists";
 
        $data['add_incident_filter']    = $this->get_add_incident_filters();

        $data['add_flag_call_filter']   = $this->get_add_flagged_call_filters();

        // *Query first local entry

        $recordings_cnt  =  $this->recording_model->check_long_recordings_cnt();

        // * Conditions if enough (recording logs > 10)
        
        if ($recordings_cnt <= 10){
        
            //* if not enough recordings query transfer recordings

            $query = $this->transfer_recordings("long_calls");
       
        }

        $data['recording_list']     = $this->recording_model->get_long_recordings_list();

        $data['call_type']          = "long_calls";

        $page = 'recording/recording_list';

        $this->page_render($page, $data);
 
  }


  public function shortcall_recording_list(){

        $data['page_title']                 = "Short Call Recording Lists";
  
        $data['add_incident_filter']    = $this->get_add_incident_filters();

        $data['add_flag_call_filter']   = $this->get_add_flagged_call_filters();

        // *Query first local entry

        $recordings_cnt  =  $this->recording_model->check_short_recordings_cnt();

        // * Conditions if enough (recording logs > 10)
        
        if ($recordings_cnt <= 10){
        
            //* if not enough recordings query transfer recordings

            $query = $this->transfer_recordings("short_calls");
       
        }

        $data['recording_list']     = $this->recording_model->get_short_recordings_list();

        $data['call_type']          = "short_calls";

        $page = 'recording/recording_list';

        $this->page_render($page, $data);
 
  }




  public function transfer_recordings($recording_type){
    
    $config_info = $this->config_model->get_configs();     

    if($recording_type == "short_calls"){
        
        $cluster_no = $config_info[SHORT_CALLS_CLUSTER_NO]['config_value'];

        $config_data['config_desc'] = 'shortcalls_next_cluster_no';
    
    }

    if($recording_type == "long_calls"){

        $cluster_no = $config_info[LONG_CALLS_CLUSTER_NO]['config_value'];

        // $cluster_no = $this->session->userdata('configs')['longcalls_next_cluster_no'];

        $config_data['config_desc'] = 'longcalls_next_cluster_no';
     

    }
    
    $cluster = $this->load->database('cluster'. $cluster_no, TRUE); 

    $cluster_recording_data = $this->recording_model->get_vici_recordings_cl($cluster, $recording_type);  

    $update_data['config_value'] = ($cluster_no % 4) + 1;
 
    $this->config_model->update_configs($config_data['config_desc'] , $update_data);
 
     // $this->modify_recordings_cluster_no($cluster_recording_data, $cluster_no);
    
    //* Add cluster_no field to cluster_recordings 

    $recordings_cnt = count($cluster_recording_data);


     
    for ($i = 0; $i < $recordings_cnt; $i++) {

        $cluster_recording_data[$i]['cluster_no'] = $cluster_no;

    }

    $this->recording_model->insert_recordings($cluster_recording_data);

  }


  public function search_recording_results(){

        $data['add_audit_data']['page_title']            = "Search Recording Lists";
 
        $data['add_audit_data']['users']                  = $this->users_model->get_users_lists();

        $data['add_audit_data']['qa_list']                = $this->users_model->get_auditor_lists();

        $data['add_audit_data']['gen_observational_list'] = $this->filters_model->get_general_observation_list();

        $data['add_audit_data']['soundboard_list']  = $this->filters_model->get_soundboard_voice_list();
  
        $data['add_incident_filter']                = $this->get_add_incident_filters();

        $data['add_flag_call_filter']               = $this->get_add_flagged_call_filters();

        // *Query first local entry

        $recordings_cnt                     = $this->recording_model->check_recordings_cnt();
            
        $search_filters['user']             = $this->input->post('agent_login_id');

        $data['search_filters']             = $search_filters;

        $data['recording_list']             = $this->recording_model->search_record_lists($search_filters);

        //* Query Agent Info Here

        $page = 'recording/search_recording_results';

        $this->page_render($page, $data);
 
  }

  public function trash_recording(){
 
    $recording_id       = $this->input->post('recording_id');
   
    $recording_log_data =  $this->recording_model->get_record_log_info($recording_id);

    if($this->input->post('state_reason_dropdown') == "Others"){

            $recording_log_data['reason_archive'] = $this->input->post('state_reason_tb'); 

    }else {

            $recording_log_data['reason_archive'] = $this->input->post('state_reason_dropdown');

    }

    $this->recording_model->insert_recording_archive($recording_log_data);

    $this->recording_model->trash_recording($recording_id);

    redirect($this->agent->referrer(), 'refresh');

  }

  public function clear_agent_recordings(){

    $filters['recording_log_status']    = "new";

    $filters['user'] =  $agent_login_id = $this->input->post('agent_login_id');

    $recordings_log_data =  $this->recording_model->get_recordings_filter($filters);

    foreach($recordings_log_data as $recording_logs_row){

        $recording_ids[]               = $recording_logs_row['recording_cluster_id'];

        $recording_logs_row['reason_archive'] = "Outdated";

    }


    $this->recording_model->insert_batch_recording_archives($recordings_log_data);

    $this->recording_model->trash_recordings($recording_ids);
    
    redirect("Recording/view_agents_recordings/".$agent_login_id);

  }

   public function clear_recordings(){

        $data['page_title']  = "Recordings";
        
        $page = 'recording/recordings';

        $call_type           = $this->input->post('call_type');

        $filters['recording_log_status']    = "new";

        if($call_type == "long_calls"){

             $filters['length_in_sec >=']   = 60;

        }

        if($call_type == "short_calls"){

             $filters['length_in_sec <']    = 60;

        }
        
        if($this->input->post('state_reason_dropdown') == "Others"){

            $reason_archive = $this->input->post('state_reason_tb'); 

        }else {

            $reason_archive = $this->input->post('state_reason_dropdown');

        }
    
        $recordings_log_data =  $this->recording_model->get_recordings_filter($filters);

        foreach($recordings_log_data as $recording_logs_row){

            $recording_ids[]               = $recording_logs_row['recording_cluster_id'];

            $recording_logs_row['reason_archive'] = $reason_archive;

        }

        $this->recording_model->insert_batch_recording_archives($recordings_log_data);

        $this->recording_model->trash_recordings($recording_ids);

        $this->load->library('user_agent');

        if($call_type == 'long_calls')

            redirect("longcall_recording_list");
        
        if($call_type == 'short_calls')

            redirect("shortcall_recording_list");

   }


  public function agents_search(){

        $data['page_title']             = "Agents Search";

        $data['disposition_list']       = $this->filters_model->get_vici_dispo_list();

        $page = 'recording/agents_search_form';

        $this->page_render($page, $data);
 

  }

  public function qa_group_recordings(){

    $data['page_title'] = "Q.A. Group Recordings";

    //* List Q.As

    $page = 'recording/qa_group_recordings';

    $this->page_render($page, $data);
 

  }

  public function sme_group_recordings(){

    //* Gets SME IDs
    
    // Gets all sme groups with q.a. field to be displayed
     
 
    if($this->session->userdata("login_info")['user_roles_id'] == ADMIN){
            
        $data['sme_lists']   = $this->recording_model->get_sme_group_recordings();

    }

    if($this->session->userdata("login_info")['user_roles_id'] == QUALITY_ASSURANCE){

        $data['sme_lists']   = $this->recording_model->get_sme_group_recordings($this->session->userdata("login_info")['acct_nick_name']);

    }
    
    $i = 0;

    foreach($data['sme_lists'] as $sme_list_row){

        $filters['fs'] = $sme_list_row['fs'];
        
        $sme = $this->audit_model->get_audit_lists_cnt($filters);

    }

    //* List of SMEs handled by Q.A.

    $page = 'recording/sme_group_recordings';

    // $data['sme_lists']   = $this->rosters_model->get_rosters_fs();

    $this->page_render($page, $data);

  }


  public function view_fs_agents_recordings($fs = ''){
      
      $data['page_title'] = "View Floor Support - Agents Recordings";
 
      $filter['roster_agents.fs']       = $this->input->post('fs');

      if(isset($fs) && !empty($fs))

          $filter['roster_agents.fs']   = $fs;
        

      if($this->input->post('fs') === NULL)

        $fs = NULL;
        
      $agents             = $this->rosters_model->get_agents_audits($filter);
      
      for($i = 0; !empty($agents[$i]); $i++){
        
         $agents[$i]['audit_info'] = $this->rosters_model->get_agents_last_audit($agents[$i]['login']);

      }

      $data['agents']     = $agents;

      $data['fs']         = $filter['roster_agents.fs'];

      $page               = 'recording/view_fs_agents_recordings';

      $this->page_render($page, $data);
 
  }

  public function view_agents_recordings($agent_login_id = ''){

    $data['page_title'] = "View Agents Recordings";
     
    $data['add_incident_filter']    = $this->get_add_incident_filters();

    $data['add_flag_call_filter']   = $this->get_add_flagged_call_filters();
    
    $agent_login    = ($agent_login_id === NULL) ? $this->input->post('agent_login_id') : $agent_login_id;
    
    $agent_roster_info = $this->rosters_model->get_agents_roster_info($agent_login);

    $agent_name     = $agent_roster_info['agent'];

    $cluster_no     = $agent_roster_info['agent_cluster_no'];

    // * Default Config Dispositions

    $call_type = "long_calls";

    $filters['status']                  =   "NI"; 
    $filters['recording_log_status']    =   "new"; 

    //* Query First Local Entry

    $data['agent_login']    =   $agent_login;

    $data['agent_name']     =   $agent_name;
 
    $data['recording_list'] = $this->recording_model->get_agents_recording_list($agent_login, $filters, $call_type);

    $recordings_cnt  = count($data['recording_list']);


    // * Conditions if enough (recording logs > 10)

    if ($recordings_cnt < 1){
        
        //* If not enough recordings query transfer recordings

        $query = $this->transfer_agent_recordings($agent_login, $cluster_no);

        $data['recording_list'] = $this->recording_model->get_agents_recording_list($agent_login, $filters, $call_type);

    }

    $page               = 'recording/view_agents_recordings';

    $this->page_render($page, $data);
 
  }

  public function transfer_agent_recordings($agent_login_id, $cluster_no = ''){
    

    //* 3rd Paramater should be the configuration default dispo of the Q.A. team
    
    $recording_type = "long_calls";

    $filter_dispo   = "NI";


    if($cluster_no == 0 ){
 
    for($i = 1; $i < 3; $i++){

        //* Query Agent's Recordings to each Clusters

        // $database = 'cluster'. $cluster_no + ($i % 4) + 1;
       
        $database = 'cluster'. $i;

        $cluster = $this->load->database($database, TRUE); 


        //* Soon to be replaced by search

        $cluster_recording_data = $this->recording_model->get_vici_recordings_agents($cluster, $recording_type, $filter_dispo, $agent_login_id, 5);  

         
        if(!empty($cluster_recording_data)){
            
            $cluster_no = $i;

            break;
        }

     }

    }else {

        $database = 'cluster'. $cluster_no;
        
        $cluster = $this->load->database($database, TRUE); 

        $cluster_recording_data = $this->recording_model->get_vici_recordings_agents($cluster, $recording_type, $filter_dispo, $agent_login_id, 5);  
 
    }
    
    $recordings_cnt = count($cluster_recording_data);

     //* If there are recordings in the agent
    
    if($recordings_cnt >= 1){

        $this->rosters_model->update_roster_cluster($cluster_no, $agent_login_id);

        //* Add Cluster_no field to cluster_recordings 

        for ($i = 0; $i < $recordings_cnt; $i++) {

            $cluster_recording_data[$i]['cluster_no'] = $cluster_no;

        }
        
        
        $this->recording_model->insert_recordings($cluster_recording_data);
       
    } else {

        $this->session->set_flashdata('error_recordings_query', 1);
        
    }

  }

  public function agents_search_results(){

        $data['page_title']             = "Agents Search";

        //$search_filters['agent_name']  = $this->input->post('agent_name');

        $search_filters['loginId']      = $this->input->post('agent_login_id');
        $search_filters['limit']        = $this->input->post('no_calls');
        $search_filters['dispo']        = $this->input->post('status');
        //$search_filters['call_type']    = $this->input->post('call_type');
        $cluster_no                     = $this->input->post('cluster');


        //* Cluster should be automated according to login id
    
        $cluster                        = $this->load->database('cluster'. $cluster_no, TRUE);
        
        // * Search 

        $data['recording_list']         = $this->recording_model->search_recording_in_clusters($cluster, $search_filters);

        $data['search_filters']         = $search_filters;

        $page = 'recording/search_recording_results';

        $this->page_render($page, $data);
 
  }

  public function recordings(){

        $data['page_title']         = "Recordings";
      
        // $data['recording_list']         = $this->recording_model->search_recording_in_clusters($cluster, $search_filters);

        // $data['search_filters']         = $search_filters;

        $agent_login_id             = $this->input->post('agent_login_id');


        $page = 'recording/recordings';

        $this->page_render($page, $data);
 
  }

  public function view_recent_agents_recordings($sme_id = ''){



  }

  public function retrieve_term_reason($cluster_no){

    //* Get All IDs in the specific cluster

    //* Query All recording ids in the specific cluster fields : recording_id and term_reason

    //* Update All Recordings with the group recordings


  }

}
