<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Rosters extends MY_Controller  {

  public function list_rosters(){

      $data['page_title']       = "Roster Lists";

      $page                     = 'rosters/list/list_rosters';
      
      $data['roster_list']        = $this->rosters_model->get_rosters_lists();
        
      $data['add_roster_data']['team_lead_list']  = $this->rosters_model->get_rosters_tl();

      $data['add_roster_data']['fs_list']        = $this->rosters_model->get_rosters_fs();

      // $data['add_roster_data']['supervisor_list']  = $this->users_model->get_supervisor_lists();

      $data['add_roster_data']['wave_list']        = $this->filters_model->get_wave_list();
 
      $this->page_render($page, $data);

  }
  
  public function list_teamleads(){

      $data['page_title']       = "Team Leads Lists";

      $page                     = 'rosters/list/list_teamleads';
      
      $data['team_lead_list']   = $this->rosters_model->get_rosters_tl();

      $data['add_roster_data']['team_lead_list']  = $this->users_model->get_tl_lists();
  
      $this->page_render($page, $data);

  }

  public function list_agents(){

      $data['page_title']       = "Agents Lists";

      $page                     = 'rosters/list/list_agents';
      
      $data['agents_list']      = $this->rosters_model->get_agents();
  
      $this->page_render($page, $data);

  }

  public function list_fs(){

      $data['page_title']       = "List of Floor Support";

      $page                     = 'rosters/list/list_fs';
      
      $data['fs_list']          = $this->rosters_model->get_rosters_fs();

      $this->page_render($page, $data);

  }


  public function list_qa(){

      $data['page_title']       = "QA Team";

      $page                     = 'rosters/list/list_qa';
      
      $data['fs_list']          = $this->rosters_model->get_rosters_qa();

      $this->page_render($page, $data);

  }

  public function list_wave(){

      $data['page_title']       = "QA Team";

      $page                     = 'rosters/list/list_wave';

      $data['fs_list']          = $this->rosters_model->get_rosters_wave();

      $this->page_render($page, $data);

  }


  public function shuffle_rosters(){

      $data['page_title']       = "Users Lists";

      $page                     = 'rosters/shuffle_rosters';
      
      $data['roster_list']        = $this->rosters_model->get_rosters_lists();

      $data['add_roster_data']['team_lead_list']  = $this->users_model->get_tl_lists();

      $data['add_roster_data']['supervisor_list']  = $this->users_model->get_supervisor_lists();

      $data['add_roster_data']['wave_list']        = $this->filters_model->get_wave_list();
 
      $this->page_render($page, $data);

  }
  
    public function insert_roster(){

    // * insert for the roster raw info

    $roster_data['agent']      = $this->input->post('agent');
    $roster_data['login']   = $this->input->post('login1');
    $roster_data['wave']       = $this->input->post('wave1');
    $roster_data['fs']         = $this->input->post('fs');
    $roster_data['tl']         = $this->input->post('tl');
    
    $this->rosters_model->insert_roster_raw($roster_data);

    // * check if 2 accounts are on the agent roster and insert the second login information

    if($this->input->post('two_accts')){
 
      $roster_data['login']   = $this->input->post('login2');

      $roster_data['wave']       = $this->input->post('wave2');

    }

    $this->rosters_model->insert_roster_raw($roster_data);

    redirect('Rosters/list_rosters');

  }

    public function edit_rosters($agent_login_id){

    $data['page_title']   = "Users Lists";

    $page                 = 'rosters/edit/edit_roster_agents_form';
       
    $data['roster_info']  = $this->rosters_model->get_rosters_info($agent_login_id);
    
    $this->page_render($page, $data);

  }

    public function view_teamlead_agents($teamlead){
      
      $data['page_title'] = "View Team Lead Agents";

      $filter['tl']       = $teamlead;

      $agents             = $this->rosters_model->get_agents_filter($filter);
      
      $data['agents']     = $agents;

      $data['tl']         = $teamlead;

      $data['reassign_agent_data']['current_tl']  = $teamlead;

      $data['reassign_agent_data']['fs_list']     = $this->rosters_model->get_rosters_fs();

      $data['reassign_agent_data']['tl_list']     = $this->rosters_model->get_rosters_tl();

      $page               = 'rosters/view/view_teamlead_agents';

      $this->page_render($page, $data);
 
  }    


  public function update_roster(){

    $login_id                               =  $this->input->post("login_id");

    $update_roster_data['emp_id']           = $this->input->post("emp_id");
    $update_roster_data['agent']            = $this->input->post("agent");
    $update_roster_data['tl']               = $this->input->post("tl");
    $update_roster_data['fs']               = $this->input->post("fs");
    $update_roster_data['wave']             = $this->input->post("wave");
    $update_roster_data['qa']               = $this->input->post("qa");
    $update_roster_data['agent_cluster_no'] = $this->input->post("agent_cluster_no");




 
    $user_id = $this->rosters_model->update_roster($update_roster_data, $login_id);

    redirect('Rosters/list_rosters/'. $user_id);

  }

  public function update_tl_agents($teamlead){

 
      $data['page_title'] = "Update Team Lead Agents";

      $filter['tl']       = $teamlead;

      $agents             = $this->rosters_model->get_agents_filter($filter);
      
      $data['agents']     = $agents;

      $data['tl']         = $teamlead;

      $data['reassign_agent_data']['current_tl']  = $teamlead;

      $data['reassign_agent_data']['fs_list']     = $this->rosters_model->get_rosters_fs();

      $data['reassign_agent_data']['tl_list']     = $this->rosters_model->get_rosters_tl();
  
      $data['current_tl']                         = $teamlead;

      $page               = 'rosters/update/update_tl_agents';

      $this->page_render($page, $data);

  }

  public function view_fs_agents(){
      
      $data['page_title'] = "View Floor Support - Agents";

      $filter['fs']       = $this->input->post('fs');
      
      if($this->input->post('fs') === NULL)
          $fs = NULL;
        
      $agents             = $this->rosters_model->get_agents_filter($filter);
      
      $data['agents']     = $agents;

      $data['fs']         = $filter['fs'];

      $page               = 'rosters/view/view_fs_agents';

      $this->page_render($page, $data);
 
  }

  public function clusters_listings($cluster_no = ''){

      if(isset($cluster_no) && empty($cluster_no)){

        $page             = 'rosters/list/cluster_selection';

        $this->page_render($page);
        
       }

      else {

        $data['page_title'] = "List of Users- Cluster ". $cluster_no;

        $data['cluster_no'] = $cluster_no;
    
        $cluster = $this->load->database('cluster'. $cluster_no, TRUE); 

        $page             = 'rosters/list/list_cluster';

        $data['agents']   = $this->rosters_model->get_agents_cluster($cluster);
   

        $this->page_render($page, $data);

      }

  }


}