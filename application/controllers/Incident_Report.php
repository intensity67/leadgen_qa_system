<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Incident_Report extends MY_Controller  {

 	public function __construct(){
 
		parent::__construct();

            // Your own constructor code
       
     }

	public function incident_reports(){

        $data['page_title']         = "Incident Reports";
 
        $data['incident_reports']   = $this->incident_model->get_incident_report_list();
       
        $page = 'incident_report/incident_reports';
        
        $data['add_incident_filter'] = $this->get_add_incident_filters();

        $this->page_render($page, $data);
        
 
	}


	public function my_incident_reports(){

        $data['page_title']         = "Incident Reports";

        $data['incident_reports']   = $this->incident_model->get_incident_report_list();
       
        $page = 'incident_report/incident_reports';
        

        $this->page_render($page, $data);
        
 
	}


	public function add_incident_report($audit_id, $source){

        $data  = $this->get_add_incident_filters();

        $data['page_title']         = "Add Incident Repors";

        $data['audit_info']			= $audit_info = $this->audit_model->get_audit_info($audit_id);

        $data['source']				= $source;

        $page = 'incident_report/add_incident_report_form';
        
        $this->page_render($page, $data);
	}

	public function edit_incident_report($audit_id, $source){

        $data  = $this->get_add_incident_filters();

        $data['page_title']         = "Add Incident Repors";

        $data['audit_info']			= $audit_info = $this->audit_model->get_audit_info($audit_id);

        $data['source']				= $source;

        $page = 'incident_report/add_incident_report_form';
        
        $this->page_render($page, $data);
	}

	public function view_incident_report($incident_report_id){

        $data  = $this->get_add_incident_filters();

        $data['page_title']         = "Add Incident Reports";

        $data['ir_info']			= $this->incident_model->get_incident_report_info($incident_report_id);
 		
		$page = 'incident_report/view_incident_report';
        
        $this->page_render($page, $data);
	}

	public function insert_incident_report(){


      	$incident_report_data['source']				=	$this->input->post('source');
      	$incident_report_data['audit_id']			=	$this->input->post('audit_id');
      	$incident_report_data['agent_login_id']		=	$this->input->post('agent_login_id');
      	$incident_report_data['phone_number']		=	$this->input->post('phone_number');
       	$incident_report_data['sanction_id']		=	$this->input->post('sanction_id');
      	$incident_report_data['auditor_id']			=	$this->input->post('auditor_user_id');
      	$incident_report_data['program_id']			=	$this->input->post('program_list');
      	$incident_report_data['evaluation_date']	=	$this->input->post('evaluation_date');
      	$incident_report_data['disposition_id']		=	$this->input->post('vici_dispo_id');
      	$incident_report_data['call_synopsis']		=	$this->input->post('call_synopsis');

      	$incident_report_data['call_date']			=	$this->input->post('call_date');

      	$incident_report_data['offense_type']		=	$this->input->post('offense_type');

      	$incident_report_data['offense_id']			=	$this->input->post('offense_id');
      	$incident_report_data['team_lead_email']	=	$this->input->post('team_lead_email');

      	$incident_report_data['team_lead']			=	$this->input->post('team_lead');

        $incident_report_id   						= 	$this->incident_model->insert_incident_report($incident_report_data);

        $incident_report_info 						= $this->incident_model->get_incident_report_info($incident_report_id, $incident_report_data['offense_type']);

        $update_audit_data['incident_report_id']	= 	$incident_report_id;

      	$incident_report_data['incident_report_id']	=	$this->input->post('incident_report_id');

		$incident_report_data['team_lead_email']	=	$this->input->post('team_lead_email');

        //* Update the audit's incident report id field to mark audit as already filled with incident.

        $this->audit_model->update_audit($update_audit_data, $incident_report_data['audit_id']);
 

	    if(!is_local())

	        $this->sendmail_live($incident_report_info);

		else

	        $this->incident_report_sendmail($incident_report_info);
 
        redirect("Incident_Report/incident_reports");

 	}

	public function update_incident_report(){


      	$incident_report_data['source']				=	$this->input->post('source');
      	$incident_report_data['audit_id']			=	$this->input->post('audit_id');
      	$incident_report_data['agent_login_id']		=	$this->input->post('agent_login_id');
      	$incident_report_data['phone_number']		=	$this->input->post('phone_number');
       	$incident_report_data['sanction_id']		=	$this->input->post('sanction_id');
      	$incident_report_data['auditor_id']			=	$this->input->post('auditor_user_id');
      	$incident_report_data['program_id']			=	$this->input->post('program_list');
      	$incident_report_data['evaluation_date']	=	$this->input->post('evaluation_date');
      	$incident_report_data['disposition_id']		=	$this->input->post('vici_dispo_id');
      	$incident_report_data['call_synopsis']		=	$this->input->post('call_synopsis');
      	$incident_report_data['team_lead']			=	$this->input->post('team_lead');

      	$incident_report_data['offense_type']		=	$this->input->post('offense_type');

      	$incident_report_data['offense_id']			=	$this->input->post('offense_id');

        $incident_report_id   						= 	$this->incident_model->insert_incident_report($incident_report_data);

        $update_audit_data['incident_report_id']	= 	$incident_report_id;
      	$incident_report_data['incident_report_id']			=	$this->input->post('incident_report_id');


        //* Update the audit's incident report id field to mark audit as already filled with incident.

        $this->audit_model->update_audit($update_audit_data, $incident_report_data['audit_id']);

        redirect("Incident_Report/incident_reports");

	}

	public function incident_report_sendmail($incident_report_info){
 
		$this->load->library('email');

		// Send Email

 		$this->email->set_mailtype("html");

		$headers =  "MIME-Version: 1.0" . "\r\n"; 

		$headers .= "From: ". $row['auditor']. " <quality@digicononline@gmail.com>" .  "\r\n";

		$headers .= "Content-type: text/html; charset=iso-8859-1" . "\r\n"; 

		$subject = "Incident Report";

		 // q.a. team

		$recipients = array( 

		  $incident_report_info['team_lead_email'],
		  'quality@digicononline.com'
		
		);
 

		$this->email->from('quality@digicononline.com', 'Q.A. Department');
		
		$this->email->to($recipients);
 
		//* Send Mail to Team Lead

		$sendmail_message = $this->load->view("incident_report/sendmail/incident_report_mail", 
								array('incident_report_data' => $incident_report_info), TRUE);

		$this->email->subject('Incident Report');

		$this->email->message($sendmail_message);
		
		$r 	=	$this->email->send();

		if($r){

			echo 'email sent';
  			
		} else {

		  $this->email->print_debugger();

		}
	}

 	public function sendmail_live($incident_report_data){

		require '/usr/share/php/libphp-phpmailer/class.smtp.php';
		require '/usr/share/php/libphp-phpmailer/class.phpmailer.php';

		$mail = new PHPMailer;

 		$mail->setFrom('quality@digicononline.com');

		$mail->addAddress($incident_report_data['team_lead_email']);
		
		$mail->addAddress('quality@digicononline.com');
 
		$mail->Subject = 'Incident Report';

		$mail->Body = '<h2> Incident Report </p>';
		
		$sendmail_message = $this->load->view("incident_report/sendmail/incident_report_mail",
								array('incident_report_data' => $incident_report_data), TRUE);

		$mail->Body = $sendmail_message;

		$mail->isHTML(true);
  
		$mail->IsSMTP();

		$mail->SMTPSecure = 'ssl';

		$mail->Host = 'smtp.gmail.com';

		$mail->SMTPAuth = true;

		$mail->Port = 465;

		$mail->Username = 'intensitystore67@gmail.com';

		$mail->Password = 'jetaime123619123';

		if(!$mail->send()) {

		  echo 'Email is not sent.';

		  echo 'Email error: ' . $mail->ErrorInfo;

		} else {

		  echo 'Email has been sent.';

		}

	}
}