<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Users extends MY_Controller  {


	public function __construct(){
 
			parent::__construct();

            // Your own constructor code
       
      $this->session->set_userdata('account_id', 1);

    }
       
	public function list_users(){

      $data['page_title']      = "Users Lists";

      $page                    = 'users/users_lists';
      
      $data['user_list']       = $this->users_model->get_users_lists();

      $data['user_roles']      = $this->users_model->get_user_roles();

      $this->page_render($page, $data);

    }

    public function add_user(){

      $data['page_title']      = "Users Lists";

      $page                    = 'users/add_users-form';
      
      $data['user_roles']      = $this->users_model->get_user_roles();

      $this->page_render($page, $data);

    }

    public function insert_user(){

    $user_id = $this->users_model->insert_user($this->input->post());

    redirect('Admin/list_users/'. $user_id);

  }




}
