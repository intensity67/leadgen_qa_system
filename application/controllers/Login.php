<?php

class Login extends CI_Controller  {


	public function __construct(){
 
			parent::__construct();
  
    }
      
	public function index(){
		
		$this->load->view('signin_form');

	}

	// Ajax Login Check 

	public function login_check(){

		$username 		= $this->input->post('username');

		$password 		= md5($this->input->post('password'));

		$login_result   = $this->users_model->get_users_info($username, $password);

		// indicates valid login... 

		if( !empty($login_result) ) {

			$session_data['login_info']		=	$login_result;
			
			$session_data['login_role']		=  	$this->users_model->get_user_roles_info($session_data['login_info']['user_roles_id']);
  
			$this->session->set_userdata($session_data);
 
			if( $login_result['acct_type_id'] == ADMIN){

 				redirect("Dashboard/");

			}

			if( $login_result['acct_type_id'] == QUALITY_ASSURANCE){
  
				redirect('Recording/sme_group_recordings');
			
			}

			redirect('Dashboard/');

		} else {

			$this->session->set_userdata("error_login", 1);

			redirect("Login/");

		}

	}


	public function logout(){
 		
 		$this->session->unset_userdata('account_session');

		redirect("/");

	}

}

?>