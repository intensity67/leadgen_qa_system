<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Audit extends MY_Controller  {

	public function __construct(){
 
		parent::__construct();

            // Your own constructor code
	}

  
  public function audit_list(){

        $data['page_title']             = "Audit Lists";
 
        $data['disposition_list']       = $this->filters_model->get_vici_dispo_list();

        $data['users']                  = $this->users_model->get_users_lists();

        $data['qa_list']                = $this->users_model->get_auditor_lists();

        $data['audit_list']             = $this->audit_model->get_audit_lists();
	 	        
        $data['gen_observational_list'] = $this->filters_model->get_general_observation_list();

        $data['program_list'] 			= $this->filters_model->get_program_list();

        $data['add_incident_filter']    = $this->get_add_incident_filters();

        $page = 'audit/audit_list';

        // $this->page_render($page, $data);
        
 	    $this->load->view('layout/header');

    	$this->load->view($page, $data);
         
    	$this->load->view('layout/footer');
  }

  public function my_audit_list(){

        $data['page_title']             = "Audit Lists";
 
        $data['disposition_list']       = $this->filters_model->get_vici_dispo_list();

        $data['users']                  = $this->users_model->get_users_lists();

        $data['qa_list']                = $this->users_model->get_auditor_lists();

        $data['audit_list']             = $this->audit_model->get_audit_lists_by_auditor($this->user_account_id);
        
        $data['gen_observational_list'] = $this->filters_model->get_general_observation_list();

        $data['program_list'] 			= $this->filters_model->get_program_list();

        $data['add_incident_filter']    = $this->get_add_incident_filters();

        $page = 'audit/audit_list';

        $this->page_render($page, $data);
        
  }

	public function add_audit_long_call_form($recording_id, $i){

	    $data['page_title']         	= "Add Audit";

	    $page                       	= 'audit/add_audit_long_call_form';
	    
	    $data                       	= $this->recording_model->get_record_info($recording_id);
 
	    $data['audio']              	= audio_directory($data); 

	    $data['recording_id']      		= $recording_id;
	  
	    $data['script_list']        	= $this->script_model->get_script_lists();

	    $data['disposition']        	= $this->filters_model->get_vici_dispo_list();

	    $data['issue_comments']        	= $this->filters_model->get_issue_comments();

	    $data['users']              	= $this->users_model->get_users_lists();

	    $data['qa_list']            	= $this->users_model->get_auditor_lists();
	    
	    $data['agents']             	= $this->users_model->get_agent_lists();

 	    $data['gen_observational_list'] = $this->filters_model->get_general_observation_list();

	    $data['soundboard_list']		= $this->filters_model->get_soundboard_voice_list();

	    $data['cnt']                    = $i;
 
 	    $this->load->view('layout/header');

    	$this->load->view($page, $data);
         
    	$this->load->view('layout/footer');

 
	}  


	public function edit_audit($audit_id){

	    $data['page_title']         = "Edit Audit";

		$data['audit_info']			= $this->audit_model->get_audit_info($audit_id);
	   
	    $data['audio']				= audio_directory($data['audit_info']); 

	    //* $data['recording_id']       = $recording_id;
	  
	    $data['script_list']        = $this->script_model->get_script_lists();

	    $data['disposition']        = $this->filters_model->get_vici_dispo_list();

	    $data['users']              = $this->users_model->get_users_lists();

	    $data['qa_list']            = $this->users_model->get_auditor_lists();

	    $data['issue_comments']        	= $this->filters_model->get_issue_comments();

	    $data['agents']             	= $this->users_model->get_agent_lists();

 	    $data['gen_observational_list'] = $this->filters_model->get_general_observation_list();

	    $data['soundboard_list'] 		= $this->filters_model->get_soundboard_voice_list();

		$data['soundboard_list'] 		= $this->filters_model->get_soundboard_voice_list();
	   
	    $data['audit_scripts']     		= $this->audit_model->get_audit_scripts($audit_id);
	 
	   	$audit_issue_comments 			= $this->audit_model->get_audit_issue_comments($audit_id);
	    
	    $data['audit_issue_comments'] 	=	array();

   		foreach($audit_issue_comments as $audit_issue_comments_row){

	  		$data['audit_issue_comments'][] = $audit_issue_comments_row['issue_comment_id'];

  		}

 	    $page = 'audit/edit_audit_long_call_form';

	    $this->page_render($page, $data);

	}

	public function view_audit($audit_id){

	    $page = 'audit/view_audit';

	    $data['audit_info']     	= $this->audit_model->get_audit_info($audit_id);

	    $data['audio']				= audio_directory($data['audit_info']); 

	    $data['audit_scripts']     	= $this->audit_model->get_audit_scripts($audit_id);
 
 	    $this->page_render($page, $data);

	}

	public function insert_audit(){

 	 //* Call Info

	    $audit_data['recording_id']         	= 	$this->input->post('recording_id');

	    $audit_data['audit_timestart']			= 	$this->input->post('audit_timestart');

	    $audit_data['audit_timeend']			= 	date("Y-m-d H:i:s");

	    $audit_data['work_week']            	= 	$this->input->post('work_week');

	    $audit_data['lead_id']         			= 	$this->input->post('lead_id');
	    $audit_data['phone_number']         	= 	$this->input->post('phone_number');
	    $audit_data['agent_login_id']       	= 	$this->input->post('agent_user_id');
	    $audit_data['agent_name']           	= 	$this->input->post('agent_name');
	    $audit_data['call_timestamp']			= 	$this->input->post('call_timestamp');
	    $audit_data['cluster_no']				= 	$this->input->post('cluster_no');
	    $audit_data['recording_location']  		= 	$this->input->post('recording_location');

	    $audit_data['agent_disposition']  		= 	$this->input->post('agent_disposition');
	
	    $audit_data['correct_disposition_id']  	= 	$this->input->post('correct_disposition_id');

	    $audit_data['mark_status']        		= 	$this->input->post('mark_status');

	    $audit_data['incorrect_response']       = 	$this->input->post('incorrect_response');
	    
	    $audit_data['soundboard_voice_id']  	= 	$this->input->post('soundboard_voice_id');

	    $audit_data['prospect_tone']  			= 	$this->input->post('prospect_tone');

	    $audit_data['agent_issue']  			=	$this->input->post('agent_issue');

	    $audit_data['general_observation_id']  	= 	$this->input->post('general_observation_id');

	    $audit_data['fs']  						= 	$this->input->post('fs');

	    $audit_data['tl']  						= 	$this->input->post('tl');

	    $audit_data['qa_remarks']  				= 	$this->input->post('qa_remarks');
	    
	    $audit_data['cluster_no']  				= 	$this->input->post('cluster_no');

	    $audit_data['recording_cluster_id']		= 	$this->input->post('recording_cluster_id');

	    $audit_data['audit_timestamp']			= 	date("Y-m-d H:i:s");

	    $audit_data['auditor_id']  				= 	$this->user_account_id;

 	    $audit_data['audit_type']				= 	$this->input->post('audit_type');

	    $audit_data['issue_comment_id']			= 	$this->input->post('issue_comment_id');

	    $audit_id								= 	$this->audit_model->insert_internal_audit($audit_data);
 	 	
 	 	//* Update Recording Data 

	 	$update_recording_data['recording_log_status']	=	'audited';

	 	$update_recording_data['cluster_no']			=	$audit_data['cluster_no'];


	 	$update_recording_data['recording_cluster_id']	=	$audit_data['recording_cluster_id']	;
	 	
	 	$recording_cluster_id							=	$audit_data['recording_cluster_id'];

	    $this->recording_model->update_recording($update_recording_data, $recording_cluster_id);

	    $script_list                    				= 	$this->input->post('script_list_id');

	    $audit_script_data['audit_id']					=	$audit_id;

	    $i = 0;

	    $issue_comments = $this->input->post('issue_comments[]');

	  //* Audit Script Info
	  
	    foreach ($script_list  as $audit_script_id){

	      $audit_script_data['script_lists_id']  		 	= $audit_script_id;

	      $audit_script_data['agent_acknowledgement'] 		= $this->input->post('acknowledgement_'.$i);

	      $audit_script_data['customer_statement'] 			= $this->input->post('statement')[$i];  

	      $audit_script_data['agent_accuracy_speed']        = $this->input->post('speed')[$i];

	      $audit_script_data['agent_accuracy_response']     = $this->input->post('agent_response')[$i];   

	      $audit_script_data['agent_accuracy_cor_response'] = $this->input->post('correct_response')[$i];

	      $audit_script_data['info_cust_details'] 	= $this->input->post('custom_details')[$i];

	      $audit_script_data['info_agent_input']  	= $this->input->post('agent_input')[$i];
	      	
	      	if(!empty($audit_script_data['info_call_ends']))

		      $audit_script_data['info_call_ends']  	= $this->input->post('call_ends')[$i];

	      $audit_script_data['comment']         	= $this->input->post('comment')[$i];


	      $i++;
	      
	      $audit_script_id         = $this->audit_model->insert_internal_audit_scripts($audit_script_data);
	    
	    }

	    $i = 0;

	  //* Issue Comment Checkbox
 
	    foreach ($issue_comments  as $issue_comments_id){

	    	$issue_comment_data['audit_id'] 		= $audit_id;

	    	$issue_comment_data['issue_comment_id'] = $issue_comments_id;

	    	$this->audit_model->insert_audit_issue_comments($issue_comment_data);

	    }

	    // print_r($audit_script_data);

	    redirect('Audit/audit_list');

	}

	public function update_audit(){

	    $update_audit_data['soundboard_voice_id']	=	$this->input->post('soundboard_voice_id');
	    
	    $update_audit_data['correct_disposition_id']	=	$this->input->post('correct_disposition_id');

	    $update_audit_data['mark_status']			=	$this->input->post('mark_status');

	    $update_audit_data['agent_issue']			=	$this->input->post('agent_issue');

	    $update_audit_data['incorrect_response']	=	$this->input->post('incorrect_response');
	    
	    $update_audit_data['prospect_tone']  		= 	$this->input->post('prospect_tone');

	    $update_audit_data['general_observation_id'] =	$this->input->post('general_observation_id');

	    $update_audit_data['qa_remarks']			=	$this->input->post('qa_remarks');
 
	    $audit_id									= 	$this->input->post('audit_id');
  	  
  	    $update_audit_data['issue_comment_id']		= 	$this->input->post('issue_comment_id');

	    $this->audit_model->update_audit($update_audit_data, $audit_id);

	    $script_list                   	 			= $this->input->post('script_list_id');

	    $audit_script_data['audit_id']  			= $audit_id;

	    $data['audit_info']     					= $this->audit_model->get_audit_info($audit_id);

	  //* Audit Script Info

	    $i = 0;
	  
	    foreach ($script_list  as $audit_script_id){

	      $audit_script_data['script_lists_id']   			= $audit_script_id;

	      $audit_script_data['agent_acknowledgement'] 		= $this->input->post('acknowledgement')[$i];

	      $audit_script_data['customer_statement'] 			= $this->input->post('statement')[$i];  

	      $audit_script_data['agent_accuracy_speed']        = $this->input->post('speed')[$i];

	      $audit_script_data['agent_accuracy_response']     = $this->input->post('agent_response')[$i];   

	      $audit_script_data['agent_accuracy_cor_response'] = $this->input->post('correct_response')[$i];

	      $audit_script_data['info_cust_details'] 			= $this->input->post('custom_details')[$i];

	      $audit_script_data['info_agent_input']  			= $this->input->post('agent_input')[$i];
 
	      	if(!empty($audit_script_data['info_call_ends']))

		      $audit_script_data['info_call_ends']  		= $this->input->post('call_ends')[$i];

	      $audit_script_data['comment']          			= $this->input->post('comment')[$i];


	      $i++;
	      
		  $this->audit_model->update_audit_scripts($audit_script_data, $audit_id, $audit_script_id );
	    
	    }

		redirect("Audit/audit_list");
	}
	  
	public function script_audit($audit_id){

	    $data['audit_info']     = $this->audit_model->get_audit_info($audit_id);
	    $data['script_list']	= $this->script_model->get_script_lists();
	    
	     $page = 'audits/script_audit';

	    //print_r($data);

	    $this->page_render($page, $data);
	        
	 }

	 public function delete_audit($audit_id){

		$this->audit_model->delete_internal_audit($audit_id);
		
		redirect("Audit/audit_list");

	 }
	 

	public function export_audits($filters = ''){
	
	//* File Name 

		$date_from = $this->input->post('date_from');
	
		$date_to = $this->input->post('date_to');

		$audit_data = $this->audit_model->get_audit_lists_export($filters, $date_from, $date_to);


		
		$filename = 'audits_'.date('Ymd').'.csv'; 

 		header("Content-Description: File Transfer"); 

		header("Content-Disposition: attachment; filename=$filename"); 
		
		header("Content-Type: application/csv; ");


		//* Creating Files 

		$file = fopen('php://output', 'w');
		 
		$header = $this->export_header();		

		$script_lists = $this->filters_model->get_script_list();

		foreach($script_lists as $script_lists_row){

			$header_scripts = $this->export_header_scripts($script_lists_row['script_list_code']);
			
			$header = array_merge($header, $header_scripts);

		}

 		//* $scripts_list = $this->filters_model->get_script_list();

  		$i = 0;

		foreach($audit_data as $audit_data_row){

			$audit_scripts = $this->audit_model->get_audit_scripts($audit_data_row['audit_id']);
			$audit_scripts_cnt = count($audit_scripts);
 
 			//array_search( , $audit_scripts)

			$script_lists = $this->filters_model->get_script_list();

			// foreach($script_lists as $script_lists_row){

			// 		= $audit_scripts[$script_lists_row['script_lists_id']];
			
			// }

 			//* Segregate Arrays in Audit Scripts

			for($j = 0; $j < $audit_scripts_cnt; $j++){

				$audit_scripts_data[$i]['customer_statement']['customer_statement'. $audit_scripts[$j]['script_list_code']] = $audit_scripts[$j]['customer_statement'];

				$audit_scripts_data[$i]['agent_accuracy_response']['agent_acknowledgement'. $audit_scripts[$j]['script_list_code']] = $audit_scripts[$j]['agent_acknowledgement'];
			
				$audit_scripts_data[$i]['agent_accuracy_response']['agent_accuracy_response'. $audit_scripts[$j]['script_list_code']] = $audit_scripts[$j]['agent_accuracy_response'];
				
				$audit_scripts_data[$i]['agent_accuracy_speed']['agent_accuracy_speed'. $audit_scripts[$j]['script_list_code']] = $audit_scripts[$j]['agent_accuracy_speed'];
				
				$audit_scripts_data[$i]['agent_accuracy_cor_response']['agent_accuracy_cor_response'. $audit_scripts[$j]['script_list_code']] = $audit_scripts[$j]['agent_accuracy_cor_response'];
				
				$audit_scripts_data[$i]['info_cust_details']['info_cust_details'. $audit_scripts[$j]['script_list_code']] = $audit_scripts[$j]['info_cust_details'];
				
				$audit_scripts_data[$i]['info_agent_input']['info_agent_input'. $audit_scripts[$j]['script_list_code']] = $audit_scripts[$j]['info_agent_input'];
				
				$audit_scripts_data[$i]['info_call_ends']['info_call_ends'. $audit_scripts[$j]['script_list_code']] = $audit_scripts[$j]['info_call_ends'];
				
				$audit_scripts_data[$i]['agent_acknowledgement']['agent_acknowledgement'. $audit_scripts[$j]['script_list_code']] = $audit_scripts[$j]['agent_acknowledgement'];

				$audit_scripts_data[$i]['comment']['comment'. $audit_scripts[$j]['script_list_code']] = $audit_scripts[$j]['comment'];
 				
 				array_push($audit_data[$i], $audit_scripts[$j]['customer_statement'], $audit_scripts[$j]['agent_acknowledgement'], 
 					$audit_scripts[$j]['agent_accuracy_response'], $audit_scripts[$j]['agent_accuracy_speed'], $audit_scripts[$j]['agent_accuracy_cor_response'], $audit_scripts[$j]['info_cust_details'], $audit_scripts[$j]['info_agent_input'], $audit_scripts[$j]['info_call_ends'], $audit_scripts[$j]['agent_acknowledgement'], $audit_scripts[$j]['comment']);
   
			}
 
  			//$customer_statement = explode(", ", $audit_scripts_data[$i]['customer_statement']);

  			//array_push($audit_data[$i], $customer_statement);

  			//print_r($audit_scripts);
  
			$i++;

		}

  		fputcsv($file, $header);

		foreach ($audit_data as $key => $line){ 
	
 		    fputcsv($file,	$line); 

		}

		fclose($file); 
 
		exit;
	}

	public function export_header(){

		$header = array('Audit ID', 
						'Audit Time Start',
						'Audit Time End',
						'Week Beginning Date',
						'Call ID',
						'Phone Number',
 						'Customer',
						'Agent ID',
						'Agent Name',
						'Timestamp',
						'Recording',
						'Agent Disposition',
						'Correct Disposition',
						'Agent Issue',
						'Agent Issue Comment',
						'ZT mark',
						'ZT mark Comment',
						'General Observation',
						'QA Remarks',
						'Audit Date',
						'Recording Date',
						'Recording Time',
						'Auditor',
						'Soundboard Voice',
						'Team Lead',
						'Floor Support',
						'Tone',
 					); 
		
 		return $header;

	}


	public function export_header_scripts($script_code){

		$header = array(
			$script_code . 'Customer - Statement', 
			'Acknowledgement',
			'Agent Response', 
			'Speed', 
			'Correct Response',
			'Customer Details',
			'Agent Input ', 
			'Call Ends', 
			'Comments'); 
		
 		return $header;
	}

}