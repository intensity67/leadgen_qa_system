<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Dashboard extends MY_Controller  {

	public function __construct(){
 
		parent::__construct();
 
    }

	public function index(){

  	  if($this->session->userdata('login_info')['user_roles_id'] == ADMIN){

			redirect("Dashboard/admin");

  	  }

  	  if($this->session->userdata('login_info')['user_roles_id'] == QUALITY_ASSURANCE){

			redirect("Recording/sme_group_recordings");

  	  }
  	  
  	  if($this->session->userdata('login_info')['user_roles_id'] == TEAM_LEAD){

			redirect("Dashboard/team_lead");

  	  }


  	  if($this->session->userdata('login_info')['user_roles_id'] == SUPERVISOR){

			redirect("Dashboard/supervisor");


  	  }
  	  

	}

  public function admin(){
 
      $data['page_title']      = "Account Lists";

      $data['user_list']       = $this->users_model->get_users_lists();

      $data['user_roles']      = $this->users_model->get_user_roles();

      $page = 'dashboard/admin';

      $this->page_render($page, $data);
 
  }

  public function auditor(){
 
      $data['page_title']      = "Auditor - Dashboard";

      $data['user_list']       = $this->users_model->get_users_lists();

      $data['user_roles']      = $this->users_model->get_user_roles();

      $page = 'dashboard/auditor';

      $this->page_render($page, $data);
 
  }

  public function supervisor(){
 
      $data['page_title']      = "Supervisor - Dashboard";

      $data['user_list']       = $this->users_model->get_users_lists();

      $data['user_roles']      = $this->users_model->get_user_roles();

      $page = 'dashboard/supervisor';

      $this->page_render($page, $data);
 
  }

  public function team_lead(){
 
      $data['page_title']      = "Team Lead - Dashboard";

      $data['user_list']       = $this->users_model->get_users_lists();

      $data['user_roles']      = $this->users_model->get_user_roles();

      $page = 'dashboard/team_lead';

      $this->page_render($page, $data);
 
  }



}
