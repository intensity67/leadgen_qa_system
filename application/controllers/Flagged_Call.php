<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Flagged_Call extends MY_Controller  {

    public function __construct(){
 
        parent::__construct();

            // Your own constructor code
       

    }

  public function flagged_calls(){

        $data['page_title']             = "Flagged Calls";

        $data['users']                  = $this->users_model->get_users_lists();         

        $data['flagged_calls']          = $this->flagged_calls_model->get_flagged_calls_list();

        $data['add_flag_call_filter']   = $this->get_add_flagged_call_filters();

        $page = 'flagged_call/flagged_calls';

        $this->page_render($page, $data);
        
  }

  public function insert_flagged_calls(){

   //* Call Info

      $flagged_calls_data['recording_id']   = $this->input->post('recording_id');
      $audit_data['work_week']              = $this->input->post('work_week');
      $audit_data['call_id']                = $this->input->post('call_id');
      $audit_data['phone_number']           = $this->input->post('phone_number');
      $audit_data['customer']               = $this->input->post('customer');
      $audit_data['agent_user_id']          = $this->input->post('agent_user_id');
      $audit_data['agent_name']             = $this->input->post('agent_name');
      $audit_data['timestamp']              = $this->input->post('timestamp');
      $audit_data['soundboard_voice_id']    = $this->input->post('soundboard_voice_id');
      $audit_data['auditor_user_id']        = $this->input->post('auditor_user_id');
      
      print_r($audit_data);

      $audit_id                       = $this->audit_model->insert_long_call_audit($audit_data);
      
  }

  public function flagged_calls_search_form(){

        $data['page_title']         = "Flagged Calls";

        $page                       = 'flagged_call/flagged_call_search_form';

        $this->page_render($page, $data);
        
  }

  public function searched_flagged_calls_results(){
        
        $data['page_title']                       = "Search Flagged Calls";

        $page                                     = 'flagged_call/flagged_call_search_form';
        

        $search_internal_filters['phone_number']  = $this->input->post('phone_number');

        $search_internal_filters['user']          = $this->input->post('user');

        $data['search']   = $search_internal_filters;

        //* Check in (internal) recording cluster  if phone number and agents exist
        
        $data['results']                  = $this->recording_model->search_record_lists($search_internal_filters);
         
        if(!empty($data['results'])){

          //* Status be updated as flagged call
          
          $data['source'] =   "Internal";

        } else {
          
          //* Set Propeer Search Filters

          $search_clusters_filters['phoneNumber']  = $search_internal_filters['phone_number'];
          
  

          //* Search all through the live clusters (cluster 1, 2, 3, and 4 until it is found or if search is finished)

          //* Starts with cluster 4
 
            for($cluster_cnt = 4; $cluster_cnt >= 1; $cluster_cnt--){
            
            $cluster              = $this->load->database('cluster'. $cluster_cnt, TRUE); 
            
            $results              = $this->recording_model->search_recording_in_clusters($cluster, $search_clusters_filters);

            if(!empty($results)){
             
               print_r($results);

               break;
             
                //* Transfer recording in internal
               
                $this->recording_model->insert_batch($data);

             }

          }

          if(!empty($results)){
            

            $cluster_no             = $cluster_cnt;

            $recordings_cnt         = count($cluster_recording_data);
             
            for ($i = 0; $i < $recordings_cnt; $i++) {

                $cluster_recording_data[$i]['cluster_no']           = $cluster_no;

                $cluster_recording_data[$i]['recording_log_status'] = "flagged";

            }

            $data['results'] = $cluster_recording_data;

          }
           
          $data['source'] =   "Vici";

        }


        $data['add_flag_call_filter'] = $this->get_add_flagged_call_filters();

        $page                         = 'flagged_call/searched_flagged_calls_results';

        //* If not query and search to clusters

        $this->page_render($page, $data);
        

  }

}