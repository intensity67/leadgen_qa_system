<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class Report extends MY_Controller  {
	
	public function __construct(){
 
		parent::__construct();

      	// Your own constructor code
       

  }

  public function quality_production_report(){

        $data['page_title']                 = "Quality Production Report";
 
        $page = 'report/quality_production_report';

        $this->page_render($page, $data);

  }

  public function coaching_assistance_form(){

        $data['page_title']                 = "Coaching Assistance Form";
 
        $page = 'report/coaching_assistance_form';

        $this->page_render($page, $data);

  }

  public function qa_results_summary($agent_login_id, $work_week = ''){

    $page = 'report/qa_results_summary';
    
    //* If work week is not set by custom value , then set default to current week.

    if(empty($work_week) || !isset($work_week) || $this->input->post('work_week') === NULL)
    
      $work_week = date("Y-m-d", strtotime('monday this week'));

    $data['week_audits_report'] = $this->reports_model->get_agent_work_week_audits($agent_login_id, $work_week);

      $month = date("m", strtotime('this month'));

    $data['month_audits_report']  = $this->reports_model->get_agent_monthly_audit($agent_login_id, $month);

    $data['scripts_reports']      = $this->reports_model->get_agent_work_week_scripts($agent_login_id, $month);

    $data['script_lists']         = $this->filters_model->get_script_list();

    foreach($data['script_lists'] as $script_lists_row){

      $script_lists_cnt[$script_lists_row['script_lists_id']]['script_list_code'] = $script_lists_row['script_list_code'];

      $script_lists_cnt[$script_lists_row['script_lists_id']]['speed_cnt'] = 0;
 
      $script_lists_cnt[$script_lists_row['script_lists_id']]['cust_acknowledgement'] = 0;

      $script_lists_cnt[$script_lists_row['script_lists_id']]['incorrect_response_cnt'] = 0;

      $script_lists_cnt[$script_lists_row['script_lists_id']]['script_cnt'] = 0;

    }


    foreach($data['scripts_reports'] as  $scripts_reports_row){
      

      $script_lists_cnt[$scripts_reports_row['script_lists_id']]['speed_cnt'] += $scripts_reports_row['agent_accuracy_speed'];

      if($scripts_reports_row['agent_acknowledgement'] == "No")

        $script_lists_cnt[$scripts_reports_row['script_lists_id']]['cust_acknowledgement']++;
      
      if(!empty($scripts_reports_row['agent_accuracy_cor_response']))

        $script_lists_cnt[$scripts_reports_row['script_lists_id']]['incorrect_response_cnt']++;

      $script_lists_cnt[$scripts_reports_row['script_lists_id']]['script_cnt']++;

     // reserve for call ends cnt: 

    }


    $data['script_lists_cnt'] = $script_lists_cnt;

    $data['work_week'] = $work_week;

    $this->page_render($page, $data);

  }

  public function qa_results_view($agent_login_id){

    $page = 'report/qa_results_view';
    
    $data = '';
    
    $this->page_render($page, $data);

  }

  public function agent_issues_report(){

    $data['page_title']   = "Agent System Issue Comment";

    //* Query Team Leads

    $team_leads           = $this->rosters_model->get_rosters_tl();
    
    foreach($team_leads as $row){
      
      $team_lead = $row['tl'];

      
      //* $agent_issues_comment = $this->filters_model->get_issue_comments();

    }
    
    $data['agent_issues_report'] =  $this->reports_model->get_agent_system_issues_report_general();
    
    $data['total_audits']        =  $this->audit_model->get_audit_lists_cnt(); 

    //* Make Counter Per Team Leads & Issue Comment Id Condition on Audits Table

     $page = 'report/agent_issues_report';
  
    //* Agent/System Issue Comment Report

    $this->page_render($page, $data);

  }
  
  public function agent_issues_audit_details($issue_id){
    
        $data['page_title']             = "Audit Lists";

        $filters['issue_comment_id']    = $issue_id;
 
        $data['disposition_list']       = $this->filters_model->get_vici_dispo_list();

        $data['users']                  = $this->users_model->get_users_lists();

        $data['qa_list']                = $this->users_model->get_auditor_lists();
  
        $data['audit_list']             =  $this->audit_model->get_audit_lists($filters);

        $data['gen_observational_list'] = $this->filters_model->get_general_observation_list();

        $data['program_list']           = $this->filters_model->get_program_list();

        $data['add_incident_filter']    = $this->get_add_incident_filters();

        $page = 'audit/audit_list';

        $this->page_render($page, $data);
 
  }

  public function audit_reports_date(){
    
    $data['audit_date_list']       =  $this->reports_model->get_agent_issues_date();

    $page = 'report/agent_issues_by_date';

    $this->page_render($page, $data);

  }

  public function audit_reports($date_groups = ''){
    
    $data['audit_date_list']       =  $this->reports_model->get_agent_issues_date();

    if($date_groups == "weekly"){

    $data['audit_date_list']       =  $this->reports_model->get_agent_issues_weekly();

    }

    $data['date_groups'] = $date_groups;

    $page = 'report/audit_reports';

    $this->page_render($page, $data);

  }

  public function agent_issues_by_date(){


  }

  public function agent_issues_by_date_details($date){

      
  }

  public function audit_report_tally($date_from = '', $date_to = ''){
  

        $data['page_title']             = "Audit Tally Report";

        $page = "report/audit_tally_report";

        if($this->input->post('date_from') !== NULL && $this->input->post('date_to')){
          
          $date_from = $this->input->post('date_from');
          
          $date_to   = $this->input->post('date_to');

        }

        if($date_to == NULL){
  
          $date_to = date("Y-m-d H:i", strtotime("+1 days", strtotime($date_from . " 7:00")));
 
         }

        $date_from              =   date("Y-m-d H:i", strtotime($date_from. "+20hours"));

        //$this->db->select('issue_comments.issue_comment_id, issue_comments.issue_comment, tl, fs, COUNT( audit_issue_comments_id ) AS counts');

        $data['issue_comments_tally'] = $this->reports_model->get_issue_comments_reports_per_sme($date_from, $date_to);

         $this->page_render($page, $data);
 
  }


  public function audit_report_details($date_from = '', $date_to = ''){

        $data['page_title']             = "Audit Report Details";

        if($this->input->post('date_from') !== NULL && $this->input->post('date_to')){
          
          $date_from = $this->input->post('date_from');
          
          $date_to   = $this->input->post('date_to');

        }

        if($date_to == NULL){
  
          $date_to = date("Y-m-d H:i", strtotime("+1 days", strtotime($date_from . " 7:00")));
 
         }
 
        $data['date_to']                =   $date_to;

        $data['date_from']              =   date("Y-m-d H:i", strtotime($date_from. "+20hours"));

        $data['audit_list']              =  $this->audit_model->get_audit_lists('', $date_from, $date_to);
         
        //* Get SME Audits Count (with date from and to)

        // $this->reports_model->get_audit_issue_comments($filter);

        $page = 'report/audit_report_details';

        $data['gen_observational_report'] = $this->reports_model->get_general_observation_report($date_from, $date_to);

        $data['issue_comments']           = $this->filters_model->get_issue_comments();

        $data['qa_list']                  = $this->users_model->get_auditor_lists();

        $data['sme_list']                 = $this->rosters_model->get_sme_lists();

        $data['add_incident_filter']      = $this->get_add_incident_filters();
        
        $data['sme_issues_cnt']           = $this->reports_model->get_issue_comments_reports_per_sme($date_from, $date_to);

        foreach($data['sme_list'] as $sme_list){

          foreach($data['issue_comments'] as $issue_comments_row){

            //$this->reports_model->get_issue_cnts_by_sme($sme_list['roster_support_id'], $date_from, $date_to);

            $sme_issues_cnt[$sme_list['roster_support_id']][$issue_comments_row['issue_comment_id']] = 0;
 
          }

        }

        $this->load->view('layout/header');

        $this->load->view($page, $data);
           
        $this->load->view('layout/footer');
  }


}
