<?php
 
  class Config_model extends CI_Model{

        public function __construct(){

                // Call the CI_Model constructor

                parent::__construct();
 
        }
 
        public function get_configs(){

                // $this->db->where("recstat", "active");
                
                $query = $this->db->get('config');
                
                return $query->result_array();

        }

        public function update_configs($config_desc, $update_data){

                $this->db->where("config_desc", $config_desc);
                
                $this->db->update('config', $update_data);

        }
}