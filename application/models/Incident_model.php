<?php
 
  class Incident_model extends CI_Model{

        public function __construct(){

                // Call the CI_Model constructor

                parent::__construct();
                
                $this->load->database('leadgen_qa_system ');

        }

        public function get_incident_report_list(){

//* Commented queries will be applicable after employee masterlist has match with vicidial login

            // $this->db->select('*, CONCAT(audit_user.first_name, " ", audit_user.last_name) AS auditorName,
            //         CONCAT(agent_user.first_name, " ", agent_user.last_name) as agentName');

            // $this->db->join('user_accounts as audit_user', 'audit_user.user_account_id = incident_reports.auditor_id', 'left');

            // $this->db->join('user_accounts as agent_user', ' agent_user.user_account_id = incident_reports.agent_id', 'left');
            
            // $this->db->join('user_accounts as agent_user', ' agent_user.user_account_id = incident_reports.team_lead_id', 'left');

            $this->db->join('roster_agents', 'incident_reports.agent_login_id = roster_agents.login', 'left');

            $this->db->join('sanction_list', ' sanction_list.sanction_list_id = incident_reports.sanction_id', 'left');

            $this->db->join('program_list', ' program_list.program_list_id = incident_reports.incident_reports_id', 'left');

            $this->db->order_by('incident_reports_id', 'DESC');

            $query = $this->db->get('incident_reports', 60);
                    
            return $query->result_array();

        }

        public function get_incident_report_info($incident_reports_id, $offense_type = ''){

            $this->db->join('roster_agents', 'incident_reports.agent_login_id = roster_agents.login', 'left');

            $this->db->join('sanction_list', ' sanction_list.sanction_list_id = incident_reports.sanction_id', 'left');

            $this->db->join('program_list', ' program_list.program_list_id = incident_reports.program_id', 'left');

            $this->db->join('vici_disposition_list', ' incident_reports.disposition_id = 
                vici_disposition_list.vici_dispo_list_id', 'left');

            $this->db->join('audits', ' incident_reports.audit_id = 
                audits.audit_id', 'left');

            $this->db->join('user_accounts', ' user_accounts.user_account_id = 
                audits.auditor_id', 'left');            

            $this->db->join('cluster_recordings', 'cluster_recordings.recording_cluster_id = 
                audits.recording_cluster_id', 'left');


            $this->db->join('lol_mark_list', 'lol_mark_list.lol_mark_list_id = 
                incident_reports.offense_id', 'left');

            $this->db->join('ztp_mark_list', 'ztp_mark_list.ztp_mark_list_id = 
                incident_reports.offense_id', 'left');


             $this->db->where('incident_reports_id', $incident_reports_id);

            $query = $this->db->get('incident_reports');

            return $query->row_array();

        }

        public function insert_incident_report($incident_report_data){
 
            $this->db->insert('incident_reports', $incident_report_data);

            return $this->db->insert_id();

        }
 

    }


    ?>