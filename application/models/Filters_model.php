<?php
 
  class Filters_model extends CI_Model{

        public function __construct(){

                // Call the CI_Model constructor

                parent::__construct();
 
        }

        public function get_script_list(){

                // $this->db->where("recstat", "active");
                
                $query = $this->db->get('script_lists');
                
                return $query->result_array();

        }

        public function get_script_source_list(){

                // $this->db->where("recstat", "active");
                
                $query = $this->db->get('script_source');
                
                return $query->result_array();

        }


        public function get_general_observation_list(){

                // $this->db->where("recstat", "active");
                
                $query = $this->db->get('general_observation_list');
                
                return $query->result_array();

        }

        public function get_issue_comments(){

                // $this->db->where("recstat", "active");
                
                $query = $this->db->get('issue_comments');
                
                return $query->result_array();

        }

        public function get_agent_issue_comments(){

                $this->db->where("issue_type", "Agent");
                
                $query = $this->db->get('issue_comments');
                
                return $query->result_array();

        }

        public function get_system_issue_comments(){

                $this->db->where("issue_type", "System");
                
                $query = $this->db->get('issue_comments');
                
                return $query->result_array();

        }

        public function get_soundboard_voice_list(){

                // $this->db->where("recstat", "active");
                
                $query = $this->db->get('soundboard_voice');
                
                return $query->result_array();

        }

        public function get_ztp_mark_list(){
                
                $query = $this->db->get('ztp_mark_list');
                
                return $query->result_array();

        }

        public function get_lol_mark_list(){
                
                $query = $this->db->get('lol_mark_list');
                
                return $query->result_array();

        }

        public function get_wave_list(){

                // $this->db->where("recstat", "active");
                
                $query = $this->db->get('wave_list');
                
                return $query->result_array();

        }

        public function get_program_list(){

                // $this->db->where("recstat", "active");
                
                $query = $this->db->get('program_list');
                
                return $query->result_array();

        }

        public function get_client_feedback_list(){

                // $this->db->where("recstat", "active");
                
                $query = $this->db->get('client_feedback_list');
                
                return $query->result_array();  

        }

        public function get_site_list(){
                
                $query = $this->db->get('sites');
                
                return $query->result_array();

        }
 
        public function get_vici_dispo_list(){
                
                $query = $this->db->get('vici_disposition_list');
                
                return $query->result_array();

        }

        public function get_webform_dispo_list(){
                
                $query = $this->db->get('webform_disposition_list');
                
                return $query->result_array();

        }

        public function get_sanction_list(){
                
                $query = $this->db->get('sanction_list');
                
                return $query->result_array();

        }

        public function get_quotefire_list(){
                
                $query = $this->db->get('quotefire');
                
                return $query->result_array();

        }

        public function get_quotefire_list_by_type($quotefire_type){

                $this->db->where("quotefire_type", $quotefire_type);

                $query = $this->db->get('quotefire');
                
                return $query->result_array();

        }

  }


?>