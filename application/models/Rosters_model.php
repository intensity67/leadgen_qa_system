<?php
 
  class Rosters_model extends CI_Model{


        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function insert_roster_agents($roster_data)
        {
             
              $this->db->insert('roster_agents', $roster_data);

              return $this->db->insert_id();
        }

        public function insert_rosters($roster_data)
        {
             
              $this->db->insert('rosters', $roster_data);

              return $this->db->insert_id();
        }

        public function get_rosters_lists(){

                // $this->db->where("recstat", "active");
                
                // $this->db->join('user_roles', 'user_roles.user_roles_id = user_accounts.user_roles_id');

                $query = $this->db->get('roster_agents');
                
                return $query->result_array();

        }

        public function get_sme_lists(){

                $this->db->where("role_type", "floor_support");

                $query = $this->db->get('roster_support');
                
                return $query->result_array();

        }

        public function get_rosters_info($login_id){
                
                $this->db->where("login", $login_id);

                $query = $this->db->get('roster_agents');
                
                return $query->row_array();

        }

        public function get_agents_cluster($cluster){

                // $this->db->where("recstat", "active");
                 
            //$cluster->select('vicidial_users.user, vicidial_users.user_group, user_level, event_date, event');
            
            $cluster->select('user, user_group, user_level');

            $cluster->where('active', "Y");
            
            $cluster->where('user_level', 1);

            //$cluster->join('vicidial_user_log ','vicidial_users.user = vicidial_user_log.user');
            
            //$cluster->group_by("vicidial_users.user");

            //$cluster->order_by("event_date", "DESC");

            $query = $cluster->get('vicidial_users');

            $vici_users = $query->result_array();
            
            //* Converts to singular array all users login

            foreach ($vici_users as $user) {
                
                $users[] =  $user['user'];

                //$users[] =  $user_log = $this->get_agents_last_login($cluster, $user['user']);

            }
  
            $this->db->where_in('login', $users);

            $rosters_query = $this->db->get("roster_agents");

            $rosters_agent = $rosters_query->result_array();

            return $vici_users;
  
        }

        public function get_agents_roster_info($login_id){

            //$this->db->select("event, event_date, server_ip, extension, computer_ip, server_phone", $login_id);
            
            $this->db->where("login", $login_id);

            $query = $this->db->get('roster_agents');
                
            return $query->row_array();
        }

        public function get_agents_last_login($cluster, $agent_login_id){

            $cluster->where("user", $agent_login_id);

            $cluster->order_by('event_date', 'desc');

            $cluster->limit(1);

            $query = $cluster->get('vicidial_user_log');
                
            return $query->row_array();

        }

  
        public function get_rosters_tl(){

                // $this->db->where("recstat", "active");
                
                // $this->db->join('user_roles', 'user_roles.user_roles_id = user_accounts.user_roles_id');
                  
                $this->db->select('tl, COUNT(DISTINCT(agent)) AS agents_cnt');
                
                $this->db->group_by('tl'); 

                $query = $this->db->get('roster_agents');
                
                return $query->result_array();

        }

        public function get_rosters_qa(){

                // $this->db->where("recstat", "active");
                
                // $this->db->join('user_roles', 'user_roles.user_roles_id = user_accounts.user_roles_id');
                  
                $this->db->select('qa, COUNT(DISTINCT(agent)) AS agents_cnt');
                
                $this->db->group_by('qa'); 

                $query = $this->db->get('roster_agents');
                
                return $query->result_array();

        }


        public function get_rosters_wave(){

                // $this->db->where("recstat", "active");
                
                // $this->db->join('user_roles', 'user_roles.user_roles_id = user_accounts.user_roles_id');
                  
                $this->db->select('wave, COUNT(DISTINCT(agent)) AS agents_cnt');
                
                $this->db->group_by('wave'); 

                $query = $this->db->get('roster_agents');
                
                return $query->result_array();

        }

        public function get_rosters_fs(){

                // $this->db->where("recstat", "active");
                
                // $this->db->join('user_roles', 'user_roles.user_roles_id = user_accounts.user_roles_id');
                  
                $this->db->select('fs, COUNT(DISTINCT(agent)) AS agents_cnt');
                
                $this->db->group_by('fs'); 

                $query = $this->db->get('roster_agents');
                
                return $query->result_array();

        }
 
        public function get_agents_filter($filter){

             $this->db->where($filter);

            $this->db->group_by('agent'); 

            $query = $this->db->get('roster_agents');
            
            return $query->result_array();

        }
 
        public function get_agents(){

            $this->db->where('tl !=', 'QA');

            $this->db->group_by('agent'); 

            $query = $this->db->get('roster_agents');
            
            return $query->result_array();

        }

        public function get_agents_audits($filter){

             $this->db->select('login, agent, wave, agent_cluster_no, roster_agents.fs AS floorSupport, COUNT(DISTINCT(agent)) AS agents_cnt, COUNT(audit_id) AS auditCount, audit_timestamp');

            $this->db->join('roster_agents', 'audits.agent_login_id = roster_agents.login', 'right');

            $this->db->where($filter);

            $this->db->group_by('login'); 

            $query = $this->db->get('audits');

            return $query->result_array();

        }
        
        public function get_agents_last_audit($agent_login_id){

            $this->db->where('agent_login_id', $agent_login_id);

            $this->db->order_by("audit_id", "DESC");

            $this->db->limit(1); 

            $query = $this->db->get('audits');

            return $query->row_array();


        }

        public function get_rosters_filter($filter){

            $this->db->where($filter);

 
            $query = $this->db->get('roster_agents');
            
            return $query->result_array();

        }
 
        public function update_roster_cluster($cluster_no, $agent_login_id){

            $this->db->set('agent_cluster_no', $cluster_no);

            $this->db->where("login", $agent_login_id);

            $this->db->update('roster_agents');

         }

        public function update_roster($update_roster_data, $agent_login_id){

            $this->db->set($update_roster_data);

            $this->db->where("login", $agent_login_id);

            $this->db->update('roster_agents');
 
        }
}