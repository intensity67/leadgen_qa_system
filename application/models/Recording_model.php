<?php
 
  class Recording_model extends CI_Model{

        public function __construct(){

                // Call the CI_Model constructor

                parent::__construct();

         }
      
        public function check_short_recordings_cnt(){
            
            $this->db->where('length_in_sec <', 60);

            $this->db->where('recording_log_status', 'new');

            if(isset($agent_login)){

                $this->db->where('user', $agent_login);

            }

            $result = $this->db->get('cluster_recordings'); 

            return $result->num_rows();

        }

        public function get_agents_recording_list($agent_login, $filters, $call_type){

            $this->db->where($filters);
            
            if($call_type == "long_calls")

                $this->db->where('length_in_sec >=', 60);
            
            if($call_type == "short_calls")

                $this->db->where('length_in_sec <', 60);

            $this->db->where('user', $agent_login);
            
            $this->db->join('roster_agents', 'roster_agents.login =  cluster_recordings.user', 'left');

            $result = $this->db->get('cluster_recordings'); 

            return $result->result_array();
        }
        

        public function check_long_recordings_cnt(){

            $this->db->where('length_in_sec >=', 60);

            $this->db->where('recording_log_status', 'new');

            $result = $this->db->get('cluster_recordings'); 

            return $result->num_rows();

        }

        public function check_recordings_cnt(){

            $this->db->where('recording_log_status', 'new');

            $result = $this->db->get('cluster_recordings'); 

            return $result->num_rows();

        }

        public function get_short_recordings_list(){

            $this->db->where('length_in_sec <', 60);

            $this->db->where('recording_log_status', 'new');

            $this->db->join('roster_agents', 'roster_agents.login =  cluster_recordings.user', 'left');

            $query = $this->db->get('cluster_recordings'); 

            return $query->result_array();
        }

        public function get_long_recordings_list(){
            
            $this->db->select('*, cluster_no');

            $this->db->where('length_in_sec >=', 60);

            $this->db->where('recording_log_status', 'new');

            $this->db->join('roster_agents', 'roster_agents.login =  cluster_recordings.user', 'left');

            $query = $this->db->get('cluster_recordings'); 

            return $query->result_array();
        }

        public function get_recordings_filter($filters){

            $this->db->where($filters);

            $query = $this->db->get('cluster_recordings'); 

            return $query->result_array();

        }

        public function search_record_lists($search_filters){

            $this->db->where($search_filters);

            $this->db->join('roster_agents', 'roster_agents.login =  cluster_recordings.user', 'left');

            $query = $this->db->get('cluster_recordings'); 

            return $query->result_array();
        }

        public function search_recording_in_clusters($cluster, $search_filters){

            $cluster->select('recording_id, channel, term_reason, server_ip, extension, rc_log.start_time, vc_log.start_epoch, rc_log.end_time, vc_log.end_epoch, rc_log.length_in_sec, length_in_min, filename, location, rc_log.lead_id, rc_log.user, vc_log.user_group, rc_log.vicidial_id, vc_list.list_id, vc_log.call_date, vc_log.status AS `dispo`, vc_log.phone_number, vc_log.called_count, vc_log.alt_dial, rc_log.user AS agent');

            $cluster->from('recording_log rc_log');

            $cluster->join('vicidial_log vc_log', 'vc_log.lead_id = rc_log.lead_id', 'left');

            $cluster->join('vicidial_list vc_list', 'vc_list.lead_id = rc_log.lead_id', 'left');
            
            $cluster->order_by("recording_id", "desc");
            
            $cluster->where('vc_log.status', "NI");

            if(!empty($search_filters['phoneNumber']))

                $cluster->where('vc_list.phone_number', $search_filters['phoneNumber']);

            if(!empty($search_filters['loginId']))

                $cluster->where('rc_log.user',  $search_filters['loginId']);

            if(!empty($search_filters['dispo']))

                $cluster->where('vc_log.status',  $search_filters['dispo']);

            if(!empty($search_filters['call_type']) && $search_filters['call_type'] == "long_calls")

                $cluster->where('`rc_log`.`length_in_sec` >=',  60);
            
            if(!empty($search_filters['call_type']) && $search_filters['call_type'] == "short_calls")

                $cluster->where('`rc_log`.`length_in_sec` <',  60);
            

             if(!empty($search_filters['limit']))

                $cluster->limit($search_filters['limit']);

            else 
                
                //* Default limit : 10

                $cluster->limit(10);

            $query = $cluster->get(); 

            return $query->result_array();

        }
 
        public function delete_recording($recording_id){

                $this->db->delete('recordings', array('recording_id' => $recording_id)); 

        } 
    // * Query Cluster Recordings - General

        public function get_vici_recordings_cl($cluster, $recording_type, $filter_dispo = '', $agent_login_id = '', $limits = ''){

            $cluster->select('recording_id, channel, term_reason, server_ip, extension, rc_log.start_time, vc_log.start_epoch, rc_log.end_time, vc_log.end_epoch, rc_log.length_in_sec, length_in_min, filename, location, rc_log.lead_id, rc_log.user, vc_log.user_group, rc_log.vicidial_id, vc_list.list_id, vc_log.call_date, vc_log.status, vc_log.phone_number, vc_log.called_count, vc_log.alt_dial ');

            $cluster->from('recording_log rc_log');

            $cluster->join('vicidial_log vc_log', 'vc_log.lead_id = rc_log.lead_id', 'left');

            $cluster->join('vicidial_list vc_list', 'vc_list.lead_id = rc_log.lead_id', 'left');
  
            $cluster->where('location is NOT NULL', NULL, FALSE);
            
            if($recording_type == "short_calls"){

                $cluster->where("rc_log.length_in_sec <", 60);
            
            }

            if($recording_type == "long_calls"){

                $cluster->where("rc_log.length_in_sec >=", 60);
        
            }


            $cluster->limit(30);

            $cluster->order_by("recording_id", "desc");
            

            $query = $cluster->get(); 

            return $query->result_array();
         }

    // * Query Cluster Recordings - Agents

        public function get_vici_recordings_agents($cluster, $recording_type, $filter_dispo, $agent_login_id, $limits){

            $cluster->select('recording_id, channel, term_reason, server_ip, extension, rc_log.start_time, vc_log.start_epoch, rc_log.end_time, vc_log.end_epoch, rc_log.length_in_sec, length_in_min, filename, location, rc_log.lead_id, rc_log.user, vc_log.user_group, rc_log.vicidial_id, vc_list.list_id, vc_log.call_date, vc_log.status, vc_log.phone_number, vc_log.called_count, vc_log.alt_dial ');

            $cluster->from('recording_log rc_log');

            $cluster->join('vicidial_log vc_log', 'vc_log.lead_id = rc_log.lead_id', 'left');
            

            $cluster->join('vicidial_list vc_list', 'vc_list.lead_id = rc_log.lead_id', 'left');

            $cluster->where('location is NOT NULL', NULL, FALSE);
            $this->db->last_query();    

            if($recording_type == "short_calls"){

                $cluster->where("rc_log.length_in_sec <", 60);
            
            }

            if($recording_type == "long_calls"){

                $cluster->where("rc_log.length_in_sec >=", 50);
        
            }

            $cluster->where("vc_log.status", $filter_dispo);

            $cluster->where("rc_log.user",  $agent_login_id);

            $cluster->limit(5);
            
            $cluster->order_by("recording_id", "desc");

            $query = $cluster->get(); 

            return $query->result_array();
         }


        public function insert_recording($data){

                $this->db->insert('cluster_recordings', $data); 

        }

        public function insert_recordings($data){

                $this->db->insert_batch('cluster_recordings', $data); 

        }

        public function update_recording($update_recording_data, $recording_cluster_id){
                
                $this->db->where("recording_cluster_id", $recording_cluster_id);

                $this->db->update("cluster_recordings", $update_recording_data); 

        }
        
        public function trash_recording($recording_id){
            
            $this->db->where("recording_id", $recording_id);

            $this->db->delete("cluster_recordings"); 

        }

        public function trash_recordings($recording_ids){
            
            $this->db->where_in('recording_cluster_id', $recording_ids);

            $this->db->delete("cluster_recordings"); 

        }

        public function insert_recording_archive($recording_log_data){

            $this->db->insert('cluster_recordings_archive', $recording_log_data); 

        }

        public function insert_batch_recording_archives($recording_log_data){

            $this->db->insert_batch('cluster_recordings_archive', $recording_log_data); 

        }

        public function get_record_info($recording_id){
            
            $this->db->where("recording_id", $recording_id);

            $this->db->join('roster_agents', 'roster_agents.login = cluster_recordings.user', 'left');
 
            $query = $this->db->get('cluster_recordings'); 
         
            return $query->row_array();

        }

        public function get_record_log_info($recording_id){

            $this->db->where("recording_id", $recording_id);

            $query = $this->db->get('cluster_recordings'); 

            return $query->row_array();
        }

        public function get_sme_group_recordings($auditor = ''){
            
            $this->db->select('qa, COUNT(login) AS `agents_cnt`, roster_agents.fs, rs3.roster_support_name AS floorSupport');

            $this->db->join('roster_agents', 'roster_agents.qa = roster_support.roster_support_name', 'LEFT');

            // $this->db->join('roster_support', 'roster_agents.fs = roster_support.roster_support_id', 'left');
            
            $this->db->join('roster_support as rs2', 'roster_agents.qa = rs2.roster_support_name', 'LEFT');
            
            $this->db->join('roster_support as rs3', 'roster_agents.fs = rs3.roster_support_id', 'LEFT');

            $this->db->group_by('roster_agents.fs'); 

            //$this->db->where("role_type", "auditor");

            if(!empty($auditor)) 
 
                $this->db->where('roster_agents.qa', $auditor);
            
            $query = $this->db->get('roster_support');
            
            return $query->result_array();

        }
  }


?>