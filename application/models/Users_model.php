<?php
 
  class Users_model extends CI_Model{


        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function insert_user($user_data)
        {
             
              $this->db->insert('user_accounts', $user_data);

              return $this->db->insert_id();
        }

        public function get_users_lists(){

                // $this->db->where("recstat", "active");
                
                $this->db->join('user_roles', 'user_roles.user_roles_id = user_accounts.user_roles_id');

                $query = $this->db->get('user_accounts');
                
                return $query->result_array();

        }

        public function get_user_roles(){

                // $this->db->where("recstat", "active");


                $query = $this->db->get('user_roles');
                
                return $query->result_array();

        }
        public function get_user_roles_info($role_id){
            
            $this->db->where("user_roles_id", $role_id);

            $query = $this->db->get('user_roles');
                
            return $query->row_array();
        
        }
        public function get_auditor_lists(){
            
                $this->db->where("user_roles_id", QUALITY_ASSURANCE);

                $query = $this->db->get('user_accounts');
                
                return $query->result_array();

        }

        public function get_sme_lists(){

                $this->db->where("user_roles_id", TEAM_LEAD);
                $this->db->where("user_roles_id", SME);

                $query = $this->db->get('user_accounts');
                
                return $query->result_array();

        }
  
        public function get_tl_lists(){
             
                $this->db->where("user_roles_id", TEAM_LEAD);

                $query = $this->db->get('user_accounts');
                
                return $query->result_array();

        }

        public function get_agent_lists(){
             
                $this->db->where("user_roles_id", AGENT);

                $query = $this->db->get('user_accounts');
                
                return $query->result_array();

        }

        public function get_supervisor_lists(){
             
                $this->db->where("user_roles_id", SUPERVISOR);

                $query = $this->db->get('user_accounts');
                
                return $query->result_array();

        }

        public function get_users_info($username, $password){
            
            $this->db->where('username', $username);
            $this->db->where('password', $password);
            
            $query = $this->db->get('user_accounts');

            return $query->row_array();

        }   
 


  }


?>