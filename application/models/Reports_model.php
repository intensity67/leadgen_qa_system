<?php
 
  class Reports_model extends CI_Model{


        public function __construct()
        {
                // Call the CI_Model constructor
                parent::__construct();
        }

        public function get_agent_issues_date(){

            $this->db->select('audit_id, DATE(audit_timestamp) AS auditDate,
                                COUNT(audit_id) AS audit_count
                              ');

            $this->db->where('HOUR(audits.audit_timestamp) >', "20:00");
            
            $this->db->group_by('DATE(audit_timestamp)'); 
            
            $query = $this->db->get('audits');

            return $query->result_array();
        }


        public function get_agent_issues_weekly(){

            $this->db->select('work_week');
            
            $this->db->group_by('work_week'); 

            $query = $this->db->get('audits');

            return $query->result_array();
        }

 
        public function get_agent_system_issues_report_general(){
            
            $this->db->select('issue_comments.issue_comment_id AS issueCommentID, 
                                COUNT(audit_id) AS issue_comment_cnt, 
                                issue_comments.issue_comment AS issueComment  
                              ');

            $this->db->join('audits', 'issue_comments.issue_comment_id = audits.issue_comment_id', 'left');

            // if($type == 'daily')

            //     $this->db->where('audits.audit_timestamp', $period);

            $this->db->group_by('issue_comments.issue_comment_id'); 
            
            $this->db->order_by("issue_comment_cnt", "desc");

            $query = $this->db->get('issue_comments');
                
            return $query->result_array();
 
        }

        public function get_general_observation_report($date_from = '', $date_to = ''){
            
            $this->db->select('general_observation_list_id, general_observation, 
                                COUNT(audit_id) AS auditCnt ');

            $this->db->join('audits', 'audits.general_observation_id = general_observation_list.general_observation_list_id', 'left');

            $this->db->where('audit_timestamp BETWEEN "'. date('Y-m-d h:i', strtotime($date_from)). '" and "'. date('Y-m-d h:i', strtotime($date_to)).'"');

            $this->db->group_by('general_observation_list_id'); 
            
            $this->db->order_by("auditCnt", "desc");

            $query = $this->db->get('general_observation_list');
                
            return $query->result_array();
 
        }
  
        public function get_rosters_tl(){

                // $this->db->where("recstat", "active");
                
                // $this->db->join('user_roles', 'user_roles.user_roles_id = user_accounts.user_roles_id');
                  
                $this->db->select('tl, COUNT(DISTINCT(agent)) AS agents_cnt');
                
                $this->db->group_by('tl'); 

                $query = $this->db->get('roster_agents');
                
                return $query->result_array();

        }

        public function get_rosters_qa(){

                // $this->db->where("recstat", "active");
                
                // $this->db->join('user_roles', 'user_roles.user_roles_id = user_accounts.user_roles_id');
                  
                $this->db->select('qa, COUNT(DISTINCT(agent)) AS agents_cnt');
                
                $this->db->group_by('qa'); 

                $query = $this->db->get('roster_agents');
                
                return $query->result_array();

        }


        public function get_rosters_wave(){

                // $this->db->where("recstat", "active");
                
                // $this->db->join('user_roles', 'user_roles.user_roles_id = user_accounts.user_roles_id');
                  
                $this->db->select('wave, COUNT(DISTINCT(agent)) AS agents_cnt');
                
                $this->db->group_by('wave'); 

                $query = $this->db->get('roster_agents');
                
                return $query->result_array();

        }

        public function get_rosters_fs(){

                // $this->db->where("recstat", "active");
                
                // $this->db->join('user_roles', 'user_roles.user_roles_id = user_accounts.user_roles_id');
                  
                $this->db->select('fs, COUNT(DISTINCT(agent)) AS agents_cnt');
                
                $this->db->group_by('fs'); 

                $query = $this->db->get('roster_agents');
                
                return $query->result_array();

        }
 
        public function get_agents_filter($filter){

            $this->db->select('login, agent, wave, agent_cluster_no, roster_agents.fs AS floorSupport, COUNT(DISTINCT(agent)) AS agents_cnt, COUNT(audit_id) AS auditCount');

            $this->db->where($filter);

            $this->db->join('audits', 'audits.agent_login_id = roster_agents.login', 'left');

            $this->db->group_by('agent'); 

            $query = $this->db->get('roster_agents');
            
            return $query->result_array();

        }
 
        public function get_agents(){

            $this->db->group_by('agent'); 

            $query = $this->db->get('roster_agents');
            
            return $query->result_array();

        }
 
        public function get_rosters_support_qa($auditor = ''){
            
            $this->db->select('qa, COUNT(login) AS agents_cnt, fs');

            $this->db->join('roster_agents', 'roster_agents.qa = roster_support.roster_support_name', 'left');
            
            $this->db->group_by('fs'); 

            //$this->db->where("role_type", "auditor");

            if(!empty($auditor))
 
                $this->db->where('qa', $auditor);


            $query = $this->db->get('roster_support');
            
            return $query->result_array();

        }
 

        public function get_rosters_filter($filter){

            $this->db->where($filter);

 
            $query = $this->db->get('roster_agents');
            
            return $query->result_array();

        }
 
        public function update_roster_cluster($cluster_no, $agent_login_id){

            $this->db->set('agent_cluster_no', $cluster_no);

            $this->db->where("login", $agent_login_id);

            $this->db->update('roster_agents');

         }

        public function get_audit_issue_comments_filters($filters, $date_from, $date_to){
            
            $this->db->where($filter);
            
            // $date_from, $date_to

            // $this->db->where($filter);

            // $this->db->where('audit_timestamp BETWEEN "'. date('Y-m-d h:i', strtotime($date_from)). '" and "'. date('Y-m-d h:i', strtotime($date_to)).'"');
            // 

            $this->db->join('audit_issue_comments', 'issue_comments.issue_comment_id = audit_issue_comments.issue_comment_id' ,'left');

            $this->db->join('audits', 'audits.audit_id = audit_issue_comments.audit_id' ,'left');

            $result = $this->db->get("issue_comments");

            return $result->result_array();

        }

        
        public function get_agent_work_week_audits($agent_login_id, $work_week, $filters = ''){

            $this->db->select('COUNT(audit_id) AS audits_cnt');

            $this->db->group_by("agent_login_id");

            $this->db->where("agent_login_id", $agent_login_id);
 
            $this->db->where("work_week", $work_week);

            $result = $this->db->get("audits");

            return $result->row_array();

        }

        public function get_agent_monthly_audit($agent_login_id, $month, $filters = ''){

            $this->db->select('COUNT(audit_id) AS audits_cnt');

            $this->db->group_by("agent_login_id");

            $this->db->where("agent_login_id", $agent_login_id);
 
            //$this->db->where("MONTH(work_week)", $month);
           
            $this->db->where("MONTH(audit_timestamp)", $month);

            $this->db->where("YEAR(audit_timestamp)", date("Y"));

            $result = $this->db->get("audits");

            return $result->row_array();

        }


        public function get_agent_work_week_scripts($agent_login_id, $work_week){

            $this->db->select('long_call_audit_scripts.*, audits.work_week');

            $this->db->join('long_call_audit_scripts', 'script_lists.script_lists_id = long_call_audit_scripts.script_lists_id' ,'left');

            $this->db->join('audits', 'audits.audit_id = long_call_audit_scripts.audit_id' ,'left');

            // $this->db->group_by("agent_login_id");

            $this->db->where("agent_login_id", $agent_login_id);
 
            $this->db->where("work_week", $work_week);

            $result = $this->db->get("script_lists");

            return $result->result_array();

        }

        // public function get_issue_cnts_by_sme($sme_roster_support_id, $date_from, $date_to){

        //      $this->db->select('COUNT()');

        //      return $result->row_array();

        // }

        public function get_issue_comments_reports_per_sme($date_from, $date_to){

            $this->db->select('issue_comments.issue_comment_id, issue_comments.issue_comment, tl, fs, COUNT( audit_issue_comments_id ) AS counts');

            $this->db->join('audit_issue_comments', 'issue_comments.issue_comment_id =  audit_issue_comments.issue_comment_id' ,'left');

            $this->db->join('audits', 'audits.audit_id =  audit_issue_comments.audit_id' ,'left');

            $this->db->where('audit_timestamp BETWEEN "'. date('Y-m-d h:i', strtotime($date_from)). '" and "'. date('Y-m-d h:i', strtotime($date_to)).'"');

            $this->db->group_by(array('tl', 'fs', 'issue_comments.`issue_comment_id`'));

            $result = $this->db->get("issue_comments");

            return $result->result_array();

        }
  

}