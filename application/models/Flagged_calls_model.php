<?php
 
  class Flagged_calls_model extends CI_Model{

        public function __construct(){

                // Call the CI_Model constructor

                parent::__construct();
                
                $this->load->database('leadgen_qa_system ');

        }

        public function get_flagged_calls_list(){
    
//* Commented queries will be applicable after employee masterlist has match with vicidial login

            // $this->db->where("recstat", "active");
        
            // $this->db->select('*, CONCAT(audit_user.first_name, " ", audit_user.last_name) AS auditorName,
            //         CONCAT(agent_user.first_name, " ", agent_user.last_name) as agentName');
            
            $this->db->select('*');

            $this->db->join('script_source', 'script_source.script_source_id = flagged_calls.script_used_id');

            $this->db->join('client_feedback_list', 'client_feedback_list.client_feedback_list_id = flagged_calls.client_feedback_id');
            
            // $this->db->join('user_accounts as audit_user', ' flagged_calls.auditor_id = audit_user.user_account_id', 'left');

            // $this->db->join('user_accounts as agent_user', ' flagged_calls.user_id = agent_user.user_account_id', 'left');
            
            $this->db->join('roster_agents', 'flagged_calls.user_id = roster_agents.login', 'left');


            // Connect SME and TL
             
            $query = $this->db->get('flagged_calls', 60);
                    
                return $query->result_array();

        }


      }


    ?>