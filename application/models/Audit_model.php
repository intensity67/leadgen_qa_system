<?php
 
  class Audit_model extends CI_Model{

        public function __construct(){

                // Call the CI_Model constructor

                parent::__construct();

                //* reconnect to 205 database
                
        }

        public function get_audit_lists($filters = '',  $date_from = '', $date_to = ''){

            // $this->db->where("recstat", "active");

            $this->db->select("*, cluster_recordings.phone_number AS phoneNumber, cluster_recordings.length_in_sec AS callLength");

            $this->db->join('cluster_recordings', 'cluster_recordings.recording_cluster_id = audits.recording_cluster_id', 'left');

            $this->db->join('general_observation_list', 'general_observation_list.general_observation_list_id = audits.general_observation_id', 'left');

            $this->db->join('vici_disposition_list', 'vici_disposition_list.vici_dispo_list_id = audits.correct_disposition_id' ,'left');

            $this->db->join('user_accounts', 'user_accounts.user_account_id = audits.auditor_id' ,'left');

            if(!empty($filters)){

                $this->db->where($filters);

            }

            if(isset($date_from) && !empty($date_from)){
                
                $this->db->where("audit_timestamp >", $date_from);

            }

            if(isset($date_to) && !empty($date_to)){

                $this->db->where("audit_timestamp <", $date_to);

            }
            

            if(isset($date_to) && !empty($date_to) && (isset($date_from) && !empty($date_from))){

                $this->db->where('audit_timestamp BETWEEN "'. date('Y-m-d h:i', strtotime($date_from)). '" and "'. date('Y-m-d h:i', strtotime($date_to)).'"');

            }



            $this->db->order_by('audit_id', 'DESC');

            $result = $this->db->get("audits");

            return $result->result_array();

        }
 
        public function get_audit_lists_export($filters = '', $date_from = '', $date_to = ''){

            $this->db->select(" audit_id, 
                                audit_timestart, 
                                audit_timeend, 
                                work_week, 
                                cluster_recordings.recording_id,
                                cluster_recordings.phone_number,
                                customer,
                                agent_login_id,
                                agent_name,
                                call_timestamp,
                                agent_disposition,
                                dispo_code,
                                vici_disposition_list.dispo_code,
                                agent_issue, 
                                issue_comment, 
                                mark_status,
                                zt_mark_comment,
                                general_observation,
                                qa_remarks,
                                audit_timestamp,
                                cluster_recordings.call_date,
                                cluster_recordings.start_time,
                                user_accounts.acct_nick_name,
                                soundboard_voice_name,
                                tl,
                                fs,
                                prospect_tone

                                ");

            $this->db->join('cluster_recordings', 'cluster_recordings.recording_cluster_id = audits.recording_cluster_id', 'left');

            $this->db->join('general_observation_list', 'general_observation_list.general_observation_list_id = audits.general_observation_id', 'left');

            $this->db->join('vici_disposition_list', 'vici_disposition_list.vici_dispo_list_id = audits.correct_disposition_id' ,'left');

            $this->db->join('issue_comments', 'audits.issue_comment_id = issue_comments.issue_comment_id' ,'left');

            $this->db->join('soundboard_voice', 'audits.soundboard_voice_id = soundboard_voice.soundboard_voice_id' ,'left');

            $this->db->join('user_accounts', 'user_accounts.user_account_id = audits.auditor_id' ,'left');
 
            $this->db->order_by('audit_id', 'DESC');

            if(isset($date_from) && !empty($date_from))

                $this->db->where('audit_timestamp > ', $date_from);
           
            if(isset($date_to) && !empty($date_to))

                $this->db->where('audit_timestamp < ', $date_to);

            $result = $this->db->get("audits");

            return $result->result_array();

        }

        public function get_audit_lists_by_auditor($auditor_id){

            // $this->db->where("recstat", "active");
            
            $this->db->select("*, cluster_recordings.phone_number AS phoneNumber, cluster_recordings.length_in_sec AS callLength");

            $this->db->join('cluster_recordings', 'cluster_recordings.recording_cluster_id = audits.recording_cluster_id', 'left');

            $this->db->join('general_observation_list', 'general_observation_list.general_observation_list_id = audits.general_observation_id', 'left');

            $this->db->join('vici_disposition_list', 'vici_disposition_list.vici_dispo_list_id = audits.correct_disposition_id' ,'left');

            $this->db->join('user_accounts', 'user_accounts.user_account_id = audits.auditor_id' ,'left');

            $this->db->order_by('audits.audit_id', 'DESC');

            $this->db->where("audits.auditor_id", $auditor_id);

            $result = $this->db->get("audits");

            return $result->result_array();

        }

        public function get_audit_issue_comments($audit_id){
            
            $this->db->where("audit_id", $audit_id);
  
            $result = $this->db->get("audit_issue_comments");

            return $result->result_array();

        }


        public function get_audit_scripts($audit_id){
            
            $this->db->where("audit_id", $audit_id);

            // $this->db->join('disposition', 'disposition.disposition_id = long_call_audit_scripts.correct_disposition_id' ,'left');

            $this->db->join('script_lists', 'script_lists.script_lists_id = long_call_audit_scripts.script_lists_id' ,'left');


            $result = $this->db->get("long_call_audit_scripts");

            return $result->result_array();

        }

        public function get_audit_info($audit_id){

            $this->db->where("audit_id", $audit_id);

            $this->db->join('general_observation_list', 'general_observation_list.general_observation_list_id = audits.general_observation_id', 'left');

            $this->db->join('vici_disposition_list', 'vici_disposition_list.vici_dispo_list_id = audits.correct_disposition_id' ,'left');

            $this->db->join('cluster_recordings', 'cluster_recordings.recording_cluster_id = audits.recording_cluster_id', 'left');

            $this->db->join('soundboard_voice', 'soundboard_voice.soundboard_voice_id = audits.soundboard_voice_id', 'left');
            
            $this->db->join('user_accounts', 'user_accounts.user_account_id = audits.auditor_id' ,'left');

            $result = $this->db->get("audits");

            return $result->row_array(); 
                  
 
        }

        public function insert_internal_audit($audit_data){
 
            $this->db->insert('audits', $audit_data);

            return $this->db->insert_id();

        }

        public function insert_internal_audit_scripts($audit_data){
 
            $this->db->insert('long_call_audit_scripts', $audit_data);

            return $this->db->insert_id();

        }        

        public function insert_audit_issue_comments($issue_comment_data){

            $this->db->insert('audit_issue_comments', $issue_comment_data);

            return $this->db->insert_id();
        }
 
        public function update_audit($audit_data, $audit_id){
            
            $this->db->where("audit_id", $audit_id);

            $this->db->update('audits', $audit_data);

         }

        public function update_audit_scripts($script_update_data, $audit_id, $script_list_id){
            
            $this->db->where("audit_id", $audit_id);
            $this->db->where("script_lists_id", $script_list_id);

            $this->db->update('long_call_audit_scripts', $script_update_data);

         }


        public function delete_internal_audit($audit_id){

            $this->db->delete('audits', array('audit_id' => $audit_id)); 

            $this->db->delete('long_call_audit_scripts', array('audit_id' => $audit_id)); 

        }

        public function get_audit_lists_cnt($filters = ''){

            if(!empty($filters))

                $this->db->where($filters);

            $result = $this->db->get("audits");
            
            return $result->num_rows();

        }

 
  }


?>