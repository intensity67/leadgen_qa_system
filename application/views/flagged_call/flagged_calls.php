<style type="text/css">

  /*dark background to support form theme*/

  input{

  	display: block;

  }

</style> 

<div id="content-wrapper">

	<div class="container-fluid">

	<script type="text/javascript">

	  $(document).ready(function() {

		  $('#flagged_calls').DataTable({

		                responsive: true
		        });

	    });

	</script>
 
	 <h1 class="page-header"> Flagged Calls </h1>

<!-- 	 <a href="#" data-toggle = "modal" data-target = '#search-flagged-call-modal' class="btn btn-primary"> <i class='fa fa-search'></i> Search for Flagged Recording </a>
 -->	 

		<a href="#" class="btn btn-success" data-toggle="modal" data-target="#new_flagged_call_modal"> 
			Add Flagged Calls <i class="fa fa-flag"> </i> <i class="fa fa-plus"> </i>
		</a>

	 <br><br><br>

 		<table class="table table-striped" id = "flagged_calls" style="width: 100%;">   

			<thead>
				<tr>
					<th> Flagged Call Id </th>
					<th> Auditor Name </th>
					<th> Soundboard Voice </th>
  					<th> Agent Name  </th>
 					<th> Client Feed Back Id  </th>
 					<th> Phone number  </th>
  					<th> Recording Id  </th>
  					<th> </th>
				</tr>
			</thead>

			<tbody>

 				<?php $i = 1; ?>

				<?php foreach($flagged_calls as $flagged_calls_row): ?>

					<tr>
						<td><?php echo $flagged_calls_row['flagged_calls_id']; ?> </td> 
						<td><?php echo $flagged_calls_row['qa']; ?> </td> 
						<td><?php echo $flagged_calls_row['script_desc']; ?> </td> 
						<td><?php echo $flagged_calls_row['agent']; ?> </td> 
 						<td><?php echo $flagged_calls_row['client_feedback_desc']; ?> </td> 
						<td><?php echo $flagged_calls_row['phone_number']; ?> </td> 
						<td><?php echo $flagged_calls_row['recording_id']; ?> </td> 

						<td>
     						
    						<a href="#" class="btn btn-warning" data-toggle="modal" data-target = '#add-incident-modal'><i class="fa fa-bell"> </i> </a>

    						<a href="#" class="btn btn-success" data-toggle="modal" data-target="#edit-flagged-call-modal<?php echo $i; ?>"><i class="fa fa-edit"> </i> </a>

    						<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#delete-audit-modal"><i class="fa fa-trash-o"> </i> </a>

					   	<?php 

				   			$row['cnt'] 	= $i; 


							// $add_incident_filter['rec_info'] = $flagged_calls_row;
 							

						?>

						 </td> 

					</tr>
	  				
				<?php endforeach; ?>

 			</tbody>

		</table>       
		
		<?php 

			$this->load->view('flagged_call/modals/new_flagged_call_modal', $add_flag_call_filter); 
		?>


	</div>

</div>

 
 