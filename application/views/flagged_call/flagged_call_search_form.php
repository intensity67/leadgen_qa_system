<div id="content-wrapper">

		<div class="container-fluid">

			<h2> Search Flagged Calls </h2>

				<?php echo form_open(base_url('Flagged_Call/searched_flagged_calls_results')); ?>
	
				<table class="table table-striped">

  					<tr><td> Phone Number: *<td><input type="text" name="phone_number" class="form-control" required=""> 

  					<tr><td> Agent Id: *<td><input type="text" name="user" class="form-control">

  					<tr><td> Call / Recording ID: <td><input type="text" name="recording_id" class="form-control">

  					<tr><td> Customer ID: <td><input type="text" name="customer_id" class="form-control">

  					<tr><td> Disposition: <td><select name="flagged_calls_dispo" class="form-control">
  							<option value="DTO">DTO</option>
  							<option value="DEAD">DEAD</option>
  							<option value="HUP">HUP</option>
  							<option value="TrFail">TrFail</option>
  							<option value="VM">VM</option>
  							<option value="TrSuc">TrSuc</option>
  							<option value="RING">RING</option>
  					</select>

   					<tr><td> Recording URL: <td><input type="text" name="recording_url" class="form-control">

				</table>

				<?php 

				$data = array(  
					
							'auditor_user_id' 	=> $this->session->userdata('account_id')
	 
						);

				form_hidden($data);

				?> 
	 
	        <button type="submit" class="btn btn-success" data-dismiss="modal"> 
	        	Search Recording <i class="fa fa-search"> </i> 
	        </button>

	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

			</form>

		</div>

</div>

<div id = "ccp_result"></div>

<div id = "qf_result"></div>

<script type="text/javascript">

	quotefire_api = "http://calls.quotefire.com/analytics/xfer.php";

	$("document").ready(function(){
		
		$('[name="phone_number"]').on('keyup', function(){

			if( $(this).val().length > 8 ){

                $('.result').find('.row').addClass('hide');

                $.post(quotefire_api, {phone: $(this).val(), ajax: true}, function(data) {

                    $('#ccp_result').html(data.ccp);
                    $('#qf_result').html(data.qf);

                 }, 'json');                

			}

		});

	});

</script>