<style type="text/css">

	textarea{
		resize: none;
	}	

</style>

	<div id="search-flagged-call-modal" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!--  -->
	    <div class="modal-content">
	      <div class="modal-header alert alert-info">   
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h2 class="modal-title"> Search for Recording </h4>
	        </span>
	      </div>

		<?php echo form_open(base_url('Flagged_Call/flagged_calls_search')); ?>

	      <div class="modal-body">
 				
 				<table class="table table-condensed">
  					
  					<tr><td> Phone Number: *<td><input type="text" name="phone_number" class="form-control" required=""> 
  					<tr><td> Call / Recording ID: <td><input type="text" name="recording_id" class="form-control">
  					<tr><td> Customer ID: <td><input type="text" name="customer_id" class="form-control">
   					<tr><td> Recording URL: <td><input type="text" name="recording_url" class="form-control">

 				</table>

				<?php 

				$data = array(  
					
					'auditor_user_id' 	=> $this->session->userdata('account_id')
	 
						);

				form_hidden($data);

				?> 
	 
	 	      </div>

 	  	  </form>

	      <div class="modal-footer">

	        <button type="submit" class="btn btn-success" data-dismiss="modal"> Search Recording <i class="fa fa-search"> </i> </button>

	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

	      </div>

	    </div>

	  </div>

	</div>