<style type="text/css">

	textarea{
		resize: none;
	}	

</style>

	<div id="add-flagged-call-modal<?php echo $recording_id; ?>" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!--  -->
	    <div class="modal-content">
	      <div class="modal-header alert alert-info">   
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h2 class="modal-title"> Flag Call </h4>
	        </span>
	      </div>

		<?php echo form_open(base_url('Admin/insert_audit')); ?>

	      <div class="modal-body">
 				
 				<table class="table table-condensed">
  
 				  	<tr><td> Agent Id: <td><input type="text" 
 				  		name="" class="form-control" required="" 
 				  		value="<?php echo $user; ?>" 
 				  		disabled>
<!-- 
 				  	<tr><td> Agent Name: 
 				  		<td> <input type="text" 
		 				  		name="" class="form-control" required="" 
		 				  		value="<?php echo $rec_info['user']; ?>" 
		 				  		disabled>
 -->
 				  	<tr><td> Client Feedback: <td><select class="form-control" name="client_feedback_id"> 

		 				  							<option value="">Select a Client Feedback</option>
		 				  							
		 				  							<?php foreach($client_feedback as $client_feedback_row ){ ?>

		 				  								<option value="<?php echo $client_feedback_row['client_feedback_list_id'] ?>"> <?php echo $client_feedback_row['client_feedback_desc']; ?> </option>
		 				  							
		 				  							<?php } ?>

		 				  						 </select>

 				  	<tr><td> Phone Number: <td><input type="text" class="form-control" required="" value="<?php echo $phone_number; ?>" disabled>

			  		<tr><td> Team Lead: <td><input type="text" name="" class="form-control" required="" value="<?php echo 'tl'; ?>" disabled>

 				  	<tr><td> Call ID: <td><input type="text" name="" class="form-control" required="" value="<?php echo $rec_info['recording_id']; ?>" >

 				  	<tr><td> Customer: <td><input type="text" 
 				  		name="" 
 				  		class="form-control" required="" 
 				  		value="<?php echo ''; ?>">

 				  	<tr><td> Recording Link: <td><input type="text" name="" class="form-control" required="" disabled="">

 				  	<tr><td> Timestamp: <td><input type="text" name="" class="form-control" required="" value="<?php echo date("M d, y h:i:s", strtotime($start_time)); ?>">

 				  	<tr><td> VICI - Disposition: <td><select class="form-control"> 
		 				  							
		 				  							<option value="">Select Vici Disposition</option>
		 				  							
		 				  							<?php foreach($vici_dispo_list as $vici_dispo_row ){ ?>

		 				  								<option value="<?php echo $vici_dispo_row['vici_dispo_list_id'] ?>"> <?php echo $vici_dispo_row['vici_dispo_desc']; ?> </option>
		 				  							
		 				  							<?php } ?>

		 				  						 </select>

 				  	<tr><td> Web Form - Disposition: <td><select class="form-control"> 
		 				  				
		 				  							<option value="">Select Web Form Disposition</option>
		 				  							
		 				  							<?php foreach($webform_dispo_list as $webform_dispo_row ){ ?>

		 				  								<option value="<?php echo $webform_dispo_row['webform_disp_list_id'] ?>"> <?php echo $webform_dispo_row['webform_dispo_desc']; ?> </option>
		 				  							
		 				  							<?php } ?>

		 				  						 </select>


 				  	<tr><td> Quotefire - Comment: <td><input type="text" name="" class="form-control" required="">

 				  	<tr><td> Quotefire - Priority: <td><input type="text" name="" class="form-control" required="">
 				  	<tr><td> Quotefire - comments and notes findings: 

 				  									<td><select class="form-control" required=""> 
		 				  							
		 				  							<option value="">Select Quote Fire Comment </option>
		 				  							
				 				  				  <optgroup label="Unsuccessful">

		 				  							<?php foreach($quotefire['unsuccessful'] as $quotefire_row ){ ?>

		 				  								<option value="<?php echo $quotefire_row['quotefire_id'] ?>"> <?php echo $quotefire_row['quotefire_desc']; ?> </option>
		 				  							<?php } ?>

  													</optgroup>

				 				  				  <optgroup label="Not Interested">

		 				  							<?php foreach($quotefire['not_interested'] as $quotefire_row ){ ?>

		 				  								<option value="<?php echo $quotefire_row['quotefire_id'] ?>"> <?php echo $quotefire_row['quotefire_desc']; ?> </option>
		 				  							<?php } ?>

  													</optgroup>

				 				  				  <optgroup label="Others">

		 				  							<?php foreach($quotefire['others'] as $quotefire_row ){ ?>

		 				  								<option value="<?php echo $quotefire_row['quotefire_id'] ?>"> <?php echo $quotefire_row['quotefire_desc']; ?> </option>
		 				  							<?php } ?>

  													</optgroup>

		 				  							</select>

 				  	<tr><td> SME: <td><select class="form-control"> 
		 				  							<option>Anthony</option>
		 				  							<option>Elna</option>
		 				  							<option>Steve</option>
 		 				  							<option>Gran</option>
 		 				  							<option>Jelly</option>
 		 				  							<option>QA</option>
		 				  						 </select>

			  		<tr><td> Team Lead: <td><select class="form-control"> 
		 				  							<option>Jelly Yap</option>
		 				  							<option>Revie</option>
		 				  							<option>Steve</option>
 		 				  							<option>Anthony</option> 
		 				  						 </select>

					<tr><td> Site: <td>		<select class="form-control"> 

		 				  							<option value="">Select Site </option>
		 				  							
		 				  							<?php foreach($sites as $sites_row ){ ?>

		 				  								<option value="<?php echo $sites_row['site_id'] ?>"> <?php echo $sites_row['site_name']; ?> </option>
		 				  							
		 				  							<?php } ?>

		 				  						 </select>
		 					  						   			  						 
		 			<tr><td> LOL/ZTP: <td><select class="form-control">

		 				  							<option value=""> Choose </option>

		 				  							<option value="NULL">None</option>

				 				  				  	<optgroup label="ZTP">

		 				  							<?php foreach($ztp_mark_list as $ztp_mark_list_row ){ ?>

		 				  								<option value="<?php echo $ztp_mark_list_row['ztp_mark_list_id'] ?>"> 
		 				  									
		 				  									<?php echo $ztp_mark_list_row['ztp_mark_desc']; ?>

		 				  								</option>
		 				  							
		 				  							<?php } ?>

  													</optgroup>

				 				  				  	<optgroup label="LOL">

		 				  							<?php foreach($lol_mark_list as $lol_mark_list_row ){ ?>

		 				  								<option value="<?php echo $lol_mark_list_row['lol_mark_list_id'] ?>"> 

		 				  									<?php echo $lol_mark_list_row['lol_mark']; ?> 
		 				  								</option>
		 				  							
		 				  							<?php } ?>

  													</optgroup>
 
		 				  						 </select>

 					<tr><td> QA Comment - please include opportunities: <td>

  					<tr><td colspan="2"><textarea name="qa_remarks" class="form-control" rows= 7 cols = 15> </textarea>

					<tr><td> Client feedback validation: 
						<td> 
							<select class="form-control" name="client_validation">

								<option  value="Valid">Valid</option>
								<option  value="Invalid">Invalid</option>

							</select>	 

 				</table>

				<?php 

						$data = array(
								              'auditor_user_id' 	=> $this->session->userdata('account_id'),
								              'agent_user_id'  		=> $user,

	 
								            );

						echo form_hidden($data);

				?> 
	 
	 	      </div>

 	  	  </form>

	      <div class="modal-footer">

	        <button type="button" class="btn btn-success" data-dismiss="modal"> Submit Flagged Call <i class="fa fa-send"> </i> </button>

	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

	      </div>

	    </div>

	  </div>

	</div>