<h3 class="page-header"> Agents Lists Team Lead: <b> <?php echo $tl; ?> </b> </h3>

	<div class="row">
 		
 	 <a href="<?php echo base_url('Rosters/update_tl_agents/'.$tl); ?>" class="btn btn-success"> Update This Roster <i class="fa fa-edit"></i></a>

	 <br><br><br>

 		<table class="table table-striped datatable" id = "user_account_list" 
 						style="width: 100%; ">                                    
			<thead>
				<tr>
					<th> Agents </th>
					<th> FS </th>
 
 					<th> </th>

 				</tr>
			</thead>

			<tbody id = "user_account_body">

				<?php foreach($agents as $row): ?>

					<tr>
						<td> <?php echo $row['agent']; ?></td>
						<td> <?php echo $row['fs']; ?></td>
 
						<td> 
							<a href="#" class="btn btn-warning" data-toggle = "modal"data-target = '#reassign_roster-modal<?php echo $row['login']; ?>'> 
								<i class="fa fa-reply"></i>   
							</a>
						
					<?php 

					 	$reassign_agent_data['roster_info'] = $row;
					 	
					 	$this->load->view("rosters/modals/reassign_roster-modal", $reassign_agent_data); 
					
					?>
					
  				<?php endforeach; ?>

 			</tbody>

		</table>       


 	</div>

</div>

 