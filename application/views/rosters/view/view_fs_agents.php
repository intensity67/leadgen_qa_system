<h1 class="page-header"> Agents Lists SME: <?php echo $fs; ?> </h1>

	<div class="row">
 
	 <br><br><br>

 		<table class="table table-striped datatable" id = "user_account_list" 
 						style="width: 100%; ">                                    
			<thead>
				<tr>
					<th> Agents </th>
 
 					<th> </th>

 				</tr>
			</thead>

			<tbody id = "user_account_body">

				<?php foreach($agents as $row): ?>

					<tr>
						<td> <?php echo $row['agent']; ?></td> 
 
						<td> 
							<a href="#" class="btn btn-success"> 
								<i class="fa fa-edit"></i>  
							</a>

							
							<a href="#" class="btn btn-info "> 
								<i class="fa fa-file-audio-o"></i>  
							</a>
	 				
	 			<?php 

	 				$reassign_agent_data['roster_info'] = $row;

	 				$this->load->view("rosters/modals/reassign_roster-modal", $reassign_agent_data); 

	 			?>

 	  				
				<?php endforeach; ?>

 			</tbody>

		</table>       
 	

 	</div>

</div>

 