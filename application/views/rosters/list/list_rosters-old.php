<div id="content-wrapper">

	<div class="container-fluid">
 

	<script type="text/javascript" src="<?php echo base_url('js/user_list.js'); ?>"></script>

	 <h1 class="page-header">  Rosters Lists </h1>

		<table class="table add_entry">

		<tr><td> Wave Role: <td> <select class="form-control field_filter" id = "1">
										
	 				  							<option>Select Wave List</option>

												<?php foreach($add_roster_data['wave_list'] as $wave_list_row): ?>

		 				  							<option value="<?php echo $wave_list_row['wave_id']; ?>"> 

		 				  								<?php echo $wave_list_row['wave_desc']; ?> 

		 				  							</option>
												
												<?php endforeach; ?>

	 				  						</select>

								</select>

		</table>


	 	<button class="btn btn-info pull-right">Filter <i class="fa fa-filter"></i></button>

		<a href="" data-toggle = "modal" style="text-align: center;" data-target = '#add-roster' class="btn btn-success"> <i class='fa fa-plus'></i> Add Roster </a>

		<a href="" data-toggle = "modal" style="text-align: center;" data-target = '#add-roster' class="btn btn-success"> <i class='fa fa-edit'></i> Update Roster</a>

	 <br><br><br>

 		<table class="table table-striped" id = "user_account_list" style="width: 100%; ">                                    
			<thead>
				<tr>
					<th> Agent Id </th>
					<th> Agent Name </th>
					<th> Team Lead </th>
 					<th> SME </th>
 					<th> Wave </th>
  					<th> Date Created </th>
 					<th> </th>
				</tr>
			</thead>

			<tbody id = "user_account_body">

				<?php foreach($roster_list as $row): ?>

					<tr>
						<td> <?php echo $row['user_account_id']; ?></td> 
						
						<td> <?php echo $row['user_roles_desc']; ?></td> 

						<td> <?php echo $row['username']; ?></td> 

						<td> <?php echo $row['first_name']; ?></td>

						<td> <?php echo $row['last_name']; ?></td>

						<td> <?php echo $row['acct_nick_name']; ?></td>

						<td> <?php echo $row['email']; ?></td>

 						<td> <?php echo date("M d, Y", strtotime($row['date_created'])); ?></td>

 						<td> 
    						<a href="#" class="btn btn-success" data-toggle="modal" data-target="#edit-users<?php echo $row['user_account_id']; ?>"><i class="fa fa-edit"> </i> </a>

    						<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#delete-users<?php echo $row['user_account_id']; ?>"><i class="fa fa-trash"> </i> </a>

						<?php $this->load->view('users/edit_users-modal', $row); ?>


					</tr>
	  				
				<?php endforeach; ?>

 			</tbody>

		</table>       

 
		<?php $this->load->view('rosters/add_rosters-modal', $add_roster_data); ?>

	</div>

</div>

 