<h1 class="page-header">  Wave List </h1>

	<div class="row">

 		<a href="" data-toggle = "modal" style="text-align: center;" 
 		data-target = '#add-wave-modal' class="btn btn-success"> <i class='fa fa-plus'></i> Add Wave </a>


	 <br><br><br>

 		<table class="table table-striped datatable" id = "user_account_list" 
 						style="width: 100%; ">                                    
			<thead>
				<tr>
					<th> QA Staff </th>
 
					<th> # </th>
					<th> </th>

 				</tr>
			</thead>

			<tbody id = "user_account_body">

				<?php foreach($fs_list as $row): ?>

					<tr>
						<td> <?php echo $row['wave']; ?> 			</td> 
						<td> <?php echo $row['agents_cnt']; ?>	</td> 

						<td> 
							<a href="<?php echo base_url('Rosters/view_wave/'.$row['wave']); ?>" class="btn btn-info"> View </a>

 	  				
				<?php endforeach; ?>

 			</tbody>

		</table>       

  
	</div>

</div>

 