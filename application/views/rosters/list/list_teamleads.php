<h1 class="page-header">  Team Leads Lists </h1>

	<div class="row">

	 	<button class="btn btn-info pull-right"> Filter <i class="fa fa-filter"></i></button>

		<a href="" data-toggle = "modal" style="text-align: center;" data-target = '#add-roster' class="btn btn-success"> <i class='fa fa-plus'></i> 
			Add Team Leads 
		</a>


	 <br><br><br>

 		<table class="table table-striped datatable" id = "user_account_list" 
 						style="width: 100%; ">                                    
			<thead>
				<tr>
					<th> Name </th>
 
					<th> # of Agents </th>

					<th> </th>

 				</tr>
			</thead>

			<tbody id = "user_account_body">

				<?php foreach($team_lead_list as $row): ?>

					<tr>
						<td> <?php echo $row['tl']; ?></td> 
						<td>  <a href="<?php echo base_url('Rosters/view_teamlead_agents/'.$row['tl']); ?>"> <?php echo $row['agents_cnt']; ?> </a></td> 

						<td> 								
							<a href="<?php echo base_url('Rosters/view_teamlead_agents/'.$row['tl']); ?>" class="btn btn-info">  
								<i class="fa fa-info"> </i>
							</a>
						</td> 
 	  				
				<?php endforeach; ?>

 			</tbody>

		</table>       

 
	</div>

</div>

 