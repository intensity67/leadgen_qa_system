<h1 class="page-header">  Rosters Lists </h1>

	<div class="row">

	<form>

	  <div class="form-group">
	  	Cluster #: <?php echo $cluster_no; ?>
	  </div>

	</form> 

	 	<button class="btn btn-info pull-right"> Filter <i class="fa fa-filter"></i></button>

		<a href="" data-toggle = "modal" style="text-align: center;" data-target = '#add-roster-modal' class="btn btn-success"> <i class='fa fa-plus'></i> Add Agent Roster </a>

		<a href="" data-toggle = "modal" style="text-align: center;" data-target = '#add-roster' class="btn btn-success"> <i class='fa fa-edit'></i> Update Roster</a>
  
	 	<br><br><br>
	 
	 	<script type="text/javascript" src="<?php echo base_url('js/rosters-list.js'); ?>"></script>

 		<table class="table table-striped" id = "rosters_list" style="width: 100%; ">                                    
			
			<thead>

				<tr>
					<th> Login Id </th>
 
					<th> Group  </th>

					<th> User Level  </th>
					 
  				</tr>

			</thead>

			<tbody >

				<?php $i = 0; ?>

				<?php foreach($agents as $row): ?>

					<tr>

						<td id="login_<?php echo $i; ?>"><?php echo $row['user']; ?></td> 
  
						<td id="login_<?php echo $i; ?>"><?php echo $row['user_group']; ?></td> 

						<td id="login_<?php echo $i; ?>"><?php echo $row['user_level']; ?></td> 
<!-- 					 
						<td id="login_<?php echo $i; ?>"><?php echo $row['event_date']; ?></td> 

						<td id="login_<?php echo $i; ?>"><?php echo $row['event']; ?></td>  
					 -->

						<?php $i++; ?>

				<?php endforeach; ?>

 			</tbody>

		</table>       
	
	</div>
 
 
 