<h1 class="page-header">  Rosters Lists </h1>

	<div class="row">

	<form>

	  <div class="form-group">

	    <label for="wave_list_dropdown">Select Wave List</label>

				<select id= "wave_list_dropdown" class="form-control">
 
 					<option></option>

					<?php foreach($add_roster_data['wave_list'] as $wave_list_row): ?>

						<option value="<?php echo $wave_list_row['wave_id']; ?>"> 

							<?php echo $wave_list_row['wave_desc']; ?> 

						</option>
					
					<?php endforeach; ?>	   

				</select>

		
		</div>

	
	</form> 

	 	<button class="btn btn-info pull-right"> Filter <i class="fa fa-filter"></i></button>

		<a href="" data-toggle = "modal" style="text-align: center;" data-target = '#add-roster-modal' class="btn btn-success"> <i class='fa fa-plus'></i> Add Agent Roster </a>

		<a href="" data-toggle = "modal" style="text-align: center;" data-target = '#add-roster' class="btn btn-success"> <i class='fa fa-edit'></i> Update Roster</a>
  
	 	<br><br><br>
	 
	 	<script type="text/javascript" src="<?php echo base_url('js/rosters-list.js'); ?>"></script>

 		<table class="table table-striped" id = "rosters_list" style="width: 100%; ">                                    
			<thead>
				<tr>
					<th> Login Id </th>
					<th> Employee Id </th>
					<th> Agent Name </th>
					<th> Team Lead </th>
 					<th> Floor Support </th>
 					<th> Wave </th>
  					<th> Quality Assurance </th>
					<th> Cluster No </th>
					<th> Site No </th>
					<th> </th>
 				</tr>
			</thead>

			<tbody >

				<?php $i = 0; ?>

				<?php foreach($roster_list as $row): ?>

					<tr>

						<td id="login_<?php echo $i; ?>"><?php echo $row['login']; ?></td> 

						<td id="emp_<?php echo $i; ?>"><?php echo $row['emp_id']; ?></td> 

						<td id="agent_<?php echo $i; ?>"><?php echo $row['agent']; ?></td> 
 
						<td id="tl_<?php echo $i; ?>"><?php echo $row['tl']; ?></td> 

						<td id="fs_<?php echo $i; ?>"><?php echo $row['fs']; ?></td>

						<td id="wave_<?php echo $i; ?>"><?php echo $row['wave']; ?></td> 

						<td id="qa_<?php echo $i; ?>"><?php echo $row['qa']; ?></td>
						
						<td id="agent_cluster_no_<?php echo $i; ?>"><?php echo $row['agent_cluster_no']; ?></td>
						
						<td id="site_id_<?php echo $i; ?>"><?php echo $row['site_id']; ?></td>

						<td> 

							<a href = "<?php echo base_url('Rosters/edit_rosters/'.$row['login']); ?>" class="btn btn-success edit_agents_modal">

								<i class='fa fa-edit'></i> 

							</a>


							<a href= "<?php echo base_url('Report/qa_results_summary/'.$row['login']); ?>" class="btn btn-info">
									<i class='fa fa-bar-chart'></i> 

							</a>

  	  					</td>
  	  					 
						<?php $i++; ?>

				<?php endforeach; ?>

 			</tbody>

		</table>       
	
	</div>

<?php $this->load->view('rosters/modals/edit_roster_agents-modal'); ?>

<?php $this->load->view('rosters/modals/add-roster-modal'); ?>

 
 