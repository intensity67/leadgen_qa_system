
        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">

                    <div class="col-lg-12">

                        <h1 class="page-header">
                           Cluster Selection <small> Users Clusterings List</small>
                        </h1>

                    </div>

                </div>
                <!-- /.row -->
 
                <!-- /.row -->

                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa   fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"> 1 </div>
                                        <div> Cluster </div>
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo base_url('Rosters/clusters_listings/1'); ?>" target= "_new">
                                <div class="panel-footer">
                                     <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"> 2 </div>
                                        <div> Cluster </div>
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo base_url('Rosters/clusters_listings/2'); ?>" target= "_new">
                                <div class="panel-footer">
                                     <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge"> 3 </div>
                                        <div> Cluster </div>
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo base_url('Rosters/clusters_listings/3'); ?>" target= "_new">
                                <div class="panel-footer">
                                     <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-yellow">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">4</div>
                                        <div> Cluster </div>
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo base_url('Rosters/clusters_listings/4'); ?>" target= "_new">
                                <div class="panel-footer">
                                     <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

                <!-- /.row -->
 
                    <div class="col-lg-3 col-md-6">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="huge">5</div>
                                        <div> Cluster </div>
                                    </div>
                                </div>
                            </div>
                            <a href="<?php echo base_url('Rosters/clusters_listings/5'); ?>" target= "_new">
                                <div class="panel-footer">
                                     <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                    <div class="clearfix"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
 