<h1 class="page-header">  Lists of Floor Support </h1>

	<div class="row">

	 	<button class="btn btn-info pull-right"> Filter <i class="fa fa-filter"></i></button>

		<a href="" data-toggle = "modal" style="text-align: center;" data-target = '#add-roster' class="btn btn-success"> <i class='fa fa-plus'></i> Add SME </a>


	 <br><br><br>

 		<table class="table table-striped datatable" id = "user_account_list" 
 						style="width: 100%; ">                                    
			<thead>
				<tr>
					<th> Agent Name </th>
 
					<th> </th>
					<th> </th>

 				</tr>
			</thead>

			<tbody id = "user_account_body">

				<?php foreach($fs_list as $row): ?>

					<tr>
						<td> <?php echo $row['fs']; ?> 			</td> 
						<td> <?php echo $row['agents_cnt']; ?>	</td> 

						<td>

						<form action = "<?php echo base_url('Rosters/view_fs_agents'); ?>" method="POST">

							<button type="submit" name="" class="btn btn-info">
								<i class="fa fa-info"> </i>
							</button> 

							<input type="hidden" name = "fs" value="<?php echo $row['fs']; ?>">

 						</form>
 	  				
				<?php endforeach; ?>

 			</tbody>

		</table>       

  
	</div>

</div>

 