<h1 class="page-header">  Lists of Agents </h1>

	<div class="row">

		<a href="" data-toggle = "modal" style="text-align: center;" data-target = '#add-roster' class="btn btn-success"> <i class='fa fa-plus'></i> Add Team Lead </a>


	 <br><br><br>

 		<table class="table table-striped datatable" id = "user_account_list" 
 						style="width: 100%; ">                                    
			<thead>
				<tr>
					<th> Agent Name </th>
 
 					<th> Team Lead </th>

 					<th> Floor Support </th>

					<th> </th>

 				</tr>
			</thead>

			<tbody id = "user_account_body">

				<?php foreach($agents_list as $row): ?>

					<tr>
						<td> <?php echo $row['agent']; ?>	
						<td> <?php echo $row['fs']; ?> 		
						<td> <?php echo $row['tl']; ?> 		

						<td> 

							<a href = "<?php echo base_url('Report/qa_results_summary'); ?>" class="btn btn-info"> <i class="fa fa-info"> </i> </a>
	 	  				
							<a href= "#" data-toggle = "modal"
										data-target = "#reassign_roster-modal<?php echo $row['login']; ?>" 
										class="btn btn-warning">
									<i class='fa fa-reply'></i> 
								</a>

							<a href= "<?php echo base_url('Report/qa_results_summary/'.$row['login']); ?>" class="btn btn-success" target= "_blank">
									<i class='fa fa-bar-chart'></i> 
							</a>

							<?php 
							
							$add_roster_data['roster_info'] = $row;

							?>

							<?php $this->load->view('rosters/modals/reassign_roster-modal', 
									$add_roster_data); ?>


				<?php endforeach; ?>

 			</tbody>

		</table>       

  
	</div>

</div>

 