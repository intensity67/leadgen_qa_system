<style type="text/css">
	
	textarea{
		resize: none;
	}	

</style>
	
	<h3>Update Roster</h3>

 	      <form action = "<?php echo base_url('Rosters/update_roster'); ?>" method="POST">
 
 				
 				<table class="table table-condensed">
 
					<tr><td> Login ID: <td><input type="text" id= "login_id"  class="form-control" value="<?php echo $roster_info['login']; ?>" disabled>
					
					<tr><td> Employee ID: <td><input type="text" id= "emp_id" name="emp_id" class="form-control"  value="<?php echo $roster_info['emp_id']; ?>">
					
					<tr><td> Agent: <td><input type="text" id= "agent" name="agent" class="form-control" value="<?php echo $roster_info['agent']; ?>" >

					<tr><td> Team Leads: <td><input type="text" id= "tl" name="tl" class="form-control" value="<?php echo $roster_info['tl']; ?>" >
				
					<tr><td> Floor Support: <td><input type="text" id= "fs" name="fs" class="form-control" value="<?php echo $roster_info['fs']; ?>" >

					<tr><td> Wave: <td><input type="text" name="wave" id= "wave" class="form-control" value="<?php echo $roster_info['wave']; ?>" >

					<tr><td> QA: <td><input type="text" name="qa" id= "qa" class="form-control" value="<?php echo $roster_info['qa']; ?>" >

					<tr><td> Agent Cluster No: <td><input type="text" name="agent_cluster_no" id= "agent_cluster_no" class="form-control" value="<?php echo $roster_info['agent_cluster_no']; ?>" >

  				</table>
 				
			<input type="hidden" value="<?php echo $roster_info['login']; ?>" name="login_id">
 
			<button type="submit" class="btn btn-success"> Update Roster <i class="fa fa-save"> </i> </button>

	    </form>