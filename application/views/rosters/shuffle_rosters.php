<h1 class="page-header">  Rosters Lists </h1>

	<div class="row">

	<form>

	  <div class="form-group">

	    <label for="wave_list_dropdown">Select Wave List</label>

				<select id= "wave_list_dropdown" class="form-control">
 
 					<option></option>

					<?php foreach($add_roster_data['wave_list'] as $wave_list_row): ?>

						<option value="<?php echo $wave_list_row['wave_id']; ?>"> 

							<?php echo $wave_list_row['wave_desc']; ?> 

						</option>
					
					<?php endforeach; ?>	   

				</select>

		
		</div>

	  </div>
	
	</form> 

	 	<button class="btn btn-info pull-right"> Filter <i class="fa fa-filter"></i></button>

		<a href="" data-toggle = "modal" style="text-align: center;" data-target = '#add-roster' class="btn btn-success"> <i class='fa fa-plus'></i> Add Agent Roster </a>

		<a href="" data-toggle = "modal" style="text-align: center;" data-target = '#add-roster' class="btn btn-success"> <i class='fa fa-edit'></i> Update Roster</a>

	 <br><br><br>

 		<table class="table table-striped datatable" id = "user_account_list" 
 						style="width: 100%; ">                                    
			<thead>
				<tr>
					<th> Login Id </th>
					<th> Agent Name </th>
					<th> Team Lead </th>
 					<th> Floor Support </th>
 					<th> Wave </th>
  					<th> Quality Assurance </th>
					<th> </th>
 				</tr>
			</thead>

			<tbody id = "user_account_body">

				<?php foreach($roster_list as $row): ?>

					<tr>
						<td> <?php echo $row['login']; ?></td> 
						
						<td> <?php echo $row['agent']; ?></td> 

						<td> <?php echo $row['tl']; ?></td> 

						<td> <?php echo $row['wave']; ?></td> 

						<td> <?php echo $row['fs']; ?></td>

						<td> <?php echo $row['qa']; ?></td>

						<td> 

							<a href= "" class="btn btn-success">
								<i class='fa fa-edit'></i> 
							</a>

						<?php $this->load->view('rosters/add_rosters-modal', $add_roster_data); ?>

 
 	  				
				<?php endforeach; ?>

 			</tbody>

		</table>       

 
		<?php $this->load->view('rosters/add_rosters-modal', $add_roster_data); ?>

	</div>

</div>

 