<style type="text/css">
	
	textarea{
		resize: none;
	}	

</style>

	<div id="add-roster" class="modal fade" role="dialog">
	  <div class="modal-dialog"> 

	    <!--  -->
	    <div class="modal-content">
	      <div class="modal-header alert alert-success">   
	        <button type="button" class="close" data-dismiss="modal"></button>
	        <h2 class="modal-title">Add Roster <i class="fa fa-user"></i> </h4>
	        </span>
	      </div>

	      <form action = "<?php echo base_url('user/insert_roster'); ?>" method="POST">

	      <div class="modal-body">
 				
			<table class="table table-condensed">

		      <form action = "<?php echo base_url('user/insert_roster'); ?>" method="POST">

	 				<table class="table table-condensed">

 				  	<tr><td> Login Id : <td> <input type="text" class="form-control"  name="login_id"> 

 				  	<tr><td> Agent Name : <td> <input type="text" class="form-control"  name="agent_name"> 

 				  	<tr><td> Wave List : <td> <select class="form-control" name="user_roles_id"> 
 				  								
	 				  							<option>Select Wave List</option>

												<?php foreach($wave_list as $wave_list_row): ?>

		 				  							<option value="<?php echo $wave_list_row['wave_id']; ?>"> 

		 				  								<?php echo $wave_list_row['wave_desc']; ?> 

		 				  							</option>
												
												<?php endforeach; ?>

	 				  						</select>

 				  	<tr><td> Team Lead: <td> <select class="form-control" name="user_roles_id"> 
 				  								
	 				  							<option>Select Team Lead </option>

												<?php foreach($team_lead_list as $team_lead_list_row): ?>

		 				  							<option value="<?php echo $team_lead_list_row['user_account_id']; ?>"> 

		 				  								<?php echo $team_lead_list_row['first_name'] . ' ' .$team_lead_list_row['last_name']; ?> 

		 				  							</option>
												
												<?php endforeach; ?>

	 				  						</select>
 
 				  	<tr><td> Supervisor Id: <td> <select class="form-control" name="user_roles_id"> 
 				  								
	 				  							<option>Select Supervisor </option>

												<?php foreach($supervisor_list as $supervisor_list_row): ?>

		 				  							<option value="<?php echo $supervisor_list_row['user_account_id']; ?>"> 

		 				  								<?php echo $supervisor_list_row['first_name'] . ' ' .$supervisor_list_row['last_name']; ?> 

		 				  							</option>
												
												<?php endforeach; ?>

	 				  						</select>
 
 
 				</table>


		      <div class="modal-footer">


		        <button type="submit" class="btn btn-success"> Save Roster <i class="fa fa-save"> </i> </button>

		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

			</div>

	    </form>
		
		<div>

	</div>

</div>