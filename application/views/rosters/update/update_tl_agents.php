<h3 class="page-header"> Agents Lists Team Lead: <b> <?php echo $tl; ?> </b> </h3>

	<div class="row">
 		
	 <br><br><br>

 	 <button type = "submit" class="btn btn-primary btn-lg btn-block"> Update Agents <i class="fa fa-save"></i></button>

 		<table class="table table-striped" id = "user_account_list" 
 						style="width: 100%; ">                                    
			<thead>
				<tr>
					<th> Agents </th>
 
 					<th> Team Lead </th>
 					<th> SME </th>

 				</tr>
			</thead>

			<tbody id = "user_account_body">

				<?php foreach($agents as $row): ?>

					<tr>
						<td> <?php echo $row['agent']; ?></td> 
 
						<td> 
 							<select class="form">

			 					<option></option>

								<?php foreach($reassign_agent_data['tl_list'] as $tl_row): ?>

									<option value="<?php echo $tl_row['tl']; ?>" 
										<?php if($current_tl == $tl_row['tl']) 
												echo 'selected'; 
										?>> 

										<?php echo $tl_row['tl']; ?> 
	 									
 									</option>
						
								<?php endforeach; ?>	

							</select>

						<td> 
 							<select class="form">

			 					<option></option>

								<?php foreach($reassign_agent_data['fs_list'] as $fs_row): ?>

									<option value="<?php echo $fs_row['fs']; ?>" 
										<?php if(1) 
												echo 'selected'; 
										?>> 

										<?php echo $fs_row['fs']; ?> 
	 									
 									</option>
						
								<?php endforeach; ?>	

							</select>
					<?php 

					 	$reassign_agent_data['roster_info'] = $row;
					 	
					 	$this->load->view("rosters/modals/reassign_roster-modal", $reassign_agent_data); 
					
					?>
					
  				<?php endforeach; ?>

 			</tbody>

		</table>       

 	 <button type = "submit" class="btn btn-primary btn-lg btn-block"> Update Agents <i class="fa fa-save"></i></button>

 	</div>

</div>

 