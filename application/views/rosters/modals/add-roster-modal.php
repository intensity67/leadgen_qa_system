<style type="text/css">
	
	textarea{
		resize: none;
	}	

	#two_accts{
		width: 25px;
		height: 25px;
	}

</style>

<div id="add-roster-modal" class="modal fade" role="dialog">

	  <div class="modal-dialog"> 

	    <!--  -->
	    <div class="modal-content">
	      <div class="modal-header alert alert-warning">   
	        <button type="button" class="close" data-dismiss="modal"></button>
	        <h2 class="modal-title"> Add Roster <i class="fa fa-user"></i> </h4>
	        </span>
	      </div>

	      <form action = "<?php echo base_url('Rosters/insert_roster'); ?>" method="POST">

	      <div class="modal-body">
 				
			<table class="table table-condensed">

		      <form action = "<?php echo base_url('Rosters/insert_roster'); ?>" method="POST">

	 				<table class="table table-condensed">

	 				<tr><td> Agent has 2 accounts <td> <input type="checkbox" id= "two_accts" name="two_accts">

	 				<tr><td> Agent Name: <td> <input type="text" name="agent" class="form-control">

	 				<tr><td> Agent Login Id 1: <td> <input type="text" name="login1" class="form-control">

   				  	<tr><td> Wave 1: <td> 

						<select id= "wave_id1" name="wave1" class="form-control">
 
		 					<option></option>

							<?php foreach($wave_list as $wave_row): ?>

								<option value="<?php echo $wave_row['wave_desc']; ?>"> 

									<?php echo $wave_row['wave_desc']; ?> 
 
								</option>
					
							<?php endforeach; ?>	
						
						</select>

	 				<tr class="agent_login2"><td> Agent Login Id 2: <td> <input type="text" name="login2" class="form-control">
 
    				<tr class="agent_login2"><td> Wave 2: <td> 

						<select id= "wave2" name="wave2" class="form-control">
 
		 					<option></option>

							<?php foreach($wave_list as $wave_row): ?>

								<option value="<?php echo $wave_row['wave_desc']; ?>"> 

									<?php echo $wave_row['wave_desc']; ?> 
 
								</option>
					
							<?php endforeach; ?>	
 
						</select>
 
  				  	<tr><td> Team Lead: <td>

						<select id= "tl" name="tl" class="form-control">
 
		 					<option></option>

							<?php foreach($team_lead_list as $tl_row): ?>

								<option value="<?php echo $tl_row['tl']; ?>"> 
 									
									<?php echo $tl_row['tl']; ?> 

								</option>
					
							<?php endforeach; ?>	   

						</select>
   				  
   				  	<tr><td> Floor Support: <td> 

						<select name="fs" class="form-control">
 
		 					<option></option>

							<?php foreach($fs_list as $fs_row): ?>

								<option value="<?php echo $fs_row['fs']; ?>"> 

									<?php echo $fs_row['fs']; ?> 

									<?php 

										if($fs_row['fs'] == NULL) {

												echo "None"; 

											}
									?> 

								</option>
					
							<?php endforeach; ?>	   

						</select>
    

						</select>

 				</table>


		      <div class="modal-footer">


		        <button type="submit" class="btn btn-success"> Add Roster <i class="fa fa-save"> </i> </button>

		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

			</div>

	    </form>
		
		<div>

	</div>

</div>