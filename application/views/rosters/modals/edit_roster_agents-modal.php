<style type="text/css">
	
	textarea{
		resize: none;
	}	

</style>

	<div id="edit-users" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!--  -->
	    <div class="modal-content">
	      <div class="modal-header alert alert-success">   
	        <button type="button" class="close" data-dismiss="modal"></button>
	        <h2 class="modal-title">Edit Agents <i class="fa fa-user"></i> </h4>
	        </span>
	      </div>

	      <form action = "<?phpe echo base_url('user/insert_user'); ?>" method="POST">

	      <div class="modal-body">
 				
 				<table class="table table-condensed">
 
					<tr><td> Login ID: <td><input type="text" id= "login_id" name="" class="form-control" >
					
					<tr><td> Employee ID: <td><input type="text" id= "emp_id" name="" class="form-control" >
					
					<tr><td> Agent: <td><input type="text" id= "agent" name="" class="form-control" >

					<tr><td> Team Leads: <td><input type="text" id= "tl" name="" class="form-control" >
				
					<tr><td> Floor Support: <td><input type="text" id= "fs" name="" class="form-control" >

					<tr><td> Wave: <td><input type="text" name="" id= "wave" class="form-control" >

					<tr><td> QA: <td><input type="text" name="" id= "qa" class="form-control" >

					<tr><td> Agent Cluster No: <td><input type="text" name="" id= "agent_cluster_no" class="form-control" >

					<tr><td> Site Id: <td><input type="text" name="" id= "site_id" class="form-control" >

 				</table>

 	      </div>

	      <div class="modal-footer">

	        <button type="submit" class="btn btn-success" data-dismiss="modal"> Update Roster <i class="fa fa-save"> </i> </button>

	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

	      </div>
	    
	    </form>
	      
	    </div>

	  </div>
	</div>
 				  	