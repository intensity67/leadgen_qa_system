<h1 class="page-header"> Agents Lists SME: <?php echo $fs; ?> </h1>

	<div class="row">
 
	 <br><br><br>

 		<table class="table table-striped datatable" id = "user_account_list" 
 						style="width: 100%; ">                                    
			<thead>
				<tr>
					<th> Login Id </th>
					<th> Agents </th>
					<th> Cluster No.</th>
					<th> Last Audit </th>
  
 					<th> </th>

 				</tr>
			</thead>

			<tbody id = "user_account_body">

				<?php foreach($agents as $row): ?>

					<tr>
						<td><?php echo $row['login']; ?></td> 

						<td><?php echo $row['agent']; ?></td> 

						<td><?php echo $row['agent_cluster_no']; ?></td> 

						<td><?php if(!empty($row['audit_info']['audit_timestamp'])) 
									echo date("M d, Y h:i:s A", strtotime($row['audit_info']['audit_timestamp'])); ?></td> 
 						<td> 
							
 
								<a type="submit" class="btn btn-primary" href="<?php echo base_url("Recording/view_agents_recordings/".$row['login']."/") ?>" target = "_blank"> 
									
									<i class="fa fa-file-audio-o"></i>  
								
								</a>

								<a type="submit" class="btn btn-success" href="<?php echo base_url("Report/qa_results_summary/".$row['login']."/") ?>" target = "_blank"> 
									
									<i class="fa fa-bar-chart"></i>  
								
								</a>

								<input type="hidden" name = "agent_login_id" value="<?php echo $row['login']; ?>">

								<input type="hidden" name = "agent_name" value="<?php echo $row['agent']; ?>">
								
								<input type="hidden" name = "agent_cluster_no" value="<?php echo $row['agent_cluster_no']; ?>">

  	  						</form>

<!--   	  						<button class="btn btn-success">
  	  							<i class="fa fa-edit"></i>
  	  						</button> -->

				<?php endforeach; ?>

 			</tbody>

		</table>       
 	

 	</div>

</div>

 