<div id="content-wrapper">

		<div class="container-fluid">
			
			<h2> Agents Search </h2>

			<form action="<?php echo base_url('Recording/agents_search_results'); ?>" method = 'POST'>

					<table class="table">

						<tr><td> Agent Name: 	<td><input type="text" name="agent_name" class="form-control">

						<tr><td> Agent ID: 		*<td><input type="text" name="agent_login_id" class="form-control" required="">		

						<tr><td> Agent Cluster #: *<td>

												<select name="cluster" required="" class="form-control"> 
														
														<option value=1>1</option>
														<option value=2>2</option>
														<option value=3>3</option>
														<option value=4>4</option>

													</select>	

 						<tr><td> Call Type: 		*<td><select name="call_type" required="" class="form-control"> 
														<option value="short_calls">Short Calls</option>
														<option value="long_calls">Long Calls</option>
													</select>	

						<tr><td> Disposition:	<td><select class="form-control" name="status" required>
 											<option> Select Disposition </option>

 											<?php foreach($disposition_list as $dispo_row): ?>

 												<option value="<?php echo $dispo_row['dispo_code']; ?>"

 													<?php if($dispo_row['dispo_code'] == 'NI') echo 'selected'; ?>> 

 													<?php echo $dispo_row['dispo_code']; ?> 
 												
 												</option>

 											<?php endforeach; ?>

 										</select>

						<tr><td># of Calls: 	<td><select name="no_calls" class="form-control"> 
														<option value=5>5</option>
														<option value=10>10</option>
														<option value=15>15</option>
														<option value=20>20</option>
														<option value=30>30</option>
													</select>		
						
					</table>
					
					<button type="submit" class="btn btn-success btn-lg"> Search <i class="fa fa-search"></i></button>
			</form>
	
		</div>

</div>