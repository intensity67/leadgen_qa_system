<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/select2/css/select2.css'); ?>">

<script type="text/javascript" src="<?php echo base_url('assets/select/js/js/select2.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('js/trash-recording.js'); ?>"></script>

<script type="text/javascript" src="<?php echo base_url('assets/select/js/select2.full.min.js'); ?>"></script>


<center>

	 <h1 class="page-header"> Agents Recordings: </h1>

	 <h4> Agent Name: <?php echo $agent_name; ?>
	
	 <h4> Agent Login: <?php echo $agent_login; ?>

	 <br><br>

  			<small class="text text-info"> 
  				Recordings are automatically reloaded if all recordings are empty.
  			</small>

	 <br><br>

	 <?php if($this->session->flashdata('error_recordings_query')) {?>
		
		<div class="alert alert-danger"> No recordings found at this moment. </div> 

		<div class = 'alert alert-info'> It could be that agent's cluster no is wrong or that the agent has no recordings yet in that specific disposition and recording type (long/short call) or that agent is in the wrong cluster </div>
		
		<br> <a href= "#"> Change Agent Cluster? </a>
 		
		<br><br><br>

	<?php } ?>

 	<div class = "row"> 

 	<table class="table table-striped" id = "recordings_list">
                                          
			<thead>

				<tr>
					<td>  
 					<td> Recording id
 					<td> Lead Id
					<td> Date/Time
					<td> Disposition
					<td> Seconds

					<?php if($this->session->userdata("login_info")['user_roles_id'] == ADMIN ): ?> 
						<td> Login ID
						<td> Agent Name
						<td> Cluster No
 					
 					<?php endif; ?>

  					<td>  
  				
			</thead>

			<tbody>

				<?php $i = 1; ?>
				
				<?php foreach($recording_list as $row): ?>

					<tr>
						 
						<td style="width: 30px;"> 

   						<?php 
   						
   							$data 		  = $row;

   							$data['cnt']  = $i;
							
							$row['audio'] = audio_directory($data); 

   						?>

						<!-- 

						<audio controls style = "width: 150px; height: 70px;" >
							
							<source src="<?php echo $row['audio']; ?>" 
								type="audio/ogg">

							<source src="<?php echo $row['audio']; ?>" >
	  
								Your browser does not support the audio element.

						</audio> -->
 
						<td> <?php echo $row['recording_id']; ?>

						<td> <?php echo $row['lead_id']; ?> 

						<td> <?php echo date("M, d Y h:i:s", strtotime($row['start_time'])); ?>

						<td> <?php echo $row['status']; ?>

						<td> <?php echo $row['length_in_sec']; ?>
						
									
							<?php if($this->session->userdata("login_info")['user_roles_id'] == ADMIN ): ?> 
								
								<td><?php echo $row['user']; ?>
								<td><?php echo $row['agent']; ?>
								<td><?php echo $row['cluster_no']; ?>

							<?php endif; ?>

 						<td> 
							<a href = "#" data-toggle="modal" data-target="#view-recording-details<?php echo $row['recording_id']; ?>" class="btn btn-info"> 
							
							<i class="fa fa-eye"> </i> </a>

							<!-- <a href="#" class="btn btn-success" data-toggle="modal" data-target="#add-audit-long-call-form<?php echo $row['recording_id']; ?>"><i class="fa fa-headphones"> </i> </a>
							 -->

    						<a href="<?php echo base_url('Audit/add_audit_long_call_form/'). $row['recording_id'].'/'.$i; ?>" class="btn btn-primary"><i class="fa fa-headphones"> </i> </a>

    						<a href="#" class="btn btn-warning" data-toggle="modal" data-target="#add-flagged-call-modal<?php echo $row['recording_id']; ?>"> 
    							<i class="fa fa-flag"> </i> 
    						</a>


    						<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#trash-recording<?php echo $row['recording_id'] ?>">
    							<i class="fa fa-trash"> </i> 
    						</a>
 
	 						<?php $add_flag_call_filter['rec_info'] = $row; ?>


							<?php $this->load->view('recording/modals/view-recording-modal', $row); ?>

 	 						<?php $this->load->view('recording/modals/trash-recording', $row); ?>

							<?php $this->load->view('flagged_call/modals/add_flagged_call_modal', $add_flag_call_filter); ?>

 				<?php $i++; ?>


 				<?php endforeach; ?>

 			</tbody>

	</table>
	
	 <center> <a href = "#" class="btn btn-danger" data-toggle="modal" 
	 	data-target="#clear-recordings-agents-modal"> Clear Agent Recordings </a> 
	 </center>

	<?php 

		$clear_recordings_data['title']  	 = $page_title;
	?>

	<?php $this->load->view('recording/modals/clear-recordings', $clear_recordings_data); ?>

	<?php $this->load->view('recording/modals/clear-recordings-agents', 

							array('agent_login_id' => $agent_login,
									'agent_name' 	=> $agent_name)); ?>

	<?php 

		$rosters_info['agent_name']  	 = $agent_name;

		$rosters_info['agent_login']  	 = $agent_login;

	?>
 
 	<?php $this->load->view('rosters/modals/change_cluster', $rosters_info); ?>

	</div>

</center>