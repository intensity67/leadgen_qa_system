
<div id="content-wrapper">

		<div class="container-fluid">
			
			<h2> Q.A. Lists </h2>

	 		<table class="table table-striped datatable" id = "user_account_list" 
	 						style="width: 100%; ">                                    
				<thead>
					<tr>
						<th> Q.A </th>
						<th> </th>
	 				</tr>
				</thead>

				<tbody id = "qa_">

					<?php foreach($qa_lists as $row): ?>

						<tr>
							<td> <?php echo $row['roster_support_name']; ?></td> 
							
							<td> <?php echo $row['agent']; ?></td> 

							<td> 

								<a href= "" class="btn btn-success">
									<i class='fa fa-edit'></i> 
								</a>

							<?php $this->load->view('rosters/add_rosters-modal', $add_roster_data); ?>

	 
	 	  				
					<?php endforeach; ?>

	 			</tbody>

			</table>       


		</div>

</div>