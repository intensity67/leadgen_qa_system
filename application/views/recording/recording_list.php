
<script type="text/javascript" src="<?php echo base_url('js/trash-recording.js'); ?>"></script>

 <h1 class="page-header"> <?php echo $page_title; ?> </h1>
 
	<br><br>


		<button type="submit" class="btn btn-danger" data-toggle="modal" data-target="#clear-recordings"> 		
			Clear All Recordings <i class="fa fa-eraser"></i> 
		</button>
		
		<input type="hidden" value = "<?php echo $call_type; ?>" name="call_type">
		
	<br><br>

 	<div class = "row" > 

 	<table class="table table-striped" id = "recordings_list">
                                          
			<thead>

				<tr>
					<td>  
 					<td> Recording Id
 					<td> Lead Id
					<td> Date/Time
					<td> Disposition
					<td> Seconds

					<?php if($this->session->userdata("login_info")['user_roles_id'] == ADMIN ): ?> 

						<td> Login ID
						<td> Agent Name
						<td> Cluster No
 					
 					<?php endif; ?>

  					<td>  
  				
			</thead>

			<tbody>

				<?php $i = 1; ?>
				
				<?php foreach($recording_list as $row): ?>

					<tr>
						 
						<td style="width: 30px;"> 

   						<?php 
   						
   							$data 		  = $row;

   							$data['cnt']  = $i;
							
							$row['audio'] = audio_directory($data); 

   						?>
<!-- 
						<audio controls style = "width: 150px; height: 70px;" >
							
							<source src="<?php echo $row['audio']; ?>" 
								type="audio/ogg">

							<source src="<?php echo $row['audio']; ?>" >
	  
								Your browser does not support the audio element.

						</audio> -->
 
						<td> <?php echo $row['recording_id']; ?>

						<td> <?php echo $row['lead_id']; ?> 

						<td> <?php echo date("M, d Y h:i:s", strtotime($row['start_time'])); ?>

						<td> <?php echo $row['status']; ?>

						<td> <?php echo $row['length_in_sec']; ?>
						
									
							<?php if($this->session->userdata("login_info")['user_roles_id'] == ADMIN ): ?> 
								
								<td><?php echo $row['user']; ?>
								<td><?php echo $row['agent']; ?>
								<td><?php echo $row['cluster_no']; ?>

							<?php endif; ?>

 						<td> 
							<a href = "#" data-toggle="modal" data-target="#view-recording-details<?php echo $row['recording_id']; ?>" class="btn btn-info"> 
							
							<i class="fa fa-eye"> </i> </a>

							<!-- <a href="#" class="btn btn-success" data-toggle="modal" data-target="#add-audit-long-call-form<?php echo $row['recording_id']; ?>"><i class="fa fa-headphones"> </i> </a>
							 -->

    						<a href="<?php echo base_url('Audit/add_audit_long_call_form/'). $row['recording_id'].'/'.$i; ?>" class="btn btn-primary"><i class="fa fa-headphones"> </i> </a>

    						<a href="#" class="btn btn-warning" data-toggle="modal" data-target="#add-flagged-call-modal<?php echo $row['recording_id']; ?>"> 
    							<i class="fa fa-flag"> </i> 
    						</a>


    						<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#trash-recording<?php echo $row['recording_id'] ?>">
    							<i class="fa fa-trash"> </i> 
    						</a>
 
	 						<?php $add_flag_call_filter['rec_info'] = $row; ?>


							<?php $this->load->view('recording/modals/view-recording-modal', $row); ?>

 	 						<?php $this->load->view('recording/modals/trash-recording', $row); ?>

							<?php $this->load->view('flagged_call/modals/add_flagged_call_modal', $add_flag_call_filter); ?>

 				<?php $i++; ?>


 				<?php endforeach; ?>

 			</tbody>

	</table>
	
	<?php 

		$clear_recordings_data['title']  	 = $page_title;
		$clear_recordings_data['call_type']  = $call_type;

	?>

	<?php $this->load->view('recording/modals/clear-recordings', $clear_recordings_data); ?>

 
</div>
  