<h1 class="page-header">  SME Group Recordings </h1>

<div class="form-group">

<table class="table">
  <tr>

    <td>Auditor:

    <td> <select id = "auditor_filter" class="form-control">

            <option></option>
           
            <option value="Lester">Lester</option>
            <option value="Ish">Ish</option>
            <option value="Francis">Francis</option>
          
    </select> 
</table>

  </p>

</div>

<script type="text/javascript" src="<?php echo base_url('js/group-recordings.js'); ?>"></script>

<div class="row">


 	<table class="table table-striped" id = "group_list">
                                          
		<thead>

				<tr>
  					<td> SME/TL
 					  <td> Agents Count
 						
 						<?php if($this->session->userdata("login_info")['user_roles_id'] == ADMIN): ?>

  						<td> QA

  						<?php endif; ?>
 					
            <td> 
            <td>
  				
		</thead>

		<tbody>
      
        <?php $total_audit_cnt = 0; ?>

 				<?php foreach($sme_lists as $row): ?>

					<tr>

            <?php if($row['agents_cnt'] != 0): ?>
 
 				  		<td><?php echo $row['fs']; ?>

   						<td><?php echo $row['agents_cnt']; ?>

 						  <?php if($this->session->userdata("login_info")['user_roles_id'] == ADMIN): ?>

  						<td> <?php echo $row['qa']; ?>

  						<?php endif; ?>

    
  						<td>

                <form action="<?php echo base_url('Recording/view_fs_agents_recordings/'.$row['fs']); ?>" method="POST" target="_new">
 
                  <button type="submit" class="btn btn-primary">         
                          Agents List
                  </button>

                  <input type="hidden" name="fs" value="<?php echo $row['fs']; ?>">
  
  						  </form>

              <td><form action="<?php echo base_url('Recording/view_recent_agents_recordings'); ?>" method="POST" target="_new">

                  <button type="submit" name="" class="btn btn-info" > 
                    All Agents Recent Recordings
                  </button>

                </form>

                <?php //$total_audit_cnt += $row['auditCount']; ?>

              <?php endif; ?>

				<?php endforeach; ?>

		</tbody>
		
	</table>
  <!-- 
  <h4>Total Audits: <?php echo $total_audit_cnt; ?></h4> -->

</div>
