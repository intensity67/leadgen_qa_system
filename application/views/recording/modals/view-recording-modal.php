
<div id="view-recording-details<?php echo $recording_id; ?>" class="modal fade" role="dialog">
	
	  <div class="modal-dialog">

	    <div class="modal-content">

	      <div class="modal-header alert alert-primary">

	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h2 class="modal-title"> <center>  Recording Info </center> </h2>
	
	      </div>

	      <div class="modal-body" >

 				<center>

					<audio controls>
							
							<source src="<?php echo $audio; ?>" 
								type="audio/ogg">

							<source src="<?php echo $audio; ?>" >
	  
								Your browser does not support the audio element.

					</audio>

	 				<table class="table table-condensed table-striped">
	 				 
	 					<tr><td> Recording Id: <td><?php echo $recording_id; ?>

	 					<tr><td> Phone Number: <td><?php echo $phone_number; ?>

	 					<tr><td> Start Time: <td><?php echo $start_time; ?>

	   					<tr><td> Start Epoch: <td><?php echo $start_epoch; ?>

	   					<tr><td> End Time: <td><?php echo $end_time; ?>

	   					<tr><td> End Epoch: <td><?php echo $end_epoch; ?>

	   					<tr><td> Length in Seconds: <td><?php echo $length_in_sec; ?>

	   					<tr><td> Length in Minutes: <td><?php echo $length_in_min; ?>

	   					<tr><td> Lead Id: <td><?php echo $lead_id; ?>

	   					<tr><td> Vici Dial: <td><?php echo $vicidial_id; ?>
						
						<?php if($this->session->userdata("login_info")['user_roles_id'] == ADMIN ): ?> 

	   					<tr><td><b><h2> Agent Info </h2> <td>

		   					<tr><td> Login Id: 			<td><?php echo $user; ?>

		   					<tr><td> Agent Name: 		<td><?php echo $agent; ?>
		   					
		   					<tr><td> Team Lead: 		<td><?php echo $tl; ?>

		   					<tr><td> Floor Support:		<td><?php echo $fs; ?>

		   					<tr><td> Cluster No: 		<td><?php echo $cluster_no; ?>

						<?php endif; ?> 

	   					<tr><td><b><h2> Misc </h2> <td>

		   					<tr><td> Location: <td><?php echo $location; ?>

		 					<tr><td> Channel: <td><?php echo $channel; ?>

		 					<tr><td> Server IP: <td><?php echo $server_ip; ?>

		 					<tr><td> Extension: <td><?php echo $extension; ?>

	   						<tr><td> Filename: <td><?php echo $filename; ?>
	 					
	 					</select>

	 				</table>

 				</center>
 	      </div>

	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>

	  </div>

</div>