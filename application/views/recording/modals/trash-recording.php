<div id="trash-recording<?php echo $recording_id; ?>" class="modal fade" role="dialog">
	  <div class="modal-dialog">
 
	    <!--  -->
	    <div class="modal-content">

	      <div class="modal-header">

	        <button type="button" class="close" data-dismiss="modal">&times;</button>

	        <h2 class="modal-title"> Delete Recording Confirmation </h4>

	      </div>

	      <div class="modal-body">

	      	<form action = "<?php echo base_url('Recording/trash_recording'); ?>" 				method="POST" id = "trash_recording_form">

			<h4><span class="text text-danger"> Are you sure you want to delete this recording? </span> </h4>
 					
 				<table class="table table-condensed text text-info" style="margin-top: 25px;">  

 					<tr><td>Recording Id: 	<td><?php echo $recording_id; ?> 
 					
 					<tr><td>State Reason:  	<td>
 					 
 										<select class="form-control state_reason" id = "state_reason" name="state_reason_dropdown">

 												<option value="Empty Recording"> 
 													Empty Recording
 												</option>
 												<option value="Hung Up"> Hung Up </option>

 												<option value="Others"> Others </option>

 											</select>
		 									
		 									<br><textarea class="form-control state_reason_tb" name="state_reason_tb" id = "state_reason_tb"></textarea>

 					<input type = "hidden" name="recording_id" 
 						value="<?php echo $recording_id; ?>">

 				</table>

 	      </div>

	      <div class="modal-footer">
	      	
	        	<button type="submit" class="btn btn-danger"> Trash Recording </button>
				
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

	        </form>


	      </div>

	    </div>

	  </div>

</div>