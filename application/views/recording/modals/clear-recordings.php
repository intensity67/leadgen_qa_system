<div id="clear-recordings" class="modal fade" role="dialog">
	  <div class="modal-dialog">
 
	    <!--  -->
	    <div class="modal-content">

	      <div class="modal-header">

	        <button type="button" class="close" data-dismiss="modal">&times;</button>

	        <center> <h2 class="modal-title"> Clear Recordings </h2> </center>

	      </div>
 
	      	<form action = "<?php echo base_url('Recording/clear_recordings'); ?>" 				method="POST" id = "trash_recording_form">
	      	
	      	<div class="modal-body">

					<h4>
						<center>
							<span class="text text-danger"> 
								Are you sure to clear all recordings? 
							</span> 
						</center>
					</h4>
		 					
	 				<table class="table table-condensed text text-info" style="margin-top: 25px;">  
	 					
	 					<tr><td> Clearing Reasons:	<td>
	 					 
	 										<select class="form-control state_reason" id = "state_reason" name="state_reason_dropdown">

	 												<option value="Empty Recording"> 
	 													Empty Recording
	 												</option>

	 												<option value="Hung Up"> Hung Up </option>

	 												<option value="Outdated"> Outdated </option>

	 												<option value="Others"> Others </option>

	 											</select>
			 									
			 									<br><textarea class="form-control state_reason_tb" name="state_reason_tb" id = "state_reason_tb"></textarea>

	 					<input type = "hidden" name="recording_id" 
	 						value="<?php echo $recording_id; ?>">

	 				</table>

 	     	</div>

			<div class="modal-footer">
	      	
	        	<button type="submit" class="btn btn-danger"> Clear Recordings </button>
				
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

			</div>

		</form>



	    </div>

	  </div>

</div>