<div id="clear-recordings-agents-modal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
 
	    <!--  -->
	    <div class="modal-content">

	      <div class="modal-header">

	        <button type="button" class="close" data-dismiss="modal">&times;</button>

	        <center> <h2 class="modal-title"> Clear Recordings </h2> </center>

	      </div>
 
	      	<form action = "<?php echo base_url('Recording/clear_agent_recordings/'); ?>" method="POST">
		      	
		      	<div class="modal-body">

						<h4>
							<center>
								<span class="text text-danger"> 
									Are you sure to clear all Agent Recordings? 
								</span> 
							</center>
						</h4>
			 					
		 				<table class="table table-condensed text text-info" style="margin-top: 25px;">  
		 					
		 					<center>

		 						<h4> Agent Login Id: <?php echo $agent_login_id; ?> </h4> 					  
		 					
			 					<h4> Agent Name: <?php echo $agent_name; ?> </h4> 					  
			 				</center>
			 				
		 					<input type = "hidden" name="agent_login_id" 
		 						value="<?php echo $agent_login_id; ?>">

		 				</table>

	 	     	</div>

				<div class="modal-footer">
		      	
		        	<button type="submit" class="btn btn-danger"> Clear Recordings </button>
					
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

				</div>

			</form>



	    </div>

	  </div>

</div>