        
    <div class="navbar-header">
        <a class="navbar-brand" href="<?php echo base_url('/'); ?>"> LeadGen Q.A. System  </a>
            <p class="navbar-brand"> (Login As:  Admin) </p>
    </div>
    
    <div class="collapse navbar-collapse navbar-ex1-collapse">
    
            <ul class="nav navbar-nav side-nav">

                    <li><a href = "<?php echo base_url('Admin/dashboard'); ?>"> <i class="fa fa-dashboard"></i> Dashboard </a></li>

                         <li><a href = "<?php echo base_url('Users/list_users'); ?>"> <i class="fa fa-users"></i> Users <span class="fa arrow"></span> </a>
                        
                                <ul class="nav nav-second-level">

                                    <li> 
                                        
                                        <a href= "<?php echo base_url('Users/list_users'); ?>">  <i class = 'fa fa-list'> </i> List Users
                                        </a>  

                                        <a href= "<?php echo base_url('Users/add_user'); ?>">  <i class = 'fa fa-plus'> </i> Add User 
                                        </a>  

                                    </li>
 
                                </ul>

                        </li>

                         <li><a  href = "<?php echo base_url('Rosters/list_rosters'); ?>"> 
                          
                                <i class="fa fa-headphones"></i> Rosters 
                                <span class="fa arrow"></span> 
                            </a>

                                <ul class="nav nav-second-level">

                                    <li> 
                                        
                                        <a href= "<?php echo base_url('Rosters/list_rosters'); ?>">  <i class = 'fa fa-list'> </i> List of Rosters
                                        </a>

                                        <a href= "<?php echo base_url('Admin/add_rosters-form'); ?>">  <i class = 'fa fa-plus'> </i> Add User 
                                        </a>

                                        <a href= "<?php echo base_url('Admin/add_rosters-form'); ?>">  <i class = 'fa fa-plus'> </i> Team Lead List
                                        </a>

                                        <a href= "<?php echo base_url('Admin/add_rosters-form'); ?>">  <i class = 'fa fa-plus'> </i> SME List
                                        </a>

                                        <a href= "<?php echo base_url('Admin/add_rosters-form'); ?>">  <i class = 'fa fa-plus'> </i> Agents List
                                        </a>

                                    </li>
 
                                </ul>

                        </li>

                        <li><a href = "<?php echo base_url('Recording/recording_list'); ?>"> <i class="fa fa-file-audio-o"></i> Call Recordings </a></li>

                        <li><a href = "<?php echo base_url('Audit/audit_list'); ?>"> <i class="fa fa-check-square-o"></i>  Audit Lists </a></li>

                         <li><a href = "<?php echo base_url('Incident_Report/incident_reports'); ?>">  <i class="fa fa-bell"></i> Incident Reports  </a>  </li>
                        
                         <li><a href = "<?php echo base_url('Flagged_Call/flagged_calls'); ?>">  <i class="fa fa-flag"></i> Flagged Calls  </a> </li>
                    
                        <li><a href = "<?php echo base_url('Report/quality_production_report'); ?>"> <span class="fa fa-area-chart"></span> Quality Production Report </a>

                        <li><a href = "<?php echo base_url('Report/qa_results_summary'); ?>" > <span class="fa fa-bar-chart"></span> Q.A. Results </a>
                        
                        <li><a href = "<?php echo base_url('FilterData/list_filter_data_list'); ?>"> <span class="fa fa-gear"></span> Filter and Data  <span class="fa arrow"></span> </a>

                        <?php if($nav_menus['is_parent'] == True) { ?>

                                <ul class="nav nav-second-level">

                                    <li> 
                                        
                                        <a href= "<?php echo base_url('FilterData/list_filter_data_list'); ?>">  <i class = 'fa fa-list'> </i> All Filter and Data
                                        </a>  

                                        <a href= "<?php echo base_url('FilterData/add_filter_data_list'); ?>">  <i class = 'fa fa-plus'> </i> Add Filter and Data 
                                        </a>  

                                    </li>
 
                                </ul>
                        <?php } ?>

                        </li>

                        <li><a href = "<?php echo base_url('Login/logout'); ?>"> <i class="fa fa-power-off"></i> Log- Out</a></li>

                    </ul>

            </div>
                <!-- /.sidebar-collapse -->
 
