     
    <div class="navbar-header" >
    <a class="navbar-brand" href="<?php echo base_url('/'); ?>"> LeadGen Q.A. System  </a>
        <p class="navbar-brand"> (Login As:  Admin) </p>
    </div>
    
    <div class="navbar-default sidebar" style="width: 30px;">
 
       <!-- Sidebar -->
      <ul class="navbar-default sidebar navbar-collapse" role="navigation" >

                    <ul class="nav" id="side-menu" >


                        <li><a href = "<?php echo base_url('Admin/dashboard'); ?>"> <i class="fa fa-dashboard"></i> Dashboard </a></li>

                         <li><a href = "<?php echo base_url('Admin/list_users'); ?>"> <i class="fa fa-users"></i> Users <span class="fa arrow"></span> </a>
                        
                                <ul class="nav nav-second-level">

                                    <li> 
                                        
                                        <a href= "<?php echo base_url('Admin/list_users'); ?>">  <i class = 'fa fa-list'> </i> List Users
                                        </a>  

                                        <a href= "<?php echo base_url('Admin/add_user'); ?>">  <i class = 'fa fa-plus'> </i> Add User 
                                        </a>  

                                    </li>
 
                                </ul>

                        </li>

                        <li><a href = "<?php echo base_url('Admin/list_rosters'); ?>"> <i class="fa fa-headphones"></i> Rosters Lists </a></li>

                        <li><a href = "<?php echo base_url('Admin/recording_list'); ?>"> <i class="fa fa-file-audio-o"></i> Call Recordings </a></li>

                        <li><a href = "<?php echo base_url('Admin/audit_list'); ?>"> <i class="fa fa-check-square-o"></i>  Audit Lists </a></li>

                         <li><a href = "<?php echo base_url('Admin/incident_reports'); ?>">  <i class="fa fa-bell"></i> Incident Reports  </a>  </li>
                        
                         <li><a href = "<?php echo base_url('Admin/flagged_calls'); ?>">  <i class="fa fa-flag"></i> Flagged Calls  </a> </li>
                    
                        <li><a href = "<?php echo base_url('Admin/quality_production_report'); ?>"> <span class="fa fa-area-chart"></span> Quality Production Report </a>

                        <li><a href = "<?php echo base_url('Admin/qa_results_summary'); ?>" > <span class="fa fa-bar-chart"></span> Q.A. Results </a>
                        
                        <li><a href = "<?php echo base_url('Admin/list_filter_data_list'); ?>"> <span class="fa fa-gear"></span> Filter and Data  <span class="fa arrow"></span> </a>
                        
                                <ul class="nav nav-second-level">

                                    <li> 
                                        
                                        <a href= "<?php echo base_url('Admin/list_filter_data_list'); ?>">  <i class = 'fa fa-list'> </i> All Filter and Data
                                        </a>  

                                        <a href= "<?php echo base_url('Admin/add_filter_data_list'); ?>">  <i class = 'fa fa-plus'> </i> Add Filter and Data 
                                        </a>  

                                    </li>
 
                                </ul>

                        </li>


                        <li><a href = "<?php echo base_url('Login/logout'); ?>"> <i class="fa fa-power-off"></i> Log- Out</a></li>

                    </ul>

    </div>

                <!-- /.sidebar-collapse -->
 
