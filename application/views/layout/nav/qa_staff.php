     
    <div class="navbar-header" >
    <a class="navbar-brand" href="<?php echo base_url('/'); ?>"> LeadGen Q.A. System  </a>
            <p class="navbar-brand"> (Login As:  <?php echo $this->session->userdata('login_role')['user_roles_desc']; ?>)
    </div>
    
    <ul class="nav navbar-right top-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> 
                        <?php echo $this->session->userdata('login_info')['acct_nick_name']; ?> 
                    <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li> 

                        <li class="divider"></li>
                        <li>
                            <a href = "<?php echo base_url('Login/logout'); ?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
     </ul>

    <div class="navbar-default sidebar" style="width: 30px;">
    
        <ul class="nav navbar-nav side-nav">
 
                        <li><a href = "<?php echo base_url('Dashboard/auditor'); ?>"> <i class="fa fa-dashboard"></i> Dashboard </a></li>

                        <li><a href = "<?php echo base_url('Rosters/list_rosters'); ?>"> <i class="fa fa-users-o"></i> List Rosters </a></li>

                        <li><a href = "<?php echo base_url('Recording/shortcall_recording_list'); ?>"> <i class="fa fa-file-audio-o"></i> Short Call Recordings </a></li>

                        <li><a href = "<?php echo base_url('Recording/longcall_recording_list'); ?>"> <i class="fa fa-file-audio-o"></i> Long Call Recordings </a></li>

                        <li><a href = "<?php echo base_url('Recording/sme_group_recordings'); ?>"> <i class="fa fa-search"></i> SME Group Recordings </a></li>

                         <li><a href = "#"> 
                          
                                <i class="fa fa-headphones"></i> Audits 
                                <span class="fa arrow"></span> 
                            </a>

                                <ul class="nav nav-second-level">

                                    <li><a href = "<?php echo base_url('Audit/my_audit_list'); ?>"> <i class="fa fa-check-square-o"></i> My Audit List </a></li>

                                    <li><a href = "<?php echo base_url('Report/audit_reports'); ?>"> <i class="fa fa-bar-chart"></i> Audit Report </a></li>

                                </ul>

                        </li>


                         <li><a href = "<?php echo base_url('Incident_Report/incident_reports'); ?>">  <i class="fa fa-bell"></i> Incident Reports  </a>  </li>
                        
                         <li><a href = "<?php echo base_url('Flagged_Call/flagged_calls'); ?>">  <i class="fa fa-search"></i> Flagged Calls  </a> </li>

                         <li><a href = "#" data-toggle = "modal" data-target = '#search-flagged-call-modal'>  <i class="fa fa-flag"></i> Search Calls  </a> </li>
                    
                        <li><a href = "<?php echo base_url('Report/quality_production_report'); ?>"> <span class="fa fa-area-chart"></span> My Quality Production Report </a>

                        <li><a href = "<?php echo base_url('Report/agent_issues_report'); ?>" > <span class="fa fa-bar-chart"></span> Agent System Issues Report </a>
                        
                        <li><a href = "<?php echo base_url('Login/logout'); ?>"> <i class="fa fa-power-off"></i> Log- Out</a></li>

                    </ul>

    </div>

    <!-- /.sidebar-collapse -->
 
