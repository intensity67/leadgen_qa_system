        
    <div class="navbar-header">
        <a class="navbar-brand" href="<?php echo base_url('/'); ?>"> LeadGen Q.A. System  </a>
            <p class="navbar-brand"> (Login As:  <?php echo $this->session->userdata('login_role')['user_roles_desc']; ?>)</p>
    </div>

    <div class="collapse navbar-collapse navbar-ex1-collapse">

        <ul class="nav navbar-right top-nav">

                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $this->session->userdata('login_info')['acct_nick_name']; ?> <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="#"><i class="fa fa-fw fa-user"></i> Profile </a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-fw fa-gear"></i> Settings </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href = "<?php echo base_url('Login/logout'); ?>"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
       </ul>

    </div>
    
    
            <ul class="nav navbar-nav side-nav toggled">

                <?php for($i = 0; $i < $parent_menu_cnt; $i++){ ?>
                    
                    <?php $parent_menu = $parent_menus[$i]; ?>
 
                        <li>

                            <a href = "<?php echo base_url($parent_menu['url']); ?>">

                            <i class="fa fa-<?php echo $parent_menu['fa_icon']; ?> "></i>

                                <?php echo $parent_menu['title']; ?> 
                                
                                <span class="fa arrow"></span> 

                            </a>

                        
                        <?php if($parent_menu['children_cnt'] != 0){ ?>
                            

                            <ul class="nav nav-second-level">
                           
                            <li> 

                                <?php for($j = 0; $j < $parent_menu['children_cnt']; $j++){ ?>

                                    <?php $sub_menu_info = $parent_menu['menus'][$j]; ?>
                                 
                                                    
                                                    <a href= "<?php echo base_url($sub_menu_info['url']); ?>">  

                                                        <i class = "fa fa-<?php echo $sub_menu_info['fa_icon']; ?>"> </i> 

                                                        <?php echo $sub_menu_info['title']; ?>
                                                    </a>  
                                                

                                <?php } ?>
                           
                            </li>
                            
                            </ul>
                        
                        <?php } ?>

                        </li>

                    <?php } ?>
 
                        <li><a href = "<?php echo base_url('Login/logout'); ?>"> <i class="fa fa-power-off"></i> Log- Out</a></li>

                    </ul>

                </ul>

 

<!-- /.sidebar-collapse -->
 
