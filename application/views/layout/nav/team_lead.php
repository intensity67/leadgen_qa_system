     
    <div class="navbar-header" >
    <a class="navbar-brand" href="<?php echo base_url('/'); ?>"> LeadGen Q.A. System  </a>
        <p class="navbar-brand"> (Login As:  Team Lead) </p>
    </div>
    
    <div class="navbar-default sidebar" style="width: 30px;">
 
       <!-- Sidebar -->
      <ul class="navbar-default sidebar navbar-collapse" role="navigation" >

                    <ul class="nav" id="side-menu" >


                        <li><a href = "<?php echo base_url('Admin/dashboard'); ?>"> <i class="fa fa-dashboard"></i> Dashboard </a></li>
  
                        <li><a href = "<?php echo base_url('Admin/list_rosters'); ?>"> <i class="fa fa-headphones"></i> Rosters Lists </a></li>

                        <li><a href = "<?php echo base_url('Admin/audit_list'); ?>"> <i class="fa fa-check-square-o"></i>  Audit Lists </a></li>

                         <li><a href = "<?php echo base_url('Admin/incident_reports'); ?>">  <i class="fa fa-bell"></i> Incident Reports  </a>  </li>
                        
                         <li><a href = "<?php echo base_url('Admin/flagged_calls'); ?>">  <i class="fa fa-flag"></i> Flagged Calls  </a> </li>

                        <li><a href = "<?php echo base_url('Admin/qa_results_summary'); ?>" > <span class="fa fa-bar-chart"></span> My Q.A. Results </a>

                        <li><a href = "<?php echo base_url('Login/logout'); ?>"> <i class="fa fa-power-off"></i> Log- Out</a></li>

                    </ul>

    </div>

                <!-- /.sidebar-collapse -->
 
