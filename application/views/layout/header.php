<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content=">width=device-width,height=device-height,initial-scale=1.0"/>

    <title> LeadGen Q.A. System -  <?php echo $this->session->userdata('login_role')['user_roles_desc']; ?>  </title>


    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url('assets/dist/dark-sbadmin/css/bootstrap.css'); ?> " rel="stylesheet">
    
     <link href="<?php echo base_url('assets/dist/dark-sbadmin/css/sb-admin.css'); ?> " rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url('assets/dist/dark-sbadmin/font-awesome/css/font-awesome.min.css'); ?> " rel="stylesheet" type="text/css">

   <!-- jQuery -->
 
    
    <script type="text/javascript" language="javascript" src="<?php echo base_url('assets/vendor/jquery/jquery.js'); ?>"> </script>

    <script type="text/javascript" language="javascript" src="<?php echo base_url('assets/datatables/js/jquery.dataTables.js'); ?>"> </script>

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/datatables/css/jquery.dataTables.css'); ?>">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/custom/style.css'); ?>"> 
  
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
    

         <?php if(isset($nav)): ?>
            
            <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
                 <?php $this->load->view("layout/nav/" . $nav); ?>

            </nav>
        
        
        <?php endif;   ?>


        <!-- Page Content -->

                 <div id="page-wrapper">
                    
                    <div class="container-fluid">

