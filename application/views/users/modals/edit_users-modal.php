<style type="text/css">
	
	textarea{
		resize: none;
	}	

</style>

	<div id="edit-users<?php echo $user_account_id; ?>" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!--  -->
	    <div class="modal-content">
	      <div class="modal-header alert alert-success">   
	        <button type="button" class="close" data-dismiss="modal"></button>
	        <h2 class="modal-title">Edit User <i class="fa fa-user"></i> </h4>
	        </span>
	      </div>

	      <form action = "<?phpe echo base_url('user/insert_user'); ?>" method="POST">

	      <div class="modal-body">
 				
 				<table class="table table-condensed">

 				  	<tr><td> User Role : <td> <select class="form-control"> 
 				  								
 				  								<option>Select User</option>

											<?php foreach($user_roles as $user_roles_row): ?>

	 				  							<option value="<?php echo $user_roles_row['user_roles_id']; ?>" <?php if($user_roles_id == $user_roles_row['user_roles_id']) echo 'selected'; ?>> 

	 				  								<?php echo $user_roles_row['user_roles_desc']; ?> 

	 				  							</option>
											
											<?php endforeach; ?>

	 				  						</select>
  
					<tr><td> Employee ID: <td><input type="text" name="" value="<?php echo $employee_id; ?>" class="form-control" >
					
					<tr><td> Username: <td><input type="text" name="" value="<?php echo $username; ?>" class="form-control" >
					
					<tr><td> Password: <td><input type="password" name="" value="<?php echo $password; ?>"  class="form-control" >
				
					<tr><td> Nickname: <td><input type="text" name="" value="<?php echo $acct_nick_name; ?>"  class="form-control" >

 				</table>

 	      </div>

	      <div class="modal-footer">

	        <button type="submit" class="btn btn-success" data-dismiss="modal"> Update Info <i class="fa fa-save"> </i> </button>

	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

	      </div>
	    
	    </form>
	      
	    </div>

	  </div>
	</div>
 				  	