<style type="text/css">
	
	textarea{
		resize: none;
	}	

</style>

	<div id="add-users" class="modal fade" role="dialog">
	  <div class="modal-dialog"> 

	    <!--  -->
	    <div class="modal-content">
	      <div class="modal-header alert alert-success">   
	        <button type="button" class="close" data-dismiss="modal"></button>
	        <h2 class="modal-title">Add User <i class="fa fa-user"></i> </h4>
	        </span>
	      </div>

	      <form action = "<?php echo base_url('user/insert_user'); ?>" method="POST">

	      <div class="modal-body">
 				
 				<table class="table table-condensed">

 				  	<tr><td> User Role : <td> <select class="form-control" name="user_roles_id"> 
 				  								
 				  								<option>Select User</option>

											<?php foreach($user_roles as $user_roles_row): ?>

	 				  							<option value="<?php echo $user_roles_row['user_roles_id']; ?>"> 

	 				  								<?php echo $user_roles_row['user_roles_desc']; ?> 

	 				  							</option>
											
											<?php endforeach; ?>

	 				  						</select>


					<tr><td> Employee ID: <td><input type="text" name="employee_id" class="form-control" >
					
					<tr><td> Username: <td><input type="text" 	name="username" class="form-control" >
					
					<tr><td> Password: <td><input type="password" name="password"  class="form-control" >

					<tr><td> First Name: <td><input type="text" name = "first_name" class="form-control" >

					<tr><td> Last Name: <td><input type="text" name = "last_name" class="form-control" >

					<tr><td> Nickname: <td><input type="text" name = "acct_nick_name" class="form-control" >

					<tr><td> Email: <td><input type="text" name = "email"  class="form-control" >

 				</table>

 	      </div>

	      <div class="modal-footer">

	        <button type="submit" class="btn btn-success"> Save User <i class="fa fa-save"> </i> </button>

	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

	      </div>
	    
	    </form>
	      
	    </div>

	  </div>
	</div>
 				  	