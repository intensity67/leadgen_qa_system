<div id="content-wrapper">

	<div class="container-fluid">
 

	<script type="text/javascript" src="<?php echo base_url('js/user_list.js'); ?>"></script>

	 <h1 class="page-header">  User Lists </h1>

		<table class="table add_entry">

		<tr><td> User Role: <td> <select class="form-control field_filter" id = "1">
										
									<option value=''>All</option>

									<?php foreach($user_roles as $user_roles_row): ?>

										<option value="<?php echo $user_roles_row['user_roles_desc']; ?>">  <?php echo $user_roles_row['user_roles_desc'] ?>  </option>
									
									<?php endforeach; ?>

								</select>

		</table>


	 	<button class="btn btn-info pull-right">Filter <i class="fa fa-filter"></i></button>

		<a href="" data-toggle = "modal" style="text-align: center;" data-target = '#add-users' class="btn btn-success"> <i class='fa fa-plus'></i> Add Users </a>

	 <br><br><br>

 		<table class="table table-striped" id = "user_account_list" style="width: 100%; ">                                    
			<thead>
				<tr>
					<th> Account Id </th>
					<th> Account Role </th>
					<th> Username </th>
 					<th> Account Nickname </th>
 					<th> Date Created </th>
 					<th> </th>
				</tr>
			</thead>

			<tbody id = "user_account_body">

				<?php foreach($user_list as $row): ?>

					<tr>
						<td> <?php echo $row['user_account_id']; ?></td> 
						
						<td> <?php echo $row['user_roles_desc']; ?></td> 

						<td> <?php echo $row['username']; ?></td> 

						<td> <?php echo $row['acct_nick_name']; ?></td>

 						<td> <?php echo date("M d, Y", strtotime($row['date_created'])); ?></td>

 						<td> 
    						<a href="#" class="btn btn-success" data-toggle="modal" data-target="#edit-users<?php echo $row['user_account_id']; ?>"><i class="fa fa-edit"> </i> </a>

    						<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#delete-users<?php echo $row['user_account_id']; ?>"><i class="fa fa-trash"> </i> </a>

						<?php $this->load->view('users/modals/edit_users-modal', $row); ?>


					</tr>
	  				
				<?php endforeach; ?>

 			</tbody>

		</table>       

		<?php $data['user_roles'] = $user_roles; ?>

		<?php $this->load->view('users/add_users-modal', $data); ?>

	</div>

</div>

 