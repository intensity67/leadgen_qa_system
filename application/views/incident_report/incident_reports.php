<!-- 	<button type = "submit" name="" value="Search" class="btn btn-success"> Search <i class="fa fa-search"> </i> </button>
 --> 
<!-- print button -->

	<div id="content-wrapper">

		<div class="container-fluid">


		<h1 class="page-header">  Incident Report </h1>

<!-- 	 <a href="" data-toggle = "modal" data-target = '#add-incident-modal' class="btn btn-primary"> <i class='fa fa-plus'></i> Add Incident Report </a>
 -->	 
	 <br><br><br>

 		<table class="table table-striped" id = "ir_list" style="width: 100%; ">                                    
			<thead>

				<tr>
					<th> Incident Report Id </th>
					<th> Source </th>
					<th> Agent Id </th>
 					<th> Team Lead</th>
 					<th> Evaluator </th>
 					<th> Evaluation Date </th>
  					<th> Phone Number  </th>
  					<th> Sanction  </th>
  					<th>   </th>
				</tr>

			</thead>

			<tbody>

				<?php foreach($incident_reports as $row): ?>

					<tr>
						<td> <?php echo $row['incident_reports_id']; ?> </td> 
						<td> <?php echo $row['source']; ?> </td> 
						<td> <?php echo $row['agent']; ?> </td> 
						<td> <?php echo $row['tl']; ?> </td> 
						<td> <?php echo $row['qa']; ?> </td> 
						<td> <?php echo date("M d, Y", strtotime($row['evaluation_date'])); ?> </td> 
						<td> <?php echo $row['phone_number']; ?> </td> 
						<td> <?php echo $row['sanction_list_desc']; ?> </td> 
					
						<td>  
							<a href = "<?php echo base_url('Incident_Report/view_incident_report/'. $row['incident_reports_id']); ?>" 
								class="btn btn-info"> 

							<i class="fa fa-eye"> </i> </a>

    						<a href="#" class="btn btn-success" data-toggle="modal" data-target="#delete-audit-modal"><i class="fa fa-edit"> </i> </a>

						</td> 

					</tr>
	  				
				<?php endforeach; ?>

 			</tbody>

		</table>

	</div>

</div>

 