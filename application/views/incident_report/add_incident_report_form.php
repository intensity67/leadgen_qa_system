<style type="text/css">
	
	textarea{
		resize: none;
	}	

</style>

<?php echo form_open(base_url('Incident_Report/insert_incident_report')); ?>
	
	<?php if($source == 1) $source = "Internal Audit";?>

	<center><h2 class="alert alert-danger"> Incident Report </h2></center>

	<table class="table table-condensed">

 				  	<tr><td> Source: <td> <input type="text" name="" disabled="" class="form-control" value="<?php echo $source; ?>"> 

 				  	<tr><td> Agent ID: <td><input type="text" name="agent_login_id"  disabled="" class="form-control" value="<?php echo $audit_info['user']; ?>">

 				  	<tr><td> Team Lead Email Add: <td><select name="team_lead_email" class="form-control" required="">

 				  			<option value="">Select Team Lead Email</option>

 				  			<option value="gogtip@digicononline.com">Granville</option>
 				  			<option value="cenriquez@digicononline.com">Anthony Pielago</option>
 				  			<option value="sgimenez@digicononline.com">Steven Gimenez</option>
 				  			<option value="fdegamo@digicononline.com">Miao Fernando</option>
 				  			<option value="raquino@digicononline.com">Revie Aquino</option>
  				  			<option value="pvillarin@digicononline.com">Platon villarin</option>
 				  			<option value="mbaterna@digicononline.com">Merben Baterna</option>
 				  			<option value="cabrasaldo@toti.ph">Cabrasaldo</option>
 				  			<option value="jellyyap18@gmail.com">Jelly Yap</option>

				  	</select>

 				  	<tr><td> Team Lead: <td><input type="text" disabled="" value="<?php echo $audit_info['tl']; ?>" name= "team_lead" class="form-control">

 				  	<tr><td> Program: <td> <select name="program_list" class="form-control">

 				  							<?php foreach($program_list as $program_row){ ?>

 				  									<option value="<?php echo $program_row['program_list_id']; ?>"><?php echo $program_row['program_name']; ?></option>
 				  								
 				  							<?php } ?>

 				  							</select>

 				  	<tr><td> Call Date: <td><input type="text" name="" class="form-control" disabled value="<?php echo date("M d, Y", strtotime($audit_info['start_time'])); ?>">

 				  	<tr><td> Evaluation  Date: <td><input type="text" name="evaluation_date" disabled="" value="<?php echo date('m/d/Y'); ?>" class="form-control">

 				  	<tr><td> Vici Dispo : <td><select class="form-control" name="vici_dispo_id"> 
		 				  							<option value="">Select Vici Disposition</option>
		 				  							
		 				  							<?php foreach($vici_dispo_list as $vici_dispo_row ){ ?>

		 				  							<option value="<?php echo $vici_dispo_row['vici_dispo_list_id'] ?>" 

		 				  								<?php if($vici_dispo_row['dispo_code'] == $audit_info['agent_disposition']) 
		 				  										echo 'selected'; ?>> 
		 				  									<?php echo $vici_dispo_row['vici_dispo_desc']. ' / '.$vici_dispo_row['dispo_code']; ?>  
		 				  							</option>
		 				  							
		 				  							<?php } ?>

											</select>


 				  	<tr><td> BTN : <td><input type="text" name="phone_number" disabled="" 
 				  						value="<?php echo $audit_info['recording_id']; ?>" class="form-control">

 				  	<tr><td> Duration : <td><input type="text" disabled="" name="" value="<?php echo $audit_info['length_in_sec']; ?> secs" class="form-control">

 				  	<tr><td> Evaluator : <td><input type="text" class="form-control" value="<?php echo $audit_info['acct_nick_name']; ?>" disabled="" name="">

 				  	<tr><td> Offense Type : <td><input type="text" class="form-control" value="<?php echo $audit_info['mark_status']; ?>" disabled="" name="offense_type">

  				  	<tr><td> Offense  : <td><select class="form-control" name="offense_id" id = "offense_id" required> 

		 				  							<option value="NULL">None</option>

				 				  				  	<optgroup label="ZTP">

		 				  							<?php foreach($ztp_mark_list as $ztp_mark_list_row ){ ?>

		 				  								<option value="<?php echo $ztp_mark_list_row['ztp_mark_list_id'] ?>"> 
		 				  									
		 				  									<?php echo $ztp_mark_list_row['ztp_mark_desc']; ?>

		 				  								</option>
		 				  							
		 				  							<?php } ?>

  													</optgroup>

				 				  				  	<optgroup label="LOL">

		 				  							<?php foreach($lol_mark_list as $lol_mark_list_row ){ ?>

		 				  								<option value="<?php echo $lol_mark_list_row['lol_mark_list_id'] ?>"> 

		 				  									<?php echo $lol_mark_list_row['lol_mark']; ?> 
		 				  								</option>
		 				  							
		 				  							<?php } ?>

  													</optgroup>


											</select>

 					<tr><td> CALL SYNOPSIS/NOTES *:  <td>
 						
  				  	<tr><td colspan="2"> <textarea class="form-control" name="call_synopsis" rows= 7 cols = 15></textarea> 

  				  	<tr><td> Sanction : <td><select name="sanction_id" class="form-control"> 

 				  								<option> Select Sanction </option>
		 				  							
		 				  						<?php foreach($sanction_list as $sanction_list_row ){ ?>

		 				  								<option value="<?php echo $sanction_list_row['sanction_list_id'] ?>"> 

		 				  									<?php echo $sanction_list_row['sanction_list_desc']; ?>

		 				  								</option>
		 				  							
		 				  						<?php } ?>
 
 				  						 </select>

 					<tr><td> Email Subject Header *:  <td>
 						
  				  	<tr><td colspan="2"> <input type = 'text' class="form-control" /> 

  				  	<input type="hidden" id = "offense_type" name="offense_type" value="">

			<?php 

					$data = array(
							              'auditor_user_id' 	=> $this->session->userdata('login_info')['user_account_id'],
							              'source'  		=> $source,
							              'agent_login_id'	=> $audit_info['user'],
							              'team_lead' 	=> $audit_info['tl'],
							              'phone_number' 	=> $audit_info['phone_number'],
							              'recording_id' 	=> $audit_info['recording_id'],
 							              'disposition_id'	=> $audit_info['status'],
							              'audit_id' 		=> $audit_info['audit_id'],
							              'call_date' 		=> $audit_info['start_time'],
							              'offense_type'	=> $audit_info['mark_status']

							            );

					echo form_hidden($data);

			?> 
 
 				</table>
 			

			<div class="modal-footer">

	        <button type="submit" class="btn btn-success"> Submit Incident  <i class="fa fa-upload"> </i>  </button>

	      </div>

	    </div>

	  </div>

	</div>	

</div>

</form>
