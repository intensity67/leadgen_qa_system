<style type="text/css">
	
	textarea{
		resize: none;
	}	

</style>

<?php echo form_open(base_url('Incident_Report/insert_incident_report')); ?>

	<div id="add-incident-modal<?php echo $cnt; ?>" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!--  -->
	    <div class="modal-content">
	      <div class="modal-header alert alert-danger">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h2 class="modal-title"> Incident Report </h4>
	        </span>
	      </div>


	      <div class="modal-body">
 				
 				<table class="table table-condensed">

 				  	<tr><td> Source: <td> <input type="text" name="" disabled="" class="form-control" value="<?php echo $source; ?>"> 

 				  	<tr><td> Agent ID: <td><input type="text" name="agent_login_id"  disabled="" class="form-control" value="<?php echo $rec_info['user']; ?>">

 				  	<tr><td> Team Lead Email Add: <td><input type = "text" name="team_lead" disabled="" class="form-control" value="">

 				  	<tr><td> Program: <td> <select name="program_list" class="form-control">

 				  							<?php foreach($program_list as $program_row){ ?>

 				  									<option><?php echo $program_row['program_name']; ?></option>
 				  								
 				  							<?php } ?>

 				  							</select>

 				  	<tr><td> Call Date: <td><input type="text" name="" class="form-control" disabled value="<?php echo date("M d, Y", strtotime($rec_info['start_time'])); ?>">

 				  	<tr><td> Evaluation  Date: <td><input type="text" name="evaluation_date" disabled="" value="<?php echo date('m/d/Y'); ?>" class="form-control">

 				  	<tr><td> Vici Dispo : <td><select class="form-control" name="vici_dispo_id"> 
		 				  							<option value="">Select Vici Disposition</option>
		 				  							
		 				  							<?php foreach($vici_dispo_list as $vici_dispo_row ){ ?>

		 				  								<option value="<?php echo $vici_dispo_row['vici_dispo_list_id'] ?>"> <?php echo $vici_dispo_row['vici_dispo_desc']. ' / '.$vici_dispo_row['dispo_code']; ?>  </option>
		 				  							
		 				  							<?php } ?>

											</select>


 				  	<tr><td> BTN : <td><input type="text" name="phone_number" disabled="" 
 				  						value="<?php echo $rec_info['recording_id']; ?>" class="form-control">

 				  	<tr><td> Duration : <td><input type="text" disabled="" name="" value="<?php echo $rec_info['length_in_sec']; ?> secs" class="form-control">

 				  	<tr><td> Evaluator : <td><input type="text" class="form-control" value="<?php echo $rec_info['acct_nick_name']; ?>" disabled="" name="">

  				  	<tr><td> Offense  : <td><select class="form-control" name="offense_id" id = "offense_id" required> 

		 				  							<option value="NULL">None</option>

				 				  				  	<optgroup label="ZTP">

		 				  							<?php foreach($ztp_mark_list as $ztp_mark_list_row ){ ?>

		 				  								<option value="<?php echo $ztp_mark_list_row['ztp_mark_list_id'] ?>"> 
		 				  									
		 				  									<?php echo $ztp_mark_list_row['ztp_mark_desc']; ?>

		 				  								</option>
		 				  							
		 				  							<?php } ?>

  													</optgroup>

				 				  				  	<optgroup label="LOL">

		 				  							<?php foreach($lol_mark_list as $lol_mark_list_row ){ ?>

		 				  								<option value="<?php echo $lol_mark_list_row['lol_mark_list_id'] ?>"> 

		 				  									<?php echo $lol_mark_list_row['lol_mark']; ?> 
		 				  								</option>
		 				  							
		 				  							<?php } ?>

  													</optgroup>


											</select>

 					<tr><td> CALL SYNOPSIS/NOTES *:  <td>
 						
  				  	<tr><td colspan="2"> <textarea class="form-control" name="call_synopsis" rows= 7 cols = 15></textarea> 

  				  	<tr><td> Sanction : <td><select name="sanction_id" class="form-control"> 

 				  								<option> Select Sanction </option>
		 				  							
		 				  						<?php foreach($sanction_list as $sanction_list_row ){ ?>

		 				  								<option value="<?php echo $sanction_list_row['sanction_list_id'] ?>"> 

		 				  									<?php echo $sanction_list_row['sanction_list_desc']; ?>

		 				  								</option>
		 				  							
		 				  						<?php } ?>
 
 				  						 </select>

 					<tr><td> Email Subject Header *:  <td>
 						
  				  	<tr><td colspan="2"> <input type = 'text' class="form-control" /> 

  				  	<input type="hidden" id = "offense_type" name="offense_type" value="">

			<?php 

					$data = array(
							              'auditor_user_id' 	=> $this->session->userdata('login_info')['user_account_id'],
							              'source'  		=> $source,
							              'agent_login_id'	=> $rec_info['user'],
							              'team_lead_id' 	=> 35,
							              'phone_number' 	=> $rec_info['phone_number'],
							              'recording_id' 	=> $rec_info['recording_id'],
 							              'disposition_id'	=> $rec_info['status'],
							              'audit_id' 		=> $audit_id

							            );

					echo form_hidden($data);

			?> 
 
 				</table>
 			

			<div class="modal-footer">

	        <button type="submit" class="btn btn-success"> Submit Incident  <i class="fa fa-upload"> </i>  </button>

	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

	      </div>

	    </div>

	  </div>

	</div>	

</div>

</form>
