<style type="text/css">
	
	table{
		text-align: center;
	}

</style>

<div class = "row" >

<h2> <center> Q.A. Result </center></h2>

<table class="table table-striped">
	

	<tr><td> Agent Name <td> Najarro, Fritz Michael

	<tr><td> Week <td> 12/17/2018

</table>

<table class="table table-striped">

	<tr><td><td> Week 						<td> Running Month 
	
	<tr><td>  Number of submits audited 	<td> <td>
	<tr><td>  Number of non-submits audited <td> <td>
	<tr><td>  Flagged Calls 				<td> <td>

</table>

<table class="table table-striped">

	<tr><td>LOL/ZTP <td> Week <td> Running Month 
	
	<tr><td>  <td> 0 <td> 1
	<tr><td>  Top 3 <td> <td>
	<tr><td>  L.O.L. - Aggressive Selling 	<td> 8 <td>
	<tr><td>  L.O.L. - Script 				<td> 5 <td>
	<tr><td>  L.O.L. - Webform Information 	<td> 3 <td>


	<tr><td>LOL/ZTP <td> Week <td> Running Month 

	<tr><td>  L.O.L. - Call avoidance 											<td> 12 <td> 0
	<tr><td>  L.O.L. - Escalated the call to the SUP without customer request 	<td> 8 <td> 0
	<tr><td>  L.O.L. - Transferred the call inappropriately 					<td> 7 <td> 0
	<tr><td>  L.O.L. - Did not brand the call 									<td> 5 <td> 0
	<tr><td>  L.O.L. - Inappropriate response								 	<td> 4 <td> 0
	<tr><td>  L.O.L. - Unprofessional								 			<td> 3 <td> 0
	<tr><td>  L.O.L. - Making disparaging remarks								<td> 2 <td> 0
	<tr><td>  L.O.L. - Speaking in Vernacular									<td> 1 <td> 0
	<tr><td>  L.O.L. - Tagging the call incorrectly on the webform 				<td> 0 <td> 0
	<tr><td>  L.O.L. - Submitting webform more than once 						<td> 0 <td> 0
	<tr><td>  L.O.L. - Aggressive Selling 										<td> 0 <td> 0
	<tr><td>  L.O.L. - Script 													<td> 0 <td> 0
	<tr><td>  L.O.L. - Webform Information 										<td> 0 <td> 0
	<tr><td>  L.O.L. - Language Barrier/Confused 								<td> 0 <td> 0
	<tr><td>  ZTP - DNC 														<td> 0 <td> 0
	<tr><td>  ZTP - ROBO call 													<td> 0 <td> 0
	<tr><td>  ZTP - Call Riding 												<td> 0 <td> 0
	<tr><td>  ZTP - Score Manipulation 											<td> 0 <td> 0

</table>

<h2> Errors made in submitted calls for the week selected </h2>

<table class="table table-striped">

	<tr><td> Script 
		<td> Speed 
		<td> Customer Acknowledgement
		<td> Incorrect responses
	
	<tr><td>  z01 <td> 0 <td> 0 <td> 0
	<tr><td>  z02 <td> 0 <td> 0 <td> 0
	<tr><td>  z03 <td> 0 <td> 0 <td> 0
	<tr><td>  z04 <td> 0 <td> 0 <td> 0
	<tr><td>  z05 <td> 0 <td> 0 <td> 0
	<tr><td>  z06 <td> 0 <td> 0 <td> 0
	<tr><td>  z07 <td> 0 <td> 0 <td> 0
	<tr><td>  z15 <td> 0 <td> 0 <td> 0
	<tr><td>  z08 <td> 0 <td> 0 <td> 0
	<tr><td>  z09 <td> 0 <td> 0 <td> 0
	<tr><td>  z10 <td> 0 <td> 0 <td> 0
	<tr><td>  z11 <td> 0 <td> 0 <td> 0
	<tr><td>  z12 <td> 0 <td> 0 <td> 0


</table>



<table class="table table-striped">

	<tr><td>  <td> This Week <td> Previous Week <td> This Month <td> Previous Month
	<tr><td> Flagged Calls <td> <td><td><td>
	<tr><td> -------------unsuccessful call back------------- <td> <td><td><td>
	<tr><td> does not want to reveal information <td> <td><td><td>
	<tr><td> hung up during call back <td> <td><td><td>
	<tr><td> no answer <td> <td><td><td>
	<tr><td> voicemail reached  <td> <td><td><td>
	<tr><td> call back rescheduled <td> <td><td><td>
	<tr><td> unspecific  <td> <td><td><td>
	<tr><td> scheduled call back  <td> <td><td><td>
	<tr><td> -------------not interested to switch-------------  <td> <td><td><td>
	<tr><td>  Not Interested <td> <td><td><td>
	<tr><td> loyal to company  <td> <td><td><td>
	<tr><td> price of quote  <td> <td><td><td>
	<tr><td> coverage of policy  <td> <td><td><td>
	<tr><td> e-mail request for review  <td> <td><td><td>
	<tr><td> change of company unaccepted  <td> <td><td><td>
	<tr><td> -------------others------------- <td> <td><td><td>
	<tr><td> unqualified <td> <td><td><td>
	<tr><td> not specific <td> <td><td><td>
	<tr><td> busy  <td> <td><td><td>
	<tr><td> confused  <td> <td><td><td>
	<tr><td> lang barrier  <td> <td><td><td>
	<tr><td> DNC <td> <td><td><td>
	<tr><td> prank <td> <td><td><td>
	<tr><td> arranged call back <td> <td> <td><td>
	<tr><td> Didn't want to be transfer <td> <td><td><td>

</table>