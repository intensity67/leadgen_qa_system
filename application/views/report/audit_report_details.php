	<h1 class="page-header"> Audit List </h1>

	<div class = "row">

	<style type="text/css">

	  /*dark background to support form theme*/

	  input{

	  	display: block;

	  }

	  nav{
	  	display: hiden;
	  }

	</style>

	<script type="text/javascript" src="<?php echo base_url('js/audit_report.js'); ?>"></script>

	<a href="<?php echo base_url('Dashboard/'); ?>"> <i class="fa fa-dashboard"></i> Dashboard </a>

	<a href="<?php echo base_url('Report/audit_reports'); ?>"> <i class="fa fa-bar-chart"></i> Reports Date Summary </a>

	<br><br>

	<p class="alert alert-success">

	 		Audits from <span class="text text-danger"><?php echo date("M d, Y h:i A", strtotime($date_from)); ?> </span>

	 		to <span class="text text-danger"><?php echo date("M d, Y h:i A", strtotime($date_to)); ?> </span>
	 
	</p>

	<button class="btn btn-success" id="hide_audit_table_btn"> 
		Show/Hide Audit List <i class="fa fa-eye"></i> 
	</button>

	<br><br>

 	<?php 
 	
 	//* Summary Report Variables

 		$total_duration 		= 0; 
 		$incorrect_response_cnt = 0; 
 		$agent_issues_cnt 		= 0; 
 		$system_issues_cnt 		= 0; 
 		$issues_cnt 			= 0; 
 		$other_issues_cnt 		= 0; 

 		$ztp_cnt 		= 0; 
 		$lol_cnt 		= 0; 

 		// $issue_comments_cnt

 		foreach ($issue_comments as $issue_comments_row) {

 			$issue_comments_cnt[$issue_comments_row['issue_comment_id']] = 0;

 		}

 		foreach($qa_list as $qa_row){

 			$auditors_cnt[$qa_row['acct_nick_name']] = 0;

 		}
 			$auditors_cnt['Administrator'] = 0;

 	?>

 
 	<?php $audit_cnt = count($audit_list); ?>

 	<div id = "audit_table_container">

	 	<table class="table table-striped"  id = "audit_list">
	                                          
				<thead>

					<tr>
	 					<td> Audit Id
						<td> Week Beginning Date
						<td> Timestamp
						<td> Recording/Call Id
	 					<td> Agent					

							<?php if($this->session->userdata("login_info")['user_roles_id'] == ADMIN ): ?> 

							<td> Auditor
							<td> Audit Duration (in seconds)
						
							<?php endif; ?>

						<td> Audit Type
						<td> Agent Disposition
						<td> Correct Disposition
						<td> Mark Status
						<td> Incorrect Response 
	 					<td>
						<td>

				</thead>

				<tbody>
					
					<?php $i = 1; ?>

					<?php foreach($audit_list as $row): ?>

						<tr>
							 <?php 									

							 $duration_seconds = strtotime($row['audit_timeend']) - strtotime($row['audit_timestart']);

							 ?>

	  						<td> <?php echo $row['audit_id']; ?> 

							<td> <?php echo date("M, d Y", strtotime($row['work_week'])); ?>

							<td> <?php echo date("M, d Y h:m:s A", strtotime($row['audit_timestamp'])); ?>

							<td> <?php echo $row['recording_id']; ?>

							<td> <?php echo $row['agent_name']; ?>
							
							<?php if($this->session->userdata("login_info")['user_roles_id'] == ADMIN ): ?> 

							<td> <?php echo $row['acct_nick_name']; ?>
							<td> <?php echo $duration_seconds; ?>

	 						
	 						<?php endif; ?>
	 						
							<td> <?php echo $row['agent_disposition']; ?>

							<td> <?php echo $row['vici_dispo_desc']; ?>

							<td> <?php echo $row['audit_type']; ?>

							<td> <?php echo $row['mark_status']; ?>

							<td> <?php echo $row['incorrect_response']; ?>

	 						<td style="width: 100%; height: 15px;"> 
					<?php 
 	 
					   	$add_incident_filter['cnt'] 		= $i; 

						$add_incident_filter['rec_info'] 	= $row;

	 					$add_incident_filter['source'] 		 = "Internal Audit";

						$add_incident_filter['audit_id'] 	 = $row['audit_id'];

					?>
					
								<a href = "<?php echo base_url('Audit/view_audit/'.$row['audit_id']); ?>" class="btn btn-info"> 
								
									<i class="fa fa-eye"> </i> 

								</a>

	    						<a href="<?php echo base_url('Audit/edit_audit/'.$row['audit_id']); ?>" class="btn btn-success" ><i class="fa fa-edit"> </i> </a>
								
								<a href="#" class="btn btn-danger" data-toggle="modal" data-target="#delete-audit-modal<?php echo $row['audit_id']; ?>"><i class="fa fa-trash"> </i> </a>
						<td> 
	    					
	    					<?php
	    					
	    						$i++;

	    						$total_duration += $duration_seconds;

	    						if($row['incorrect_response'] == "Yes"){
	    							$incorrect_response_cnt++;
	    						}

	    						if($row['agent_issue'] == 'Agent'){
	    							$agent_issues_cnt++;
	    						}

	    						if($row['agent_issue'] == 'System'){
	    							$system_issues_cnt++;
	    						}

	    						if($row['agent_issue'] == 'Others'){
	    							$other_issues_cnt++;
	    						}

	    						if($row['issue_comment_id'] != '' && $row['issue_comment_id'] != 1 ){

	    							$issues_cnt++;
 	    							$issue_comments_cnt[$row['issue_comment_id']] = $issue_comments_cnt[$row['issue_comment_id']] + 1;
	    						}

	    						if($row['mark_status'] == 'ZTP'){
	    							$ztp_cnt++;
	    						}

	    						if($row['mark_status'] == 'LOL'){
	    							$lol_cnt++;
	    						}
								 
	    						$auditors_cnt[$row['acct_nick_name']]++;

		    					if($row['incident_report_id'] == NULL && $row['mark_status'] != ''){ ?>

			    					<a href="<?php echo base_url('Incident_Report/add_incident_report/'. $row['audit_id'].'/1'); ?>" class="btn btn-warning">
			    							<i class="fa fa-bell"> </i> 
									</a>
		 
								<?php	} 	?>
	  

	 				<?php endforeach; ?>

	 			</tbody>

		</table>
		
	</div>

	<?php 

		$hours   = floor($total_duration / 3600);
		$minutes = floor(($total_duration  - ($hours * 3600)) /60);
		$seconds = floor(($total_duration  - ($hours * 3600) - ($minutes*60)));

	?>

	<!-- To be added: Per Agents Report -->

	<h4> Audit Summary Reports</h4>

	<table class="table table-striped"> 
		
		<tr><td><b> QA/Auditors </b><td> Audits
			
 			<?php foreach($qa_list as $qa):?>

				<tr>
					<td><?php echo $qa['acct_nick_name']; ?>
					<td><?php echo $auditors_cnt[$qa['acct_nick_name']]; ?>
			
			<?php endforeach; ?>

		 <tr><td> <b> Total Audits: </b> <td><?php echo $audit_cnt; ?>

		 <tr><td> <b> Total Audit Duration:		<td><?php echo $hours. " hrs. ". $minutes . " mins. ". $seconds . " secs."; ?>

		 <tr><td> <b> Average Audit Duration Per Recordings:  <td><?php echo ($audit_cnt != 0) ? number_format($total_duration / $audit_cnt, 1). " seconds " : 0; ?>
		 
		 <tr><td><b> Issues Comment		<td>

		 <tr><td>Incorrect Responses:		<td><?php echo $incorrect_response_cnt; ?>

		 <tr><td>Issues Count: 				<td><?php echo $issues_cnt; ?>

		 <tr><td>Agent Issues: 				<td><?php echo $agent_issues_cnt; ?>

		 <tr><td>System Issues: 			<td><?php echo $system_issues_cnt; ?>

		 <tr><td>Other Issues: 				<td><?php echo $other_issues_cnt; ?>

		 <tr><td><b>ZTP/LOL Mark Status	<td>

		 <tr><td>ZTP Count: 				<td><?php echo $ztp_cnt; ?>

		 <tr><td>LOL Count: 				<td><?php echo $lol_cnt; ?>

	</table>

	<table class="table table-striped" id="issue_comment_report"> 

		 <thead> 

		 	<tr>
		 		<td rowspan="2"><b>Issue Comment Report </b>
		 		<td colspan="14"> <b> SMEs/TL </b> </td>
 		 	<tr>	
 		 		
 		 		<?php 
 		 		
 		 			foreach($sme_list as $sme_list_row) {
 		 				
 		 				echo "<td>" . $sme_list_row['roster_support_name'];
 		 			}
 		 			

 		 		?>

		 		<td>  <b> Total Counts </b>

		 </thead>

		 <tbody>
		 
		 	<?php foreach ($issue_comments as $issue_comments_row) {
		 				
		 				echo "<tr><td>". $issue_comments_row['issue_comment'];

 		 				
		 				for($sme_count = count($sme_list); $sme_count >= 0; $sme_count--)
		 					echo "<td>";

		 				echo $issue_comments_cnt[$issue_comments_row['issue_comment_id']];	
		 	} 

		 	?>
		 
		 </tbody>

	</table>

	<table class="table table-striped"> 

		 <tr>
		 	<td><b> General Observations </b>
		 		<td> 
		 		<td> <b> Total Counts

		 	<?php foreach ($gen_observational_report as $gen_observational_report_row) {
		 				
		 				echo "<tr><td>". $gen_observational_report_row['general_observation']. 	
		 						"<td>".
		 						"<td>".$gen_observational_report_row['auditCnt'];	
		 	} ?>

	</table>

<br><br>

<form action= "<?php echo base_url('Audit/export_audits'); ?>" method = 'POST'>

		<button class = 'btn btn-success'> 
			Export Audits <i class="fa fa-download"></i> 
		</button>

		<input type="hidden" name="date_from" value="<?php echo $date_from; ?>">

		<input type="hidden" name="date_to" value="<?php echo $date_to; ?>">

</form>

<br><br>

<form action= "<?php echo base_url('Audit/qa_remarks_summary'); ?>" method = 'POST'>

		<button class = 'btn btn-danger'> 
			View QA Remarks Summary  <i class="fa fa-bar-chart"></i> 
		</button>

		<input type="hidden" name="date_from" value="<?php echo $date_from; ?>">

		<input type="hidden" name="date_to" value="<?php echo $date_to; ?>">

</form>

