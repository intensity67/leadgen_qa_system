<style type="text/css">

	.report_summary{
		text-align: center;
	}

</style>

	<div id="content-wrapper">

		<div class="container-fluid">

	 	<h1 class="page-header">  Quality Production Report </h1>

	 	<form>
	 	
	 	<table class="table table-striped table-condensed">
	 	
	 		<tr><td> Start Time: <td> <input type="datetime-local" name="">
	 	
	 		<tr><td> End Time: <td><input type="datetime-local" name="">
	 	
	 	</table>

	 	</form>

		<table class="table table-striped ">
			
			<tr><td> For Timezone Difference <span class="text text-info"> 20:00 </span> </p>

	 		<tr><td> Shift Date: <span class="text text-info"> 12/27/2018 </span> </p>

	 	</table>

	 	<table class="table table-striped report_summary">

	 			<tr>
	 				<td>
	 				<td>
	 				<td>Lester Galenzoga 
	 				<td>Francis Jomar Balitad
	 				<td>Mary Cristine Go
 	 				<td>Iris Costanilla
	 				<td>Nick Costanilla
	 				<td>John Aves

	 			<tr><td> Average Handle Time <td><td> Lester 	<td> Francis 	<td> Cristine	<td> Ish <td> Nica <td> John

	 			<tr>
	 				<td>8<td>Long Form 		<td> 0	<td> 0	<td> 0	<td> 0	<td> 0 	<td> 0
	 			<tr>
	 				<td>4<td>Flagged Form 	<td> 0	<td> 0  <td> 24 <td> 0	<td> 32	<td> 0
	 			<tr>
	 				<td>3<td>Short Form 	<td> 0	<td> 0 	<td> 0	<td> 0	<td> 0 	<td> 0
	 			<tr>
	 				<td>4<td>IR Form 		<td> 0 	<td> 0	<td> 0	<td>1	<td> 11	<td> 0
	 			<tr>
	 				<td>3<td>Training Observation Form <td> 0 	<td> 0 	<td> 0	<td> 0	<td> 0 	<td> 0
	 			<tr>
	 				<td>5.5<td>Temp Transfer Check <td> 0 	<td> 0	<td> 0 	<td> 0 <td> 0 	<td> 0
	 			<tr>
	 				<td>390<td>Total Minutes Consumed <td> 0	<td> 24	<td>	1<td>	4<td> 172 	<td>

	 			<tr>
	 				<td><td><td> 0.00% <td> 0.00% 	<td> 0.00% 	<td> 0.00% <td>	 0.00%  <td> 0.00% <td>
 	 				 
	 	</table>
