<style type="text/css">
	
	table{
		text-align: center;
	}

</style>

<div class = "row" >

<h2> <center> Q.A. Result </center></h2>

<form action = "<?php echo base_url('Report/'); ?>" method="POST"> 
	
	<table class="table table-striped">
		
		<tr><td>Date Type: 
			<td> <input type="radio" name="date_type"> Work Week  
				 <input type="radio" name="date_type"> Date Range

		<tr><td>Choose Date: <td><input type="date" name="" class="form form-control">

		<tr><td>Date From: <td><input type="date" name="date_from" class="form form-control">
		<tr><td>Date To: <td><input type="date" name="date_to" class="form form-control">
	

	<tr><td><td> <button type="submit" class="btn btn-success"> Filter Date </button>

	</table>

</form>

<table class="table table-striped">
	

	<tr><td> Agent Name <td> Najarro, Fritz Michael

	<tr><td> Week <td> <?php echo date("M d, Y", strtotime($work_week)); ?>

</table>

<table class="table table-striped">

	<tr><td><td> Week 						
			<td> Running Month 
	
	<tr><td>  Numbers audited	<td> <?php echo ($week_audits_report['audits_cnt']) ? $week_audits_report['audits_cnt']: 0; ?> 
								<td> <?php echo ($month_audits_report['audits_cnt']) ? $month_audits_report['audits_cnt']: 0; ?> 

	<tr><td>  Number of submits audited 	<td> <td>
	<tr><td>  Number of non-submits audited <td> <td>
	<tr><td>  Flagged Calls 				<td> <td>

</table>

<table class="table table-striped">

	<tr><td>LOL/ZTP <td> Week <td> Running Month 
	
	<tr><td>  <td>  <td> 
	<tr><td>  Top 3 <td> <td>
	<tr><td>  L.O.L. - Aggressive Selling 	<td> 0 <td>
	<tr><td>  L.O.L. - Script 				<td> 0 <td>
	<tr><td>  L.O.L. - Webform Information 	<td> 0 <td>


	<tr><td>LOL/ZTP <td>   <td>  

	<tr><td>  L.O.L. - Call avoidance 											<td> 0 <td> 0
	<tr><td>  L.O.L. - Escalated the call to the SUP without customer request 	<td> 0 <td> 0
	<tr><td>  L.O.L. - Transferred the call inappropriately 					<td> 0 <td> 0
	<tr><td>  L.O.L. - Did not brand the call 									<td> 0 <td> 0
	<tr><td>  L.O.L. - Inappropriate response								 	<td> 0 <td> 0
	<tr><td>  L.O.L. - Unprofessional								 			<td> 0 <td> 0
	<tr><td>  L.O.L. - Making disparaging remarks								<td> 0 <td> 0
	<tr><td>  L.O.L. - Speaking in Vernacular									<td> 0 <td> 0
	<tr><td>  L.O.L. - Tagging the call incorrectly on the webform 				<td> 0 <td> 0
	<tr><td>  L.O.L. - Submitting webform more than once 						<td> 0 <td> 0
	<tr><td>  L.O.L. - Aggressive Selling 										<td> 0 <td> 0
	<tr><td>  L.O.L. - Script 													<td> 0 <td> 0
	<tr><td>  L.O.L. - Webform Information 										<td> 0 <td> 0
	<tr><td>  L.O.L. - Language Barrier/Confused 								<td> 0 <td> 0
	<tr><td>  ZTP - DNC 														<td> 0 <td> 0
	<tr><td>  ZTP - ROBO call 													<td> 0 <td> 0
	<tr><td>  ZTP - Call Riding 												<td> 0 <td> 0
	<tr><td>  ZTP - Score Manipulation 											<td> 0 <td> 0

</table>

<h2> Errors made in submitted calls for the week selected </h2>

<table class="table table-striped">

	<tr><td> Script 
		<td> Speed 
		<td> Customer Acknowledgement
		<td> Incorrect responses
 
	<?php foreach($script_lists_cnt as $script_index => $scripts_reports_row){ ?>
		
		<tr><td> <?php echo $scripts_reports_row['script_list_code']; ?> 
			<td> <?php echo ($scripts_reports_row['script_cnt'] != 0) ? number_format($scripts_reports_row['speed_cnt'] / $scripts_reports_row['script_cnt'], 2) : 0; ?> 
			<td> <?php echo $scripts_reports_row['cust_acknowledgement']; ?> 
			<td> <?php echo $scripts_reports_row['incorrect_response_cnt']; ?> 
 
	
	<?php } ?>
 
</table>



<table class="table table-striped">

	<tr><td>  <td> This Week <td> Previous Week <td> This Month <td> Previous Month
	<tr><td> Flagged Calls <td> <td><td><td>
	<tr><td> -------------unsuccessful call back------------- <td> <td><td><td>
	<tr><td> does not want to reveal information <td> <td><td><td>
	<tr><td> hung up during call back <td> <td><td><td>
	<tr><td> no answer <td> <td><td><td>
	<tr><td> voicemail reached  <td> <td><td><td>
	<tr><td> call back rescheduled <td> <td><td><td>
	<tr><td> unspecific  <td> <td><td><td>
	<tr><td> scheduled call back  <td> <td><td><td>
	<tr><td> -------------not interested to switch-------------  <td> <td><td><td>
	<tr><td>  Not Interested <td> <td><td><td>
	<tr><td> loyal to company  <td> <td><td><td>
	<tr><td> price of quote  <td> <td><td><td>
	<tr><td> coverage of policy  <td> <td><td><td>
	<tr><td> e-mail request for review  <td> <td><td><td>
	<tr><td> change of company unaccepted  <td> <td><td><td>
	<tr><td> -------------others------------- <td> <td><td><td>
	<tr><td> unqualified <td> <td><td><td>
	<tr><td> not specific <td> <td><td><td>
	<tr><td> busy  <td> <td><td><td>
	<tr><td> confused  <td> <td><td><td>
	<tr><td> lang barrier  <td> <td><td><td>
	<tr><td> DNC <td> <td><td><td>
	<tr><td> prank <td> <td><td><td>
	<tr><td> arranged call back <td> <td> <td><td>
	<tr><td> Didn't want to be transfer <td> <td><td><td>

</table>