<div class = "row" >

<h2> Audit Reports Summary </h2>

<center>

	<table class="table table-striped">
	  	
	  	<tr>
	 		<?php if($date_groups == 'weekly'){ ?>

	 			<b> Work Week </b>

	 		<?php }else { ?>

	  			<td> <b> Date </b>
	  		
	  		<?php } ?>

	 	<?php foreach($audit_date_list as $row): ?>
	 	
	 		<?php if($date_groups == 'weekly'){ ?>

			<tr><td> <a href="<?php echo base_url('Report/audit_report_details/'.$row['work_week']); ?>" target = "_blank">

					 <?php echo date("M d, Y", strtotime($row['work_week'])); ?>

				</a>
		
	 		<?php }else { ?>

			<tr><td>
				
				<?php $next_date = strtotime("+1 days", strtotime($row['auditDate'])); ?>
 				<a href="<?php echo base_url('Report/audit_report_details/'.$row['auditDate']); ?>" target = "_blank">

					<?php echo date("M d, Y", strtotime($row['auditDate'])) .' 10:00 pm - '. date("M d, Y ", $next_date) . "  7:00 am"; ?>
				</a>
				
				<td> <a href="<?php echo base_url('Report/audit_report_tally/'.$row['auditDate']); ?>" target = "_blank" class = 'btn btn-primary'>
					View Tally
 				</a>

			<?php } ?>
		
		<?php endforeach; ?> 
 

	</table>
	
	<h4>Select Audit Report Dates: </h4>

	<form method="POST" action="<?php echo base_url('Report/audit_report_details/'); ?>">

		<table class="table">
		
			<tr><td>Date From:  <td><input type="date" name="date_from">
			<tr><td>Date To: 	<td><input type="date" name="date_to">

		</table>

		<button type="submit" class="btn btn-primary">
			Generate Report <i class="fa fa-bar-chart"></i>
		</button>

	</form>

	<br><br>
		<a href="<?php echo base_url('Report/audit_reports/weekly'); ?>" class="btn btn-primary"> Work Week </a>

 </center>
