
<style type="text/css">
  /*dark background to support form theme*/
body{
  background:url(<?php echo base_url('assets/dark_wall.png'); ?>);
}

/*sass variables used*/
$full:100%;
$auto:0 auto;
$align:center;

@mixin disable{
  outline:none;
  border:none;
}

@mixin easeme{
  -webkit-transition:1s ease;
  -moz-transition:1s ease;
  -o-transition:1s ease;
  -ms-transition:1s ease;
  transition:1s ease;
}

/*site container*/
.wrapper{
  width:420px;
  margin: auto;
  margin-top: 50px;
}

h1{
  text-align:$align;
  padding:30px 0px 0px 0px;
  font:25px Oswald;
  color:#FFF;
  text-transform:uppercase;
  text-shadow:#000 0px 1px 5px;
  margin:0px;
}

p{
  font:13px Open Sans;
  color:#6E6E6E;
  text-shadow:#000 0px 1px 5px;
  margin-bottom:30px;
}

.form{

  width:$full;
  
}

input[type="text"],input[type="password"]{

  width:98%;
  padding:15px 0px 15px 8px;
  border-radius:5px;
  box-shadow:inset 4px 6px 10px -4px rgba(0,0,0,0.3), 0 1px 1px -1px rgba(255,255,255,0.3);
  background:rgba(0,0,0,0.2);
  @include disable;
  border:1px solid rgba(0,0,0,1);
  margin-bottom:10px;
  color:#6E6E6E;
  text-shadow:#000 0px 1px 5px;

}

input[type="submit"]{

  width:100%;
  padding:15px;
  border-radius:5px;
  @include disable;
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#28D2DE), to(#1A878F));
  background-image: -webkit-linear-gradient(#28D2DE 0%, #1A878F 100%);
  background-image: -moz-linear-gradient(#28D2DE 0%, #1A878F 100%);
  background-image: -o-linear-gradient(#28D2DE 0%, #1A878F 100%);
  background-image: linear-gradient(#28D2DE 0%, #1A878F 100%);
  font:14px Oswald;
  color:#FFF;
  text-transform:uppercase;
  text-shadow:#000 0px 1px 5px;
  border:1px solid #000;
  opacity:0.7;
  -webkit-box-shadow: 0 8px 6px -6px rgba(0,0,0,0.7);
  -moz-box-shadow: 0 8px 6px -6px rgba(0,0,0,0.7);
  box-shadow: 0 8px 6px -6px rgba(0,0,0,0.7);
  border-top:1px solid rgba(255,255,255,0.8)!important;
  -webkit-box-reflect: below 0px -webkit-gradient(linear, left top, left bottom, from(transparent), color-stop(50%, transparent), to(rgba(255,255,255,0.2)));

}

input:focus{

  box-shadow:inset 4px 6px 10px -4px rgba(0,0,0,0.7), 0 1px 1px -1px rgba(255,255,255,0.3);
  background:rgba(0,0,0,0.3);
  @include easeme;

}

input[type="submit"]:hover{

  opacity:1;
  cursor:pointer;

}

.name-help,.email-help{

  display:none;
  padding:0px;
  margin:0px 0px 15px 0px;

}

.optimize{

  position:fixed;
  right:3%;
  top:0px;

}


</style>

<script type="text/javascript">

  $(".name").focus(function(){

    $(".name-help").slideDown(500);

  }).blur(function(){

    $(".name-help").slideUp(500);

  });

  $(".password").focus(function(){

    $(".email-help").slideDown(500);

  }).blur(function(){

    $(".email-help").slideUp(500);

  });

</script>
    <link href="<?php echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">

<link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>

<div class="wrapper">
  <center> <img src="<?php echo base_url('assets/leadgen_final_logo.svg'); ?>"  height = 70 width = 200> </img> </center>

  <h1> <center class= 'text text-info'> LeadGen QA System </center></h1>
  <p></p>
    
    <form class="form" action = "<?php echo base_url('Login/login_check'); ?>" method="POST">
 
    <input type="text" class="name" name="username" placeholder="Username">
    
    <div>
      <p class="name-help">Please enter your username.</p>
    </div>

    <input type="password" class="password" name="password" placeholder="Password">
    
    <div>
      <p class="email-help">Please enter your password.</p>
    </div>

    <?php if($this->session->userdata('error_login') !== NULL): ?>

          <label class="text text-danger"> Invalid Username/Password </label>
          
          <br><br>

    <?php endif; ?>

    <input type="submit"  value="Login">
  
  </form>

</div>
<p class="optimize">        

 </p>