<!DOCTYPE html>

<html lang="en">

  <head>

    <meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <meta name="description" content="">

    <meta name="author" content="">
 
    <title> LeadGen Q.A. System- Signin  </title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<!--     <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">
 -->
    <!-- Custom styles for this template -->
    <link href="<?php echo base_url('assets/dist/signin/signin.css'); ?>" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<!--     <script src="<?php echo base_url('assets/js/ie-emulation-modes-warning.js'); ?>"></script>
 -->
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <style type="text/css">
      
      body{
    
        background-color: #696969 !important; */

      }
      
     #login-form{ 
    
        width: 500px; 
        margin-top: 50px; 
        background-color: #696969;
        background-repeat: no-repeat; 
        background-size: auto;   
     
    }

    </style>

  </head>

  <body>

    <div class="container" id = "login-form">

      <form class="form-signin" action = "<?php echo base_url('Login/login_check'); ?>" method="POST">
        
        <center> <img src="<?php echo base_url('assets/leadgen_final_logo.svg'); ?>"  height = 70 width = 80> </img> </center>

        <center> <h2 class="form-signin-heading"> LeadGen Q.A. System </h2> </center>

        <label for="inputEmail" class="sr-only"> Username </label>

        <input type="text" name = "username" class="form-control" placeholder="Username" required autofocus>
        
        <label for="inputPassword" class="sr-only">Password</label>
   
        <input type="password"  name = "password" class="form-control" placeholder="Password" required>
        
        <br><br>
        
        <?php if($this->session->userdata('error_login') !== NULL): ?>

          <label class="text text-danger"> Invalid Username/Password </label>
          
          <br><br>

        <?php endif; ?>

        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
     
      </form>

    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<!--     <script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>
 -->  

    </body>

</html>
