	<style type="text/css">
			
		.call_ends{

			width: 15px;
			height: 20px;			
			-webkit-appearance: checkbox; /* Chrome, Safari, Opera */
			-moz-appearance: checkbox;    /* Firefox */
			-ms-appearance: checkbox;     /* not currently supported */
		}

		.td_call_ends{
			
			text-align: center;

		}

		.acknowledgement_col{
			font-size: 8px;
		} 
		
		.speed_field{
			width: 40px;
		}

		.comment_field{
			width: 150px;
		}

	</style>

  
	<table class="table table-striped" border = 1>
		
		<thead>

			<tr>
				<td rowspan="3">Script Code
				<td rowspan="2">Customer
				<td colspan="4">Agent</td>	
				<td colspan="3" rowspan="2"> Information </td>
				<td rowspan="3">Comment

			<tr>
				<td> <td colspan="3">   Accuracy 
			<tr>
				<td>Statement
					<td class="acknowledgement_field">Acknowledgement<td>Agent Response <td> Speed <td> Correct Response 
					<td> Customer Details <td> Agent Input <td> Call Ends  

		</thead>

		<tbody>
			
		<?php foreach($audit_scripts as $row_audit_scripts): ?>

			<tr><td><?php echo $row_audit_scripts['script_list_code']; ?>

				<td> <input type="" name="statement[]" class="form-control statement-input" value="<?php echo $row_audit_scripts['customer_statement']; ?>">

				<td> 
					<select name= "acknowledgement[]" class="form-control"> 
						
						<option >  </option> 
						
						<option value="Yes" <?php if($row_audit_scripts['agent_acknowledgement'] == "Yes") echo 'selected'; ?>> Yes </option> 
						
						<option value="No" <?php if($row_audit_scripts['agent_acknowledgement'] == "No") echo 'selected'; ?>> No </option> 
					
					</select>

				<td> <input type="text" name="agent_response[]" class="form-control input-sm speed-input" value="<?php echo $row_audit_scripts['agent_accuracy_response']; ?>">

				<td> <input type="text" name="speed[]" class="form-control input-sm" value="<?php echo $row_audit_scripts['agent_accuracy_speed']; ?>">

				<td> <input type="text" name="correct_response[]" class="form-control correct-response-input" value="<?php echo $row_audit_scripts['agent_accuracy_cor_response']; ?>">

				<td> <input type="text" name="custom_details[]" class="form-control correct-response-input" value="<?php echo $row_audit_scripts['info_cust_details']; ?>">
				<td> <input type="text" name="agent_input[]" class="form-control correct-response-input" value="<?php echo $row_audit_scripts['info_agent_input']; ?>">

				<td><input type="radio" name="call_ends" value="<?php echo $row_audit_scripts['script_list_code']; ?>" class="call_ends form-control" 
					<?php if(!empty($row_audit_scripts['info_call_ends'])) echo "checked".$row_audit_scripts['info_call_ends']; ?>>

				<td><textarea name="comment[]" class="form-control comment_field" rows = 7 cols = 50><?php echo $row_audit_scripts['comment']; ?></textarea>

				<input type="hidden" name="script_list_id[]" value="<?php echo $row_audit_scripts['script_lists_id']; ?>" class="form-control correct-response-input">	

			<?php endforeach; ?>
		
		</tbody>


	</table>
