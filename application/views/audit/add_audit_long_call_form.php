<style type="text/css">
	
	textarea {
		resize: none;
	}	

	.recordings{

		width: 500px;
	}

	table{

	  	text-align: center;

	}

	textarea{
		resize: none;
	}
/*
	#audio-recording{
		position: sticky;
		height: 200px;
		top: 0px;
	}*/

</style>

<script type="text/javascript">

var currentAudio;

$("document").ready(function(){

	$(".slow-audio").click(function(){
		
		currentAudio.playbackRate = 0.5;

	});

	$(".fast-audio").click(function(){
	
		// $("audio").playbackRate = 5;
		
		currentAudio.playbackRate = 2;

	});

    $("audio").on("play", function() {
    	
    	currentAudio = $(this);

        $("audio").not(this).each(function(index, audio) {
            audio.pause();
        });
    });

});

</script>

<h2> <center> Audit (Long Form)</center> </h2>

<a href="<?php echo base_url('Audit/audit_list'); ?>"> <i class="fa fa-reply"></i> Back to Audit Listings </a>

<?php echo form_open(base_url('Audit/insert_audit')); ?>
	
		<center> 
			
			<div id = "audio-recording">

		 		<audio controls class = "recordings" >

								<source src="<?php echo $audio; ?>" 
														type="audio/ogg">

								<source src="<?php echo $audio; ?>" >

				<audio>

			</div>

		</center>
		
		<table class="table table-condensed">

			<tr>
			
<!-- 				<td>Speed: <td><input type="range" class="form-control" href= "#" class="btn btn-info" min = 0 max = 2 /> 
 -->			
		</table>

		<br>
 				<?php $work_week = date("Y-m-d", strtotime('monday this week')); ?>
 				
 				<table class="table table-condensed">
 					
 					<tr><td><h4> <b> Recording Info </b> </h4><td>

 
 					<tr><td> Work Week (Start Date): <td><input type="date" name="work_week" value="<?php echo $work_week; ?>" class="form-control">

 					<tr><td> Recording Id: <td><input type="text" disabled=""  class="form-control" value="<?php echo $recording_id; ?>">

 					<tr><td> Recording/Call Id: <td><input type="text" disabled="" class="form-control" value="<?php echo $recording_id; ?>">

 					<tr><td> Phone Number / BTN: <td><input disabled="" value = "<?php echo $phone_number; ?>" type="text" name="phone_number" class="form-control">

					<?php if($this->session->userdata("login_info")['user_roles_id'] == ADMIN ): ?> 

						<tr><td> <h4> <b> Agent Info </b> </h4> <td>

	 					<tr><td> Agent Id: <td><input type="text"  class="form-control" disabled="disabled" value="<?php echo $user; ?>">

	 					<tr><td> Agent Name: <td> <input type="text"  class="form-control" disabled="disabled" value="<?php echo $agent; ?>">
 						
 						<tr><td> Team Lead: <td> <input type="text"  class="form-control" disabled="disabled" value="<?php echo $tl; ?>" name = "tl">
 						
 						<tr><td> Floor Support: <td> <input type="text" name="fs" class="form-control" disabled="disabled" value="<?php echo $fs; ?>">

					<?php endif; ?> 

 					<tr><td> Timestamp: <td><input type="text" disabled class="form-control" value="<?php echo date("m/d/Y h:i:s a", strtotime($call_date)); ?>">
 					
 					<tr><td> Prospect Tone: <td class="pull-left form-control">
 											 
						<label class="checkbox-inline"><input type="radio" value="1" name="prospect_tone" required> 1</label>
						<label class="checkbox-inline"><input type="radio" value="2" name="prospect_tone" required> 2</label>
						<label class="checkbox-inline"><input type="radio" value="3" name="prospect_tone" required> 3</label>

 					<tr><td> Sript: <td><select class="form-control" name="soundboard_voice_id" required>
 												
 												<option selected="selected"> Select Soundboard Invoice </option>

 												<?php foreach($soundboard_list as $row) { ?>

		 											<option value="<?php echo $row['soundboard_voice_id']; ?>"
		 												 
		 												> <?php echo $row['soundboard_voice_name']; ?>  </option>
		 										
		 										<?php } ?>

											</select>
 					
 					<tr><td> Recording: 
 						<td><a href= "<?php echo $location; ?>" ><?php echo $location; ?></a>

 				</table>
			 
  
		<?php 

			if($length_in_sec >= 60 )
				
				$audit_type = 'long_call';

			else

				$audit_type = 'short_call';
 
			$data = array(

				'recording_cluster_id'	=> $recording_cluster_id,
				'work_week'  			=> $work_week,
				'audit_type'  			=> $audit_type,
				'agent_user_id'  		=> $user,
				'phone_number'  		=> $phone_number,
				'recording_id'  		=> $recording_id,
				'lead_id' 				=> $lead_id,
				'tl' 					=> $tl,
				'agent_name' 			=> $agent,
				'fs' 					=> $fs,
				'agent_disposition'		=> $status,
				'audit_date'   			=> date("Y-m-d"),
				'audit_timestart'		=> date("Y-m-d H:i:s"),
				'call_timestamp'  		=> $call_date,
				'cluster_no'  			=> '4',
				'recording_location'	=> $location,
				'auditor_user_id' 		=> $this->session->userdata('login_info')['user_account_id'],	
				'auditor' 				=> $this->session->userdata('login_info')['acct_nick_name']

 							            );
						
			echo form_hidden($data);

		?> 
 		
		<?php $this->load->view('audit/script_audits_form'); ?>

<!-- 		<center> <a href = "#" data-toggle="modal" data-target="#add-script-modal" class="btn btn-success"> Add Script <i class="fa fa-plus"></i> </a> </center>
 -->		
		<br><br>

		<table class="table table-condensed">

			<tr><td>Speed: <td><input type="range" class="form-control" href= "#" class="btn btn-info" min = 0 max = 2 /> 

 			<tr><td> Agent Disposition: 	<td> <input type="text" name="agent_disposition_id" value="<?php echo $status; ?>" disabled  class="form-control" />

 			<tr><td> Correct Disposition: 	<td> <select class="form-control" name="correct_disposition_id" required>
 											<option> Select Correct Disposition </option>

 											<?php foreach($disposition as $dispo_row): ?>

 												<option value="<?php echo $dispo_row['dispo_code']; ?>" 
 													<?php if($dispo_row['dispo_code'] == $status) echo 'selected'; ?> > <?php echo $dispo_row['dispo_code']; ?> </option>

 											<?php endforeach; ?>

 										</select>

 			<tr><td> Does this call have a ZTP or LOL mark? <td class="pull-left form-control">	
 
						<label class="checkbox-inline"><input type="radio" value="N/A" name="mark_status" required checked=""> N/A </label>
						<label class="checkbox-inline"><input type="radio" value="ZTP" name="mark_status" required> ZTP </label>
						<label class="checkbox-inline"><input type="radio" value="LOL" name="mark_status" required> LOL </label>
   											
 			<tr><td> Were there any incorrect responses: <td class="pull-left form-control">

						<label class="checkbox-inline"><input type="radio" value="N/A" name="incorrect_response" required checked=""> N/A </label>
						<label class="checkbox-inline"><input type="radio" value="Agent" name="incorrect_response" required> Yes </label>
						<label class="checkbox-inline"><input type="radio" value="System" name="incorrect_response" required> No </label> 

 			<tr><td> Agent/System issue?: <td class="pull-left form-control">
 											 
						<label class="checkbox-inline"><input type="radio" value="N/A" name="agent_issue" required checked=""> N/A </label>
						<label class="checkbox-inline"><input type="radio" value="Agent" name="agent_issue" required> Agent </label>
						<label class="checkbox-inline"><input type="radio" value="System" name="agent_issue" required> System </label>
						<label class="checkbox-inline"><input type="radio" value="Others" name="agent_issue" required> Others </label>
 
 			<tr><td> Agent Issue Comment: 
 			 		
  			<tr><td><?php $i = 1; ?>

 			 				<?php foreach($issue_comments as $issues_row): ?>
 			 					
 			 					<?php if($i % 2 == 0) echo "<tr>"; ?>

 								 <td>  
	
 									 <input type= "checkbox" name = "issue_comments[]" value="<?php echo $issues_row['issue_comment_id']; ?>"/> 
 									<?php echo $issues_row['issue_comment']; ?> 
 								
 
 							<?php $i++; ?>

 							<?php endforeach; ?>
 						
			<tr><td colspan="2">General Observation:

  			<tr><td colspan="2"> <select class="form-control" name="general_observation_id" required>

 
 											<?php foreach($gen_observational_list as $gen_obs_row): ?>
 											
 											<option value="<?php echo $gen_obs_row['general_observation_list_id']; ?>"> <?php echo $gen_obs_row['general_observation']; ?> </option>

 											<?php endforeach; ?>
 											
 			<tr><td colspan="2"> QA Remarks:

  			<tr><td colspan="2"> <textarea name="qa_remarks" class="form-control" rows= 7 cols = 15></textarea>

 					</select>
	</table>

 
	<button type="submit" class="btn btn-success" name=""> <i class="fa fa-save"></i> 	
		Save and Submit  
	</button>

		
</form>

<?php $this->load->view('audit/modals/add_script_audit_modal'); ?>

