<style type="text/css">
	
	textarea{
		resize: none;
	}	

</style>

	<div id="long-call-form<?php echo $recording_id; ?>" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!--  -->
	    <div class="modal-content">
	      <div class="modal-header">	
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h2 class="modal-title"> Long Call Form </h4>
	      </div>

	      <div class="modal-body">
 				
 				<table class="table table-condensed">
 				 
 					<tr><td> Work Week (Start Date): <td><input type="date" name="work_week" value="<?php echo date("Y-m-d"); ?>" class="form-control">

 					<tr><td> Recording Id: <td><input type="text" disabled="" name="call_id" class="form-control" value="<?php echo $recording_info['recording_id']; ?>">
 					
 					<tr><td> Call Id: <td><input type="text" disabled="" name="call_id" class="form-control" value="<?php echo $recording_info['recording_id']; ?>">

 					<tr><td> Phone Number: <td><input disabled="" value = <?php echo $recording_info['phone_number']; ?> type="text" name="phone_number" class="form-control">

 					<tr><td> Customer: <td><input type="text" name = "customer" class="form-control">

 					<tr><td> Agent Id: <td><input type="text" name="agent_id" class="form-control" disabled="disabled" value="<?php echo $recording_info['user']; ?>">

 					<tr><td> Agent Name: <td> <input type = 'text' value = 'Ghian Alchumbre' class="form-control"  disabled="" />

 					<tr><td> Timestamp: <td><input type="text" name="timestamp" disabled class="form-control" value="<?php echo date("m/d/Y h:i A", strtotime($recording_info['start_time'])); ?>">

 					<tr><td> Soundboard Invoice: <td><select class="form-control">

 												<?php foreach($qa_list as $row) { ?>

		 											<option value="<?php echo $row['users_id']; ?>"> <?php echo $row['first_name']; ?>  </option>
		 										
		 										<?php } ?>

											</select>
 					
 					<tr><td> Recording: <td><input type="text" name="recording_id" class="form-control" disabled="" value="<?php echo $recording_info['location']; ?>">

 					<tr><td> Agent Disposition: <td><select class="form-control" name="agent_disposition_id">
 											<option selected="selected"></option>

 											<?php foreach($disposition as $dispo_row): ?>

 												<option value="<?php echo $dispo_row['disposition_id']; ?>"> <?php echo $dispo_row['disposition']; ?> </option>

 											<?php endforeach; ?>

 										</select>

 					<tr><td> Correct Disposition: <td><select class="form-control" name="correct_disposition_id">
 											<option selected="selected"></option>

 											<?php foreach($disposition as $dispo_row): ?>

 												<option value="<?php echo $dispo_row['disposition_id']; ?>"> <?php echo $dispo_row['disposition']; ?> </option>

 											<?php endforeach; ?>

 										</select>

   											
 					<tr><td> Does this call have a ZTP mark? <td><select class="form-control">
 											<option selected="selected" value="N/A">N/A</option>
 											<option value="Yes">Yes</option>
 											<option value="No">No</option>

 											
 					<tr><td> Were there any incorrect responses: <td><select class="form-control">
 											<option selected="selected" value="N/A">N/A</option>
 											<option value="Yes">Yes</option>
 											<option value="No">No</option>

 					<tr><td> Agent/System issue?: <td><select class="form-control" name="">
 											<option selected="selected" value="N/A">N/A</option>
 											<option value="Agent">Agent</option>
 											<option value="Syste,">System</option>
 											<option value="Others">Others</option>

 					<tr><td> General Observation: <td>
 					
 					<tr><td colspan="2"><textarea name="general_observation" class="form-control" rows= 7 cols = 15> </textarea>

 					<tr><td> QA Remarks: <td>

  					<tr><td colspan="2"><textarea name="qa_remarks" class="form-control" rows= 7 cols = 15> </textarea>

 					</select>

 					<input type="hidden" name="lead_id" value="<?php echo $recording_info['lead_id']; ?>">

 				</table>

			<source src="<?php echo base_url('uploads/audios/'.$cnt.'.wav'); ?>" 
							type="audio/ogg">

			<source src="<?php echo base_url('uploads/audios/'.$cnt.'.wav'); ?>" >
				
 	      </div>

	      <div class="modal-footer">
	      	
	      	<form action = "script_audit/1">

	        	<button type="submit" class="btn btn-success">Save and Proceed</button>
	        

	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

	        </form>


	      </div>

	    </div>

	  </div>
	</div>