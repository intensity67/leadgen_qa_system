<div id="add-script-modal" class="modal fade" role="dialog">
	  <div class="modal-dialog">
 
	    <!--  -->
	    <div class="modal-content">
	      <div class="modal-header">	
	        <button type="button" class="close" data-dismiss="modal">&times;</button>

	        <h2 class="modal-title"> Add Audit Script Line  </h4>

	      </div>

	      <div class="modal-body">
			 					
			<table class="table">
				
				<thead>
 
				<tbody>
					 
						<tr><td>Script Code<td> <select name="script_code[]" class="form-control">

							<option>z01</option>
							<option>z02</option>
							<option>z03</option>
							<option>z04</option>
							<option>z16</option>
							<option>z05</option>
							<option>z06</option>
							<option>z07</option>
							<option>z15</option>
							<option>z08</option>
							<option>z09</option>
							<option>z10</option>
							<option>z11</option>
							<option>z12</option>

						</select>  

						<tr><td>Customer Statement<td> <input type="text" name="statement[]" class="form-control ">

						<tr><td> Agent Acknowledgement<td> <select name= "acknowledgement[]" class="form-control"> 
								<option >  </option> 
								<option value="Yes"> Yes </option> 
								<option value="No"> No </option> 
							</select>

						<tr><td> <b> Accuracy </b> <td> 

						<tr><td> Agent Response <td> <input type="text" name="agent_response[]" class="form-control ">

						<tr><td> Agent Speed (Agent) <td> <input type="text" name="speed[]" class="form-control ">

						<tr><td> Correct  Response <td> <input type="text" name="correct_response[]" class="form-control ">

						<tr><td> <b> Information </b> <td> 

						<tr><td> Customer Details <td> <input type="text" name="custom_details[]" class="form-control ">

						<tr><td> Agent Input <td> <input type="text" name="agent_input[]" class="form-control ">

						<tr><td> Call Ends <td> <input type="text" name="call_ends[]" class="form-control ">

						<tr><td> <td> 

						<tr><td> Comment <td> <input type="text" name="comment[]" class="form-control ">
 
				</tbody>


			</table>


 	      </div>

	      <div class="modal-footer">
	      	
	      	<form action = "script_audit/1">

	        	<button type="submit" class="btn btn-success">  Submit </button>
	        

	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

	        </form>


	      </div>

	    </div>

	  </div>
	</div>