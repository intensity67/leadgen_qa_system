<div id="delete-audit-modal<?php echo $audit_id; ?>" class="modal fade" role="dialog">

	  <div class="modal-dialog">
 
	    <!--  -->
	    <div class="modal-content">
	      <div class="modal-header">	
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h2 class="modal-title"> Delete Audit Confirmation </h4>
	      </div>

	      <div class="modal-body">

	      	<form action = "<?php echo base_url('Audit/delete_audit/'.$audit_id); ?>" method="POST">

			<h4><span class="text text-danger"> Are you sure you want to delete this Audit? </span> </h4>
 					
 				<table class="table table-condensed text text-info" style="margin-top: 25px;">  

 					<tr><td>Audit Id: 	<td><?php echo $audit_id; ?> 
 					
 
 					<input type = "hidden" name="audit_id" value="<?php echo $audit_id; ?>">

 				</table>

 	      </div>

			<div class="modal-footer">
		      	
				<button type="submit" class="btn btn-danger">Yes</button>

		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

			</div>
		    
	    	</form>

	    </div>

	  </div>
	</div>
