<style type="text/css">
	
	textarea {
		resize: none;
	}	

	.recordings{

		width: 500px;
	}

</style>

<script type="text/javascript">

var currentAudio;

$("document").ready(function(){

	$(".slow-audio").click(function(){
		
		currentAudio.playbackRate = 0.5;

	});

	$(".fast-audio").click(function(){
	
		// $("audio").playbackRate = 5;
		
		currentAudio.playbackRate = 2;

	});

    $("audio").on("play", function() {
    	
    	currentAudio = $(this);

        $("audio").not(this).each(function(index, audio) {
            audio.pause();
        });
    });

});

</script>

	<div id="add-audit-long-call-form<?php echo $recording_id; ?>" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!--  -->
	    <div class="modal-content">
	      <div class="modal-header">	
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h2 class="modal-title"> Audit- Long Call Form </h4>
	      </div>

		<form action = "<?php echo base_url('Admin/insert_audit'); ?>" method = 'POST'> 
	     
	    <div class="modal-body">

		<center> 
 			Recording		
			<audio controls class = "recordings" >

				<source src="<?php echo $audio; ?>" 
										type="audio/ogg">

				<source src="<?php echo $audio; ?>" >
							
			<audio>
		
		</center>
		
		<table class="table table-condensed">

			<tr><td>Speed:

				<td><button type="button" href= "#" class="btn btn-info slow-audio">x.5</button>
				<td><button type="button" href= "#" class="btn btn-info fast-audio">x2</button>

			
		</table>

		<br>
 				
 				<table class="table table-condensed">

 					<tr><td> Recording Id: <td><input type="text" disabled="" name="recording_id" class="form-control" value="<?php echo $recording_id; ?>">

 					<tr><td> Work Week (Start Date): <td><input type="date" name="work_week" value="<?php echo date("Y-m-d"); ?>" class="form-control">

 					<tr><td> Call Id: <td><input type="text" disabled="" name="call_id" class="form-control" value="<?php echo $uniqueid; ?>">

 					<tr><td> Phone Number: <td><input disabled="" value = <?php echo $phone_number; ?> type="text" name="phone_number" class="form-control">

 					<tr><td> Customer: <td><input type="text" name = "customer" class="form-control" autofocus="">

 					<tr><td> Agent Id: <td><input type="text" name="agent_user_id" class="form-control" disabled="disabled" value="<?php echo $user; ?>">

 					<tr><td> Agent Name: <td> <input type = 'text' name="agent_name" value = 'Ghian Alchumbre' class="form-control"  disabled="" />

 					<tr><td> Timestamp: <td><input type="text" name="timestamp" disabled class="form-control" value="<?php echo date("m/d/Y h:i A", strtotime($start_time)); ?>">

 					<tr><td> Soundboard Invoice: <td><select class="form-control">
 												
 												<option selected="selected"> Select Soundboard Invoice </option>

 												<?php foreach($soundboard_list as $row) { ?>

		 											<option value="<?php echo $row['soundboard_voice_id']; ?>"
		 													
		 												> <?php echo $row['soundboard_voice_name']; ?>  </option>
		 										
		 										<?php } ?>

											</select>
 					
 					<tr><td> Recording: <td><input type="text" name="recording_id" class="form-control" disabled="" value="<?php echo $recording_id; ?>">

 					<tr><td> Agent Disposition: <td><select class="form-control" name="agent_disposition_id">
 											<option selected="selected"> Select Agent Disposition </option>

 											<?php foreach($disposition as $dispo_row): ?>

 												<option value="<?php echo $dispo_row['disposition_id']; ?>"> <?php echo $dispo_row['disposition']; ?> </option>

 											<?php endforeach; ?>

 										</select>

 					<tr><td> Correct Disposition: <td><select class="form-control" name="correct_disposition_id">
 											<option selected="selected"> Select Correct Disposition </option>

 											<?php foreach($disposition as $dispo_row): ?>

 												<option value="<?php echo $dispo_row['disposition_id']; ?>"> <?php echo $dispo_row['disposition']; ?> </option>

 											<?php endforeach; ?>

 										</select>

   											
 					<tr><td> Does this call have a ZTP or LOL mark? <td><select class="form-control">
 											<option selected="selected" value="N/A">N/A</option>
 											<option value="ZTP">ZTP</option>
 											<option value="LOL">LOL</option>

 											
 					<tr><td> Were there any incorrect responses: <td><select class="form-control">
 											<option selected="selected" value="N/A">N/A</option>
 											<option value="Yes">Yes</option>
 											<option value="No">No</option>

 					<tr><td> Agent/System issue?: <td><select class="form-control" name="agent_issue">
 											<option selected="selected" value="N/A">N/A</option>
 											<option value="Agent">Agent</option>
 											<option value="Syste,">System</option>
 											<option value="Others">Others</option>

 					<tr><td> Agent/System Issue Comment: <td>
 					
 					<tr><td colspan="2"><textarea name="agent_issue_comment" class="form-control" rows= 7 cols = 15> </textarea>
 
					<tr><td colspan="2"> General Observation:

  					<tr><td colspan="2"> <select class="form-control" name="general_observation_id">

 											<option selected="selected" value="1" name= "general_observation">N/A</option>

 											<?php foreach($gen_observational_list as $gen_obs_row): ?>
 											
 											<option value="<?php echo $gen_obs_row['general_observation_list_id']; ?>"> <?php echo $gen_obs_row['general_observation']; ?> </option>

 											<?php endforeach; ?>

 					<tr><td colspan="2">QA Remarks:

  					<tr><td colspan="2"><textarea name="qa_remarks" class="form-control" rows= 7 cols = 15> </textarea>

 					</select>

 					<input type="hidden" name="lead_id" value="<?php echo $lead_id; ?>">

 					<input type="hidden" name="audit_date" value="<?php echo date("Y-m-d"); ?>">
 					<input type="hidden" name="recording_datetime" value="<?php echo date("Y-m-d h:i:s"); ?>">

 					<input type="hidden" name="auditor_user_id" value="<?php echo $this->session->userdata('account_id'); ?>">


 				</table>
			
 
 	      </div>

	      <div class="modal-footer">

			<button type="submit" class="btn btn-success"> <i class = 'fa fa-send'> </i> Save and Proceed</button>
	        
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>


	      </div>

	    </div>
		
		</form>

	  </div>

	</div>