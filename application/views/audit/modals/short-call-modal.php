
	<div id="short-call-form" class="modal fade" role="dialog">
	  <div class="modal-dialog">

	    <!--  -->
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal">&times;</button>
	        <h4 class="modal-title"> Short Call Form </h4>
	      </div>

	      <div class="modal-body">
 				
 				<table class="table table-condensed">
 				
 					<tr><td> Name: <td>
 								<select>
 										<option>Ghian Alchumbre</option>
 										<option>Jenny Albino </option>
 										<option>Lenox Bajao</option>
								</select>

 					<tr><td> Date: <td><input type="date" name="">

 					<tr><td> Phone: <td><input type="text" name="">

 					<tr><td> Dispo: <td><input type="text" name="">

 					<tr><td> Recording: <td><input type="text" name="">

 					<tr><td> Was the disposition accurate?: <td><select>
 											<option>Yes</option>
 											<option>No</option>
 											<option>N/A</option>
 					</select>

 					<tr><td> Did the agent stay too long on the line unnecessarily?: <td><select>
 											<option>Yes</option>
 											<option>No</option>
 											<option>N/A</option>

 					<tr><td> How far did the call get? <td><select>
 											<option>Yes</option>
 											<option>No</option>
 											<option>N/A</option>

 					<tr><td> Were there any late responses? <td><select>
 											<option>Yes</option>
 											<option>No</option>
 											<option>N/A</option>
 											
 					<tr><td> Does this call have a ZT mark? <td><select>
 											<option>Yes</option>
 											<option>No</option>
 											<option>N/A</option>
 											
 					<tr><td> Were there any incorrect responses: <td><select>
 											<option>Yes</option>
 											<option>No</option>
 											<option>N/A</option>

 					</select>

 				</table>
 	      </div>

	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>

	  </div>
	</div>