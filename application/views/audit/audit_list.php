<h1 class="page-header"> Audit List </h1>

<div class = "row">

<style type="text/css">

  /*dark background to support form theme*/

  input{

  	display: block;

  }

</style>

<a href="<?php echo base_url('Dashboard/'); ?>"> <i class="fa fa-dashboard"></i> Dashboard </a>

<a href="<?php echo base_url('Recording/sme_group_recordings'); ?>"> <i class="fa fa-file-audio-o"></i> SME Group Recordings </a>

<script type="text/javascript" src="<?php echo base_url('js/recording_list.js'); ?>"></script>

		<form action = '' method="POST">

			<table class="table table-striped">
			
			
				<tr><td> Show <td>
					
								<select class="form-control">

								<option>Today's Audits</option>
								<option>This Week's Audits</option>

								</select> 
			
			<?php if($this->session->userdata("login_info")['user_roles_id'] == ADMIN ): ?> 

				<tr><td> Auditor <td>
					
								<select class="form-control" id = "auditor_filter">
	 								
	 								<option>Select Auditor</option>

	 								<?php foreach($qa_list as $auditor_list_row): ?>

	 										<option value="<?php echo $auditor_list_row['acct_nick_name']; ?>"> 

	 											<?php echo $auditor_list_row['acct_nick_name']; ?> 

	 										</option>

	 								<?php endforeach; ?>

								</select> 
			<?php endif; ?>

			</table>
		
		</form>

 	<table class="table table-striped"  id = "audit_list">
                                          
			<thead>

				<tr>
 					<td> Audit Id
					<td> Audit Timestamp
					<td> Recording/Call Id
 					<td> Agent					

						<?php if($this->session->userdata("login_info")['user_roles_id'] == ADMIN ): ?> 

						<td> Auditor
						<td> Audit Duration (in seconds)
					
						<?php endif; ?>

					<td> Audit Type
					<td> Hung Up Reason
					<td> Agent Disposition
					<td> Correct Disposition
					<td> Mark Status
					<td> Incorrect Response 
					<td>
					<td>

			</thead>

			<tbody>
				
				<?php $i = 1; ?>

				<?php foreach($audit_list as $row): ?>

					<tr>
						 
  						<td> <?php echo $row['audit_id']; ?> 

						<td> <?php echo date("M, d Y h:m:s A", strtotime($row['audit_timestamp'])); ?>

						<td> <?php echo $row['recording_id']; ?>

						<td> <?php echo $row['agent_name']; ?>
						
						<?php if($this->session->userdata("login_info")['user_roles_id'] == ADMIN ): ?> 

						<td> <?php echo $row['acct_nick_name']; ?>
						<td> <?php
							
								$duration_seconds = strtotime($row['audit_timeend']) - strtotime($row['audit_timestart']);
								
								echo $duration_seconds;

							?>

 						
 						<?php endif; ?>

						<td> <?php echo $row['audit_type']; ?>

						<td> <?php echo $row['term_reason']; ?>

						<td> <?php echo $row['agent_disposition']; ?>

						<td> <?php echo $row['vici_dispo_desc']; ?>

						<td> <?php echo $row['mark_status']; ?>

						<td> <?php echo $row['incorrect_response']; ?>
						

						<td style="width: 100%; height: 15px;"> 
			<?php 
					
		   			$row['qa_list'] 	= $qa_list; 
				   	
					$row['disposition'] = $disposition_list;

					$row['gen_observational_list'] 		= $gen_observational_list; 

				   	$add_incident_filter['cnt'] 		= $i; 

					$add_incident_filter['rec_info'] 	= $row;

					$add_incident_filter['program_list'] = $program_list;

					$add_incident_filter['source'] 		 = "Internal Audit";

					$add_incident_filter['audit_id'] 	 = $row['audit_id'];

				?>
				
							<a href = "<?php echo base_url('Audit/view_audit/'.$row['audit_id']); ?>" class="btn btn-info"> 
							
								<i class="fa fa-eye"> </i> 

							</a>

    						<a href="<?php echo base_url('Audit/edit_audit/'.$row['audit_id']); ?>" class="btn btn-success" ><i class="fa fa-edit"> </i> </a>
							
 					<td>
    						<?php if($row['incident_report_id'] == NULL && $row['mark_status'] != ''){ ?>

	    						<a href="<?php echo base_url('Incident_Report/add_incident_report/'. $row['audit_id'].'/1'); ?>" class="btn btn-warning">
	    							<i class="fa fa-bell"> </i> 
	    						</a>
 
    						<?php
    					
    						$i++;

    					} ?>
  
 
 				<?php endforeach; ?>

 			</tbody>

	</table>              
	
<br><br>

<form action= "<?php echo base_url('Audit/export_audits'); ?>" method = 'POST'>

		<button class = 'btn btn-success'> 
			Export Audits <i class="fa fa-download"></i> 
		</button>

</form>