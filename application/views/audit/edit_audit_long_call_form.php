<style type="text/css">
	
	textarea {
		resize: none;
	}	

	.recordings{

		width: 500px;
	}

	table{

	  	text-align: center;

	}
	
	.speed_field { 
		width: 40px; 

	} 

	.comment_field { 
		width: 150px;
	}

</style>

<script type="text/javascript">

var currentAudio;

$("document").ready(function(){

	$(".slow-audio").click(function(){
		
		currentAudio.playbackRate = 0.5;

	});

	$(".fast-audio").click(function(){
	
		// $("audio").playbackRate = 5;
		
		currentAudio.playbackRate = 2;

	});

    $("audio").on("play", function() {
    	
    	currentAudio = $(this);

        $("audio").not(this).each(function(index, audio) {
            audio.pause();
        });
    });

});

</script>

<h2> <center> Update Audit </center> </h2>

<?php echo form_open(base_url('Audit/update_audit')); ?>

		<center> 
 			 		
			<audio controls class = "recordings" >

				<source src="<?php echo $audio; ?>" 
										type="audio/ogg">

				<source src="<?php echo $audio; ?>" >

			<audio>
		
		</center>
		 
		<br>
 				
 				<table class="table table-condensed">
 					
 					<tr><td><h4> <b> Recording Info </b> </h4><td>

 					<tr><td> Recording Id: <td><input type="text" disabled=""  class="form-control" value="<?php echo $audit_info['recording_id']; ?>">

 					<tr><td> Work Week (Start Date): <td><input type="date" name="work_week" value="<?php echo $audit_info['work_week']; ?>" disabled class="form-control">

 					<tr><td> Recording Id: <td><input type="text" disabled="" class="form-control" value="<?php echo $audit_info['recording_id']; ?>">
 
 					<tr><td> Phone Number / BTN: <td><input disabled="" value = "<?php echo $audit_info['phone_number']; ?>" type="text" name="phone_number" class="form-control">

					<?php if($this->session->userdata("login_info")['user_roles_id'] == ADMIN ): ?> 

						<tr><td> <h4> <b> Agent Info </b> </h4> <td>

	 					<tr><td> Agent Id: <td><input type="text"  class="form-control" disabled="disabled" value="<?php echo $audit_info['agent_login_id']; ?>">

	 					<tr><td> Agent Name: <td> <input type="text"  class="form-control" disabled="disabled" value="<?php echo $audit_info['agent_name']; ?>">
 						
 						<tr><td> Team Lead: <td> <input type="text"  class="form-control" disabled="disabled" value="<?php echo $audit_info['tl']; ?>" name = "tl">
 						
 						<tr><td> Floor Support: <td> <input type="text" name="fs" class="form-control" disabled="disabled" value="<?php echo $audit_info['fs']; ?>">

					<?php endif; ?> 

 					<tr><td> Timestamp: <td><input type="text" disabled class="form-control" value="<?php echo $audit_info['start_time']; ?>">

 					<tr><td> Prospect Tone: <td class="pull-left form-control">
 											 
						<label class="checkbox-inline"><input type="radio" value="1" name="prospect_tone" 
							<?php if($audit_info['prospect_tone'] == 1) echo 'checked'; ?>> 1</label>
						<label class="checkbox-inline"><input type="radio" value="2" name="prospect_tone" <?php if($audit_info['prospect_tone'] == 2) echo 'checked'; ?>> 2</label>
						<label class="checkbox-inline"><input type="radio" value="3" name="prospect_tone" <?php if($audit_info['prospect_tone'] == 3) echo 'checked'; ?>> 3</label>

 					<tr><td> Sript: <td><select class="form-control" 
 						name="soundboard_voice_id" value= "<?php echo $audit_info['soundboard_voice_id']; ?>">
 												
 												<option selected="selected"> Select Soundboard Invoice </option>

 												<?php foreach($soundboard_list as $row) { ?>

		 											<option value="<?php echo $row['soundboard_voice_id']; ?>"
		 												
		 												<?php if($row['soundboard_voice_id'] == $audit_info['soundboard_voice_id']) echo 'selected'; ?>	
		 												> <?php echo $row['soundboard_voice_name']; ?>  </option>
		 										
		 										<?php } ?>

											</select>
 					
 					<tr><td> Recording: <td>
 						<input type="text"
 								class="form-control" disabled="" 
 								value="<?php echo $audit_info['location']; ?>">

 				</table>
			 	
			 	<?php $this->load->view('audit/edit_audit_scripts_form'); ?>

		<center> 
 			 		
			<audio controls class = "recordings" >

				<source src="<?php echo $audio; ?>" 
										type="audio/ogg">

				<source src="<?php echo $audio; ?>" >
							
			<audio>
		
		</center>
		
	<table class="table table-condensed">

 					<tr><td> Agent Disposition: <td>
  						 <input type="text" 
  						 value="<?php echo $audit_info['agent_disposition']; ?>" disabled  class= "form-control" />

 						</select>

 					<tr><td> Correct Disposition: <td>
 						<select class="form-control" name="correct_disposition_id" required>
 											<option selected="selected"> Select Correct Disposition </option>

 											<?php foreach($disposition as $dispo_row): ?>

 												<option value="<?php echo $dispo_row['vici_dispo_list_id']; ?>"
 													<?php if($audit_info['correct_disposition_id'] == $dispo_row['vici_dispo_list_id']) 
		 													echo 'selected'; 
		 											?>
		 											> <?php echo $dispo_row['dispo_code']; ?> </option>

 											<?php endforeach; ?>

 						</select>

   											
 					<tr><td> Does this call have a ZTP or LOL mark? <td>
 						<select class="form-control" name="mark_status">
 											<option selected="selected" value="N/A">N/A</option>
 											<option value="ZTP" 
 											<?php if('ZTP' == $audit_info['incorrect_response']) 
		 										echo 'selected'; 
		 									?>>ZTP</option>
 											<option value="LOL"
 											<?php if('LOL' == $audit_info['incorrect_response']) 
		 										echo 'selected'; 
		 									?>>LOL</option>

 						</select>

 					<tr><td> Were there any incorrect responses: <td>
 						<select class="form-control" name="incorrect_response">
 											<option selected="selected" value="N/A"
 											<?php if('N/A' == $audit_info['incorrect_response']) 
		 										echo 'selected'; 
		 									?>>N/A</option>
 											<option value="Yes"
 											<?php if('Yes' == $audit_info['incorrect_response']) 
		 										echo 'selected'; 
		 									?>	>Yes</option>
 											<option value="No"
 											<?php if('No' == $audit_info['incorrect_response']) 
		 										echo 'selected'; 
		 									?>
		 									>No</option>
 						</select>

 					<tr><td> Agent/System issue?: <td class="pull-left form-control">
 											 
						<label class="checkbox-inline"><input type="radio" value="N/A" name="agent_issue" required checked="" 
							<?php if('N/A' == $audit_info['agent_issue']) echo 'selected'; ?> > N/A </label>
						<label class="checkbox-inline"><input type="radio" value="Agent" name="agent_issue" required <?php if('Agent' == $audit_info['agent_issue']) 
		 											echo 'selected'; 
		 									?>	> Agent </label>
						<label class="checkbox-inline"><input type="radio" value="System" name="agent_issue" required <?php if('System' == $audit_info['agent_issue']) 
		 											echo 'selected'; 
		 									?>	> System </label>
						<label class="checkbox-inline"><input type="radio" value="Others" name="agent_issue" required <?php if('Others' == $audit_info['agent_issue']) 
		 											echo 'selected'; 
		 									?>	> Others </label>
  						</select>

  					<tr><td> Agent/System Issue Comment: 

 			 			<td><select class="form-control" name="issue_comment_id" required>

 											<?php foreach($issue_comments as $issues_row): ?>

 												<option value="<?php echo $issues_row['issue_comment_id']; ?>"

 												<?php 

 												if($audit_info['issue_comment_id']  == $issues_row['issue_comment_id']) 
 													echo 'selected'; 
 												?>> 

 													<?php echo $issues_row['issue_comment']; ?> 
 												
 												</option>

 											<?php endforeach; ?>

 										</select>

   					<tr><td><?php $i = 1; ?>

 			 				<?php foreach($issue_comments as $issues_row): ?>
 			 					
 			 					<?php if($i % 2 == 0) echo "<tr>"; ?>

 								 <td>  
	
 									 <input type= "checkbox" name = "issue_comments[]" value="<?php echo $issues_row['issue_comment_id']; ?>"

 									 <?php if(in_array($issues_row['issue_comment_id'], $audit_issue_comments)) 
 									 		echo "checked"; ?> /> 

 									<?php echo $issues_row['issue_comment']; ?> 
 								
 
 							<?php $i++; ?>

 							<?php endforeach; ?>


					<tr><td colspan="2">General Observation:

  					<tr><td colspan="2"> <select class="form-control" name="general_observation_id">

 											<?php foreach($gen_observational_list as $gen_obs_row): ?>
 											
 											<option value="<?php echo $gen_obs_row['general_observation_list_id']; ?>"
 												
 											<?php 

 											if($audit_info['general_observation_list_id']  == $gen_obs_row['general_observation_list_id']) echo 'selected';

 											?>
 												> 

 												<?php echo $gen_obs_row['general_observation']; ?> 

 											</option>

 											<?php endforeach; ?>
 											
 					<tr><td colspan="2">QA Remarks:

  					<tr><td colspan="2"><textarea name="qa_remarks" class="form-control" rows= 7 cols = 15><?php echo $audit_info['qa_remarks']; ?></textarea>

 					</select>

					<?php 

						$data = array(
 
							'audit_id'				=> $audit_info['audit_id'],
 
			 							            );
									
			 
						echo form_hidden($data);


					?> 
 
 
	</table>

	<button type="submit" class="btn btn-success" name=""> <i class="fa fa-save"></i> 	
		Save and Update
	</button>

		
</form>
