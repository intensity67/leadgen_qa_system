	<style type="text/css">
			
		.call_ends{

				width: 15px;
				height: 20px;
		}

		.td_call_ends{
			
			text-align: center;

		}

		.acknowledgement_col{
			font-size: 8px;
		}

		.acknowledgement_field{
			width: 20px;
		}

		.statement_field{
			width: 250px;
		}


		.speed_field{
			width: 40px;
		}

		.comment_field{
			width: 150px;
		}

	</style>

<h2> View Audit </h2>

 	<center>
		
		<table class="table table-condensed">

			<tr><td> <h4> <b> Audit Info </b> </h4> <td>

			<tr><td> Audit Time Start:		<td><?php echo $audit_info['audit_timestart']; ?>

			<tr><td> Audit Time End: 		<td><?php echo $audit_info['audit_timeend']; ?>

			<tr><td> Prospect Tone: 			 <td><?php echo $audit_info['prospect_tone']; ?>

			<tr><td> Agent / System Issue:			<td><?php echo $audit_info['agent_issue']; ?>

			<tr><td> Mark Status:			<td><?php echo $audit_info['mark_status']; ?>

			<tr><td> Incorrect Response:	<td><?php echo $audit_info['incorrect_response']; ?>

			<tr><td> General Observation:	<td><?php echo $audit_info['general_observation']; ?>
		
			<tr><td> Q.A. Remarks:			<td><?php echo $audit_info['qa_remarks']; ?>

			<tr><td> General Observation:	<td><?php echo $audit_info['incorrect_response']; ?>

			<tr><td> Correct Disposition:	<td><?php echo $audit_info['agent_disposition']; ?>

			<tr><td> Call/Audit Type: 		<td><?php echo $audit_info['audit_type']; ?>

			<tr><td> <h4> <b> Recording Info </b> </h4> <td>

			<center> 
	 			 		
				<audio controls class = "recordings" >

					<source src="<?php echo $audio; ?>" 
											type="audio/ogg">

					<source src="<?php echo $audio; ?>" >

				<audio>
			
			</center>
  
			<tr><td> Recording Id: 			 	<td><?php echo $audit_info['cluster_no']; ?>

			<tr><td> Cluster : 			 <td><?php echo $audit_info['prospect_tone']; ?>

			<tr><td> Work Week (Start Date): <td><?php echo date("M d, Y", strtotime($audit_info['work_week'])); ?>

			<tr><td> Call Timestamp: 		 <td><?php echo $audit_info['call_timestamp']; ?>

			<tr><td> Phone Number: 			 <td><?php echo $audit_info['phone_number']; ?>

			<tr><td> Lead Id: 				<td><?php echo $audit_info['lead_id']; ?>

			<tr><td> Recording Location:	<td><?php echo $audit_info['recording_location']; ?>

			<tr><td> Agent Disposition:		<td><?php echo $audit_info['agent_disposition']; ?>
			
			<tr><td> Soundboard Voice:		<td><?php echo $audit_info['soundboard_voice_name']; ?>


			<?php if($this->session->userdata("login_info")['user_roles_id'] == ADMIN ): ?> 

						<tr><td> <h4> <b> Agent Info </b> </h4> <td>

	 					<tr><td> Agent Id: 		<td><?php echo $audit_info['agent_login_id']; ?> 

	 					<tr><td> Agent Name:	<td><?php echo $audit_info['agent_name']; ?>
 						
 						<tr><td> Team Lead: 	<td> <?php echo $audit_info['tl']; ?>
 						
 						<tr><td> Floor Support: <td> <?php echo $audit_info['fs']; ?>

			<?php endif; ?> 

 

		</table>

  	</center>


<h2> Script Audits </h2>

<table class="table table-condensed" border="1">
	
 		<thead>
			<tr>
				<td rowspan="3">Script Code
				<td rowspan="2">Customer
				<td colspan="4">Agent</td>	
				<td colspan="3" rowspan="2"> Information </td>
				<td rowspan="3">Comment

			<tr>
				<td> <td colspan="3">   Accuracy 
			<tr>
				<td>Statement
					<td> Acknowledgement<td>Agent Response <td> Speed <td> Correct Response 
					<td> Customer Details <td> Agent Input <td> Call Ends  
			
		</thead>

	<?php foreach($audit_scripts as $row_audit_scripts): ?>

		<tr>
 			<td> <?php echo $row_audit_scripts['script_list_code']; ?>
			<td class="statement_field"> <?php echo $row_audit_scripts['customer_statement']; ?> 
			<td class="acknowledgement_field"> <?php echo $row_audit_scripts['agent_acknowledgement']; ?> 
			<td > <?php echo $row_audit_scripts['agent_accuracy_response']; ?> 
			<td class="speed_field"> <?php echo $row_audit_scripts['agent_accuracy_speed']; ?> 
			<td> <?php echo $row_audit_scripts['agent_accuracy_cor_response']; ?> 
			<td> <?php echo $row_audit_scripts['info_cust_details']; ?> 
			<td> <?php echo $row_audit_scripts['info_agent_input']; ?> 
			<td> <?php echo $row_audit_scripts['info_call_ends']; ?> 
			<td class="comment_field"> <?php echo $row_audit_scripts['comment']; ?> 

	<?php endforeach; ?>

</table>