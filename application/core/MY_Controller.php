<?php

defined('BASEPATH') OR exit('No direct script access allowed');


class MY_Controller extends CI_Controller {

	public function __construct(){
 
		parent::__construct();
  
    $this->asterisk = $this->load->database('asterisk', TRUE);
    
    $this->load->library('user_agent');

    // $this->clusters[0] = NULL;
    // $this->clusters[1] = $this->load->database('cluster1', TRUE);
    // $this->clusters[2] = $this->load->database('cluster2', TRUE);
    // $this->clusters[3] = $this->load->database('cluster3', TRUE);

    if($this->session->userdata("login_info")['user_account_id'] === NULL){

      redirect("Login");

    }else{

      $this->user_account_id = $this->session->userdata("login_info")['user_account_id'];

    }

    //echo $this->session->userdata("user_account_id");
	}


  public function page_render($page, $data = ''){

      //$header_data['nav'] = $this->user_nav();
      
      // print_r($this->session->userdata('login_info')['user_roles_id']);

      // exit;

       if($this->session->userdata('login_info')['user_roles_id'] == ADMIN){

        $header_data['nav']              = "nav-general";

      }

      if($this->session->userdata('login_info')['user_roles_id'] == QUALITY_ASSURANCE){

         $header_data['nav']              = "qa_staff";

      }

      $header_data['parent_menus']     = $this->nav_model->get_nav_menu_parents();

      // $header_data['user_priviledges'] = $this->nav_model->get_user_priviledges();

      $parent_menu_cnt = count($header_data['parent_menus']);

      $header_data['parent_menu_cnt'] = $parent_menu_cnt;

      for($i = 0; $i < $parent_menu_cnt; $i++){
        
        //* Get Menus with the parent id stored in $parents variable

        $header_data['parent_menus'][$i]['menus'] = $this->nav_model->get_nav_menus_children($header_data['parent_menus'][$i]['nav_menus_id']);

        // if( in_array("Glenn", $header_data['parent_menus'][$i]['menus']) ){

        // }

        $header_data['parent_menus'][$i]['children_cnt'] = count($header_data['parent_menus'][$i]['menus']);

      
      }
 

      $this->load->view('layout/header', $header_data);

      $this->load->view($page, $data);
         
      $this->load->view('layout/footer');

     }

   public function user_nav() {

      if($this->auth('user_role') == ADMIN){

  	  	$nav = "admin";

  	  }

      if($this->auth('user_role') == QUALITY_ASSURANCE){


        $nav = "qa_staff";

  	  }

      if($this->auth('user_role') == TEAM_LEAD){
 
        $nav = "team_lead";

  	  }


  	  if($this->auth('user_role') == SUPERVISOR){

        $nav = "supervisor";

  	  }
  	  
      return $nav;
   }

  public function auth($param) {

    if($param == 'user_id'){
      
      return $this->session->userdata('login_info')['user_account_id'];

    }

    if($param == 'user_role'){
    
      return $this->session->userdata('login_info')['user_roles_id'];

    }

  }

  public function get_add_incident_filters(){

        $data['team_leads']      = $this->users_model->get_tl_lists();

        $data['program_list']    = $this->filters_model->get_program_list();

        $data['vici_dispo_list'] = $this->filters_model->get_vici_dispo_list();

        $data['sanction_list']    = $this->filters_model->get_sanction_list();

        $data['ztp_mark_list']  = $this->filters_model->get_ztp_mark_list();

        $data['lol_mark_list']  = $this->filters_model->get_lol_mark_list();
        
        $data['disposition_list'] = $this->filters_model->get_vici_dispo_list();

        $data['users']            = $this->users_model->get_users_lists();

        $data['qa_list']          = $this->users_model->get_auditor_lists();

        return $data;
  }

//* Flagged Calls Modal Data

  public function get_add_flagged_call_filters(){

        //* Filter and Data

        $data['script_source']      = $this->filters_model->get_script_source_list();

        $data['client_feedback']     = $this->filters_model->get_client_feedback_list();

        $data['vici_dispo_list']     = $this->filters_model->get_vici_dispo_list();

        $data['webform_dispo_list']  = $this->filters_model->get_webform_dispo_list();

        $data['sites']               = $this->filters_model->get_site_list();

        //* Users Related Data
        
        $data['team_leads']     = $this->users_model->get_tl_lists();

        $data['agents']         = $this->users_model->get_agent_lists();

        $data['quotefire']      = $this->sort_quotefire_list();

        $data['ztp_mark_list']  = $this->filters_model->get_ztp_mark_list();

        $data['lol_mark_list']  = $this->filters_model->get_lol_mark_list();

        return $data;

  }
        

  public function sort_quotefire_list(){

      $data['unsuccessful'] = $this->filters_model->get_quotefire_list_by_type('unsuccessful');
      
      $data['not_interested'] = $this->filters_model->get_quotefire_list_by_type('not_interested');

      $data['others'] = $this->filters_model->get_quotefire_list_by_type('others');

      return $data;

  }

  public function searchForId($id, $array, $key) {

     foreach ($array as $key => $val) {

         if ($val[$key] === $id) {
             return $key;
         }
         
     }

     return null;

  }

}